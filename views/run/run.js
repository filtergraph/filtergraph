var loading = false;
var availableTags = [
    {{for col in num_col_names:}}{value:"{{=col}}",label:"{{=col}}",desc:"{{=descriptions.get(col) or ""}}"},{{pass}}
    {value:"Index",label:"(Index)",desc:""},
    {value:"Random",label:"(Random)",desc:""},
    {value:"Gaussian",label:"(Gaussian)",desc:""}
];
var availableTagsNonNumeric = [
    {{for col in col_names:}}{value:"{{=col}}",label:"{{=col}}",desc:"{{=descriptions.get(col) or ""}}"},{{pass}}
    {value:"Index",label:"(Index)",desc:""},
    {value:"Random",label:"(Random)",desc:""},
    {value:"Gaussian",label:"(Gaussian)",desc:""}
];
var availableTagsPlusNone = [
    {value:"",label:"(None)",desc:""},
    {{for col in num_col_names:}}{value:"{{=col}}",label:"{{=col}}",desc:"{{=descriptions.get(col) or ""}}"},{{pass}}
    {value:"Index",label:"(Index)",desc:""},
    {value:"Random",label:"(Random)",desc:""},
    {value:"Gaussian",label:"(Gaussian)",desc:""}
];
var availableTagsNonNumericPlusNone = [
    {value:"",label:"(None)",desc:""},
    {{for col in col_names:}}{value:"{{=col}}",label:"{{=col}}",desc:"{{=descriptions.get(col) or ""}}"},{{pass}}
    {value:"Index",label:"(Index)",desc:""},
    {value:"Random",label:"(Random)",desc:""},
    {value:"Gaussian",label:"(Gaussian)",desc:""}
];
var minTags = [
    {value:"is between",label:"is between",desc:""},
    {value:"isn't between",label:"isn't between",desc:""},
    {value:"is equal to",label:"is equal to",desc:""},
    {value:"isn't equal to",label:"isn't equal to",desc:""},
    {value:"starts with",label:"starts with",desc:""},
    {value:"ends with",label:"ends with",desc:""},
    {value:"contains",label:"contains",desc:""},
];
var maxTags = [
    {value:">",label:"Greater than...",desc:""},
    {value:"<",label:"Less than...",desc:""},
    {value:"=",label:"Equal to...",desc:""},
    {value:"!",label:"Not equal to...",desc:""},
    {value:"start:",label:"Starting with...",desc:""},
    {value:"end:",label:"Ending with...",desc:""},
    {value:"contains:",label:"Contains...",desc:""},
    {value:"regex:",label:"Regular expression...",desc:""},
];
var palettes = [
    {{for p in pal:}}{value:"{{=p[0]}}",label:"{{=p[0]}}",desc:"{{=p[2]}}"}{{if p != pal[-1]:}},{{pass}}{{pass}}
];
var fonts = [
    {{for f in font_list:}}{value:"{{=f[0]}}",label:"{{=f[0]}}",desc:"{{=f[2]}}"}{{if f != pal[-1]:}},{{pass}}{{pass}}
];
data_in = ['xaxis','xaxis_log','yaxis','yaxis_log','color','color_enabled','criteria_a','x_min_bound','x_max_bound','y_min_bound','y_max_bound','c_min_bound','c_max_bound','s_min_bound','s_max_bound','z_min_bound','z_max_bound','point_size','font_size','point_type','go','p','d','invert_colors','errorbars','bins','discrete','otype','separate_ew','interactive','picture_width','picture_height','subtraction','xlabel','ylabel','clabel','title','color_log','xaxis_rev','yaxis_rev','color_rev','download','filetype','filetype_tbl','zaxis','size','zaxis_log','zaxis_rev','zlabel','angle1','angle2','zscale','tsort','t_min_bound','t_max_bound','sort_order','tbl_rows','onedim','surface','embtext','ystat','cstat','zstat','size_log','size_rev','slabel','discretey','binsy','palette','font','datacolor','hidegrid','trendline','linechart','oldrules','setinitsettings','autosize','autosize_req','filter_action',{{for i in range(len(col_names)):}}'showrow{{=i}}',{{pass}}];
function addTags(id,tags)
{
    $( "#"+id ).autocomplete({
        appendTo: "#left_sidebar",
        source: tags,
        select: function(event, ui) {text_field_key(event)},
        minLength: 0
    })
    .data( "autocomplete" )._renderItem = function( ul, item ) {
         if(item.desc == "")
         {
             return $( "<li>" )
                .append( "<a>" + item.label + "</a>" )
                .appendTo( ul );         
         }
         else
         {
             return $( "<li>" )
                .append( "<a><abbr title='" + item.desc + "'>" + item.label + "</abbr></a>" )
                .appendTo( ul );
         }
    };
}
addTags("xaxis",availableTags);
addTags("yaxis",availableTags);
addTags("color",availableTagsPlusNone);
addTags("size",availableTagsPlusNone);
addTags("zaxis",availableTagsPlusNone);
addTags("tsort",availableTagsNonNumeric);
addTags("palette",palettes);
addTags("font",fonts);
{{if not is_mobile():}}
imgarea = $("#thegraph").imgAreaSelect({instance:true,onSelectEnd:function(img,selection){zoomBounds(selection);}})
{{pass}}

//http://stackoverflow.com/questions/641857/javascript-window-resize-event
window.onresize = function(event) {if ($('#autosize').attr('checked')=='checked') {text_field_key(event)}}

    displayLevel = {{=settings.get('displayLevel') or 1}}
    xb = {{if settings.get('x_min_bound') or settings.get('x_max_bound'):}}true{{else:}}false{{pass}}
yb = {{if settings.get('y_min_bound') or settings.get('y_max_bound'):}}true{{else:}}false{{pass}}
cb = {{if settings.get('c_min_bound') or settings.get('c_max_bound'):}}true{{else:}}false{{pass}}
sb = {{if settings.get('s_min_bound') or settings.get('s_max_bound'):}}true{{else:}}false{{pass}}
zb = {{if settings.get('z_min_bound') or settings.get('z_max_bound'):}}true{{else:}}false{{pass}}
tb = {{if settings.get('t_min_bound') or settings.get('t_max_bound'):}}true{{else:}}false{{pass}}

function updateDisplay()
{
    if($("#scatter").attr("checked")=="checked")
    {
        $("*[scat]").show();
        $("*[scathist]").show();
        $("*[hist]").hide();
        $("*[table]").hide();
        $("#otype").val("scatter");
        if(!xb)
        {
            $("#xbounds").hide();
            if($('#xmin').val()+$('#xmax').val() != "")
            {
                $("#xmin").val("");
                $("#xmax").val("");
                if(document.myForm.interactive.checked==true)
                {
                    refresh(true);
                }
            }
        }
        if(!yb)
        {
            $("#ybounds").hide();
            if($('#ymin').val()+$('#ymax').val() != "")
            {
                $("#ymin").val("");
                $("#ymax").val("");
                if(document.myForm.interactive.checked==true)
                {
                    refresh(true);
                }
            }
        }
        if(!cb)
        {
            $("#cbounds").hide();
            if($('#cmin').val()+$('#cmax').val() != "")
            {
                $("#cmin").val("");
                $("#cmax").val("");
                if(document.myForm.interactive.checked==true)
                {
                    refresh(true);
                }
            }
        }
        if(!sb)
        {
            $("#sbounds").hide();
            if($('#smin').val()+$('#smax').val() != "")
            {
                $("#smin").val("");
                $("#smax").val("");
                if(document.myForm.interactive.checked==true)
                {
                    refresh(true);
                }
            }
        }
        if(!zb)
        {
            $("#zbounds").hide();
            if($('#xmin').val()+$('#xmax').val() != "")
            {
                $("#zmin").val("");
                $("#zmax").val("");
                if(document.myForm.interactive.checked==true)
                {
                    refresh(true);
                }
            }
        }
        if($("#linechart").attr("checked") == "checked")
        {
            $("#row_cstat").hide();
            $("#row_color").hide();
            $("#cbounds").hide();
            $("#row_clabel").hide();
            $("#row_palette").hide();
            $("#row_csep").hide();
            $("#row_size").hide();
            $("#sbounds").hide();
            $("#row_slabel").hide();
            $("#row_ssep").hide();
        }
    }
    if($("#hist").attr("checked")=="checked")
    {
        $("*[scat]").hide();
        $("*[scathist]").show();
        $("*[hist]").show();
        $("*[table]").hide();
        $("#otype").val("hist");
        if(!xb)
        {
            $("#xbounds").hide();
            if($('#xmin').val()+$('#xmax').val() != "")
            {
                $("#xmin").val("");
                $("#xmax").val("");
                if(document.myForm.interactive.checked==true)
                {
                    refresh(true);
                }
            }
        }
        if(!yb)
        {
            $("#ybounds").hide();
            if($('#ymin').val()+$('#ymax').val() != "")
            {
                $("#ymin").val("");
                $("#ymax").val("");
                if(document.myForm.interactive.checked==true)
                {
                    refresh(true);
                }
            }
        }
        if(!cb)
        {
            $("#cbounds").hide();
            if($('#cmin').val()+$('#cmax').val() != "")
            {
                $("#cmin").val("");
                $("#cmax").val("");
                if(document.myForm.interactive.checked==true)
                {
                    refresh(true);
                }
            }
        }
        if(!zb)
        {
            $("#zbounds").hide();
            if($('#xmin').val()+$('#xmax').val() != "")
            {
                $("#zmin").val("");
                $("#zmax").val("");
                if(document.myForm.interactive.checked==true)
                {
                    refresh(true);
                }
            }
        }
        if($("#onedim").attr("checked") == "checked")
        {
            $("#row_ybins").hide();
            if($("#ystat").val() == "count")
            {
                $("#row_yaxis").hide();
                $("#ybounds").hide();
            }
            $("#row_cstat").hide();
            $("#row_color").hide();
            $("#cbounds").hide();
            $("#row_clabel").hide();
            $("#row_csep").hide();
            $("#row_ssep").hide();
            $("#row_zstat").hide();
            $("#row_zaxis").hide();
            $("#row_zsep").hide();
            $("#zbounds").hide();
            $("#zaxislabelscales").hide();
            $("#row_zangle").hide();
            $("#row_zscale").hide();
            $("#discretelabel").show();
        }
        
        if($("#onedim").attr("checked") != "checked" && $("#surface").attr("checked") != "checked")
        {
            $("#row_ystat").hide();
            if($("#cstat").val() == "count")
            {
                $("#row_color").hide();
                $("#cbounds").hide();
            }
            $("#row_zstat").hide();
            $("#row_zaxis").hide();
            $("#row_zsep").hide();
            $("#zbounds").hide();
            $("#zaxislabelscales").hide();
            $("#row_zangle").hide();
            $("#row_zscale").hide();
            $("#discretelabel").hide();
        }
        
        if($("#surface").attr("checked") == "checked")
        {
            $("#row_ystat").hide();
            if($("#cstat").val() == "count")
            {
                $("#row_color").hide();
                $("#cbounds").hide();
            }
            if($("#zstat").val() == "count")
            {
                $("#row_zaxis").hide();
                $("#zbounds").hide();
            }
            $("#discretelabel").hide();
        }
    }
    if($("#table").attr("checked")=="checked")
    {
        $("*[scat]").hide();
        $("*[scathist]").hide();
        $("*[hist]").hide();
        $("*[table]").show();
        $("#otype").val("table");
        if(!tb)
        {
            $("#tbounds").hide();
            if($('#tmin').val()+$('#tmax').val() != "")
            {
                $("#tmin").val("");
                $("#tmax").val("");
                if(document.myForm.interactive.checked==true)
                {
                    refresh(true);
                }
            }
        }
    }
    if(displayLevel==3)
    {
        $("*[d3]").show();
        $("*[d2]").hide();
    }
    if(displayLevel==2)
    {
        $("*[evenmore]").hide();
        $("*[d3]").hide();
        $("*[d2]").show();
        $("*[d1]").hide();
    }
    if(displayLevel==1)
    {
        $("*[evenmore]").hide();
        $("*[more]").hide();
        $("*[d2]").hide();
        $("*[d1]").show();
    }
    reset_vars = false;
    set_background_color();
    setup_errorbars();
    if($("#errorbars").attr("checked") == "checked" && $("#otype").val() == "scatter")
    {
        $("#zaxislabelscales").hide();
    }
    if($("#autosize").attr("checked") == "checked")
    {
        $("#imagesize").hide();
    }
    if($("#scatter").attr("checked")=="checked" && $("#color").val() == "" && displayLevel==2)
    {
        $("#row_datacolor").show();
    }
    else if($("#scatter").attr("checked")=="checked" && $("#linechart").attr("checked")=="checked" && displayLevel==2)
    {
        $("#row_datacolor").show();
    }
    else if($("#hist").attr("checked")=="checked" && $("#onedim").attr("checked") == "checked" && displayLevel==2)
    {
        $("#row_datacolor").show();
    }
    else
    {
        $("#row_datacolor").hide();
    }
}

var filters = 0;
function add_filter(number)
{
    if(number > filters)
    {
        filters = number
        //ref: http://stackoverflow.com/questions/171027/add-table-row-in-jquery
        //ref: http://stackoverflow.com/questions/3318621/javascript-how-to-have-value-in-string-represented-by-s-and-then-replaced-with
        nstr = number.toString()
        $('#filters tr[filterrow]:last').after('<tr filterrow="filterrow"><td><input type="checkbox" name="en_%s" id="en_%s" onchange="refresh(false)"/></td><td><input type="text" placeholder="variable" name="var_%s" id="var_%s" axis="axis" onfocus="$(\'#var_%s\').autocomplete(\'search\',\'\');add_filter(%s+1);$(\'#en_%s\').attr(\'checked\',\'checked\')" onclick="$(\'#var_%s\').autocomplete(\'search\',\'\');add_filter(%s+1);$(\'#en_%s\').attr(\'checked\',\'checked\')" onkeyup="text_field_key(event)" style="width:90%;margin-bottom:3px"/><br/><input type="text" value="is between" name="opt_%s" id="opt_%s" onclick="$(\'#opt_%s\').autocomplete(\'search\',\'\');" onfocus="$(\'#opt_%s\').autocomplete(\'search\',\'\');" onkeyup="text_field_key(event);$(\'#en_%s\').attr(\'checked\',\'checked\')"  style="width:40%;"/><input type="text" placeholder="min" name="min_%s" id="min_%s" onclick="$(\'#min_%s\').autocomplete(\'search\',\'\');" onfocus="$(\'#min_%s\').autocomplete(\'search\',\'\');" onkeyup="text_field_key(event);$(\'#en_%s\').attr(\'checked\',\'checked\')" style="width:23%;margin-left:3px"/><input type="text" placeholder="max" name="max_%s" id="max_%s" onclick="$(\'#max_%s\').autocomplete(\'search\',\'\');" onfocus="$(\'#max_%s\').autocomplete(\'search\',\'\');" onkeyup="text_field_key(event);$(\'#en_%s\').attr(\'checked\',\'checked\')" style="width:23%;margin-left:3px"/></td></tr>'.replace(/%s/g, nstr));
        addTags("var_"+nstr,availableTagsNonNumericPlusNone);
        addTags("opt_"+nstr,minTags);
        data_in.push("var_"+nstr);
        data_in.push("min_"+nstr);
        data_in.push("max_"+nstr);
        data_in.push("opt_"+nstr);
        data_in.push("en_"+nstr);
    }
}
    {{for i in range(1, len([key for key in settings.keys() if key.startswith("var")]) + 1):}}
    add_filter({{=i}});
    $("#var_{{=i}}").val(cleanup('{{=settings.get("var_%s" % i)}}'));
    $("#min_{{=i}}").val(cleanup('{{=settings.get("min_%s" % i)}}'));
    $("#max_{{=i}}").val(cleanup('{{=settings.get("max_%s" % i)}}'));
    $("#opt_{{=i}}").val(cleanup('{{=settings.get("opt_%s" % i) or "is between"}}'));
    {{if settings.get("en_%s" % i):}}
    $("#en_{{=i}}").attr("checked","checked");
    {{pass}}
    {{if (str(settings.get("var_%s" % i)) + str(settings.get("min_%s" % i)) + str(settings.get("max_%s" % i))):}}
    $("#filters_cover").hide();
    $("#filters").show();
    {{pass}}
    {{pass}}
    add_filter(1);

function select_filter(s)
{
    for(var i = 1; i<=filters; i++)
    {
        nstr = i.toString()
        if($("#var_"+nstr).val() == s)
        {
            //ref: http://stackoverflow.com/questions/5797539/jquery-select-all-text-from-a-textarea
            $("#min_"+nstr).select();
            return;
        }
    }
    // not on the list; add it
    add_filter(filters+1);
    // side effect: filters has now changed
    nstr = (filters-1).toString();
    $("#en_"+nstr).attr("checked","checked");
    $("#var_"+nstr).val(s);
    $("#min_"+nstr).focus();
}

{{#http://forums.devshed.com/javascript-development-115/time-restraint-430177.html}}
var count = 0;
function text_field_key(event)
{
   {{#http://stackoverflow.com/questions/155188/trigger-button-click-with-javascript-on-enter-key-in-text-box}}
   try{
   if(event.keyCode == 13)
   {
       refresh(true);
   }
   else
   {
       count = count+1;
       setTimeout("refreshGo("+count+")",{{=min(500+(rows*0.0002),1000)}});
   }
   }catch(err){}
}
 
function refreshGo(currCount)
{
   if(currCount == count)
   {
      count = 0;
      refresh(false);
   }
} 

function refresh(buttonClicked)
{
    if($("#autosize").attr('checked')=="checked")
    {
        $('input[name="picture_width"]').val($('#graph').width());
        $('input[name="picture_height"]').val($('#graph').height());
    }
    for(var i = 1; i<=filters; i++)
    {
        nstr = i.toString()
        if($("#opt_"+nstr).val().indexOf('between') != -1)
        {
            $("#min_"+nstr).attr('placeholder','min');
            $("#max_"+nstr).attr('placeholder','max');
        }
        else
        {
            $("#min_"+nstr).attr('placeholder','this');
            $("#max_"+nstr).attr('placeholder','or this');
        }
    }
    if(document.myForm.interactive.checked==true || buttonClicked==true)
    {
        if(xhr != undefined)
        {
            xhr.abort();
        }
        
        loading = true;
        
        try
        {
            setTimeout("if(loading){$('#loading').show();}",500);
        }
        catch(e){}
        
        ajax('/{{=request.application}}/run/graph?displayLevel='+displayLevel.toString(), data_in, 'graph_data');
        
        if($("#scatter").attr("checked")=="checked" && $("#color").val() == "" && displayLevel==2)
        {
            $("#row_datacolor").show();
        }
        else if($("#scatter").attr("checked")=="checked" && $("#linechart").attr("checked")=="checked" && displayLevel==2)
    	{
        	$("#row_datacolor").show();
    	}																							
        else if($("#hist").attr("checked")=="checked" && $("#onedim").attr("checked") == "checked" && displayLevel==2)
        {
            $("#row_datacolor").show();
        }
        else
        {
            $("#row_datacolor").hide();
        }
    }
}

function reset_bounds()
{
    if($('#xmin').val()+$('#xmax').val()+$('#ymin').val()+$('#ymax').val()+$('#cmin').val()+$('#cmax').val() != "")
    {
        $('#xmin').val('');
        $('#xmax').val('');
        $('#ymin').val('');
        $('#ymax').val('');
        $('#cmin').val('');
        $('#cmax').val('');
        if(document.myForm.interactive.checked==true)
        {
            refresh(true);
        }
    }
}

function reset_labels()
{
    if($('#title').val()+$('#xlabel').val()+$('#ylabel').val()+$('#clabel').val() != "")
    {
        $('#title').val('');
        $('#xlabel').val('');
        $('#ylabel').val('');
        $('#clabel').val('');
        if(document.myForm.interactive.checked==true)
        {
            refresh(true);
        }
    }
}

<!--http://www.electrictoolbox.com/check-uncheck-checkbox-jquery/-->
function reset_scales()
{
    if($("#xaxis_log").attr('checked') + $("#yaxis_log").attr('checked') + $("#xaxis_rev").attr('checked') + $("#yaxis_rev").attr('checked') + $("#color_log").attr('checked') + $("#color_rev").attr('checked') + $("#color_3d").attr('checked') + "" != "NaN")
    {
        $('#xaxis_log').removeAttr('checked');
        $('#xaxis_rev').removeAttr('checked');
        $('#yaxis_log').removeAttr('checked');
        $('#yaxis_rev').removeAttr('checked');
        $('#color_log').removeAttr('checked');
        $('#color_rev').removeAttr('checked');
        $('#color_3d').removeAttr('checked');
        if(document.myForm.interactive.checked==true)
        {
            refresh(true);
        }
    }
}

function reset_filter_bounds()
{
    if($('#amin').val()+$('#amax').val() != "")
    {
        $('#amin').val('');
        $('#amax').val('');
        if(document.myForm.interactive.checked==true)
        {
            refresh(true);
        }
    }
}

<!--ref: http://www.electrictoolbox.com/jquery-element-is-visible/-->
function bound_click()
{
    $('tr[bound]').toggle();
    if($('tr[bound]').is(':hidden'))
    {
        $('#bound').html('Bounds...');
        reset_bounds();
    }
    else
    {
        $('#bound').html('Reset bounds');
    }
}

<!--ref: http://www.electrictoolbox.com/jquery-element-is-visible/-->
function label_click()
{
    $('tr[label]').toggle();
    if($('tr[label]').is(':hidden'))
    {
        $('#label').html('Labels...');
        reset_labels();
    }
    else
    {
        $('#label').html('Reset labels');
    }
}

<!--ref: http://www.electrictoolbox.com/jquery-element-is-visible/-->
function scale_click()
{
    $('tr[scale]').toggle();
    if($('tr[scale]').is(':hidden'))
    {
        $('#scale').html('Scales...');
        reset_scales();
    }
    else
    {
        $('#scale').html('Reset scales');
    }
}

<!--ref: http://www.electrictoolbox.com/jquery-element-is-visible/-->
function filter_click()
{
    $('table[filter]').toggle();
    if($('table[filter]').is(':hidden'))
    {
        $('#filterlabel').html('Filter...');
        reset_filter_bounds();
    }
    else
    {
        $('#filterlabel').html('Reset filter');
    }
}

$(document).ready(function ()
{
    //$("#footer_hide").show();
    $("#about").show();
    $("#popup").css('left','306px');
})

function hide_sidebar()
{
    $("#left_sidebar").hide();
    $("#statusbar").hide();
    $(".a").css('left','0px');
    $("#popup").css('left','0px');
    refresh(true);
    $("#footer_reset").show();
    $("#footer_show").show();
}

function show_sidebar()
{
    $("#left_sidebar").show();
    $("#statusbar").show();
    $(".a").css('left','306px');
    $("#popup").css('left','306px');
    refresh(true);
    $("#footer_reset").hide();
    $("#footer_show").hide();
}

function set_background_color()
{
    if(document.myForm.invert_colors.checked==true && $('#table').attr("checked") != 'checked')
    {
        $('#graph').css('background-color','black');
    }
    else
    {
        $('#graph').css('background-color','white');
    }
}

function x_conv(x)
{
    x_pct = (x-xminterm)/(xmaxterm-xminterm)
    if(xrev)
    {
        x_pct = 1 - x_pct
    }
    if(xexp)
    {
        x_c = Math.pow(10,xmin*(1-x_pct)+(xmax*x_pct))
    }
    else if(xlog)
    {
        x_c = Math.exp((Math.log(xmin)*(1-x_pct))+(Math.log(xmax)*x_pct))
    }
    else
    {
        x_c = xmin*(1-x_pct)+(xmax*x_pct)
    }
    if(Math.abs(xmax-xmin) < 0.01)
    {
        return x_c;
    }
    else
    {
        return Math.round(x_c*10000)/10000;
    }
}

function y_conv(y)
{
    y_pct = (y-yminterm)/(ymaxterm-yminterm)
    if(!yrev)
    {
        y_pct = 1 - y_pct
    }
    if(yexp)
    {
        y_c = Math.pow(10,ymin*(1-y_pct)+(ymax*y_pct))
    }
    else if(ylog)
    {
        y_c = Math.exp((Math.log(ymin)*(1-y_pct))+(Math.log(ymax)*y_pct))
    }
    else
    {
        y_c = ymin*(1-y_pct)+(ymax*y_pct)
    }
    if(Math.abs(ymax-ymin) < 0.01)
    {
        return y_c;
    }
    else
    {
        return Math.round(y_c*10000)/10000;
    }
}

function point_info(x,y)
{
    if(!zaxis)
    {
        query = "point_info=true&point_info_x="+x_conv(x)+"&point_info_y="+y_conv(y)+"&point_info_x_min="+Math.min(x_conv(x-7),x_conv(x+7))+"&point_info_x_max="+Math.max(x_conv(x-7),x_conv(x+7))+"&point_info_y_min="+Math.min(y_conv(y-7),y_conv(y+7))+"&point_info_y_max="+Math.max(y_conv(y-7),y_conv(y+7));
        $("#popup_data").html("");
        ajax("/{{=request.application}}/run/point_info?"+query,data_in,"popup_data");
        $("#myModalLabel").html("Point info");
        $("#myModal").modal();
    }
}

function get_stats()
{
    $("#popup_data").html("");
    ajax("/{{=request.application}}/run/get_stats?getstats=true",data_in,"popup_data");
    $("#myModalLabel").html("Statistics");
    $("#myModal").modal();
}

reset_vars = false
lastZoomBoundsY1 = -1
function zoomBounds(selection)
{
    if(selection.y1 == lastZoomBoundsY1)
    {
        return;
    }
    lastZoomBoundsY1 = selection.y1
    if(selection.y1 < yminterm)
    {
        imgarea.cancelSelection();
        return;
    }
    if(selection.y2 > ymaxterm)
    {
        imgarea.cancelSelection();
        return;
    }
    if(selection.x1 < xminterm)
    {
        imgarea.cancelSelection();
        return;
    }
    if(selection.x2 > xmaxterm)
    {
        imgarea.cancelSelection();
        return;
    }
    if(selection.width<20 && selection.height<20)
    {
        point_info((selection.x1+selection.x2)/2,(selection.y1+selection.y2)/2);
        imgarea.cancelSelection();
        return;
    } 
    if(!reset_vars)
    {
        reset_xmin = $("#xmin").val();
        reset_xmax = $("#xmax").val();
        reset_xb = xb;
        if(!onedim)
        {
            reset_ymin = $("#ymin").val();
            reset_ymax = $("#ymax").val();
            reset_yb = yb;
        }
        reset_vars = true;
    }
    $("#xmin").val(Math.min(x_conv(selection.x1),x_conv(selection.x2)));
    $("#xmax").val(Math.max(x_conv(selection.x1),x_conv(selection.x2)));
    xb = true;
    $("#xbounds").show();
    if(!onedim)
    {
        $("#ymin").val(Math.min(y_conv(selection.y1),y_conv(selection.y2)));
        $("#ymax").val(Math.max(y_conv(selection.y1),y_conv(selection.y2)));
        yb = true;
        $("#ybounds").show();
    }
    refresh(true);
}
function zoomBoundsMobile(selection)
{
    if(selection.w<10 && selection.h<10)
    {
        point_info((selection.x+selection.x2)/2,(selection.y+selection.y2)/2);
        jcrop_api.release();
        return
    } 
    if(!reset_vars)
    {
        reset_xmin = $("#xmin").val();
        reset_xmax = $("#xmax").val();
        reset_xb = xb;
        if(!onedim)
        {
            reset_ymin = $("#ymin").val();
            reset_ymax = $("#ymax").val();
            reset_yb = yb;
        }
        reset_vars = true;
    }
    $("#xmin").val(Math.min(x_conv(selection.x),x_conv(selection.x2)));
    $("#xmax").val(Math.max(x_conv(selection.x),x_conv(selection.x2)));
    xb = true;
    $("#xbounds").show();
    if(!onedim)
    {
        $("#ymin").val(Math.min(y_conv(selection.y),y_conv(selection.y2)));
        $("#ymax").val(Math.max(y_conv(selection.y),y_conv(selection.y2)));
        yb = true;
        $("#ybounds").show();
    }
    refresh(true);
}

function mark_zoom()
{
    if($("#hist").attr("checked")=="checked" && $("#onedim").attr("checked") == "checked")
    {
        $("#yaxis").val("("+$("#xaxis").val()+">="+$("#xmin").val()+")"+"&"+
    		"("+$("#xaxis").val()+"<="+$("#xmax").val()+")");
        $("#ystat").val("mean");
    }
    else
    {
        $("#color").val("("+$("#xaxis").val()+">="+$("#xmin").val()+")"+"&"+
            "("+$("#xaxis").val()+"<="+$("#xmax").val()+")"+"&"+
            "("+$("#yaxis").val()+">="+$("#ymin").val()+")"+"&"+
            "("+$("#yaxis").val()+"<="+$("#ymax").val()+")");
        cb = false;
        if($("#hist").attr("checked")=="checked")
        {
            $("#cstat").val("mean");
        }
    }
    reset_zoom();
    updateDisplay();
}

function filter_zoom()
{
    $("#var_"+filters).val($("#xaxis").val());
    $("#min_"+filters).val($("#xmin").val());
    $("#max_"+filters).val($("#xmax").val());
    $("#en_"+filters).attr("checked","checked");
    add_filter(filters+1);
    if($("#hist").attr("checked")!="checked" || $("#onedim").attr("checked") != "checked")
    {
        $("#var_"+filters).val($("#yaxis").val());
        $("#min_"+filters).val($("#ymin").val());
        $("#max_"+filters).val($("#ymax").val());
        $("#en_"+filters).attr("checked","checked");
        add_filter(filters+1);
    }
    $('#filters').show();
    $('#filters_cover').hide();
    $("#filter_action").val("hide");
    reset_zoom();
}

function reset_zoom()
{
    $("#xmin").val(reset_xmin);
    $("#xmax").val(reset_xmax);
    if(!reset_xb)
    {
        $("#xbounds").hide();
        xb = false;
    }
    if(!onedim)
    {
        $("#ymin").val(reset_ymin);
        $("#ymax").val(reset_ymax);
        
        if(!reset_yb)
        {
            $("#ybounds").hide();
            yb = false;
        }
    }
    reset_vars = false;
    refresh(true);
}

function show_all_rows()
{
    for(var i=0; i<{{=len(col_names)}}; i++)
    {
        $("#showrow"+i.toString()).attr("checked","checked");
    }
    refresh(false);
}

function show_none_rows()
{
    for(var i=0; i<{{=len(col_names)}}; i++)
    {
        $("#showrow"+i.toString()).removeAttr("checked");
    }
    refresh(false);
}

// Replace all &lt; and &gt; by < and >
// ref: http://stackoverflow.com/questions/5957546/javascript-regex-replacing-quot
function cleanup(a)
{
  a = a.replace(/&(lt|gt|quot);/g, function (m, p) { 
      console.log(p);
    return (p == "lt") ? "<" : ((p == "gt") ? ">" : "\"");
  });
  return a;
}

function setup_errorbars()
{
    if( $("#errorbars").attr("checked") == "checked" && $("#hist").attr("checked")!="checked")
    {
        $("#sizelabel").html("X error:");
        $("#zaxislabel").html("Y error:");
        $("#sizechar").html("s<sub>x</sub>");
        $("#zaxischar").html("s<sub>y</sub>");
        $("#zaxislabelscales").hide();
    }
    else
    {
        $("#sizelabel").html("Size:");
        $("#zaxislabel").html("Z-axis:");
        $("#sizechar").html("s");
        $("#zaxischar").html("z");
        if(displayLevel == 2 && $("#otype").val() == "scatter")
        {
            $("#zaxislabelscales").show();
        }
    }
}
setup_errorbars();

function emb_update(x) {
    if($("#emb_type_"+x).val() == "text")
    {
        $("#emb_row_pos_"+x).show();
        $("#emb_row_text_"+x).show();
        $("#emb_row_start_"+x).hide();
        $("#emb_row_end_"+x).hide();
        $("#emb_row_fx_"+x).hide();
        $("#emb_row_size_"+x).show();
        $("#emb_row_color_"+x).show();
        $("#emb_row_angle_"+x).show();
        $("#emb_row_label_"+x).hide();
    }
    if($("#emb_type_"+x).val() == "line")
    {
        $("#emb_row_pos_"+x).hide();
        $("#emb_row_text_"+x).hide();
        $("#emb_row_start_"+x).show();
        $("#emb_row_end_"+x).show();
        $("#emb_row_fx_"+x).hide();
        $("#emb_row_size_"+x).show();
        $("#emb_row_color_"+x).show();
        $("#emb_row_angle_"+x).hide();
        $("#emb_row_label_"+x).hide();
    }
    if($("#emb_type_"+x).val() == "arrow")
    {
        $("#emb_row_pos_"+x).hide();
        $("#emb_row_text_"+x).hide();
        $("#emb_row_start_"+x).show();
        $("#emb_row_end_"+x).show();
        $("#emb_row_fx_"+x).hide();
        $("#emb_row_size_"+x).show();
        $("#emb_row_color_"+x).show();
        $("#emb_row_angle_"+x).hide();
        $("#emb_row_label_"+x).hide();
    }
    if($("#emb_type_"+x).val() == "curve")
    {
        $("#emb_row_pos_"+x).hide();
        $("#emb_row_text_"+x).hide();
        $("#emb_row_start_"+x).hide();
        $("#emb_row_end_"+x).hide();
        $("#emb_row_fx_"+x).show();
        $("#emb_row_size_"+x).show();
        $("#emb_row_color_"+x).show();
        $("#emb_row_angle_"+x).hide();
        $("#emb_row_label_"+x).show();
    }
    if($("#emb_type_"+x).val() == "trendline")
    {
        $("#emb_row_pos_"+x).hide();
        $("#emb_row_text_"+x).hide();
        $("#emb_row_start_"+x).hide();
        $("#emb_row_end_"+x).hide();
        $("#emb_row_fx_"+x).hide();
        $("#emb_row_size_"+x).show();
        $("#emb_row_color_"+x).show();
        $("#emb_row_angle_"+x).hide();
        $("#emb_row_label_"+x).show();
    }
}

// ref: http://www.infotuts.com/dynamically-add-input-fields-to-form-jquery/
$(function() {
var addDiv = $('#emb_addinput');
var i = $('#emb_addinput fieldset').size() + 1;

var newItem = '\
<fieldset style="margin-bottom:10px"><table>\
<tr><td><label for="emb_type_%s">Type:</label></td>\
<td><select id="emb_type_%s" name="emb_type_%s" style="width:150px; padding: 3px 3px; margin-bottom:5px ;height:25px" onchange="emb_update(%s);refresh(false)">\
<option value="text">Text</option>\
<option value="line">Line</option>\
<option value="arrow">Arrow</option>\
<option value="curve">Function</option>\
<option value="trendline">Trendline</option>\
</select></td></tr>\
<tr id="emb_row_pos_%s"><td><label for="emb_xpos_%s">Position:</label></td>\
<td><label for="emb_xpos_%s">X</label> <input type="text" id="emb_xpos_%s" name="emb_xpos_%s" onkeyup="text_field_key(event)" style="width:60px"/> \
<label for="emb_ypos_%s">Y</label> <input type="text" id="emb_ypos_%s" name="emb_ypos_%s" onkeyup="text_field_key(event)" style="width:60px"/></td></tr>\
<tr id="emb_row_text_%s"><td><label for="emb_text_%s">Text:</label></td>\
<td><input type="text" id="emb_text_%s" name="emb_text_%s" onkeyup="text_field_key(event)" style="width:165px;"/></td></tr>\
<tr id="emb_row_start_%s"><td><label for="emb_xstart_%s">Start:</label></td>\
<td><label for="emb_xstart_%s">X</label> <input type="text" id="emb_xstart_%s" name="emb_xstart_%s" onkeyup="text_field_key(event)" style="width:60px"/> \
<label for="emb_ystart_%s">Y</label> <input type="text" id="emb_ystart_%s" name="emb_ystart_%s" onkeyup="text_field_key(event)" style="width:60px"/></td></tr>\
<tr id="emb_row_end_%s"><td><label for="emb_xend_%s">End:</label></td>\
<td><label for="emb_xend_%s">X</label> <input type="text" id="emb_xend_%s" name="emb_xend_%s" onkeyup="text_field_key(event)" style="width:60px"/> \
<label for="emb_yend_%s">Y</label> <input type="text" id="emb_yend_%s" name="emb_yend_%s" onkeyup="text_field_key(event)" style="width:60px"/></td></tr>\
<tr id="emb_row_fx_%s"><td><label for="emb_fx_%s">f(x) = </label></td>\
<td><input type="text" id="emb_fx_%s" name="emb_fx_%s" onkeyup="text_field_key(event)" style="width:165px"/></td></tr>\
<tr id="emb_row_size_%s"><td><label for="emb_size_%s">Size:</label></td>\
<td><input type="text" id="emb_size_%s" name="emb_size_%s" onkeyup="text_field_key(event)" style="width:70px"/></td></tr>\
<tr id="emb_row_color_%s"><td><label for="emb_color_%s">Color:</label></td>\
<td><input type="text" class="color" id="emb_color_%s" name="emb_color_%s" onkeyup="text_field_key(event)" onchange="text_field_key(event)" style="width:70px"/></td></tr>\
<tr id="emb_row_angle_%s"><td><label for="emb_angle_%s">Angle:</label></td>\
<td><input type="text" id="emb_angle_%s" name="emb_angle_%s" onkeyup="text_field_key(event)" style="width:70px"/></td></tr>\
<tr id="emb_row_label_%s"><td><label for="emb_label_%s">Label:</label></td>\
<td><input type="text" id="emb_label_%s" name="emb_label_%s" onkeyup="text_field_key(event)" style="width:165px"/></td></tr>\
<tr><td colspan="2"><a href="#" id="emb_remNew" class="btn">Remove</a></td></tr>\
</table></fieldset>'

function emb_add()
{
$(newItem.replace(/%s/gi, i)).appendTo(addDiv);
emb_update(i);
data_in.push("emb_type_"+i);
data_in.push("emb_xpos_"+i);
data_in.push("emb_ypos_"+i);
data_in.push("emb_text_"+i);
data_in.push("emb_xstart_"+i);
data_in.push("emb_ystart_"+i);
data_in.push("emb_xend_"+i);
data_in.push("emb_yend_"+i);
data_in.push("emb_fx_"+i);
data_in.push("emb_size_"+i);
data_in.push("emb_color_"+i);
data_in.push("emb_angle_"+i);
data_in.push("emb_label_"+i);
var myPicker = new jscolor.color(document.getElementById('emb_color_'+i), {})
if(document.myForm.invert_colors.checked==true)
{
    myPicker.fromString('FFFFFF');
}
else
{
    myPicker.fromString('000000');
}
i++;

}

$('#emb_addNew').live('click', function() {
emb_add();
return false;
});

emb_add();

{{for s in settings:}}
{{if s.startswith("emb_type_"):}}
{{ssub = s[9:]}}
{{if (settings.get('emb_xpos_'+ssub) + settings.get('emb_ypos_'+ssub) + settings.get('emb_text_'+ssub) + settings.get('emb_xstart_'+ssub) + settings.get('emb_ystart_'+ssub) + settings.get('emb_xend_'+ssub) + settings.get('emb_yend_'+ssub) + settings.get('emb_fx_'+ssub)):}}
displayLevel = 2;
$("#emb").show();
$("#emb_cover").hide();
$("#emb_type_"+(i-1)).val("{{=settings.get('emb_type_'+ssub)}}");
emb_update(i-1);
$("#emb_xpos_"+(i-1)).val("{{=settings.get('emb_xpos_'+ssub)}}");
$("#emb_ypos_"+(i-1)).val("{{=settings.get('emb_ypos_'+ssub)}}");
$("#emb_text_"+(i-1)).val("{{=settings.get('emb_text_'+ssub)}}");
$("#emb_xstart_"+(i-1)).val("{{=settings.get('emb_xstart_'+ssub)}}");
$("#emb_ystart_"+(i-1)).val("{{=settings.get('emb_ystart_'+ssub)}}");
$("#emb_xend_"+(i-1)).val("{{=settings.get('emb_xend_'+ssub)}}");
$("#emb_yend_"+(i-1)).val("{{=settings.get('emb_yend_'+ssub)}}");
$("#emb_fx_"+(i-1)).val("{{=settings.get('emb_fx_'+ssub)}}");
$("#emb_size_"+(i-1)).val("{{=settings.get('emb_size_'+ssub)}}");
$("#emb_color_"+(i-1)).val("{{=settings.get('emb_color_'+ssub)}}");
$("#emb_angle_"+(i-1)).val("{{=settings.get('emb_angle_'+ssub)}}");
$("#emb_label_"+(i-1)).val("{{=settings.get('emb_label_'+ssub)}}");
emb_add();
{{pass}}
{{pass}}
{{pass}}

$('#emb_remNew').live('click', function() {
$(this).parents('fieldset').remove();
refresh(false);
return false;
});
});

$(function() {
var container = document.querySelector('#nb-container');
var msnry = new Masonry(container);
updateDisplay();
refresh(true);
});
