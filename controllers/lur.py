from gluon.tools import prettydate
from datetime import timedelta
import json

def start():
    secure_page()
    did = int(request.args[0])
    portal = db.datasets[did].portal
    datasets = db(db.datasets.portal==portal).select()
    try:
        pid = db.datasets[did].portal
    except:
        log("portal does not exist for %s" % did)
        redirect(URL("portals"))
    admins = db(db.admins.portal==pid).select()
    if auth.user_id not in [a.zadmin.id for a in admins] and not auth.has_membership('qc'):
        log("cannot edit dataset %s" % did)
        redirect(URL("portals"))
    if len([True for a in admins if a.zadmin.id == auth.user_id and a.viewonly])>0:
        log("is viewonly and cannot edit dataset %s" % did)
        redirect(URL("portals"))
    lur = scheduler.queue_task(lur_refresh,sync_output=2, repeats=0, period=86400, immediate=True, timeout=3600, stop_time=request.now + timedelta(days=90),pvars={'did':did},retry_failed=-1,prevent_drift=True )
    db(db.datasets.id==did).update(lur_task=lur.id)
    return "$('#lur_start').hide();$('#lur_stop').show();"
     
def stop():
    secure_page()
    did = int(request.args[0])
    portal = db.datasets[did].portal
    datasets = db(db.datasets.portal==portal).select()
    try:
        pid = db.datasets[did].portal
    except:
        log("portal does not exist for %s" % did)
        redirect(URL("portals"))
    admins = db(db.admins.portal==pid).select()
    if auth.user_id not in [a.zadmin.id for a in admins] and not auth.has_membership('qc'):
        log("cannot edit dataset %s" % did)
        redirect(URL("portals"))
    if len([True for a in admins if a.zadmin.id == auth.user_id and a.viewonly])>0:
        log("is viewonly and cannot edit dataset %s" % did)
        redirect(URL("portals"))
    scheduler.stop_task(db.datasets[did].lur_task)
    db(db.datasets.id==did).update(lur_task=None)
    return "$('#lur_stop').hide();$('#lur_start').show();"
     
def status():
    secure_page()
    did = int(request.args[0])
    portal = db.datasets[did].portal
    datasets = db(db.datasets.portal==portal).select()
    try:
        pid = db.datasets[did].portal
    except:
        log("portal does not exist for %s" % did)
        redirect(URL("portals"))
    admins = db(db.admins.portal==pid).select()
    if auth.user_id not in [a.zadmin.id for a in admins] and not auth.has_membership('qc'):
        log("cannot edit dataset %s" % did)
        redirect(URL("portals"))
    if len([True for a in admins if a.zadmin.id == auth.user_id and a.viewonly])>0:
        log("is viewonly and cannot edit dataset %s" % did)
        redirect(URL("portals"))
    lur_taskid = db.datasets[did].lur_task
    if lur_taskid:
        retval = "<br><br>"
        lur = scheduler.task_status(db.datasets[did].lur_task)
        if lur.status == "ASSIGNED":
            retval += "Starting reload..."
        elif lur.status == "RUNNING":
            retval += "Reload in progress... "
            lur = scheduler.task_status(db.datasets[did].lur_task,output=True)
            if lur.scheduler_run:
                if lur.scheduler_run.run_output:
                    retval += lur.scheduler_run.run_output
        else:
            if lur.status == "TIMEOUT":
                retval += "Live URL reloading for this file has timed out.<br>"
            elif lur.status == "FAILED":
                retval += "Live URL reloading for this file has failed.<br>"
            elif lur.status == "COMPLETED":
                retval += "Live URL reloading for this file has completed.<br>"
            elif lur.status == "EXPIRED":
                retval += "Live URL reloading for this file has expired.<br>"
            retval += "File was reloaded %s time(s) and failed %s time(s).<br>" % (lur.times_run,lur.times_failed)
            if lur.last_run_time:
                retval += "Last reload attempt was " +prettydate(lur.last_run_time,T)+".<br>"
            if lur.next_run_time and lur.status != "EXPIRED":
                if lur.next_run_time > request.now:
                    retval += "Next reload attempt is scheduled " +prettydate(lur.next_run_time)+".<br>"
                else:
                    retval += "Next reload will begin shortly.<br>"
            if lur.stop_time and lur.status != "EXPIRED":
                retval += "If no one visits the file, reloading will expire " +prettydate(lur.stop_time)+"."
        return retval
    else:
        return "<br><br>Live URL reloading is turned off."