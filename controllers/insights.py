# coding: utf8
# try something like

import os
from datetime import datetime
    
@auth.requires_membership('qc')
def index(): 
    admins = db(db.admins.id>0).select()
    unique_admins = {}
    for a in admins:
        unique_admins[a.zadmin] = True
    notebook = db(db.notebook.id>0).select()
    for n in notebook:
        unique_admins[n.zuser] = True
    comments = db(db.comments.id>0).select()
    for c in comments:
        unique_admins[c.zuser] = True
    users = db(db.auth_user.id>=887).select()
    for u in users:
        unique_admins[u.id] = True
    #return repr(len(unique_admins))
    events = db(db.auth_event.description.contains("Registered")).select()
    userips = {}
    for e in events:
        if e.user_id in unique_admins:
            userips[e.user_id] = e.client_ip
    countries = []
    for ui in userips.items():
        ip = ui[1].split(".")
        ipnum = (long(ip[0])*256*256*256)+(long(ip[1])*256*256)+(long(ip[2])*256)+long(ip[3])
        #return str(ipnum)
        with open(os.path.join(request.folder,"static","GeoIPCountryWhois.csv")) as f:
            for l in f:
                line = l.split(",")
                for i in range(len(line)):
                    line[i] = line[i].strip().strip('"')
                if long(line[2])<=ipnum and ipnum<=long(line[3]):
                    countries.append((ui[0],line[5]))
                    break
            else:
                countries.append((ui[0],"Unavailable"))
    """countriesdict = {}
    for c in countries:
        if c in countriesdict.keys():
            countriesdict[c] += 1
        else:
            countriesdict[c] = 1"""
    return repr(countries)

@auth.requires_membership('qc')
def months(): 
    admins = db(db.admins.viewonly==False).select()
    unique_admins = {}
    for a in admins:
        unique_admins[a.zadmin] = True
    #notebook = db(db.notebook.id>0).select()
    #for n in notebook:
    #    unique_admins[n.zuser] = True
    #comments = db(db.comments.id>0).select()
    #for c in comments:
    #    unique_admins[c.zuser] = True
    #users = db(db.auth_user.id>=887).select()
    #for u in users:
    #    unique_admins[u.id] = True
    #return repr(len(unique_admins))
    events = db(db.auth_event.description.contains("Registered")).select()
    t = {}
    for e in events:
        if e.user_id in unique_admins:
            ts = datetime.strftime(e.time_stamp,"%m/%Y")
            if ts in t:
                t[ts] += 1
            else:
                t[ts] = 1
    return repr(t)
    
@auth.requires_membership('qc')
def months_active():
    events = db(db.auth_event.id>0).select()
    t = {}
    largest_id = 0
    for e in events:
        if e.user_id and ("Registered" not in e.description) and ("Logged-in" not in e.description) and ("portal page" not in e.description):
            ts = str(e.user_id) + " " + datetime.strftime(e.time_stamp,"%m/%Y")
            if ts in t:
                t[ts] += 1
            else:
                t[ts] = 1
        if e.user_id > largest_id:
            largest_id = e.user_id
    result = ""
    for i in range(1,largest_id+1):
        result += "%8i " % i
        months = 0
        for j in range(1,13):
            if (str(i) + " " + ("%.2i" % j) + "/2016") in t.keys():
                result += "X "
                months += 1
            else:
                result += "  "
        result += "%s \n" % months
    response.headers['Content-Type']="text/plain"
    return result
        
@auth.requires_membership('qc')
def retention():
    events = db(db.auth_event.id>0).select()
    mins = {}
    maxs = {}
    largest_id = 0
    for e in events:
        if e.user_id and e.time_stamp.year == 2016 and ("Registered" not in e.description) and ("Logged-in" not in e.description) and ("portal page" not in e.description):
            if not (e.user_id in mins):
                mins[e.user_id] = e.time_stamp
            maxs[e.user_id] = e.time_stamp
    result = ""
    for i in mins:
        result += "%8i " % i
        result += datetime.strftime(mins[i],"%m/%d/%Y ")
        result += datetime.strftime(maxs[i],"%m/%d/%Y ")
        result += "%s \n" % (maxs[i] - mins[i]).days
    response.headers['Content-Type']="text/plain"
    return result
    
@auth.requires_membership('qc')
def dataset_visits():
    events = db(db.auth_event.description.contains("loads dataset")).select()
    visits = {}
    for e in events:
        if e.time_stamp.year == 2016:
            did = int(e.description.split("dataset ")[1])
            if did in visits:
                visits[did] += 1
            else:
                visits[did] = 1
    datasets = db(db.datasets.id>0).select()
    result = ""
    for d in datasets:
        if d.id in visits:
            result += "%30.30s %30.30s %s\n" % (d.portal.name,d.name,visits[d.id])
            del(visits[d.id])
    for v in visits:
        result += "%30.30s %30.30s %s\n" % ("(deleted)","(deleted)",visits[v])
    response.headers['Content-Type']="text/plain"
    return result