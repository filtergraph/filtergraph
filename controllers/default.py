import json, os, shutil, random, urllib2
from datetime import datetime
from multiprocessing import Process, Queue

def index():
    secure_page()
    return dict()
    
def login():
    secure_page()
    if auth.user_id:
        if request.vars._next:
            return '<html><head><meta http-equiv="refresh" content="1;url='+request.vars._next+'"><script>window.location.href="'+request.vars._next+'"</script></head><body></body></html>'
        redirect(URL("portals"))
    else:
        loginfail = False
        if request.vars.username:
            u = auth.login_bare(request.vars.username,request.vars.password)
            if u:
                log("Logged-in")
                if request.vars._next:
                    #http://stackoverflow.com/questions/5411538/how-to-redirect-from-an-html-page
                    return '<html><head><meta http-equiv="refresh" content="1;url='+request.vars._next+'"><script>window.location.href="'+request.vars._next+'"</script></head><body></body></html>'
                redirect(URL("portals"))
            else:
                uname = db(db.auth_user.email == request.vars.username).select().first()
                if uname:
                    u2 = auth.login_bare(uname.username,request.vars.password)
                    if u2:
                        log("Logged-in")
                        if request.vars._next:
                            #http://stackoverflow.com/questions/5411538/how-to-redirect-from-an-html-page
                            return '<html><head><meta http-equiv="refresh" content="1;url='+request.vars._next+'"><script>window.location.href="'+request.vars._next+'"</script></head><body></body></html>'
                        redirect(URL("portals"))
                    else:
                        log("Login failed")
                        loginfail = True
                else:
                    log("Login failed")
                    loginfail = True
        return dict(loginfail = loginfail)
        
def private():
    secure_page()
    return dict()
    
def requestaccess():
    secure_page()
    portal = db(db.portals.url==request.vars.p).select().first()
    if auth.user_id:
        if db(db.admins.portal==portal.id)(db.admins.zadmin==auth.user_id).count() > 0:
            redirect("/"+portal.url) 
    if request.vars.email:
        reqs = db(db.requests.portal==portal).select(db.requests.email)
        if request.vars.email in [req.email for req in reqs]:
            portal = db(db.portals.url==request.vars.p).select().first()
            admins = db(db.admins.portal==portal)(db.admins.viewonly==False).select()
            email_list = [admin.zadmin.id for admin in admins if (admin.viewonly != True)]
            response.flash = "We already let the %s of this portal know." % (len(email_list)>1 and "administrators" or "administrator")
            return dict()
        request_url = ''.join(random.choice("ABCDEFGHJKLMNPQRSTUVWXYZ23456789") for x in range(20))
        db.requests.insert(portal=portal,url=request_url,name=request.vars.name,email=request.vars.email)        
        admins = db(db.admins.portal==portal.id).select()
        email_subject = "%s requests access to the %s portal" % (request.vars.name or request.vars.email,portal.name)
        email_content = "%s (%s) would like to request access to the %s portal. To allow this person to view this portal, click the link below." % (request.vars.name,request.vars.email,portal.name)
        email_link = "http://%s/allow/%s" % (request.env.http_host,request_url)
        email_list = [admin.zadmin.id for admin in admins if (admin.viewonly != True)]
        log("sends email to user %s, subject %s" % (repr(email_list),repr(email_subject)))
        email_user(content=email_content,subject=email_subject,link=email_link,button="Allow user",user=email_list,safety=False)
        response.flash = "You should hear back from %s of this portal soon." % (len(email_list)>1 and "one of the administrators" or "the administrator")
    elif request.vars.submit:
        request_url = ''.join(random.choice("ABCDEFGHJKLMNPQRSTUVWXYZ23456789") for x in range(20))
        db.requests.insert(portal=portal,url=request_url,zuser=auth.user)        
        admins = db(db.admins.portal==portal.id).select()
        email_subject = "%s %s requests access to the %s portal" % (auth.user.first_name,auth.user.last_name,portal.name)
        email_content = "%s %s would like to request access to the %s portal. To allow this person to view the portal, click the link below." % (auth.user.first_name,auth.user.last_name,portal.name)
        email_link = "http://%s/allow/%s" % (request.env.http_host,request_url)
        email_list = [admin.zadmin.id for admin in admins if (admin.viewonly != True)]
        log("sends email to user %s, subject %s" % (repr(email_list),repr(email_subject)))
        email_user(content=email_content,subject=email_subject,link=email_link,button="Allow user",user=email_list,safety=False)
        response.flash = "You should hear back from %s of this portal soon." % (len(email_list)>0 and "one of the administrators" or "the administrator")
    return dict()
    
def allow():
    secure_page()
    req = db(db.requests.url == request.args(0)).select().first()
    if not req:
        return dict(result="invalid_link")
    portal = req.portal
    pid = portal.id
    admins = db(db.admins.portal==pid).select()
    if auth.user_id:
        if auth.user_id not in [a.zadmin.id for a in admins]:
            log("not allowed to allow users for %s" % pid)
            return dict(result="not_authorized",portal=portal)
        if len([True for a in admins if a.zadmin.id == auth.user_id and a.viewonly])>0:
            log("is view-only and not allowed to allow users for %s" % pid)
            return dict(result="not_authorized",portal=portal)
        name = auth.user.first_name + " " + auth.user.last_name
    else:
        name = "An admin"
    if req.zuser:
        if req.zuser.id in [a.zadmin.id for a in admins]:
            return dict(result="user_already_has_access",user=req.zuser,portal=portal)
        db.admins.insert(portal=portal,zadmin=req.zuser,viewonly=True)
        email_link = "http://%s/%s" % (request.env.http_host,portal.url)
        email_content = "%s has approved your request to view the %s portal. You can view this portal anytime by visiting %s." % \
            (name,portal.name,email_link)
        email_subject = "Your request to view the %s portal has been approved" % portal.name
        email_result = email_user(content=email_content,user=req.zuser.id,subject=email_subject,link=email_link,button="View this portal",safety=False)
        return dict(result="user_approved",user=req.zuser,portal=portal,email_result=email_result)
    user = db(db.auth_user.email==req.email).select().first()
    if user:
        if user.id in [a.zadmin.id for a in admins]:
            return dict(result="user_already_has_access",user=user,portal=portal)
        db.admins.insert(portal=portal,zadmin=user,viewonly=True)
        email_link = "http://%s/%s" % (request.env.http_host,portal.url)
        email_content = "%s has approved your request to view the %s portal. You can view this portal anytime by visiting %s." % \
            (name,portal.name,email_link)
        email_subject = "Your request to view the %s portal has been approved" % portal.name
        email_result = email_user(content=email_content,user=user.id,subject=email_subject,link=email_link,button="View this portal",safety=False)
        return dict(result="user_approved",user=user,portal=portal,email_result=email_result)
    else:
        if db(db.links.portal==pid)(db.links.invited_email==req.email).count() > 0:
            return dict(result="email_already_sent",email=req.email,portal=portal)
        link_url = ''.join(random.choice("ABCDEFGHJKLMNPQRSTUVWXYZ23456789") for x in range(20))
        db.links.insert(portal=pid,url=link_url,viewonly=True,invited_email=req.email)
        email_link = "http://%s/access/%s" % (request.env.http_host,link_url)
        email_content = "%s has approved your request to view the %s portal. To visit this portal, please click the link below." % \
            (name,portal.name)
        email_subject = "Your request to view the %s portal has been approved" % portal.name
        email_result = email_user(content=email_content,user=req.email,subject=email_subject,link=email_link,button="Visit this portal",safety=False)
        return dict(result="invite_sent",email=req.email,portal=portal,email_result=email_result)
    
def access():
    secure_page()
    link = db(db.links.url == request.args(0)).select().first()
    if not link:
        result = "invalid_link"
        return dict(result=result)
    db(db.links.id==link.id).update(visits=link.visits+1)
    portal = link.portal
    pid = portal.id
    if auth.user_id:
        admins = db(db.admins.portal==pid).select()
        if auth.user_id in [a.zadmin.id for a in admins]:
            if len([True for a in admins if a.zadmin.id == auth.user_id and a.viewonly])>0 and not link.viewonly:
                db(db.admins.id==a.id).update(viewonly=False)
                result = "can_now_edit"
            else:
                result = "already_has_access"
        else:
            db.admins.insert(portal=portal,zadmin=auth.user_id,viewonly=link.viewonly)
            result = "access_granted"
    else:
        db.temp_access.insert(portal=portal,link=link)
        result = "login_splash"
    return dict(result=result,portal=portal,link=link)
        
def showcase():
    secure_page()
    response.files.append('/'+request.application+'/static/_1.0.0/js/masonry.pkgd.min.js')
    notebook = db(db.notebook.showcase == True).select(orderby='<random>')
    log("showcase page")
    return dict(notebook=notebook)
    
@auth.requires_login()
def portals():
    secure_page()
    portals = db(db.admins.zadmin==auth.user_id).select()
    portals_viewonly = portals.find(lambda row: row.viewonly == True)
    portals_editable = portals.find(lambda row: row.viewonly != True)
    response.files.append('/'+request.application+'/static/_1.0.0/js/masonry.pkgd.min.js')
    db(db.notebook.session_id == FG_BROWSER_ID).update(zuser=auth.user_id,session_id=None)
    notebook = db(db.notebook.zuser == auth.user_id).select()
    for p in portals:
        notebook = notebook | db(db.notebook.portal == p.portal)(db.notebook.zpublic == True).select()
    notebook = notebook.sort(lambda row: row.ztime, reverse=True)
    comments = []
    for n in notebook:
        comments += [db(db.comments.nbentry == n.id).select()]
    log("portal page")
    return dict(portals_viewonly=portals_viewonly,portals_editable=portals_editable,notebook=notebook,comments=comments)

@auth.requires_login()
def addportal():
    secure_page()
    log("Attempts to create portal")
    portals = db(db.admins.zadmin==auth.user_id).select()
    first_portal = (len(portals) == 0)
    quota = 1500#db.auth_user[auth.user_id].quota or 50
    form = SQLFORM.factory(
        Field("name","string",required=True),
        Field("url","string",required=True),
        Field("file","upload",uploadfolder=FG_UPLOAD_FOLDER,
            requires=IS_LENGTH(maxsize=quota*1024768L,error_message="File size limited to %sMB. To submit a larger dataset, please fill out a feedback form." % quota),label="Upload a file"),
        Field("fileurl","string",label="Enter the URL here"),
        Field("restrictions","boolean"))
    if form.process().accepted:
        input_url = request.vars.url or (''.join(random.choice("abcdefghijklmnopqrstuvwxyz") for x in range(5)))
        input_name = request.vars.name or input_url
        existingportals = db(db.portals.url == input_url).select()
        if input_url in RESERVED_LIST:
            log("Failed to create portal: reserved name")
            response.flash = "%s is a reserved name" % input_url
        elif input_url[0] in "0123456789":
            log("Failed to create portal: starts with a digit")
            response.flash = "%s starts with a digit" % input_url
        elif not hasattr(request.vars.file,"filename"):
            if request.vars.fileurl:
                urlfile = urllib2.urlopen(request.vars.fileurl)
                if urlfile.code == 200:
                    if long(urlfile.info().getheaders('Content-Length')[0]) <= (quota*1024768L):
                        urlname = "URL-" + ''.join(random.choice("ABCDEFGHJKLMNPQRSTUVWXYZ23456789") for x in range(40))
                        with open(os.path.join(FG_UPLOAD_FOLDER,urlname),"w") as out:
                            out.write(urlfile.read())
                        pid = db.portals.insert(name=input_name,url=input_url,private=request.vars.restrictions)
                        db.admins.insert(portal=pid,zadmin=auth.user_id)
                        did = db.datasets.insert(name=dataset_namer(urlfile.url.split("/")[-1]),
                             ftype=urlfile.url.split(".")[-1],
                             portal=pid,
                             zfile=urlname)
                        log("Created portal successfully")
                        redirect(URL('readdataset',args=[did,"done"]))
                    else:
                        log("Failed to create portal: file too big")
                        response.flash = "File too big" 
                else:
                    log("Failed to create portal: URL not found")
                    response.flash = "URL not found"
            else:
                log("Failed to create portal: no file uploaded")
                response.flash = "No file uploaded"
        elif len(existingportals) > 0:
            log("Failed to create portal: name taken")
            response.flash = "%s is taken" % input_url
        else:
            pid = db.portals.insert(name=input_name,url=input_url,private=request.vars.restrictions)
            db.admins.insert(portal=pid,zadmin=auth.user_id)
            did = db.datasets.insert(name=dataset_namer(request.vars.file.filename),
                                 ftype=request.vars.file.filename.split(".")[-1],
                                 portal=pid,
                                 zfile=form.vars.file)
            log("Created portal successfully")
            redirect(URL('readdataset',args=[did,"done"]))
    elif form.errors:
        log("Failed to create portal: form has errors")
        response.flash = 'form has errors'
    return dict(form=form,first_portal=first_portal,quota=quota)
    
@auth.requires_login()
def create():
    secure_page()
    log("Attempts to create portal")
    portals = db(db.admins.zadmin==auth.user_id).select()
    first_portal = (len(portals) == 0)
    quota = 1500#db.auth_user[auth.user_id].quota or 50
    form = SQLFORM.factory(
        Field("name","string",required=True),
        Field("url","string",required=True),
        Field("restrictions","boolean"))
    if form.process().accepted:
        input_url = request.vars.url or (''.join(random.choice("abcdefghijklmnopqrstuvwxyz") for x in range(5)))
        input_name = request.vars.name or input_url
        existingportals = db(db.portals.url == input_url).select()
        if input_url in RESERVED_LIST:
            log("Failed to create portal: reserved name")
            response.flash = "%s is a reserved name" % input_url
        elif input_url[0] in "0123456789":
            log("Failed to create portal: starts with a digit")
            response.flash = "%s starts with a digit" % input_url
        elif len(existingportals) > 0:
            log("Failed to create portal: name taken")
            response.flash = "%s is taken" % input_url
        else:
            pid = db.portals.insert(name=input_name,url=input_url,private=request.vars.restrictions)
            db.admins.insert(portal=pid,zadmin=auth.user_id)
            log("Created portal successfully")
            redirect(URL('upload',args=pid))
    elif form.errors:
        log("Failed to create portal: form has errors")
        response.flash = 'form has errors'
    return dict(form=form,first_portal=first_portal,quota=quota)

@auth.requires_login()
def upload():
    secure_page()
    pid = int(request.args[0])
    admins = db(db.admins.portal==pid).select()
    if auth.user_id not in [a.zadmin.id for a in admins]:
        log("not allowed to edit admins for %s" % pid)
        redirect(URL("portals"))
    if len([True for a in admins if a.zadmin.id == auth.user_id and a.viewonly])>0:
        log("is view-only and not allowed to edit admins for %s" % pid)
        redirect(URL("portals"))
    portal = db.portals[pid]
    datasets = db(db.datasets.portal==pid).select()
    if not portal.admin_code:
        db(db.portals.id==pid).update(admin_code=''.join(random.choice("ABCDEFGHJKLMNPQRSTUVWXYZ23456789") for x in range(20)))
    admins = db(db.admins.portal==pid).select()
    if auth.user_id not in [a.zadmin.id for a in admins] and not auth.has_membership('qc'):
        log("not allowed to visit portal edit page for %s" % portal.name)
        redirect(URL("portals"))
    if len([True for a in admins if a.zadmin.id == auth.user_id and a.viewonly])>0:
        log("is view-only and not allowed to visit portal edit page for %s" % portal.name)
        redirect(URL("portals"))
    portal = db.portals[pid]
    quota = 1500#db.auth_user[auth.user_id].quota or 50
    log("opens upload screen")
    if request.vars.replace:
        did = int(request.vars.replace)
        dataset = db.datasets[did]
        if dataset.portal != pid:
            log("dataset %s is not in portal %s" % dataset.name, portal.name)
            redirect(URL("portals"))
        return dict(portal=portal,dataset=dataset)
    else:
        return dict(portal=portal,dataset=None)
    
@auth.requires_login()
def fileservice():
    pid = int(request.env.http_x_portal)
    admins = db(db.admins.portal==pid).select()
    quota = 1500#db.auth_user[auth.user_id].quota or 50
    if auth.user_id not in [a.zadmin.id for a in admins]:
        log("not allowed to edit admins for %s" % pid)
        redirect(URL("portals"))
    if len([True for a in admins if a.zadmin.id == auth.user_id and a.viewonly])>0:
        log("is view-only and not allowed to edit admins for %s" % pid)
        redirect(URL("portals"))
    fname = "FILE-" + ''.join(random.choice("ABCDEFGHJKLMNPQRSTUVWXYZ23456789") for x in range(40))
    if len(request.body.read()) > (quota*1048576):
        return "quota"
    with open(os.path.join(FG_UPLOAD_FOLDER,fname),"w") as out:
        request.body.seek(0)
        out.write(request.body.read())
    ftype = request.env.http_x_filename.split(".")[-1]
    did = db.datasets.insert(name=dataset_namer(request.env.http_x_filename,datasets=db(db.datasets.portal==pid).select()),
                             ftype=ftype,
                             portal=pid,
                             zfile=fname)
    log("file %s uploaded as %s, waiting to process" % (did, fname))
    return str(did)
    
@auth.requires_login()
def textservice():
    pid = int(request.args[0])
    admins = db(db.admins.portal==pid).select()
    quota = 1500#db.auth_user[auth.user_id].quota or 50
    if auth.user_id not in [a.zadmin.id for a in admins]:
        log("not allowed to edit admins for %s" % pid)
        redirect(URL("portals"))
    if len([True for a in admins if a.zadmin.id == auth.user_id and a.viewonly])>0:
        log("is view-only and not allowed to edit admins for %s" % pid)
        redirect(URL("portals"))
    fname = "TEXT-" + ''.join(random.choice("ABCDEFGHJKLMNPQRSTUVWXYZ23456789") for x in range(40))
    if len(request.body.read()) > (quota*1048576L):
        return "quota"
    with open(os.path.join(FG_UPLOAD_FOLDER,fname),"w") as out:
        request.body.seek(0)
        out.write(request.body.read())
    ftype = "txt"
    did = db.datasets.insert(name=dataset_namer("data.txt",datasets=db(db.datasets.portal==pid).select()),
                             ftype=ftype,
                             portal=pid,
                             zfile=fname)
    log("file %s uploaded as %s, waiting to process" % (did, fname))
    return str(did)
    
@auth.requires_login()
def urlservice():
    pid = int(request.args[0])
    url = request.vars.url
    admins = db(db.admins.portal==pid).select()
    quota = 1500#db.auth_user[auth.user_id].quota or 50
    if auth.user_id not in [a.zadmin.id for a in admins]:
        log("not allowed to edit admins for %s" % pid)
        redirect(URL("portals"))
    if len([True for a in admins if a.zadmin.id == auth.user_id and a.viewonly])>0:
        log("is view-only and not allowed to edit admins for %s" % pid)
        redirect(URL("portals"))
    fname = "URL-" + ''.join(random.choice("ABCDEFGHJKLMNPQRSTUVWXYZ23456789") for x in range(40))
    #with open(os.path.join(FG_UPLOAD_FOLDER,fname),"w") as out:
    #    f = urllib2.urlopen(url)
    #    out.write(f.read(quota*1048576))
    #    f.close()
    #if os.stat(os.path.join(FG_UPLOAD_FOLDER,fname)).st_size >= (quota*1048576):
    #    os.remove(os.path.join(FG_UPLOAD_FOLDER,fname))
    #    return "quota"
    ftype = url.split(".")[-1]
    did = db.datasets.insert(name=dataset_namer(url.split("/")[-1],datasets=db(db.datasets.portal==pid).select()),
                             ftype=ftype,
                             portal=pid,
                             zfile=fname,
                             urlfeed=url)
    log("file %s uploaded as %s, waiting to process" % (did, fname))
    return str(did)
    
@auth.requires_login()
def chunkservice():
    log("adding chunk %s to file %s" % (request.env.http_x_chunk_seq,request.args[0]))
    did = int(request.args[0])
    pid = db.datasets[did].portal
    admins = db(db.admins.portal==pid).select()
    if auth.user_id not in [a.zadmin.id for a in admins]:
        log("not allowed to process files for %s" % pid)
        redirect(URL("portals"))
    if len([True for a in admins if a.zadmin.id == auth.user_id and a.viewonly])>0:
        log("is view-only and not allowed to process files for %s" % pid)
        redirect(URL("portals"))
    quota = 1500#db.auth_user[auth.user_id].quota or 50
    fname = "%s-%.8i" % (db.datasets[did].zfile,int(request.env.http_x_chunk_seq))
    #fname = db.datasets[did].zfile
    if len(request.body.read()) > (quota*1048576):
        return "quota"
    with open(os.path.join(FG_UPLOAD_FOLDER,fname),"w") as out:
        request.body.seek(0)
        out.write(request.body.read())
    return str(did)
    
@auth.requires_login()
def processservice():
    log("begin processing file %s" % request.args[0])
    did = int(request.args[0])
    pid = db.datasets[did].portal
    admins = db(db.admins.portal==pid).select()
    if auth.user_id not in [a.zadmin.id for a in admins]:
        log("not allowed to process files for %s" % pid)
        redirect(URL("portals"))
    if len([True for a in admins if a.zadmin.id == auth.user_id and a.viewonly])>0:
        log("is view-only and not allowed to process files for %s" % pid)
        redirect(URL("portals"))
    fname = db.datasets[did].zfile
    ftype = db.datasets[did].ftype
    urlfeed = db.datasets[did].urlfeed
    if request.env.http_x_uuid:
        status = scheduler.task_status(request.env.http_x_uuid, output=True)
        if status.scheduler_run.status == "COMPLETED":
            data = status.result
            revert = False
            try:
                shutil.move(data['loadfile'],data['done_fname'])
            except:pass
        elif os.path.exists(os.path.join(FG_CACHE_FOLDER,fname+".npy")):
            data = {}
            data['data'] = os.path.join(FG_CACHE_FOLDER,fname+".npy")
            revert = False
            try:
                shutil.move(os.path.join(FG_UPLOAD_FOLDER,fname),os.path.join(FG_DONE_FOLDER,fname))
            except:pass
        elif status.scheduler_run.status == "RUNNING":
            run_output = status.scheduler_run.run_output
            if run_output:
                run_output = run_output.splitlines()[-1]
            else:
                run_output = "Processing the file..."
            if run_output.endswith("%)"):
                run_pct = run_output.split("(")[1][:-1]
                return """
    		    $("#status_"+fileid).text("output");
    		    $("#button_"+fileid).html("");
    		    $("#bar_"+fileid).css("width","run_pct");
    		    setTimeout(function() {check_queue(fileid,did,"uuid")},2000);
                """.replace("fileid",request.args[1]).replace('did',request.args[0]).replace("uuid",request.env.http_x_uuid).replace("output",run_output).replace("run_pct",run_pct)
            else:
                return """
    		    $("#status_"+fileid).text("output");
    		    $("#button_"+fileid).html("");
    		    setTimeout(function() {check_queue(fileid,did,"uuid")},2000);
                """.replace("fileid",request.args[1]).replace('did',request.args[0]).replace("uuid",request.env.http_x_uuid).replace("output",run_output)
        elif status.scheduler_run.status == "FAILED":
            log("file %s has error" % did)
            try:
                db(db.datasets.id==did).delete()
            except:pass
            return """
            $("#bar_"+fileid).parent().removeClass("active");
    		$("#bar_"+fileid).addClass("danger");
    		$("#bar_"+fileid).addClass("progress-bar-danger");
    		$("#status_"+fileid).html("There was an error in processing your file.<br/><br/><a class='btn btn-default' onclick='report()'>Send report</a>");
            """.replace("fileid",request.args[1])
        elif status.scheduler_run.status == "TIMEOUT":
            log("file %s timed out" % did)
            try:
                db(db.datasets.id==did).delete()
            except:pass
            return """
            $("#bar_"+fileid).parent().removeClass("active");
    		$("#bar_"+fileid).addClass("danger");
    		$("#bar_"+fileid).addClass("progress-bar-danger");
    		$("#status_"+fileid).html("The server timed out while processing your file.<br/><br/><a class='btn btn-default' onclick='report()'>Send report</a>");
            """.replace("fileid",request.args[1])
        elif status.scheduler_run.status == "STOPPED":
            log("file %s stopped" % did)
            try:
                db(db.datasets.id==did).delete()
            except:pass
            return """
            $("#bar_"+fileid).parent().removeClass("active");
    		$("#bar_"+fileid).addClass("danger");
    		$("#bar_"+fileid).addClass("progress-bar-danger");
    		$("#status_"+fileid).html("The server stopped processing your file.<br/><br/><a class='btn btn-default' onclick='report()'>Send report</a>");
            """.replace("fileid",request.args[1])
        else:
            return """
    		$("#status_"+fileid).text("Processing the file...");
    		$("#button_"+fileid).html("");
    		setTimeout(function() {check_queue(fileid,did,"uuid")},2000)
            """.replace("fileid",request.args[1]).replace('did',request.args[0]).replace("uuid",request.env.http_x_uuid)
    else:
        if request.env.http_x_fix:
            fix = {"header":request.env.http_x_header,
                   "data":request.env.http_x_data,
                   "headertext":request.env.http_x_headertext,
                   "format":request.env.http_x_format}
            db(db.datasets.id==did).update(lur_fix=json.dumps(fix))
        else:
            fix = None
        revert = False
        try:
            if len(request.args)>2:
                data = loaddata2(os.path.join(FG_UPLOAD_FOLDER,fname),ftype=ftype,fix=fix)
                tablename=data['multipletables'].keys()[int(request.args[2])]
                data = loaddata(fname,ftype=ftype,tablename="table_"+tablename)
                db(db.datasets.id==did).update(lur_tablename="table_"+tablename)
                try:
                    shutil.move(data['loadfile'],data['done_fname'])
                except:pass
            else:
                data = loaddata(fname,ftype=ftype,fix=fix,urlfeed=urlfeed)
                try:
                    shutil.move(data['loadfile'],data['done_fname'])
                except:pass
            log("file %s successfully processed" % did)
        except:
            if request.env.http_x_fix:
                trash_cache_fname = os.path.join(FG_TRASH_CACHE_FOLDER,fname) + ".npy"
                cache_fname = os.path.join(FG_CACHE_FOLDER,fname) + ".npy"
                os.rename(trash_cache_fname,cache_fname)
                data = loaddata(fname,ftype=ftype)
                revert = True
                try:
                    shutil.move(data['loadfile'],data['done_fname'])
                except:pass
                log("file %s successfully processed" % did)
            else:
                log("file %s failed; trying to clean up" % did)
                try:
                    db(db.datasets.id==did).delete()
                    db.commit()
                except:
                    log("couldn't clean up")
                log("cleanup finished")
                raise
    if "data" in data:
        try:
            data['data'] = np.load(data['data'],mmap_mode='r')
        except:
            data['data'] = np.load(data['data'])
        result = ""
        if revert:
            result += "Couldn't load file. Reverting to old settings.<br/><br/>"
        if len(data["data"]) <= 5:
            upload_status = "Done. %s rows, %s columns." % (len(data["data"]),len(data["data"].dtype.names))
        else:
            upload_status = "Done. %s rows, %s columns. The first 5 rows are shown below."  % (len(data["data"]),len(data["data"].dtype.names))
        if "ignorelist" in data:
            if len(data["ignorelist"])>0:
                upload_status += " " + prettylist(data["ignorelist"])
        if "dups" in data:
            if len(data["dups"])>0:
                upload_status += " " + prettylist2(data["dups"])
        result += "<div style='overflow-x:scroll'><div style='width:1px'><table class='table table-striped table-hover'><thead><tr>"
        for i in range(len(data["data"].dtype)):
            result += "<th>"+data["data"].dtype.names[i]+"<br/><small>"
            if data["data"].dtype[i].kind=="S":
                result += "text"
            else:
                result += "numeric"
                if np.isnan(np.min(data["data"][data["data"].dtype.names[i]])):
                    result += "+nan"
            result += "</small></th>"
        result += "</tr></thead><tbody>"
        for i in data["data"][:5]:
            result += "<tr>"
            for j in i:
                result += "<td>"+str(j)+"</td>"
            result += "</tr>"
        result += "</tbody></table></div></div><br/>"
        result += "<a class='btn btn-default' onclick='undo(%s,%s)'>Undo</a> " % (request.args[1],did)
        if ftype not in ['xls','xlsx','sql','sqlite','vot','xml','fits','tbl','npy','db','h5']:
            result += "<a class='btn btn-default' onclick='fix(%s,%s)'>Import settings...</a>" % (request.args[1],did)
        result += "<br/><br/>"
        result = result.replace('\\','\\\\')
        result = result.replace('"','\\"')
        result = result.replace("'","\\'")
        result = result.replace('\n','<br/>')
        result = result.replace('\r','<br/>')
        return """
        $("#bar_"+fileid).parent().removeClass("active");
		$("#bar_"+fileid).addClass("success");
		$("#bar_"+fileid).addClass("progress-bar-success");
		$("#bar_"+fileid).css("width","100%");
		$("#status_"+fileid).text("Done");
		$("#button_"+fileid).html("result");
		last_fid = fileid;
		last_d = did;
        """.replace("fileid",request.args[1]).replace("did",request.args[0]).replace("Done",upload_status).replace("result",result)
    elif "fixed_length_start" in data:
        log("fixed length")
        result = "<a class='btn btn-default fixedlength' href='/%s/default/readdataset/%s'>Go to data file</a>" % (request.application,did)
        return """
        $("#bar_"+fileid).parent().removeClass("active");
		$("#bar_"+fileid).addClass("warning");
		$("#bar_"+fileid).addClass("progress-bar-warning");
		$("#status_"+fileid).text("You entered a fixed length file. Click the button below to continue.");
		$("#button_"+fileid).html("result");
        """.replace("fileid",request.args[1]).replace("result",result)
    elif "queue" in data:
        log("queued")
        return """
		$("#status_"+fileid).text("Processing the file...");
		$("#button_"+fileid).html("");
		setTimeout(function() {check_queue(fileid,did,"uuid")},2000)
        """.replace("fileid",request.args[1]).replace('did',request.args[0]).replace("uuid",data['queue'])
    elif "multipletables" in data:
        log("multiple tables")
        result = ""
        table_names = data['multipletables'].keys()
        for i in range(len(table_names)):
            result += """<a class='btn btn-default' onclick='choose_table(%s,%s,%s)'>%s</a> """ % (request.args[1],did,i,table_names[i])
        return """
        $("#bar_"+fileid).parent().removeClass("active");
		$("#bar_"+fileid).addClass("warning");
		$("#bar_"+fileid).addClass("progress-bar-warning");
		$("#status_"+fileid).text("This file has multiple tables. Select a table below.");
		$("#button_"+fileid).html("result");
        """.replace("fileid",request.args[1]).replace("result",result)
    elif "error" in data:
        log("file %s has error" % did)
        try:
            db(db.datasets.id==did).delete()
        except:pass
        return """
        $("#bar_"+fileid).parent().removeClass("active");
		$("#bar_"+fileid).addClass("danger");
		$("#bar_"+fileid).addClass("progress-bar-danger");
		$("#status_"+fileid).html("errortext<br/><br/><a class='btn btn-default' onclick='report()'>Send report</a>");
        """.replace("fileid",request.args[1]).replace("errortext",data["error"])
        
def report():
    log("sending report")
    email = "dan.burger@vanderbilt.edu"
    if auth.user_id:
        subject = "[Filtergraph] %s submitted a report" % (auth.user.username)
        content = "User %s %s (%s) submitted a report: %s" % (auth.user.first_name,auth.user.last_name,auth.user.username,request.vars.text)
        f = urllib2.urlopen("http://filtergraph.pythonanywhere.com/emailservice/default/index?email=%s&subject=%s&content=%s&safety=false" % (email,subject,content))
        if f.read() == "PASS":
            result = content
            log("report sent: %s" % request.vars.text)
        else:
            result = "Could not send email"
            log("failed to send report: %s" % request.vars.text)
        f.close()
        return result
    else:
        subject = "[Filtergraph] Guest submitted a report"
        content = "Guest submitted a report: %s" % request.vars.text
        f = urllib2.urlopen("http://filtergraph.pythonanywhere.com/emailservice/default/index?email=%s&subject=%s&content=%s&safety=false" % (email,subject,content))
        if f.read() == "PASS":
            result = content
            log("report sent: %s" % request.vars.text)
        else:
            result = "Could not send email"
            log("failed to send report: %s" % request.vars.text)
        f.close()
        return result

@auth.requires_login()    
def checkportal():
    portals = db(db.portals.url == request.vars.url).select()
    if not request.vars.url:
        return ""
    elif request.vars.url == request.vars.orig_url:
        return ""
    elif request.vars.url in RESERVED_LIST:
        return "<div class='alert alert-danger' role='alert'><b>%s</b> is a reserved name</div>" % request.vars.url
    elif request.vars.url[0] in "0123456789":
        return "<div class='alert alert-danger' role='alert'><b>%s</b> starts with a digit</div>" % request.vars.url
    elif len(portals) > 0:
        return "<div class='alert alert-danger' role='alert'><b>%s</b> is taken</div>" % request.vars.url
    else:
        return "<div class='alert alert-success' role='alert'><b>%s</b> is available</div>" % request.vars.url
    
@auth.requires_login()
def editportal():
    secure_page()
    pid = int(request.args[0])
    portal = db.portals[pid]
    datasets = db(db.datasets.portal==pid).select()
    if not portal.admin_code:
        db(db.portals.id==pid).update(admin_code=''.join(random.choice("ABCDEFGHJKLMNPQRSTUVWXYZ23456789") for x in range(20)))
    admins = db(db.admins.portal==pid).select()
    if auth.user_id not in [a.zadmin.id for a in admins] and not auth.has_membership('qc'):
        log("not allowed to visit portal edit page for %s" % portal.name)
        redirect(URL("portals"))
    if len([True for a in admins if a.zadmin.id == auth.user_id and a.viewonly])>0:
        log("is view-only and not allowed to visit portal edit page for %s" % portal.name)
        redirect(URL("portals"))
    portal = db.portals[pid]
    quota = 1500#db.auth_user[auth.user_id].quota or 50
    form = SQLFORM.factory(
        Field("name","string",default=portal.name,required=True),
        Field("url","string",default=portal.url,required=True),
        Field("restrictions","boolean",default=portal.private))
    if form.process(formname='form').accepted:
        input_url = request.vars.url or (''.join(random.choice("abcdefghijklmnopqrstuvwxyz") for x in range(5)))
        input_name = request.vars.name or input_url
        existingportals = db(db.portals.url == input_url).select()
        if input_url in RESERVED_LIST:
            log("Failed to change portal name: reserved name")
            response.flash = "%s is a reserved name" % input_url
        elif input_url[0] in "0123456789":
            log("Failed to change portal name: starts with a digit")
            response.flash = "%s starts with a digit" % input_url
        elif (len(existingportals) > 0) and (input_url != portal.url):
            log("Failed to change portal name: name taken")
            response.flash = "%s is taken" % input_url
        else:
            log("Name/privacy change successful")
            db(db.portals.id==pid).update(name=input_name,url=input_url,private=request.vars.restrictions)
            redirect(URL('editportal',args=[pid]))
    elif form.errors:
        log("Failed to change name/privacy settings: form has errors")
        response.flash = 'form has errors'
    newfileform = SQLFORM.factory(
        Field("file","upload",uploadfolder=FG_UPLOAD_FOLDER,
            requires=IS_LENGTH(maxsize=quota*1024768L,error_message="File size limited to %sMB. To submit a larger dataset, please fill out a feedback form." % quota),label="Upload a file"),
        Field("fileurl","string",label="Enter the URL here"))
    if newfileform.process(formname='newfileform').accepted:
        if not hasattr(request.vars.file,"filename"):
            if request.vars.fileurl:
                urlfile = urllib2.urlopen(request.vars.fileurl)
                if urlfile.code == 200:
                    if long(urlfile.info().getheaders('Content-Length')[0]) <= (quota*1024768L):
                        urlname = "URL-" + ''.join(random.choice("ABCDEFGHJKLMNPQRSTUVWXYZ23456789") for x in range(40))
                        with open(os.path.join(FG_UPLOAD_FOLDER,urlname),"w") as out:
                            out.write(urlfile.read())
                        did = db.datasets.insert(name=dataset_namer(urlfile.url.split("/")[-1],datasets),
                             ftype=urlfile.url.split(".")[-1],
                             portal=portal,
                             zfile=urlname)
                        log("New file successful")
                        redirect(URL('readdataset',args=[did,"done"]))
                    else:
                        log("Failed to add file: file too big")
                        response.flash = "File too big" 
                else:
                    log("Failed to add file: URL not found")
                    response.flash = "URL not found"
            else:
                log("Failed to add file: no file uploaded")
                response.flash = "No file uploaded"
        else:
            did = db.datasets.insert(name=dataset_namer(request.vars.file.filename,datasets),
                                     ftype=request.vars.file.filename.split(".")[-1],
                                     portal=pid,
                                     zfile=newfileform.vars.file)
            log("New file successful")
            redirect(URL('readdataset',args=[did,"done"]))
    dforms = []
    uforms = []
    for d in datasets:
        dform = SQLFORM.factory(
            Field("name","string",default=d.name,required=True),
            Field("description","text",default=d.description,required=True))
        if dform.process(formname='dataset%s' % d.id).accepted:
            db(db.datasets.id==d.id).update(name=dataset_namer(dform.vars.name,datasets,ignore=d.name),description=dform.vars.description)
            redirect(URL('editportal',args=[pid]))
        dforms.append(dform)
        uform = SQLFORM.factory(
            Field("file","upload",uploadfolder=FG_UPLOAD_FOLDER,
            requires=IS_LENGTH(maxsize=quota*1024768L,error_message="File size limited to %sMB. To submit a larger dataset, please fill out a feedback form." % quota),label="Upload a file"),
            Field("fileurl","string",label="Enter the URL here"))
        if uform.process(formname='upload%s' % d.id).accepted:
            cachefile = os.path.join(FG_CACHE_FOLDER,d.zfile) + ".npy"
            trashfile = os.path.join(FG_TRASH_CACHE_FOLDER,d.zfile) + ".npy"
            donefile = os.path.join(FG_DONE_FOLDER,d.zfile)
            uploadtrashfile = os.path.join(FG_TRASH_UPLOAD_FOLDER,d.zfile)
            if not hasattr(request.vars.file,"filename"):
                if request.vars.fileurl:
                    urlfile = urllib2.urlopen(request.vars.fileurl)
                    if urlfile.code == 200:
                        if long(urlfile.info().getheaders('Content-Length')[0]) <= (quota*1024768L):
                            urlname = "URL-" + ''.join(random.choice("ABCDEFGHJKLMNPQRSTUVWXYZ23456789") for x in range(40))
                            with open(os.path.join(FG_UPLOAD_FOLDER,urlname),"w") as out:
                                out.write(urlfile.read())
                            if os.path.exists(cachefile):
                                os.rename(cachefile,trashfile)
                            if os.path.exists(donefile):
                                shutil.move(donefile,uploadtrashfile)    
                            db(db.datasets.id==d.id).update(name=dataset_namer(urlfile.url.split("/")[-1],datasets,ignore=d.name),
                                 ftype=urlfile.url.split(".")[-1],
                                 zfile=urlname)
                            log("File change successful")
                            redirect(URL('readdataset',args=[d.id,"done"]))
                        else:
                            log("Failed to change file: file too big")
                            response.flash = "File too big" 
                    else:
                        log("Failed to change file: URL not found")
                        response.flash = "URL not found"
                else:
                    log("Failed to change file: no file uploaded")
                    response.flash = "No file uploaded"
            else:
                if os.path.exists(cachefile):
                    os.rename(cachefile,trashfile)
                if os.path.exists(donefile):
                    shutil.move(donefile,uploadtrashfile)   
                db(db.datasets.id==d.id).update(name=dataset_namer(request.vars.file.filename,datasets,ignore=d.name),
                                     ftype=request.vars.file.filename.split(".")[-1],
                                     zfile=uform.vars.file)
                if os.path.exists("/hd1/filtergraph-thumbs/p/%s.jpg" % pid):
                    os.remove("/hd1/filtergraph-thumbs/p/%s.jpg" % pid)
                if os.path.exists("/hd1/filtergraph-thumbs/d/%s.jpg" % d.id):
                    os.remove("/hd1/filtergraph-thumbs/d/%s.jpg" % d.id)
                log("File change successful")
                redirect(URL('readdataset',args=[d.id,"done"]))
        uforms.append(uform)
    if session.msg:
        response.flash = session.msg
        session.msg = ""
    return dict(portal=portal,form=form,admins=admins,datasets=datasets,dforms=dforms,uforms=uforms,newfileform=newfileform,quota=quota)
    
@auth.requires_login()
def editadmin():
    pid = int(request.args[0])
    admins = db(db.admins.portal==pid).select()
    if auth.user_id not in [a.zadmin.id for a in admins]:
        log("not allowed to edit admins for %s" % pid)
        redirect(URL("portals"))
    if len([True for a in admins if a.zadmin.id == auth.user_id and a.viewonly])>0:
        log("is view-only and not allowed to edit admins for %s" % pid)
        redirect(URL("portals"))
    editadmin = db.admins[int(request.args[1])]
    if not editadmin:
        log("invalid edit admin settings for %s" % pid)
        redirect(URL("portals"))
    if editadmin.zadmin == auth.user_id:
        log("changing own edit admin settings for %s" % pid)
        redirect(URL("portals"))
    if request.args[2] == "viewonly":
        log("sets %s to viewonly for %s" % (editadmin.id,pid))
        db(db.admins.id == editadmin.id).update(viewonly=not editadmin.viewonly)
    elif request.args[2] == "delete":
        log("deleting %s admin privileges for %s" % (editadmin.id,pid))
        db(db.admins.id == editadmin.id).delete()
    redirect(URL('editportal',args=[pid]))
    
@auth.requires_login()
def defaultdataset():
    did = int(request.args[0])
    pid = db.datasets[did].portal
    admins = db(db.admins.portal==pid).select()
    if auth.user_id not in [a.zadmin.id for a in admins]:
        log("not allowed to make default dataset %s" % did)
        redirect(URL("portals"))
    if len([True for a in admins if a.zadmin.id == auth.user_id and a.viewonly])>0:
        log("is view-only and not allowed to make default dataset %s" % did)
        redirect(URL("portals"))
    db(db.datasets.portal==pid).update(is_default=False)
    db(db.datasets.id==did).update(is_default=True)
    log("makes default dataset %s" % did)
    if os.path.exists(os.path.join(FG_PORTAL_THUMBS,"%s.jpg" % pid)):
        os.remove(os.path.join(FG_PORTAL_THUMBS,"%s.jpg" % pid))
    redirect(URL('editportal',args=[pid]))

@auth.requires_login()
def deletedataset():
    did = int(request.args[0])
    pid = db.datasets[did].portal
    admins = db(db.admins.portal==pid).select()
    if auth.user_id not in [a.zadmin.id for a in admins]:
        log("not allowed to delete dataset %s" % did)
        redirect(URL("portals"))
    if len([True for a in admins if a.zadmin.id == auth.user_id and a.viewonly])>0:
        log("is view-only and not allowed to delete dataset %s" % did)
        redirect(URL("portals"))
    if db.datasets[did].lur_task:
        scheduler.stop_task(db.datasets[did].lur_task)
    if len(request.args) > 1:
        if request.args[1] == "undo":
            email = "dan.burger@vanderbilt.edu"
            subject = "[Filtergraph] %s undos file %s" % (auth.user.username,db.datasets[did].name)
            content = "User %s %s (%s) has undoed file %s. Email: %s Location: %s" % (auth.user.first_name,auth.user.last_name,auth.user.username,db.datasets[did].name,auth.user.email,db.datasets[did].zfile)
            f = urllib2.urlopen("http://filtergraph.pythonanywhere.com/emailservice/default/index?email=%s&subject=%s&content=%s" % (email,subject,content))
            if f.read() == "PASS":
                print content
            else:
                print "Could not send email"
            f.close()
    donefile = os.path.join(FG_DONE_FOLDER,db.datasets[did].zfile)
    cachefile = os.path.join(FG_CACHE_FOLDER,db.datasets[did].zfile) + ".npy"
    if os.path.exists(cachefile):
        if os.path.exists(os.path.join(FG_TRASH_CACHE_FOLDER,db.datasets[did].zfile) + ".npy"):
            os.remove(os.path.join(FG_TRASH_CACHE_FOLDER,db.datasets[did].zfile) + ".npy")
        shutil.move(cachefile,FG_TRASH_CACHE_FOLDER)
    if os.path.exists(donefile):
        if os.path.exists(os.path.join(FG_TRASH_UPLOAD_FOLDER,db.datasets[did].zfile)):
            os.remove(os.path.join(FG_TRASH_UPLOAD_FOLDER,db.datasets[did].zfile))
        shutil.move(donefile,FG_TRASH_UPLOAD_FOLDER)
    db(db.datasets.id==did).delete()
    if os.path.exists(os.path.join(FG_DATASET_THUMBS,"%s.jpg" % did)):
        os.remove(os.path.join(FG_DATASET_THUMBS,"%s.jpg" % did))
    log("deletes dataset %s" % did)
    redirect(URL('editportal',args=[pid]))
    
@auth.requires_login()
def deleteportal():
    pid = int(request.args[0])
    admins = db(db.admins.portal==pid).select()
    if auth.user_id not in [a.zadmin.id for a in admins]:
        log("not allowed to delete portal %s" % pid)
        redirect(URL("portals"))
    if len([True for a in admins if a.zadmin.id == auth.user_id and a.viewonly])>0:
        log("is view-only and not allowed to delete portal %s" % pid)
        redirect(URL("portals"))
    datasets = db(db.datasets.portal==pid).select()
    for d in datasets:
        if d.lur_task:
            scheduler.stop_task(d.lur_task)
        donefile = os.path.join(FG_DONE_FOLDER,d.zfile)
        cachefile = os.path.join(FG_CACHE_FOLDER,d.zfile) + ".npy"
        if os.path.exists(cachefile):
            if os.path.exists(os.path.join(FG_TRASH_CACHE_FOLDER,d.zfile) + ".npy"):
                os.remove(os.path.join(FG_TRASH_CACHE_FOLDER,d.zfile) + ".npy")
            shutil.move(cachefile,FG_TRASH_CACHE_FOLDER)
        if os.path.exists(donefile):
            if os.path.exists(os.path.join(FG_TRASH_UPLOAD_FOLDER,d.zfile)):
                os.remove(os.path.join(FG_TRASH_UPLOAD_FOLDER,d.zfile))
            shutil.move(donefile,FG_TRASH_UPLOAD_FOLDER)
        if os.path.exists(os.path.join(FG_DATASET_THUMBS,"%s.jpg" % d.id)):
            os.remove(os.path.join(FG_DATASET_THUMBS,"%s.jpg" % d.id))
        db(db.datasets.id==d.id).delete()
    for a in admins:
        db(db.datasets.id==a.id).delete()
    db(db.portals.id==pid).delete()
    if os.path.exists(os.path.join(FG_PORTAL_THUMBS,"%s.jpg" % pid)):
        os.remove(os.path.join(FG_PORTAL_THUMBS,"%s.jpg" % pid))
    log("deletes portal %s" % pid)
    redirect(URL('portals'))   
    
@auth.requires_login()
def fix():
    log("attempts to fix %s" % request.args[0])
    did = int(request.args[0])
    portal = db.datasets[did].portal
    datasets = db(db.datasets.portal==portal).select()
    try:
        pid = db.datasets[did].portal
    except:
        if auth.has_membership('qc'):
            return "Nope"
        else:
            log("portal does not exist for %s" % did)
            redirect(URL("portals"))
    admins = db(db.admins.portal==pid).select()
    if auth.user_id not in [a.zadmin.id for a in admins] and not auth.has_membership('qc'):
        log("cannot edit dataset %s" % did)
        redirect(URL("portals"))
    if len([True for a in admins if a.zadmin.id == auth.user_id and a.viewonly])>0:
        log("is viewonly and cannot edit dataset %s" % did)
        redirect(URL("portals"))
    dataset = db.datasets[did]
    fname = os.path.join(FG_UPLOAD_FOLDER,db.datasets[did].zfile)
    if not os.path.exists(fname):
        fname = os.path.join(FG_DONE_FOLDER,db.datasets[did].zfile)
    if not os.path.exists(fname):
        log("cannot get to original file for %s" % did)
        redirect(URL("portals"))
    with open(fname) as f:
        firstlines = [f.readline() for i in range(10)]
    firstlines = [line for line in firstlines if line]
    return dict(dataset=dataset,portal=portal,firstlines=firstlines)
    
@auth.requires_login()
def replace_file():
    log("attempts to replace %s with %s" % (request.args[0],request.args[1]))
    did1 = int(request.args[0])
    portal = db.datasets[did1].portal
    admins = db(db.admins.portal==portal.id).select()
    if auth.user_id not in [a.zadmin.id for a in admins] and not auth.has_membership('qc'):
        log("cannot edit dataset %s" % did1)
        redirect(URL("portals"))
    if len([True for a in admins if a.zadmin.id == auth.user_id and a.viewonly])>0:
        log("is viewonly and cannot edit dataset %s" % did1)
        redirect(URL("portals"))
    did2 = int(request.args[1])
    if db.datasets[did2].portal != portal:
        log("%s and %s are not in the same portal" % (did1,did2))
        redirect(URL("portals"))
    did1file = db.datasets[did1].zfile
    did2file = db.datasets[did2].zfile
    donefile = os.path.join(FG_DONE_FOLDER,db.datasets[did1].zfile)
    cachefile = os.path.join(FG_CACHE_FOLDER,db.datasets[did1].zfile) + ".npy"
    if os.path.exists(cachefile):
        if os.path.exists(os.path.join(FG_TRASH_CACHE_FOLDER,db.datasets[did1].zfile) + ".npy"):
            os.remove(os.path.join(FG_TRASH_CACHE_FOLDER,db.datasets[did1].zfile) + ".npy")
        shutil.move(cachefile,FG_TRASH_CACHE_FOLDER)
    if os.path.exists(donefile):
        if os.path.exists(os.path.join(FG_TRASH_UPLOAD_FOLDER,db.datasets[did1].zfile)):
            os.remove(os.path.join(FG_TRASH_UPLOAD_FOLDER,db.datasets[did1].zfile))
        shutil.move(donefile,FG_TRASH_UPLOAD_FOLDER)
    if db.datasets[did1].lur_task:
        scheduler.stop_task(db.datasets[did1].lur_task)
    db(db.datasets.id==did1).update(zfile=did2file,ftype=db.datasets[did2].ftype,urlfeed=db.datasets[did2].urlfeed,lur_task=None,lur_tablename=db.datasets[did2].lur_tablename,lur_fix=db.datasets[did2].lur_fix)
    db(db.datasets.id==did2).delete()
    return "done"
    
@auth.requires_login()
def readdataset():
    secure_page()
    did = int(request.args[0])
    portal = db.datasets[did].portal
    datasets = db(db.datasets.portal==portal).select()
    try:
        pid = db.datasets[did].portal
    except:
        if auth.has_membership('qc'):
            return "Nope"
        else:
            log("portal does not exist for %s" % did)
            redirect(URL("portals"))
    admins = db(db.admins.portal==pid).select()
    if auth.user_id not in [a.zadmin.id for a in admins] and not auth.has_membership('qc'):
        log("cannot edit dataset %s" % did)
        redirect(URL("portals"))
    if len([True for a in admins if a.zadmin.id == auth.user_id and a.viewonly])>0:
        log("is viewonly and cannot edit dataset %s" % did)
        redirect(URL("portals"))
    if request.vars.customize:
        longnames=""
        descriptions=""
        settings=db.datasets[did].settings
        html_names=""
        for item in request.vars.keys():
            if request.vars.get(item) != "":
                if item.endswith("_n"):
                    longnames += item[:-2] + "=" + request.vars.get(item) + "\n"
                elif item.endswith("_d"):
                    descriptions += item[:-2] + "=" + request.vars.get(item) + "\n"
                elif item.endswith("_h") and request.vars.get(item).startswith("http"):
                    html_names += item[:-2] + "=" + request.vars.get(item) + "\n"
                elif item.endswith("_h"):
                    html_names += item[:-2] + "=http://" + request.vars.get(item) + "\n"
        if request.vars.reset_savestate:
            settings = ""
        db(db.datasets.id==int(request.args[0])).update(
            name=dataset_namer(request.vars.name,datasets,ignore=db.datasets[did].name),description=request.vars.desc,header_names=longnames,
            description_names=descriptions,html_names=html_names,settings=settings)#,urlfeed=newurlfeed)
        if os.path.exists("/hd1/filtergraph-thumbs/p/%s.jpg" % pid):
            os.remove("/hd1/filtergraph-thumbs/p/%s.jpg" % pid)
        if os.path.exists("/hd1/filtergraph-thumbs/d/%s.jpg" % did):
            os.remove("/hd1/filtergraph-thumbs/d/%s.jpg" % did)
        log("edited dataset %s" % did)
        redirect(URL("editportal",args=[pid]))
    fname = db.datasets[did].zfile
    data = None
    dataset = db.datasets[did]
    if os.path.exists(os.path.join(FG_DATA_FOLDER,"%s.npy" % portal.url)):
        fname = os.path.join(FG_DATA_FOLDER,"%s.npy" % portal.url)
        data = {'data':np.load(fname)}
    if os.path.exists(os.path.join(FG_DATA_FOLDER,"%s.npy" % dataset.id)):
        fname = os.path.join(FG_DATA_FOLDER,"%s.npy" % dataset.id)
        data = {'data':np.load(fname)}
    ftype = db.datasets[did].ftype
    start = datetime.now()
    force_type = {}
    session.forget(response)
    if not data:
        log("loading dataset %s to cache" % did)
        data = loaddata(fname,tablename=request.vars.table,fixedlength=request.vars.fixedlength,ftype=ftype,fix=request.vars.fix)
        if "data" in data:
            try:
                data['data'] = np.load(data['data'],mmap_mode='r')
            except:
                data['data'] = np.load(data['data'])
        log("done loading dataset %s!" % did)
    data['time'] = (datetime.now() - start)
    data['portal'] = portal
    data['dataset'] = dataset
    if 'debug' in data:
        return repr(data['debug'])
    if datasets[0].id != dataset.id:
        data['url'] = "http://" + request.env.http_host+"/"+portal.url+"?d="+dataset.name
    else:
        data['url'] = "http://" + request.env.http_host+"/"+portal.url
    data['longnames'] = parse_list(dataset.header_names)
    data['descriptions'] = parse_list(dataset.description_names)
    data['html_names'] = parse_list(dataset.html_names)
    if dataset.settings.startswith('{"'):
        data['settings'] = json.loads(dataset.settings)
    else:
        data['settings'] = parse_list(dataset.settings)
    #http://stackoverflow.com/questions/82831/how-do-i-check-if-a-file-exists-using-python
    try:
        with open(os.path.join(DONE_FOLDER,db.datasets[did].zfile)):pass
        data['orig_file_exists'] = True
    except:
        data['orig_file_exists'] = False
    return data
    
@auth.requires_login()
def adduser():
    secure_page()
    pid = int(request.args[0])
    portal = db.portals[pid]
    if not portal.admin_code:
        db(db.portals.id==pid).update(admin_code=''.join(random.choice("ABCDEFGHJKLMNPQRSTUVWXYZ23456789") for x in range(20)))
    admins = db(db.admins.portal==pid).select()
    if auth.user_id not in [a.zadmin.id for a in admins] and not auth.has_membership('qc'):
        log("not allowed to visit portal edit page for %s" % portal.name)
        redirect(URL("portals"))
    if len([True for a in admins if a.zadmin.id == auth.user_id and a.viewonly])>0:
        log("is view-only and not allowed to visit portal edit page for %s" % portal.name)
        redirect(URL("portals"))
    log("opens add user screen")
    links = db(db.links.portal==pid).select()
    if request.vars.usertype:
        if '@' in request.vars.user:
            user = db(db.auth_user.email==request.vars.user).select().first()
            if user:
                admin_record = db(db.admins.portal == pid)(db.admins.zadmin==user.id).select().first()
                if not admin_record:
                    db.admins.insert(portal=pid,zadmin=user.id,viewonly=(request.vars.usertype!="viewedit"))
                    email_link = "http://%s/%s" % (request.env.http_host,portal.url)
                    email_content = "%s %s has invited you to %s the %s portal. You can view this portal anytime by visiting %s." % \
                        (auth.user.first_name,auth.user.last_name,(request.vars.usertype=="viewedit") and "view and edit" or "view",portal.name,email_link)
                    email_subject = "You have been invited to %s the %s portal" % \
                        ((request.vars.usertype=="viewedit") and "view and edit" or "view",portal.name)
                    email_result = email_user(content=email_content,user=user.id,subject=email_subject,link=email_link,button="View this portal",safety=False)
                    response.flash = "%s %s is now able to %s this portal. %s" % \
                        (user.first_name,user.last_name,(request.vars.usertype=="viewedit") and "view and edit" or "view",email_result)
                elif admin_record.viewonly and request.vars.usertype == "viewedit":
                    email_link = "http://%s/%s" % (request.env.http_host,portal.url)
                    email_content = "%s %s has invited you to %s the %s portal. You can view this portal anytime by visiting %s." % \
                        (auth.user.first_name,auth.user.last_name,(request.vars.usertype=="viewedit") and "view and edit" or "view",portal.name,email_link)
                    email_subject = "You have been invited to %s the %s portal" % \
                        ((request.vars.usertype=="viewedit") and "view and edit" or "view",portal.name)
                    email_result = email_user(content=email_content,user=user.id,subject=email_subject,link=email_link,button="View this portal",safety=False)
                    response.flash = "%s %s is now able to %s this portal. %s" % \
                        (user.first_name,user.last_name,(request.vars.usertype=="viewedit") and "view and edit" or "view",email_result)
                    db(db.admins.id==admin_record.id).update(viewonly=False)
                    response.flash = "%s %s can now edit this portal. %s" % (user.first_name,user.last_name,email_result)
                else:
                    response.flash = "%s %s already has access to this portal." % (user.first_name,user.last_name)
            elif request.vars.user in [link.invited_email for link in links]:
                response.flash = "%s already has an invite." % (request.vars.user)
            else:
                link_url = ''.join(random.choice("ABCDEFGHJKLMNPQRSTUVWXYZ23456789") for x in range(20))
                db.links.insert(portal=pid,url=link_url,viewonly=(request.vars.usertype!="viewedit"),invited_email=request.vars.user)
                email_link = "http://%s/access/%s" % (request.env.http_host,link_url)
                email_content = "%s %s is using Filtergraph to visualize data for %s and would like to give you access to %s its data. To accept, please click the link below." % \
                    (auth.user.first_name,auth.user.last_name,portal.name,(request.vars.usertype=="viewedit") and "view and edit" or "view")
                email_subject = "You have been invited to %s the %s portal" % \
                    ((request.vars.usertype=="viewedit") and "view and edit" or "view",portal.name)
                email_result = email_user(content=email_content,user=request.vars.user,subject=email_subject,link=email_link,button="Accept this invitation",safety=False)
                response.flash = "An invite has been sent to %s. %s" % \
                    (request.vars.user,email_result)
        else:
            user = db(db.auth_user.username==request.vars.user).select().first()
            if user:
                admin_record = db(db.admins.portal == pid)(db.admins.zadmin==user.id).select().first()
                if not admin_record:
                    db.admins.insert(portal=pid,zadmin=user.id,viewonly=(request.vars.usertype!="viewedit"))
                    email_link = "http://%s/%s" % (request.env.http_host,portal.url)
                    email_content = "%s %s has invited you to %s the %s portal. You can view this portal anytime by visiting %s." % \
                        (auth.user.first_name,auth.user.last_name,(request.vars.usertype=="viewedit") and "view and edit" or "view",portal.name,email_link)
                    email_subject = "You have been invited to %s the %s portal" % \
                        ((request.vars.usertype=="viewedit") and "view and edit" or "view",portal.name)
                    email_result = email_user(content=email_content,user=user.id,subject=email_subject,link=email_link,button="View this portal",safety=False)
                    response.flash = "%s %s is now able to %s this portal. %s" % \
                        (user.first_name,user.last_name,(request.vars.usertype=="viewedit") and "view and edit" or "view",email_result)
                elif admin_record.viewonly and request.vars.usertype == "viewedit":
                    email_link = "http://%s/%s" % (request.env.http_host,portal.url)
                    email_content = "%s %s has invited you to %s the %s portal. You can view this portal anytime by visiting %s." % \
                        (auth.user.first_name,auth.user.last_name,(request.vars.usertype=="viewedit") and "view and edit" or "view",portal.name,email_link)
                    email_subject = "You have been invited to %s the %s portal" % \
                        ((request.vars.usertype=="viewedit") and "view and edit" or "view",portal.name)
                    email_result = email_user(content=email_content,user=user.id,subject=email_subject,link=email_link,button="View this portal",safety=False)
                    db(db.admins.id==admin_record.id).update(viewonly=False)
                    response.flash = "%s %s can now edit this portal. %s" % (user.first_name,user.last_name,email_result)
                else:
                    response.flash = "%s %s already has access to this portal." % (user.first_name,user.last_name)
            else:
                response.flash = "Could not find username."
        links = db(db.links.portal==pid).select()
    elif request.vars.linktype:
        link_url = ''.join(random.choice("ABCDEFGHJKLMNPQRSTUVWXYZ23456789") for x in range(20))        
        db.links.insert(portal=pid,url=link_url,viewonly=(request.vars.linktype!="viewedit"))
        response.flash = "Link added."
        links = db(db.links.portal==pid).select()
    reqs = db(db.requests.portal==pid).select()
    return dict(portal=portal,links=links,reqs=reqs)
    
@auth.requires_login()
def revokelink():
    secure_page()
    lid = int(request.args[0])
    link = db.links[lid]
    portal = link.portal
    pid = portal.id
    if not portal.admin_code:
        db(db.portals.id==pid).update(admin_code=''.join(random.choice("ABCDEFGHJKLMNPQRSTUVWXYZ23456789") for x in range(20)))
    admins = db(db.admins.portal==pid).select()
    if auth.user_id not in [a.zadmin.id for a in admins] and not auth.has_membership('qc'):
        log("not allowed to visit portal edit page for %s" % portal.name)
        redirect(URL("portals"))
    if len([True for a in admins if a.zadmin.id == auth.user_id and a.viewonly])>0:
        log("is view-only and not allowed to visit portal edit page for %s" % portal.name)
        redirect(URL("portals"))
    log("removes link %s" % lid)
    db(db.links.id==lid).delete()
    db(db.temp_access.link==link).delete()
    redirect("/filtergraph/default/adduser/%s" % pid)
    
@auth.requires_login()
def admincode():
    secure_page()
    if auth.user_id:
        form = SQLFORM.factory(
            Field("code","string",required=True,label="Enter code here"))
        if form.process().accepted:
            pid = db(db.portals.admin_code == request.vars.no_table_code.upper().strip()).select().first()
            if pid:
                if not db(db.admins.portal == pid)(db.admins.zadmin==auth.user.id).select():
                    db.admins.insert(portal=pid,zadmin=auth.user_id,viewonly=pid.private)
                log("has access to portal %s" % pid.id)
                redirect(URL('editportal',args=[pid.id]))
            else:
                log("failed to get admin access")
                response.flash = 'This code does not match any portal. Please try again.'
        elif form.errors:
            log("admin code form has errors")
            response.flash = 'form has errors'
        return dict(form=form)
    else:
        return dict()

def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    if len(request.args) > 0 and request.args[0] not in ["login","register"]:
        from gluon.tools import Recaptcha
        if request.env.http_host == "filtergraph.vanderbilt.edu":
            auth.settings.captcha = Recaptcha(request,
                '6Lcs-dUSAAAAADU6uZt2XhVO9xi22TYe5qe4lpuT', '6Lcs-dUSAAAAAM5aJIspuTnOykEEBXcBrZHztMYO')
        if request.env.http_host == "filtergraph.com":
            auth.settings.captcha = Recaptcha(request,
                '6LeBNekSAAAAAAvznNyFcF2Fs9a31vxepnDWh_bA', '6LeBNekSAAAAAAa9PNzUOav9cKXMFNp5pa2R8_Zy')
        if request.env.http_host == "delia.phy.vanderbilt.edu":
            auth.settings.captcha = Recaptcha(request,
                '6LeDNekSAAAAACDNwsPIqlN-wLLyGuyHZZyHVu_i', '6LeDNekSAAAAAKi3S-A_-gKTHQfiS4FVJuA7Irp3')
        if request.env.http_host == "delia.csb-phy.vanderbilt.edu":
            auth.settings.captcha = Recaptcha(request,
                '6LeENekSAAAAANAwSW94pukQ0I8Yqb4in0IarDS4', '6LeENekSAAAAAPQj2wGX9ByVhrS7BTvi44dlF63k')"""
    if request.args(0) == "login":
        redirect(URL("login",vars=request.vars))
    secure_page()
    if request.args(0) == "logout":
        FG_BROWSER_ID = ''.join(random.choice("ABCDEFGHJKLMNPQRSTUVWXYZ23456789") for x in range(20))
        response.cookies['uid'] = FG_BROWSER_ID
        response.cookies['uid']['expires'] = 24 * 7 * 3600
        response.cookies['uid']['path'] = '/'
    if request.args(0) == "profile":
        subscription = db(db.subscriptions.email==auth.user.email).select().first()
        if subscription:
            if subscription.noemail == True:
                #carry it over to user account
                db(db.subscriptions.id == subscription.id).update(noemail=False)
                auth.user.noemail = True
    return dict(form=auth())

def forgot():
    secure_page()
    form = SQLFORM.factory(
        Field("user","string",required=True,label="Email or username"))
    if form.process().accepted:
        user = db(db.auth_user.username==request.vars.user).select().first()
        if not user:
            user = db(db.auth_user.email==request.vars.user).select().first()
        if user:
            passwordkey = ''.join(random.choice("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789") for x in range(25))
            db(db.auth_user.id==user.id).update(reset_password_key=passwordkey)
            email = user.email
            name = user.first_name
            content = "Your Filtergraph username is %s. Use this link to reset your password:" % user.username
            link = "http://%s/%s/default/forgot2/%s" % (request.env.http_host,request.application,passwordkey)
            subject = "Password recovery"
            button = "Reset password"
            response.flash = email_user(content,user=email,subject=subject,button=button,link=link,safety=False)
        else:
            response.flash = "Could not find user."
    elif form.errors:
        response.flash = 'Form has errors.'
    return dict(form=form)
    
def forgot2():
    secure_page()
    user = db(db.auth_user.reset_password_key == request.args[0]).select().first()
    form = SQLFORM.factory(
        Field("password","password",required=True,label="Enter new password"),
        Field("password_two","password",required=True,label="Enter new password again"))
    if form.process().accepted:
        if request.vars.password and request.vars.password == request.vars.password_two:
            db(db.auth_user.id==user.id).validate_and_update(reset_password_key="",password=request.vars.password)
            redirect(URL("index"))
        elif not request.vars.password:
            response.flash = "Please enter a password."
        else:
            response.flash = "Passwords do not match."
    elif form.errors:
        response.flash = 'Form has errors.'
    return dict(form=form)
    
def subscriptions():
    secure_page()
    subscription = db(db.subscriptions.subscribe_id == request.args(0)).select().first()
    if auth.user_id:
        if auth.user.email == subscription.email:
            if auth.user.noemail and not subscription.noemail:
                #carry it over to subscription
                db(db.subscriptions.id == subscription.id).update(noemail=True)
                subscription = db(db.subscriptions.subscribe_id == request.args(0)).select().first()
        else:
            redirect(URL("index"))
    if request.vars.disable_email:
        db(db.subscriptions.id==subscription.id).update(noemail=True)
        if auth.user_id:
            auth.user.noemail = True
        subscription = db(db.subscriptions.subscribe_id == request.args(0)).select().first()
        response.flash = "You will no longer receive emails from Filtergraph to %s. If you change your mind, you can come back to the link in the email." % subscription.email
    elif request.vars.enable_email:
        db(db.subscriptions.id==subscription.id).update(noemail=False)
        if auth.user_id:
            auth.user.noemail = False
        subscription = db(db.subscriptions.subscribe_id == request.args(0)).select().first()
        response.flash = "You will now be able to receive emails from Filtergraph to %s. If you change your mind, you can come back to the link in the email." % subscription.email
    return dict(subscription=subscription)

def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


@auth.requires_signature()
def data():
    """
    http://..../[app]/default/data/tables
    http://..../[app]/default/data/create/[table]
    http://..../[app]/default/data/read/[table]/[id]
    http://..../[app]/default/data/update/[table]/[id]
    http://..../[app]/default/data/delete/[table]/[id]
    http://..../[app]/default/data/select/[table]
    http://..../[app]/default/data/search/[table]
    but URLs must be signed, i.e. linked with
      A('table',_href=URL('data/tables',user_signature=True))
    or with the signed load operator
      LOAD('default','data.load',args='tables',ajax=True,user_signature=True)
    """
    return dict(form=crud())
    
def message_all():
    form = SQLFORM.factory(
        Field("message","text",required=True,label="Enter your message here"),
        _action="/"+request.application+"/default/message_all/"+request.args[0])
    if form.process().accepted:
        pid = int(request.args[0])
        portal = db.portals[pid]
        admins = db(db.admins.portal==pid).select()
        if auth.user_id not in [a.zadmin.id for a in admins]:
            session.msg = "You are not authorized to message all users."
        elif len([True for a in admins if a.zadmin.id == auth.user_id and a.viewonly])>0:
            session.msg = "You are not authorized to message all users."
        else:
            session.msg=email_user("%s %s is sending a message to all %s of %s:\n\n%s" % (auth.user.first_name,auth.user.last_name,portal.private and "users" or "admins",portal.name,request.vars.message),subject="Message to all %s %s" % (portal.name,portal.private and "users" or "admins"),user=[a.zadmin.id for a in admins],safety=False).replace("<br/>"," ")
        redirect("/%s/default/editportal/%s" % (request.application,pid))
        #response.flash = 'form accepted'
    elif form.errors:
        response.flash = 'form has errors'
    return dict(form=form)
    
def faqs():
    secure_page()
    return dict()
    
def credits():
    secure_page()
    return dict()
    
def feedback():
    secure_page()
    form = SQLFORM(db.feedback)
    if form.process().accepted:
        email_subject = "%s sends feedback" % (form.vars.name or form.vars.email)
        email_content = '%s (%s) submitted the following feedback: %s' % (form.vars.name,form.vars.email,form.vars.zmessage)
        if form.vars.errorid:
            email_link = "https://%s/admin/default/ticket/%s" % (request.env.http_host,form.vars.errorid)
            email_button = "View ticket"
        else:
            email_link = None
            email_button = None
        email_user(content=email_content,subject=email_subject,link=email_link,button=email_button,user="dan.burger@vanderbilt.edu",safety=False)
        email_subject = "Thank you for your feedback"
        email_content = "Thank you for your recently submitted feedback to Filtergraph. Your message will be sent to members of our support team who should be able to respond to your feedback shortly. Filtergraph is still in development so we are prioritizing our responses to questions on a selective basis, but generally within 7 days."
        email_user(content=email_content,subject=email_subject,user=form.vars.email,custom_name=form.vars.name or form.vars.email, safety=False)
        response.flash = "We have received your feedback. Thank you for your response."
    elif form.errors:
        response.flash = 'form has errors'
    return dict(form=form)
    
def terms():
    secure_page()
    return dict()

def debug():
    return dict(request=request,response=response,session=session)