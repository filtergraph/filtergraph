from multiprocessing import Process, Queue
import subprocess
import os
import numpy as np
import random
import shutil
import ConfigParser
from datetime import datetime

def index():
    nitems = int(10**random.uniform(2,6))
    #nitems = np.random.randint(10,1000000)
     
    has_color = random.randint(0,1)
    has_size = random.randint(0,1)
    ncols = 2 + has_color + has_size
    dataout = np.random.randn(nitems,ncols)

    height = random.randint(300,1600)
    width = random.randint(300,1200)
    point_type = random.randint(1,7)

    command = ["set terminal %s size %s,%s" % ((point_type in [1,4,5]) and "png" or "pngcairo",height,width),
               "set output '%s'" % os.path.join(FG_BENCHMARK_FOLDER,"test.png"),
               "set xrange [-5:5]",
               "set yrange [-5:5]",
               "set cbrange [-5:5]",
               "set grid",
               "plot '-' using %s binary format='%s' %s %s pt %s notitle; show variables all" % (":".join([str(i) for i in range(1,ncols+1)]),"%float64" * ncols,has_color and "with points palette" or "", has_size and "ps variable" or "", point_type)]
    
    start_time = datetime.now()
    process = subprocess.Popen(FG_GNUPLOT_PATH,stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    print "Process started: %s" % process.pid
    output = process.communicate("\n".join(command) + "\n" + dataout.tostring())
    print "Process finished: %s (code: %s)" % (process.pid, process.returncode)
    end_time = datetime.now() - start_time
    end_time = end_time.total_seconds()
    
    #Get terminal
    sstr = (output[1].splitlines())
    stats = {}
    for s in sstr:
        sspl = s.split("=",1)
        if len(sspl) > 1:
            stats[sspl[0].strip()] = sspl[1].strip()
    
    with open(os.path.join(FG_BENCHMARK_FOLDER,"gp_times.txt"),"a") as f:
        f.write("%s %s %s %s %s %s %s\n" % (nitems, has_color, has_size, height, width, point_type, end_time))
        
    #test image merge
    if FG_ENABLE_ACCELERATION and (FG_MAX_CORES > 1):
        nmerges = random.randint(2,FG_MAX_CORES)
        for i in range(nmerges):
            shutil.copyfile(os.path.join(FG_BENCHMARK_FOLDER,"test.png"),os.path.join(FG_BENCHMARK_FOLDER,"%s.png" % i))
        start_time = datetime.now()
        cmd = "%s %s -flatten %s" % (FG_IMAGEMAGICK_PATH,
            " ".join([os.path.join(FG_BENCHMARK_FOLDER,"%s.png" % i) for i in range(nmerges)]),
            os.path.join(FG_BENCHMARK_FOLDER,"merge.png")
        )
        process = subprocess.Popen(cmd,shell=True,stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
        print "Process started: %s" % process.pid
        output = process.communicate("\n".join(command) + "\n" + dataout.tostring())
        print "Process finished: %s (code: %s)" % (process.pid, process.returncode)
        merge_time = datetime.now() - start_time
        merge_time = merge_time.total_seconds()

        with open(os.path.join(FG_BENCHMARK_FOLDER,"im_times.txt"),"a") as f:
            f.write("%s %s %s\n" % (nmerges, height*width, merge_time))
            
    with open(os.path.join(FG_BENCHMARK_FOLDER,"test.png"),'rb') as f:
        png = f.read()
    
    with open(os.path.join(FG_BENCHMARK_FOLDER,"gp_times.txt")) as f:
        lines = len([1 for ff in f])

    try:
        gptimes = np.loadtxt(os.path.join(FG_BENCHMARK_FOLDER,"gp_times.txt"))
        #gptimes = gptimes[gptimes[:,0]<20000,:]
        #a = np.column_stack((gptimes[:,:3],np.ones(len(gptimes))))
        a = gptimes[:,:3]
        pt = gptimes[:,5]
        b = gptimes[:,6]
        #return repr(np.linalg.lstsq(a,b)[0].tolist())
        gp_model = {}
        for i in range(1,8):
            gp_model[i] = np.linalg.lstsq(a[pt==i],b[pt==i])[0].tolist()
        gp_model = repr(gp_model)

        imtimes = np.loadtxt(os.path.join(FG_BENCHMARK_FOLDER,"im_times.txt"))
        #gptimes = gptimes[gptimes[:,0]<20000,:]
        #a = np.column_stack((gptimes[:,:3],np.ones(len(gptimes))))
        a = imtimes[:,:2]
        b = imtimes[:,2]
        im_model = repr(np.linalg.lstsq(a,b)[0].tolist())
        
        cfg = ConfigParser.RawConfigParser()
        cfg.read(os.path.join(request.env.gluon_parent,"filtergraph.cfg"))
        cfg.set("acceleration","gp_model",gp_model)
        cfg.set("acceleration","im_model",im_model)
        if point_type in [1,4,5]:
            cfg.set("acceleration","terminal",stats.get('GPVAL_TERM').replace('"','') or "pngcairo")
        with open(os.path.join(request.env.gluon_parent,"filtergraph.cfg"),'w') as f:
            cfg.write(f)
        
        return dict(png=png,message=output[1],timing=end_time,gp_model=gp_model,im_model=im_model,lines=lines)
    except:
        return dict(png=png,message=output[1],timing=end_time,lines=lines)
