#!/usr/local/bin/python

from __future__ import division
import os
import numpy as np
import astropy.io.ascii as ascii
import astropy.io.fits as pyfits
import astropy.io.misc as misc
try:
    import astropy.io.votable as vo
except:
    import astropy.io.vo as vo
import sqlite3
import xlrd
import csv
import shutil
import sys
import tempfile
import urllib2
import datetime
import calendar
import string 
import h5py
import pandas

mod0 = 36526.0
mod1 = 35065.0
seconds = 86400
CHUNK_SIZE = 10000

def get_args():
    if len(sys.argv) == 2:
        fin = sys.argv[1]
        if fin.startswith("http://"):
            fout = sys.argv[1].split("/")[-1] + ".npy"
        else:
            fout = sys.argv[1] + ".npy"
    elif len(sys.argv) >= 3:
        fin = sys.argv[1]
        fout = sys.argv[2]
    else:
        print "FGLoader by Dan Burger and Ethan Raymond"# % version
        print "FGLoader is part of Filtergraph, a project of the"
        print "Vanderbilt Institute in Data-intensive Astrophysics."
        print "Usage: python fgloader.py filein.txt [fileout.npy]"
        print "       - or -"
        print "       python fgloader.py http://example.com/filein.txt [fileout.npy]"
        return
    if fin.count(".") > 0:
        ftype = fin.split(".")[-1]
    else:
        ftype = ""
    if fin.startswith("http://"):
        try:
            httpfile = tempfile.NamedTemporaryFile(delete=False)
            httpfile.write(urllib2.urlopen(fin).read())
            httpfile.close()
            result = loaddata2(httpfile.name,ftype=ftype,queued=True)
        except:
            result = {"error":"Could not download file."}
        finally:
            os.remove(httpfile.name)
    else:
        result = loaddata2(fin,ftype=ftype)
    if "data" in result:
        np.save(fout,result["data"])
        print "Saved to: "+fout
        print "Done."
    else:
        print repr(result)
        print "File not saved."
    return
    
def loaddata2(loadfile,tablename = "",ftype="",fixedlength="",fix=None,forcetype=None):    
    #Numpy
    if ftype == "npy":
        print "Processing the Numpy file..."
        data = np.load(loadfile)
        if len(data.shape) == 0:
            return {"error":"Numpy file is empty."}
        elif data.shape[0] == 0:
            return {"error":"Numpy file is empty."}
        elif len(data.shape) == 1:
            return {"data":data}
        elif len(data.shape) == 2:
            numcols = data.shape[1]
            dtype = [("Column%s" % (i+1),data.dtype) for i in range(numcols)]
            data = np.array([tuple(row) for row in data],dtype=dtype)
            return {"data":data}
        else:
            return {"error":"Cannot recognize Numpy file."}
        
    #VOTable        
    elif ftype == "xml" or ftype == "vot":
        print "Processing the VOTable file..."
        data = vo.parse(loadfile, pedantic=False)
        if tablename:
            for resource in data.resources:
                for table in resource.tables:
                    if tablename[6:] == "%s:%s" % (resource.name,table.name):
                        #cache the data and return
                        try:
                            return {"data":table.array.filled(np.nan)}
                        except:
                            return {"data":table.array.filled(-1)}
            return {"error":"Cannot find table with given name."}
        else:
            tables = {}
            for resource in data.resources:
                for table in resource.tables:
                    tables["%s:%s" % (resource.name,table.name)] = table.array
            if len(tables) == 1:
                try:
                     return {"data":table.array.filled(np.nan)}
                except:
                     return {"data":table.array.filled(-1)}
            return {"multipletables":tables}
        
    #FITS
    elif ftype == "fits":
        print "Processing the FITS file..."
        data = pyfits.open(loadfile)
        if len(data) == 0:
            return {"error":"No data in FITS file."}
        elif tablename:
            for table in data:
                if tablename[6:] == table.name:
                    #cache the data and return
                    return {"data":np.array(table.data)}
            return {"error":"Cannot find table with given name."}
        else:
            tables = {}
            for table in data:
                if str(type(table)).count("Table") > 0:
                    tables[table.name] = table.data
            if len(tables) == 1:
                return {"data":np.array(table.data)}
            return {"multipletables":tables}
        
    #SQLite
    elif ftype == "sqlite" or ftype == "db":
        print "Processing the SQLite file..."
        data = sqlite3.connect(loadfile)
        if tablename:
            tables = [tablename[6:]]
            limit = ""
        else:
            tables = [row[1] for row in data.execute("select * from sqlite_master where type='table';") if row[1] != 'sqlite_sequence']
            limit = ""
            if len(tables) > 1:
                limit = "limit 10"
        retval = {}
        for t in tables:
            tabledata = data.execute("select * from %s %s;" % (t,limit))
            types = []
            names = [row[0] for row in tabledata.description]
            inspectors = [Inspector() for row in tabledata.description]
            for row in tabledata:
                for i in range(len(row)):
                    inspectors[i].read(str(row[i]))
            for i in range(len(names)):
                types.append(inspectors[i].gettype())
            dtype = [(names[i],types[i]) for i in range(len(names))]
            #return {"debug":dtype}
            retval[t] = np.array(data.execute("select * from %s %s;" % (t,limit)).fetchall(),dtype=dtype)
        if tablename:
            #cache the data and return
            return {"data":retval[tablename[6:]]}
        elif len(retval) == 1:
            return {"data":retval.values()[0]}
        else:
            return {"multipletables":retval}
    
    #IPAC
    elif ftype == "tbl":
        print "Processing the IPAC file..."
        data = ascii.read(loadfile)
        #cache the data and return
        return {"data":data}
    
    #HDF5
    elif ftype == "h5" or ftype == "hdf":
        print "Processing the HDF5 file..."
        with h5py.File(loadfile,'r') as f:
            if tablename:
                usetable = tablename[6:]
            elif len(f.keys()) == 1:
                usetable = f.keys()[0]
            else:
                retval = {}
                tables = f.keys()
                for t in tables:
                    retval[t] = None
                return {"multipletables":retval}
            try: #H5PY
                data = f[usetable][:]
                if len(data.shape) == 0:
                    return {"error":"HDF5 table is empty."}
                elif data.shape[0] == 0:
                    return {"error":"HDF5 table is empty."}
                elif len(data.shape) == 1:
                    return {"data":data}
                elif len(data.shape) == 2:
                    numcols = data.shape[1]
                    dtype = [("Column%s" % (i+1),data.dtype) for i in range(numcols)]
                    data = np.array([tuple(row) for row in data],dtype=dtype)
                    return {"data":data}
                else: #Pandas
                    pass
            except: #Pandas
                pass
        df = pd.read_hdf(loadfile,usetable)
        data = df.to_records()
        return {"data":data}
        #data = misc.hdf5.read_table_hdf5(loadfile)
        #return {"data":data}
        
    #Excel
    #ref: http://scienceoss.com/read-excel-files-from-python/
    elif "xls" in ftype:
        print "Processing the Excel file..."
        data = xlrd.open_workbook(loadfile)
        datemode = data.datemode
        if data.nsheets == 1:
        	sheet = data.sheet_by_index(0)
        elif tablename:
        	try:
        		sheet = data.sheet_by_name(tablename[6:])
        	except:
        		return {"error":"Cannot find table with given name."}
       	else:
       		return get_tables(data)
       	
        is_header = False
        if sheet.ncols == 0:
            return {"error":"The file you have submitted is blank. Please try again with a file that contains data."}
        firstrow = 0
        firstrowtypes = sheet.row_types(firstrow)
        data_start = None
        while min(firstrowtypes) == 0 or is_comment(firstrowtypes):
        	if not data_start and 2 in firstrowtypes:
        	    data_start = firstrow
        	firstrow+=1
        	try:
        		firstrowtypes = sheet.row_types(firstrow)
        	except:
        		firstrow = data_start
        		break
        if min(firstrowtypes) == max(firstrowtypes) == 1:
        	names = remove_dups(sheet.row_values(firstrow))[0]
        	firstrow+=1
        	is_header = True
        extra_headers = []
    	firstrowtypes = sheet.row_types(firstrow)
        while max(firstrowtypes) == 1:
        	if not is_comment(sheet.row_values(firstrow)):
        		extra_headers.append(sheet.row_values(firstrow))
        	firstrow+=1
        	try:
        		firstrowtypes = sheet.row_types(firstrow)
        	except:
        		extra_headers = []
        		break
        if not is_header:
            names = [("Column%s" % (i+1)) for i in range(len(firstrowtypes))]
        inspectors=[XL_Inspector() for name in names]
        for i in range(firstrow, sheet.nrows):
            row = sheet.row_values(i)
            if not is_comment(row):
                for j in range(len(row)):
                    #if the cell contains text, utf-8 encode the cell value to allow for non ascii characters
                    #if the cell does not contain text, do not encode the cell
                    if sheet.cell_type(i,j) == 1:
                        inspectors[j].xlread(row[j].encode('utf-8'), sheet.cell_type(i,j), j, names)
                    else:
                        inspectors[j].xlread(str(row[j]), sheet.cell_type(i,j), j, names)
        names = combine_headers(names, extra_headers)
        types = [i.xlgettype() for i in inspectors]
        dtype = [(names[i].encode("utf-8"), types[i]) for i in range(len(names))]
        retval = np.array([xl_remove_nans(sheet.row_values(i),types,datemode,names) for i in range(firstrow, sheet.nrows)], dtype=dtype)
        #cache the data and return
        return {"data":retval}
        
    #other binary
    elif ftype in ["png","jpg","jpeg","gif"]:
        return {"error":"Sorry, we do not accept images at this time."}
    elif ftype in ["zip","tar","gz","bz2"]:
        return {"error":"Sorry, we do not accept ZIP files or other compressed files at this time."}
    elif ftype == "pdf":
        return {"error":"Sorry, we do not accept PDF files at this time."}
    elif ftype == "doc" or ftype == "docx":
        return {"error":"Sorry, we do not accept Word documents at this time."}
    elif ftype == "ppt" or ftype == "pptx":
        return {"error":"Sorry, we do not accept PowerPoint documents at this time."}
    elif not istext(loadfile):
        return {"error":"Sorry, we do not recognize this file format at this time."}
        
    #other html/xml
    elif ishtml(loadfile):
        return {"error":"Sorry, we do not recognize HTML files at this time."}
        
    #ASCII,CSV,TSV
    else:
        #split test
        asciitest = splittest(loadfile,None)
        csvtest = splittest(loadfile,",")
        tsvtest = splittest(loadfile,"\t")
        lengths = lengthtest(loadfile)
        filecheck = {'ascii':-1,'tsv':-1,'csv':-1}
        file_format = False
        if len(asciitest) == 0:
            return {"error":"The file you have submitted is blank. Please try again with a file that contains data."}
        if min(asciitest[int(len(asciitest)/2):]) == max(asciitest[int(len(asciitest)/2):]):
            #print "Could be an ASCII file."
            filecheck['ascii'] = min(asciitest[int(len(asciitest)/2):])
            file_format = True
        if tsvtest[-1] > 1 and min(tsvtest[int(len(tsvtest)/2):]) == max(tsvtest[int(len(tsvtest)/2):]):
            #print "Could be a TSV file."
            filecheck['tsv'] = min(tsvtest[int(len(tsvtest)/2):])
            file_format = True
        if csvtest[-1] > 1 and min(csvtest[int(len(csvtest)/2):]) == max(csvtest[int(len(csvtest)/2):]):
            #print "Could be a CSV file."
            filecheck['csv'] = min(csvtest[int(len(csvtest)/2):])
            file_format = True
        if not file_format:
            filecheck['asciitest'] = mode(asciitest)[0]
            filecheck['tsv'] = mode(tsvtest)[0]
            filecheck['csv'] = mode(csvtest)[0]
        if len(filecheck) > 0:
            max_of_filecheck = max(filecheck.values())
            if fix:
                if fix['format'] == "csep":
                    print "Inspecting the comma-separated file..."
                    headertest = csvtest
                    ftype = "csv"
                elif fix['format'] == "ssep":
                    print "Inspecting the space-separated file..."
                    headertest = asciitest
                    ftype = "ascii"
                elif fix['format'] == "tsep":
                    print "Inspecting the tab-separated file..."
                    headertest = tsvtest
                    ftype = "tsv"
            else:
                if filecheck['tsv'] == max_of_filecheck:
                    print "Inspecting the tab-separated file..."
                    headertest = tsvtest
                    ftype="tsv"
                elif filecheck['csv'] == max_of_filecheck:
                    print "Inspecting the comma-separated file..."
                    headertest = csvtest
                    ftype="csv"
                else:
                    print "Inspecting the space-separated file..."
                    headertest = asciitest
                    ftype="ascii"
        elif min(lengths) == max(lengths):
            print "Inspecting the fixed-length file..."
            headertest = lengths
            ftype="fixedlength"
            col_lengths = []
            if fixedlength and sum(col_lengths) != min(lengths):
                col_lengths = [int(i) for i in fixedlength.split(",")]
            else:
                fixed_length_start = []
                with open(loadfile,'rU') as f:
                    i = 0
                    for line in f:
                        fixed_length_start.append(line)
                        i += 1
                        if i >= 10:
                            break
                return {"fixed_length_start":fixed_length_start,"fixed_length_sum":min(lengths)}
            col_starts = []
            col_ends = []
            for i in range(len(col_lengths)):
                col_starts.append(int(sum(col_lengths[:i])))
                col_ends.append(int(sum(col_lengths[:i])+col_lengths[i]))
        else:
            return {"error":"Cannot recognize the file.", "tsvtest":tsvtest, "csvtest":csvtest, "asciitest":asciitest, "lengths":lengths}
        
        #get number of columns
        if fix:
            pass #we will get numcols later
        if ftype == "fixedlength":
            numcols = len(col_lengths)
        else:
            numcols = headertest[-1]
        #print "There are %s columns in the data file." % numcols
        ignorelist = []
        
        #get number of rows
        numrows = 0
        with open(loadfile,'rU') as f:
            for ff in f:
                numrows += 1
        
        #set up fix
        fix_header = None
        fix_firstline = None
        if fix:
            if fix['header'] == "none":
                fix_header = [("Column%s" % (i+1)) for i in range(numcols)]
                fix_headercols = numcols
            elif fix['header'] == "other" or fix['header'].isdigit():
                if fix['header'] == 'other':
                    fix_headerstr = fix['headertext']
                else:
                    with open(loadfile,'rU') as f:
                        for i in range(int(fix['header'])):
                            f.readline()
                        fix_headerstr = f.readline()
                if fix_headerstr[0] == "#":
                    fix_headerstr = fix_headerstr[1:]
                fix_headerstr = fix_headerstr.strip()
                if ftype == "csv":
                    fix_header = fix_headerstr.split(",")
                elif ftype == "ascii":
                    fix_header = fix_headerstr.split()
                elif ftype == "tsv":
                    fix_header = fix_headerstr.split("\t")
                elif ftype == "fixedlength":
                    fix_header = [fix_headerstr[col_starts[i]:col_ends[i]] for i in range(len(col_starts))]
                fix_headercols = len(fix_header)
            else:
                fix_headercols = numcols
            if fix['data'].isdigit():
                fix_firstline = int(fix['data'])
                firstline = fix_firstline
            elif fix['header'].isdigit():
                fix_firstline = int(fix['header'])+1
                firstline = fix_firstline
            else:
                fix_firstline = 1
            with open(loadfile,'rU') as f:
                for i in range(fix_firstline):
                    f.readline()
                fix_firstlinestr = f.readline()
            fix_firstlinestr = fix_firstlinestr.strip()
            if ftype == "csv":
                fix_firstlinearr = fix_firstlinestr.split(",")
            elif ftype == "ascii":
                fix_firstlinearr = fix_firstlinestr.split()
            elif ftype == "tsv":
                fix_firstlinearr = fix_firstlinestr.split("\t")
            elif ftype == "fixedlength":
                fix_firstlinearr = [fix_firstlinestr[col_starts[i]:col_ends[i]] for i in range(len(col_starts))]
            numcols = len(fix_firstlinearr)
            if fix_headercols > numcols:
                fix_header = fix_header[:numcols]
            i = 1
            while len(fix_header) < numcols:
                if ("Column%s" % i) not in fix_header:
                    fix_header.append("Column%s" % i)
                i += 1
            #print numcols

        #header test
        if len(headertest) > 0:
            #print headertest
            firstline = 0
            for firstline in range(len(headertest)-1,-1,-1):
                if headertest[firstline] != headertest[-1]:
                    firstline += 1
                    break
            #print "The data starts on line %s." % firstline
            with open(loadfile,'rU') as f:
                for i in range(firstline):
                    f.readline()  
                #if ftype == "csv":
                 #   fls = f.readline()
                  #  while fls.startswith('#'):
                   #     fls = f.readline()
                    #has_header = csv.Sniffer().has_header(f.read(4096))
                if True:
                    fls = f.readline()
                    if ftype=="tsv":
                        while fls.startswith("#"):
                            if len(fls[1:].split('\t')) == numcols:
                                fls = fls[1:]
                            else:
                                fls = f.readline()
                        fls = fls.split("\t")
                    elif ftype == 'csv':
                        while fls.startswith("#"):
                            if len(fls[1:].split(',')) == numcols:
                                fls = fls[1:]
                            else:
                                fls = f.readline()
                        fls = fls.split(",")
                    elif ftype == 'ascii':
                        while fls.startswith("#"):
                            if len(fls[1:].split()) == numcols:
                                fls = fls[1:]
                            else:
                                fls = f.readline()
                        fls = fls.split()
                    elif ftype=="fixedlength":
                        fls = [fls[col_starts[i]:col_ends[i]] for i in range(len(col_starts))]
                    else:
                        fls = fls.split()
                    header_inspector = Inspector()
                    is_num = False
                    for i in fls:
                        header_inspector.read(i)
                        if header_inspector[i] == 2 or header_inspector[i] == 3:
                            is_num = True
                    if not is_num:
                        has_header = "S" in header_inspector.gettype()
                    else:
                        has_header = False
        if has_header:
            pass#print "There is a header there."
        else:
            pass#print "There is no header there."
            
        if fix_firstline:
            firstline = fix_firstline
            has_header = False
                    
        #get data types, min and max of each column
        inspectors = [Inspector() for i in range(numcols)]
        with open(loadfile,'rU') as f:
            for i in range(firstline):
                f.readline()
            if ftype == "csv":
                reader = csv.reader(f)
            else:
                reader = f
            if has_header:
                if ftype == "csv":
                    names = reader.next()
                elif ftype == "tsv":
                    names = reader.next().strip().split("\t")
                elif ftype == "fixedlength":
                    names = reader.next()
                    names = [names[col_starts[i]:col_ends[i]] for i in range(numcols)]
                else:
                    names = reader.next().split()
                while names[0] == "#" * len(names[0]):
                    if names[0] == "" and len(names) == numcols:
                        names[0] = "Column1"
                    else:
                        names = names[1:]
                while names[0][0] == "#" or names[0][0] == " ":
                    names[0] = names[0][1:]
                names = [n.strip() for n in names]
            else:
                names = [("Column%s" % (i+1)) for i in range(numcols)]
            if fix_header:
                names = fix_header
            row = 0
            for line in reader:
                if row % CHUNK_SIZE == 0:
                    pct = (float(row)/float(numrows)) * 50
                    print "!clear!Processing (%i%%)" % pct
                row += 1
                if ftype == "fixedlength":
                    linespl = [line[col_starts[i]:col_ends[i]] for i in range(numcols)]
                elif ftype == "csv":
                    linespl = line
                elif ftype == "tsv":
                    linespl = line.split('\t')
                else:
                    linespl = line.split()
                if len(linespl) == 0:
                    #end of file
                    break
                if len(linespl) != numcols:
                    ignorelist.append(row+1)
                    continue
                for i in range(len(linespl)):
                    inspectors[i].read(linespl[i])
        (names,dups) = remove_dups(names) # remove duplicates
        formats = [i.gettype() for i in inspectors]
        mins = [i.min for i in inspectors]
        maxs = [i.max for i in inspectors]
        firstnums = [i.firstnum for i in inspectors]
        for (n,f,mn,mx,fn) in zip(names,formats,mins,maxs,firstnums):
            pass#print "name=%s, type=%s, min=%s, max=%s, firstnum=%s" % (n, f, mn, mx,fn)
        dtype = [(n,f) for (n,f) in zip(names,formats)]
        
        #generate numpy file
        #print repr(dtype)
        if ftype == "tsv":
            retval = np.zeros(row-len(ignorelist),dtype=dtype)
            if has_header:
                firstline += 1
            for i in range(0,row,CHUNK_SIZE):
                pct = ((float(i)/float(numrows)) * 50) + 50
                print "!clear!Processing (%i%%)" % pct
                chunk = np.genfromtxt(loadfile,delimiter='\t',dtype=dtype,skip_header=firstline,invalid_raise=False,max_rows=CHUNK_SIZE)
                #chunk might be less than CHUNK_SIZE in length,
                #since lines with invalid numbers of lines are removed
                for j in range(len(dtype)):
                    retval[retval.dtype.names[j]][i:i+len(chunk)] = chunk[chunk.dtype.names[j]]
                firstline += len(chunk)
        elif ftype == "ascii":
            retval = np.zeros(row-len(ignorelist),dtype=dtype)
            if has_header:
                firstline += 1
            for i in range(0,row,CHUNK_SIZE):
                pct = ((float(i)/float(numrows)) * 50) + 50
                print "!clear!Processing (%i%%)" % pct
                chunk = np.genfromtxt(loadfile,dtype=dtype,skip_header=firstline,invalid_raise=False,max_rows=CHUNK_SIZE)
                #chunk might be less than CHUNK_SIZE in length,
                #since lines with invalid numbers of lines are removed
                for j in range(len(dtype)):
                    retval[retval.dtype.names[j]][i:i+len(chunk)] = chunk[chunk.dtype.names[j]]
                firstline += len(chunk)
        elif ftype == "fixedlength":
            if has_header:
                firstline += 1
            retval = np.genfromtxt(loadfile,delimiter=col_lengths,dtype=dtype,skip_header=firstline,invalid_raise=False)
        elif ftype == "csv":
            #http://stackoverflow.com/questions/2664790/reading-csv-files-in-numpy-where-delimiter-is
            retval = np.zeros(row-len(ignorelist),dtype=dtype)
            with open(loadfile,'rU') as f:
                for i in range(firstline):
                    f.readline()
                row_num = f.tell()
                line = f.readline()
                while f.readline().startswith("#"):
                    row_num = f.tell()
                    line = f.readline()
                f.seek(row_num)
                reader = csv.reader(f)
                if has_header:
                    firstline+=1
                    names = reader.next()
                current_line = 0
                for row in reader:
                    if len(row)==numcols:
                        retval[current_line] = remove_nans(row,formats)
                        current_line += 1
                        if current_line % CHUNK_SIZE == 0:
                            pct = ((float(current_line)/float(numrows)) * 50) + 50
                            print "!clear!Processing (%i%%)" % pct
                #retval = np.array([remove_nans(row,formats) for row in reader if len(row)==numcols],dtype=dtype)
        #cache the data and return
        for row in retval:
            if np.result_type(row) is np.string_:
                row = np.char.encode(row, encoding='utf-8')
        return {"data":retval,"ignorelist":ignorelist,"dups":dups,"fixable":True}


def lengthtest(file):
    retval = []
    with open(file,'rU') as f:
        i = 0
        for line in f:
            i += 1
            if i >= 1000:
                break
            retval.append(len(line))
    while len(retval) > 1 and retval[-1] == 0:
        retval = retval[:-1]
    return np.array(retval)
       
def splittest(file,delimiter):
    retval = []
    max = 0
    with open(file,'rU') as f:
        i = 0
        for line in f:
            comment = False
            i += 1
            if i >= 1000:
                break
            line = line.strip()
            while line.startswith("#"):
                line = line[1:]
                comment = True
            length = len(line.split(delimiter))
            if length == 0:
                comment = True
            if length > max:
                max = length
            if not (comment and length < max):
                retval.append(length)
    while len(retval) > 1 and retval[-1] == 1:
        retval = retval[:-1]
    return np.array(retval)

def mode(a, axis=0):
    scores = np.unique(np.ravel(a))       # get ALL unique values
    testshape = list(a.shape)
    testshape[axis] = 1
    oldmostfreq = np.zeros(testshape)
    oldcounts = np.zeros(testshape)
    for score in scores:
        template = (a == score)
        counts = np.expand_dims(np.sum(template, axis),axis)
        mostfrequent = np.where(counts > oldcounts, score, oldmostfreq)
        oldcounts = np.maximum(counts, oldcounts)
        oldmostfreq = mostfrequent
    return mostfrequent, oldcounts
    
class Inspector():
    STRINGS = 1
    INTEGERS = 2
    FLOATS = 3
    PERM_STRING = 4
    t = STRINGS
    min = None
    max = None
    maxlen = 0
    unknown_str = None
    line = 0
    firstnum = None
    _lastval = None
    def read(self,s):
        self.line += 1
        if self.t != self.PERM_STRING:
            try:
                self._lastval = float(s)
                if not self.firstnum:
                    self.firstnum = self.line
                try:
                    if self.t != self.FLOATS and int(s) == self._lastval and not self.unknown_str:
                        self.t = self.INTEGERS
                    else:
                        self.t = self.FLOATS
                except:
                    self.t = self.FLOATS
                if self.min != None:
                    if self._lastval < self.min:
                        self.min = self._lastval
                    elif self._lastval > self.max:
                        self.max = self._lastval
                else:
                    self.min = self.max = self._lastval
            except:
                if s.lower() == "nan" or s == '':
                    if self.t == self.INTEGERS:
                        self.t = self.FLOATS
                elif not self.unknown_str and (self.t in [self.INTEGERS,self.FLOATS]):
                    self.unknown_str = s
                elif self.unknown_str != s:
                    self.t = self.PERM_STRING
        if len(s) > self.maxlen:
            self.maxlen = len(s)
    def gettype(self):
        if self.t == self.INTEGERS:
            for j in ["uint8","int8","uint16","int16","uint32","int32","uint64","int64"]:
                if np.iinfo(j).min <= self.min and self.max <= np.iinfo(j).max:
                    return j
        elif self.t == self.FLOATS:
            if np.finfo("float32").min <= self.min and self.max <= np.finfo("float32").max and self.maxlen <= np.finfo("float32").precision:
                return "float32"
            else:
                return "float64"
    #add check for date type? xlrd reads in dates as tuples

        else:
            return ("S" + str(max(1,self.maxlen)))
    def __getitem__(self, index):
        return self.t
            
def prettylist(a):
    if len(a) == 1:
        return "Line %s has an unusual number of columns. This line has been ignored." % a[0]
    b = [a[0]]
    for i in range(1,len(a)-1):
        if a[i-1] + 1 == a[i] and a[i] + 1 == a[i+1]:
            b.append(None)
        else:
            b.append(a[i])
    b.append(a[-1])
    c = [str(b[0])]
    for i in range(1,len(a)-1):
        if b[i] is None and not c[-1].endswith(" through "):
            c[-1] = c[-1] + " through "
        elif b[i] is None:
            pass #do nothing
        elif c[-1].endswith(" through "):
            c[-1] = c[-1] + str(b[i])
        else:
            c.append(str(b[i]))
    if c[-1].endswith(" through "):
        c[-1] = c[-1] + str(b[-1])
    else:
        c.append(str(b[-1]))
    if len(c) == 1:
        d = c[0]
    elif len(c) == 2:
        d = c[0] + " and " + c[1]
    else:
        d = ", ".join(c[:-1]) + ", and " + c[-1]
    return "Lines %s have an unusual number of columns. These lines have been ignored." % d
    
def remove_dups(a):
    out = []
    dups = []
    for item in a:
        if item not in out:
            out.append(item)
        else:
            if item not in dups:
                dups.append(item)
            i = 2
            while (item + "_" + str(i)) in out:
                i += 1
            out.append(item + "_" + str(i))
    return (out,dups)
    
def prettylist2(a):
    if len(a) == 1:
        return "%s is used more than once as a header name. Duplicates of this header name have been changed." % a[0]
    elif len(a) == 2:
        return "%s and %s are used more than once as a header name. Duplicates of these header names have been changed." % (a[0],a[1])
    else:
        return ", ".join(a[:-1]) + ", and %s are used more than once as a header name. Duplicates of these header names have been changed." % a[-1]
        
def remove_nans(row,dtype):
    out = []
    for i in range(len(row)):
        if dtype[i].startswith("S"):
            out.append(row[i])
        else:
            try:
                out.append(float(row[i]))
            except:
                out.append(np.nan)
    return tuple(out)

def get_tables(data):
	tables={}
	for i in range(data.nsheets):
		sheet = data.sheet_by_index(i)
		cells = [sheet.row_values(j) for j in range(min(sheet.nrows,10))]
		tables[data.sheet_names()[i]] = cells
	return {"multipletables":tables}


def is_comment(row):
	for i in range(len(row)):
		if row[i] == "#":
			return True
		elif row[i] != "":
			return False
	return False

def combine_headers(names, extra):
	for i in range(len(extra)):
		for j in range(len(extra[i])):
			names[j] = names[j] + "_" + str(extra[i][j])
			names[j] = names[j][:200]
	return names


#An excel specific Inspector class
class XL_Inspector():
	EMPTY=0
	TEXT=1
	NUMBER=2
	DATE=3
	BOOLEAN=4
	ERROR=5
	BLANK=6
	INTEGERS=7
	PERM_STRING=8
	t = TEXT
	min=None
	max=None
	maxlen=0
	unknown_str=None
	line = 0
	firstnum=None
	_lastval=None
	blank = False
	def xlread(self, s, type, col, names):
		self.line+=1
		if self.t == self.PERM_STRING:
			if len(s) > self.maxlen:
				self.maxlen = len(s)
		else:
			self.t = type
			if self.t == self.TEXT:
				#tries to convert the text to a number to avoid cases where excel represents
				#a number as a string
				try:
					float(s)
					self.t = self.NUMBER
				except:
					if self._lastval and not self.unknown_str:
						self.t = self.EMPTY
					else:
						self.t = self.TEXT
				if self.t == self.TEXT:
					if s.lower() == 'nan':
						pass
					else:
						if not self.unknown_str:
							self.unknown_str = s
							self.t = self.PERM_STRING
						if len(s) > self.maxlen:
							self.maxlen = len(s)
			if self.t == self.NUMBER:
				if not self.firstnum:
					self.firstnum = self.line
				self._lastval = float(s)
				if not self.blank and int(self._lastval) == self._lastval:
						self.t = self.INTEGERS
				else:
					self.t = self.NUMBER
				if self.min != None:
					if self._lastval < self.min:
						self.min = self._lastval
					elif self._lastval > self.max:
						self.max = self._lastval
				else:
					self.min = self.max = self._lastval
			elif self.t == self.DATE and "(D)" not in names[col]:
				names[col] = names[col] + " (D)"
			elif self.t == self.EMPTY:
				self.blank = True
				if self._lastval:
					self.t = self.NUMBER
				elif self.unknown_str:
					self.t = self.TEXT

	def xlgettype(self):
		if self.t == self.INTEGERS:
			for j in ["uint8","int8","uint16","int16","uint32","int32","uint64","int64"]:
				if np.iinfo(j).min <= self.min and self.max <= np.iinfo(j).max:
					return j
		elif self.t == self.NUMBER or self.t == self.DATE:
			if np.finfo("float32").min <= self.min and self.max <= np.finfo("float32").max and self.maxlen <=np.finfo("float32").precision:
				return "float32"
			else:
				return "float64"
		else:
			return ("S" + str(max(1,self.maxlen)))

def xl_remove_nans(row,dtype,datemode,headername):
    out = []
    for i in range(len(row)):
        if dtype[i].startswith("S"):
        	try:
        		out.append(row[i].encode('utf-8'))
        	except:
        		out.append(row[i])
        elif len(headername[i]) > 3 and isinstance(row[i], float) and headername[i][len(headername[i])-3:] == "(D)":
        	if datemode == 0:
        		out.append(float((row[i] - mod0) * seconds))
        	else:
        		out.append(float((row[i] - mod1) * seconds))
        else:
        	try:
        		out.append(float(row[i]))
        	except:
        		out.append(np.nan)
    return tuple(out)

#ref:http://stackoverflow.com/questions/1446549/how-to-identify-binary-and-text-files-using-python
def istext(filename):
    s=open(filename).read(512)
    text_characters = "".join(map(chr, range(32, 127)) + list("\n\r\t\b"))
    _null_trans = string.maketrans("", "")
    if not s:
        # Empty files are considered text
        return True
    if "\0" in s:
        # Files with null bytes are likely binary
        return False
    # Get the non-text characters (maps a character to itself then
    # use the 'remove' option to get rid of the text characters.)
    t = s.translate(_null_trans, text_characters)
    # If more than 30% non-text characters, then
    # this is considered a binary file
    if float(len(t))/float(len(s)) > 0.30:
        return False
    return True
    
def ishtml(filename):
    s=open(filename).read(512).lower()
    if "<!doctype" in s:
        return True
    if "<html" in s:
        return True
    return False
                
if __name__ == "__main__":
    get_args()
