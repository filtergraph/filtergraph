import os

@auth.requires_membership('qc')
def cleanurl_check():
    datasets = db(db.datasets.id > 0).select()
    pairs = {}
    duplicates = []
    for d in datasets:
        pair = (d.portal,cleanurl(d.name))
        if pair in pairs:
            duplicates.append((d.portal.url,d.name))
        else:
            pairs[pair] = 1
    return repr(duplicates)
    
@auth.requires_membership('qc')
def index():
    admins = db(db.admins.id>0).select()
    unique_admins = {}
    for a in admins:
        unique_admins[a.zadmin] = True
    notebook = db(db.notebook.id>0).select()
    for n in notebook:
        unique_admins[n.zuser] = True
    comments = db(db.comments.id>0).select()
    for c in comments:
        unique_admins[c.zuser] = True
    spam_users = db(db.auth_user.email.contains("performicsde")).select()
    for s in spam_users:
        if s.id in unique_admins:
            del unique_admins[s.id]
    if None in unique_admins:
        del unique_admins[None]
        
    datasets = db(db.datasets.id>0).select()
    filesizes = {}
    for d in datasets:
        filepath = os.path.join(FG_CACHE_FOLDER,d.zfile+".npy")
        if os.path.exists(filepath):
            filesize = os.path.getsize(filepath)
            filesizes[d.id] = filesize
        else:
            filesizes[d.id] = 0
    portalsizes = {}
    portals = db(db.portals.id>0).select()
    for d in datasets:
        if d.portal in portalsizes:
            portalsizes[d.portal] += filesizes.get(d.id) or 0
        else:
            portalsizes[d.portal] = filesizes.get(d.id) or 0
    usersizes = {}
    for a in admins:
        if a.zadmin in usersizes:
            usersizes[a.zadmin] += portalsizes.get(a.portal) or 0
        else:
            usersizes[a.zadmin] = portalsizes.get(a.portal) or 0
            
    names = []
    for ua in unique_admins:
        names.append(db.auth_user(ua).first_name + " " + db.auth_user(ua).last_name + ": " + "%.2f" % ((usersizes.get(ua) or 0)/1048576.0))
        
    s = os.statvfs(FG_DATA_FOLDER)
    used_pct = (float(s.f_blocks - s.f_bavail)/s.f_blocks) * 100
    if used_pct > FG_JANITOR_FULL_DISK:
        days_to_delete = FG_JANITOR_CLEAR_WHEN_FULL
    else:
        days_to_delete = FG_JANITOR_CLEAR_FILES
        
    return dict(users = len(unique_admins),used_pct=used_pct,days_to_delete=days_to_delete,names=names)