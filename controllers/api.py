# coding: utf8
# try something like
import numpy as np
import json
import os

def index(): 
    return dict(message="hello from api.py")

def apierror(code,msg):
    if request.vars.reason:
        return json.dumps({'message':msg})
    else:
        raise HTTP(code)

def portal():
    #Determine if authorized
    if request.env.http_authorization:
        api_key = request.env.http_authorization.lower()
    if request.vars.api_key:
        api_key = request.vars.api_key.lower()
    if not api_key:
        authorized = False
    else:
        if api_key.startswith("basic"):
            api_key = api_key[5:]
        api_key = api_key.strip()
        user = db(db.auth_user.api_key==api_key).select().first()
        if not user:
            authorized = False
        else:
            authorized = True
    
    #Determine method
    method = request.env.request_method.upper()
    if request.vars.method:
        method = request.vars.method.upper()
    
    #GET: no authorization needed
    if method == "GET":
        return apierror(400,"GET reserved for future use")
    
    #POST: authorization needed
    if method == "POST":
        if not authorized:
            return apierror(401,"Valid API key needed")
        if not request.vars.path:
            return apierror(400,"Path required")
        path = request.vars.path
        input_url = request.vars.url or (''.join(random.choice("abcdefghijklmnopqrstuvwxyz") for x in range(6)))
        input_name = request.vars.name or input_url
        existingportals = db(db.portals.url == input_url).count()
        if input_url in RESERVED_LIST:
            return apierror(400,"%s is a reserved name" % input_url)
        elif input_url[0] in "0123456789":
            return apierror(400,"%s starts with a digit" % input_url)
        elif existingportals > 0:
            return apierror(400,"%s is taken" % input_url)
        
        #Download file if needed
        if path.startswith("/") and FG_API_PORTAL_FROM_DRIVE:
            downloaded = False
        else:
            downloaded = True
            urlfile = urllib2.urlopen(path)
            if urlfile.code == 200:
                if long(urlfile.info().getheaders('Content-Length')[0]) <= (user.quota*1024768L):
                    urlname = "URL-" + ''.join(random.choice("ABCDEFGHJKLMNPQRSTUVWXYZ23456789") for x in range(40))
                    path = os.path.join(FG_UPLOAD_FOLDER,urlname)
                    with open(path,"w") as out:
                        out.write(urlfile.read())
                else:
                     return apierror(400,"File too big")
            else:
                 return apierror(400,"URL not found")
        
        #Build portal
        if "." in path:
            ftype = request.vars.path.split(".")[-1]
        else:
            ftype = ""
        if not os.path.exists(path):
            return apierror(400,"Bad path")
        result = loaddata2(loadfile=path,ftype=ftype)
        if "data" in result:
            pid = db.portals.insert(name=input_name,url=input_url,private=request.vars.private)
            db.admins.insert(portal=pid,zadmin=user)
            apiname = "API-" + ''.join(random.choice("ABCDEFGHJKLMNPQRSTUVWXYZ23456789") for x in range(40))
            did = db.datasets.insert(name=dataset_namer(path.split("/")[-1]),
                 ftype=path.split(".")[-1],
                 portal=pid,
                 zfile=apiname)
            cache_fname = os.path.join(FG_CACHE_FOLDER,apiname)+".npy"
            np.save(cache_fname, result['data'])
            if downloaded:
                done_fname = os.path.join(FG_DONE_FOLDER,apiname)
                shutil.move(path,done_fname)
        else:
            return apierror(400,"Error loading file")
        
        return json.dumps({'url':'http://%s/%s' % (request.env.http_host,input_url)})

    #PUT: authorization needed
    if method == "PUT":
        return apierror(400,"PUT reserved for future use")
        
    #DELETE: authorization needed
    if method == "DELETE":
        return apierror(400,"DELETE reserved for future use")
    
def data():
    #Get dataset
    pid = db(db.portals.url==request.vars.p).select().first()
    if not pid:
        return apierror(400,"Portal does not exist")
    if request.vars.d:
        did = db(db.datasets.portal==pid)(db.datasets.name == request.vars.d).select().first()
    else:
        did = db(db.datasets.portal==pid).select().first()
    if not did:
        return apierror(400,"Dataset does not exist")
    
    #Determine if authorized
    if request.env.http_authorization:
        api_key = request.env.http_authorization.lower()
    if request.vars.api_key:
        api_key = request.vars.api_key.lower()
    if not api_key:
        authorized = False
    else:
        if api_key.startswith("basic"):
            api_key = api_key[5:]
        api_key = api_key.strip()
        admins = db(db.admins.portal==pid).select()
        for a in admins:
            if a.zadmin.api_key == request.vars.api_key:
                authorized = True
                break
        else:
            authorized = False
        
    #Determine method
    method = request.env.request_method.upper()
    if request.vars.method:
        method = request.vars.method.upper()

    #Load file
    cache = os.path.join(FG_CACHE_FOLDER, did.zfile + ".npy")
    a = np.load(cache)
    
    #GET: no authorization needed
    if method == "GET":
        out = {}
        b = []
        for i in a.dtype.names:
            b.append({"name":i,"type":a.dtype[i].kind,"size":a.dtype[i].itemsize})
        out["header"] = b
        if request.vars.getall:
            out["data"] = a.tolist()
        return json.dumps(out)
    
    #POST: authorization needed
    if method == "POST":
        if not authorized:
            return apierror(401,"Valid API key needed")
        b = json.loads(request.vars.json)
        if isinstance(b[0],list):
            for i in range(len(b)):
                b[i] = tuple(b[i])
            np.save(cache,np.array(a.tolist()+b,dtype=a.dtype))
            return json.dumps(dict(index=range(len(a),len(a)+len(b))))
        else:
            np.save(cache,np.array(a.tolist() + [tuple(b)],dtype=a.dtype))
            return json.dumps(dict(index=len(a)))
        
    #PUT: authorization needed
    if method == "PUT":
        if not authorized:
            return apierror(401,"Valid API key needed")
        d = a.dtype
        a = a.tolist()
        b = json.loads(request.vars.json)
        c = json.loads(request.vars.index)
        if isinstance(c,list):
            assert(len(b)==len(c))
            for i in range(len(b)):
                a[c[i]] = tuple(b[i])
            np.save(cache,np.array(a,dtype=d))
            return json.dumps(dict(index=c))
        else:
            a[c] = tuple(b)
            np.save(cache,np.array(a,dtype=d))
            return json.dumps(dict(index=c))
        
    #DELETE: authorization needed
    if method == "DELETE":
        if not authorized:
            return apierror(401,"Valid API key needed")
        b = json.loads(request.vars.index)
        if isinstance(b,list):
            c = np.zeros(len(a))
            for bb in b:
                c[bb] = 1
            np.save(cache,a[c==0])
            return json.dumps(dict(index=b))
        else:
            c = np.zeros(len(a))
            c[b] = 1
            np.save(cache,a[c==0])
            return json.dumps(dict(index=b))
