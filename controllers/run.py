# -*- coding: utf-8 -*-
from datetime import *
import os, stat, random, re, json, time, copy, glob, pdb, shutil
import subprocess32 as subprocess
from operator import itemgetter #file cleanup
from gluon.tools import prettydate #last modified display
import numpy as np
#try:
#    import scipy.stats as st
#except:
#    print "ERROR: Couldn't load scipy! Unable to do heatmaps or surface maps."
plt = False
#try:
#    import matplotlib
#    matplotlib.use("AGG")
#    import matplotlib.pyplot as plt
#except:
#    plt = False
#    print "ERROR: Couldn't load matplotlib! Unable to make thumbnails of tables."
from multiprocessing import Process, Queue, cpu_count
import numpy.lib.recfunctions as rf

def plot(command, queue):
    # Sends the command to gnuplot using a queue object.
    process = subprocess.Popen(FG_GNUPLOT_PATH,stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    print "BEGIN %5s     Gnuplot process" % process.pid
    try:
        result = process.communicate(command, timeout=FG_SERVER_TIMEOUT)
    except subprocess.TimeoutExpired:
        process.kill()
        result = process.communicate()
    print "END   %5s %3s Gnuplot process" % (process.pid,process.returncode)
    queue.put(process.returncode)
    queue.put(result)

def gptime(points,point_type=1,color=True,size=False):
    # Estimates time to render a Gnuplot image
    p = points
    s = size and 1 or 0
    c = color and 1 or 0
    pt = point_type
    model = FG_GNUPLOT_MODEL[pt]
    return (model[0]*p)+(model[1]*c)+(model[2]*s)

def mergetime(pixels,cores):
    # Estimates time to merge Gnuplot images together
    if cores == 1:
        return 0
    p = pixels
    c = cores
    return (FG_IMAGEMAGICK_MODEL[0]*c)+(FG_IMAGEMAGICK_MODEL[1]*p)

# coding: utf8
def index():
    secure_page()
    times = []
    times.append(("Setting up",datetime.now()))

    stackedhist = request.vars.stackedhist

    # block empty HTTP user agents
    # ref: http://johannburkard.de/blog/www/spam/The-top-10-spam-bot-user-agents-you-MUST-block-NOW.html
    if not request.env.http_user_agent:
        return "USER AGENT BLOCKED"

    # Fetch portal, dataset and savestate
    if len(request.args)>0:
        request.vars.p = request.args(0)
    if len(request.args)>1:
        request.vars.d = request.args(1)
    if len(request.args)>2:
        request.vars.s = request.args(2)

    # portal required
    if not request.vars.p:
        return errormessage("No portal specified")

    # Get the savestring from a long descriptive name
    savestate_str = None
    if request.vars.d:
        if len(request.vars.d.split('-')) > 0:
            if request.vars.d.split('-')[-1].isdigit():
                if len(request.vars.d.split('-')[-1]) >= 7:
                    savestate_str = request.vars.d.split('-')[-1]
                    request.vars.d = None
    if request.vars.s:
        if len(request.vars.s.split('-')) > 0:
            if request.vars.s.split('-')[-1].isdigit():
                if len(request.vars.s.split('-')[-1]) >= 7:
                    savestate_str = request.vars.s.split('-')[-1]
    if request.vars.p.isdigit() and request.vars.p != "777":
        savestate_str = request.vars.p.split('-')[0]
        
    # Handle save strings
    if savestate_str:
         if os.path.exists(os.path.join(FG_REVISIONS_FOLDER,savestate_str)):
             savestate_fname = os.path.join(FG_REVISIONS_FOLDER,savestate_str)
         elif os.path.exists(os.path.join(FG_SAVESHARE_FOLDER,savestate_str)):
             savestate_fname = os.path.join(FG_SAVESHARE_FOLDER,savestate_str)
         else:
             return errormessage("Invalid save state",details="Check the URL and try again.")
         with open(savestate_fname) as f:
             savestate = json.loads(f.read())
         savestate_id = True
         portal_name = savestate['portal_name']
         if portal_name == "nea":
             redirect("/nea/default/index/"+savestate_str)
         portal_id = savestate['portal_id']
         dataset_id = savestate['dataset_id']
         dataset_name = savestate['dataset_name']
         portal = db(db.portals.id==portal_id).select().first()
         if not portal:
             return errormessage("Portal \"%s\" no longer exists" % portal_name,details="This portal has been deleted by its administrator.")
    else:
         savestate_id = None
         portal_name = request.vars.p
         dataset_id = None
         dataset_name = request.vars.d
         portal = db(db.portals.url==portal_name).select().first()
         if not portal:
             return errormessage("Portal \"%s\" does not exist" % portal_name,details="Check the URL of the portal and try again. Perhaps you would like to <a href='/filtergraph/default/create'>set up a portal here</a> instead?")

    # Is portal closed?
    if portal.zactive == False:
        return errormessage('Portal \"%s\" is closed for maintenance' % portal_name,details="Please return in a few minutes.")
    response.title = portal.name
    # Is portal private? Does user have temporary access?
    admins = db(db.admins.portal==portal.id)(db.admins.zadmin==auth.user_id).select().first()
    link_url = ""
    if portal.private:
        if not auth.user_id:
            temp_access = db(db.temp_access.portal==portal.id)(db.temp_access.session_id == FG_BROWSER_ID).select()
            if len(temp_access) == 0:
                return response.render("default/private.html",{"portal":portal})
            if temp_access.first().link:
                link_url = temp_access.first().link.url
            else:
                return response.render("default/private.html",{"portal":portal})
        elif not admins:
            temp_access = db(db.temp_access.portal==portal.id)(db.temp_access.session_id == FG_BROWSER_ID).select()
            if len(temp_access) > 0:
                if temp_access.first().link:
                    redirect("/access/"+temp_access.first().link.url)
            if not auth.has_membership('qc'):
                return response.render("default/private.html",{"portal":portal})

    # Retrieve dataset info
    datasets = db(db.datasets.portal==portal.id).select()
    if dataset_name:
        dataset = datasets.find(lambda row: row.name==dataset_name).first()
        if not dataset:
            dataset = datasets.find(lambda row: cleanurl(row.name)==dataset_name).first()
            if not dataset:
                dataset = datasets.find(lambda row: row.id==dataset_id).first()
    else:
        dataset = datasets.find(lambda row: row.is_default==True).first()
        if not dataset:
            dataset = datasets.first()
    # Has dataset been deleted?
    if not dataset:
        if savestate_id:
            dataset = datasets.find(lambda row: row.id==savestate['dataset_id']).first()
            if not dataset:
                return errormessage("Data contained within this link has been deleted",details="The data of this save-and-share link or notebook entry has been deleted by its administrator.")
        else:
            return errormessage("No dataset has been added to the portal, or data has been deleted",details="If you set up this page, click <a href='/filtergraph/default/upload/%s'>here</a> to add a dataset." % portal.id)
    # Get numpy file
    fname = os.path.join(FG_CACHE_FOLDER,dataset.zfile+".npy")
    # Has numpy file been overridden?
    if os.path.exists(os.path.join(FG_DATA_FOLDER,"%s.npy" % portal.url)):
        fname = os.path.join(FG_DATA_FOLDER,"%s.npy" % portal.url)
    if os.path.exists(os.path.join(FG_DATA_FOLDER,"%s.npy" % dataset.id)):
        fname = os.path.join(FG_DATA_FOLDER,"%s.npy" % dataset.id)

    #Load from cache
    #http://stackoverflow.com/questions/595305/python-path-of-script
    cachename = "%s-%s-%s-%s" % (dataset.id,long(os.stat(fname).st_mtime*100),long(os.stat(os.path.realpath(__file__)).st_mtime*100),abs(hash(repr(request.vars))))
    if os.path.exists(os.path.join(FG_CACHESTAT_FOLDER,"%s.txt" % cachename)) and FG_ENABLE_CACHE:
        with open(os.path.join(FG_CACHESTAT_FOLDER,"%s.txt" % cachename)) as f:
            output = json.loads(f.read())
        return output
    if os.path.exists(os.path.join(FG_TABLE_FOLDER,"%s-table.txt" % cachename)) and FG_ENABLE_CACHE and not request.vars.set_nb_settings:
        return response.stream(os.path.join(FG_TABLE_FOLDER,cachename + "-table.txt"),chunk_size=4096)

    # Parse names, descriptions, and settings
    longnames = parse_list(dataset.header_names)
    descriptions = parse_list(dataset.description_names)
    if savestate_id:
        settings = savestate['settings']
    elif dataset.settings.startswith('{"'):
        settings = json.loads(dataset.settings)
    else:
        settings = {}

    # Open file
    times.append(("Opening file",datetime.now()))
    if os.path.exists(fname):
        a = np.load(fname,mmap_mode='r')
    else:
        return errormessage("Could not load file")
    raw_col_names = list(a.dtype.names)

    oldrules = False
    if savestate_id or dataset.settings.startswith('{"'):
        for key in settings:
            if is_dynamic_field(key):
                for name in (longnames.keys()+raw_col_names):
                    settings[key] = settings[key].encode('utf-8').replace("%%"+name.encode('utf-8')+"%%",longnames.get(name) or name)
                if settings[key].count("::") > 0:
                    settings[key] = settings[key][:settings[key].index("::")]
                if settings[key].count("%%") > 0:
                    settings[key] = "*Column no longer exists*"
                if key.startswith('var_') and len(settings[key])>0 and 'opt_'+key[4:] not in settings and savestate_id:
                    #In the case of save and shares prior to 2/7/2014 treat the filter as "between" for numeric types and "starts with" for string types.
                    oldrules = True
            else:
                #may contain %% due to earlier bug
                settings[key] = settings[key].encode('utf-8').replace("%%qf_i%%","i").replace("%%","")
    else:
        settings = parse_list(dataset.settings)

    #times.append(("Parse headers",datetime.now()))
    col_names = []
    html_names = parse_list(dataset.html_names)
    for h in html_names:
        html_names[h] = re.sub(r"\(([^\)]*)\)",r"%(\1)s",html_names[h])
    for name in copy.deepcopy(raw_col_names):
        if name in longnames:
            """if longnames[name].endswith("::HIDE"):
                raw_col_names.remove(name)
            elif longnames[name].endswith("::HTML"):
                html_names[longnames[name].split("::")[0]] = longnames[name].split("::")[1]
                col_names.append(longnames[name].split("::")[0])
                longnames[name] = longnames[name].split("::")[0]
            else:"""
            col_names.append(longnames[name])
            if name in html_names:
                html_names[longnames[name]] = html_names[name]
        else:
            col_names.append(name)
    #return repr(html_names)

    #times.append(("Get numeric data",datetime.now()))
    num_col_names = []
    for n in raw_col_names:
        if a.dtype[n].kind != 'S':
            num_col_names.append(n)
    for i in range(len(num_col_names)):
        if num_col_names[i] in longnames:
            descriptions[longnames[num_col_names[i]]] = descriptions.get(num_col_names[i])
            num_col_names[i] = longnames[num_col_names[i]]

    #times.append(("Stop here if not requesting a query",datetime.now()))
    if not request.vars.go and not request.vars.point_info:
        #response.files.append('//use.typekit.net/ioz3vhm.js')
        response.files.append('/'+request.application+'/static/_1.0.0/runcss/base.css')
        response.files.append('/'+request.application+'/static/_1.0.0/runjs/jquery-ui-1.9.2.custom.min.js')
        response.files.append('/'+request.application+'/static/_1.0.4/jqui/css/custom-theme/jquery-ui-1.9.2.custom.min.css')
        response.files.append('/'+request.application+'/static/_1.0.0/telegraph/bower_components/bootstrap/dist/js/bootstrap.min.js')
        response.files.append('/'+request.application+'/static/_1.0.0/js/msdropdown/jquery.dd.min.js')
        response.files.append('/'+request.application+'/static/_1.0.0/css/msdropdown/dd.css')
        response.files.append('/'+request.application+'/static/_1.0.1/css/colorPicker.css')
        response.files.append('/'+request.application+'/static/_1.0.1/js/jquery.colorPicker.min.js')
        response.files.append('/'+request.application+'/static/_1.0.0/runjs/jquery.fixedheadertable.min.js')
        response.files.append('/'+request.application+'/static/_1.0.0/runcss/defaultTheme.css')
        response.files.append('/'+request.application+'/static/_1.0.0/js/masonry.pkgd.min.js')
        response.files.append('/'+request.application+'/static/_1.0.0/telegraph/app/styles/app.min.css')
        response.files.append('/'+request.application+'/static/_1.0.0/telegraph/app/scripts/min/main.min.js')
        response.files.append('/'+request.application+'/static/_1.0.0/js/fabric.min.js')
        response.files.append('/'+request.application+'/static/_1.0.0/runjs/jquery.align-column.js')
        #response.files.append('https://getfirebug.com/firebug-lite.js')
        cache.ram(request.env.path_info,None)
        if auth.user_id:
            db(db.notebook.session_id == FG_BROWSER_ID)(db.notebook.zuser==None).update(zuser=auth.user_id,session_id=None)
            notebook = db(db.notebook.portal == portal)(db.notebook.zuser == auth.user_id).select()
        else:
            notebook = db(db.notebook.portal == portal)(db.notebook.zuser==None)(db.notebook.session_id == FG_BROWSER_ID).select()
        own_notebook = None
        for n in notebook:
            if request.args[0] == n.url:
                own_notebook = n
                pass
        notebook = notebook | db(db.notebook.portal == portal)(db.notebook.zpublic == True).select()
        notebook = notebook.sort(lambda row: row.ztime)
        comments = []
        for n in notebook:
            comments += [db(db.comments.nbentry == n.id).select()]
        text = "loads dataset %s" % dataset.id
        try:
            db.auth_event.insert(user_id=auth.user_id,description="User %s %s" % (auth.user_id,text))
        except:pass
        if dataset.lur_task:
            sched_db(sched_db.scheduler_task.id==dataset.lur_task).update(stop_time=request.now + timedelta(days=90))
        print "%s: User %s %s" % (request.now,auth.user_id,text)
        return dict(col_names = col_names,num_col_names=num_col_names,portal=portal,rows=len(a),dataset=dataset,settings=settings,descriptions=descriptions,datasets=datasets,fname=fname,oldrules=oldrules,admins=admins,notebook=notebook,comments=comments,link_url=link_url,own_notebook=own_notebook,last_update=prettydate(datetime.fromtimestamp(os.stat(fname).st_mtime),T))

    #Save the state
    #http://stackoverflow.com/questions/4659524/how-to-sort-by-length-of-string-followed-by-alphabetical-order
    col_names_sort = longnames.items() + [(i,i) for i in raw_col_names]
    col_names_sort.sort(key=lambda aa:len(aa[1]),reverse=True)
    lowbound = 1000000L
    security_code = str(random.randint(lowbound,lowbound*10-1))
    while os.path.exists(os.path.join(FG_SAVESHARE_FOLDER,security_code)):
        lowbound *= 10
        security_code = str(random.randint(lowbound,lowbound*10-1))
    savestate = dict(request.vars)
    for key in savestate:
        if is_dynamic_field(key):
            for i in range(len(col_names_sort)):
                savestate[key] = savestate[key].replace(col_names_sort[i][1],"%%**["+str(i)+"]**%%")
            for i in range(len(col_names_sort)):
                savestate[key] = savestate[key].replace("%%**["+str(i)+"]**%%","%%"+col_names_sort[i][0]+"%%")
    if request.vars.setinitsettings:
        #ADMIN ONLY
        if not auth.user_id:
            return "<script>alert('Please log in to set initial settings for this portal.');</script>"
        if not admins:
            return "<script>alert('You are not an admin of this portal. Only admins can set initial settings for a portal.');</script>"
        settings_chg = db(db.datasets.id==dataset.id).update(settings=json.dumps(savestate))
        return "<script>alert('Your settings will now appear by default when the portal is loaded.');</script>"
    if request.vars.set_nb_settings:
        #NOTEBOOK OWNER ONLY
        if auth.user_id:
            #print request.vars.set_nb_settings
            notebook_test = db(db.notebook.url == request.vars.set_nb_settings)(db.notebook.zuser == auth.user_id).select()
        else:
            notebook_test = db(db.notebook.url == request.vars.set_nb_settings)(db.notebook.zuser==None)(db.notebook.session_id == FG_BROWSER_ID).select()
        if not notebook_test:
            return "<script>alert('Only the creator of a notebook entry can edit it.');</script>"

    #times.append(("Preparing query",datetime.now()))
    #queryid = str(abs(hash(repr(request.vars))))
    queryid = cachename + "-%s" % abs(hash(random.random()))
    #bin = "/home/burgerdm/cache/"+queryid+".bin"
    graph_dir = FG_IMAGE_FOLDER + "/"
    graph_name = graph_dir + queryid + "-%s.%s"
    #if os.path.exists(graph_name % ("comp","png")):
    #    return dict(queryid=queryid,type="png",status="Cached<br/><br/>",zaxis=request.vars.zaxis)
    totalcount = len(a)
    
    #Layers
    if request.vars.layer_init:
        layer_setup = request.vars.layer_setup
        add_notebook = request.vars.add_notebook
        set_nb_settings = request.vars.set_nb_settings
        download_image = request.vars.download
        
        if layer_setup == "gif":
            external_type = "gif"
        elif request.vars.download and request.vars.filetype in ['png','jpg','gif']:
            external_type = request.vars.filetype
        else:
            external_type = "png"
        
        #Get file paths
        layer_files = []
        for i in range(len(request.vars)):
            if request.vars["layer_%s" % i] and request.vars["layeren_%s" % i]:
                if request.vars['layer_%s' % i] != "deleted":
                    layer_files.append(request.vars["layer_%s" % i].split('//')[-1].split('/')[1])
        if len(layer_files) == 0:
            #return repr(request.vars.layer_init) + GRAPH_HIDE
            return errormessage("No layers to display",details="Please enable at least one layer and try again.")
        layer_files.append(request.vars["layer_init"].split('//')[-1].split('/')[1])
        #return repr(layer_files) + GRAPH_HIDE
        
        #Get data
        layer_data = []
        for layer_file in layer_files:
            if os.path.exists(os.path.join(FG_REVISIONS_FOLDER,layer_file)):
                layer_file_path = os.path.join(FG_REVISIONS_FOLDER,layer_file)
            else:
                layer_file_path = os.path.join(FG_SAVESHARE_FOLDER,layer_file)
            with open(layer_file_path) as f:
                layer_data.append(json.loads(f.read())['settings'])
        
        #remove %%
        for layer in layer_data:
            for key in layer:
                if is_dynamic_field(key):
                    for i in range(len(col_names_sort)):
                        layer[key] = layer[key].replace("%%"+col_names_sort[i][0]+"%%","%%**["+str(i)+"]**%%")
                    for i in range(len(col_names_sort)):
                        layer[key] = layer[key].replace("%%**["+str(i)+"]**%%",col_names_sort[i][1])
        #return repr(layer_data) + GRAPH_HIDE
        
        #Find differences between each layer
        layer_diff = {}
        for key in layer_data[-1].keys():
            if request.vars[key] != layer_data[-1][key]:
                if not key.startswith("layer_"):
                    layer_diff[key] = request.vars[key]
            if key not in request.vars.keys():
                layer_diff[key] = ""
        for key in request.vars.keys():
            if key not in layer_data[-1].keys():
                if not key.startswith("layer_"):
                    layer_diff[key] = request.vars[key]
        for i in range(len(layer_data)):
            for key in layer_diff.keys():
                layer_data[i][key] = layer_diff[key]
            if i>0 and layer_setup == "overlap":
                layer_data[i]['transparent'] = 'true'
        #return repr(layer_data[-1].get('en_1')) + GRAPH_HIDE
        #now pop the last layer
        layer_data.pop()
        
        #Get stats for each layer
        if layer_setup == "side":
            #skip ahead to rendering stage
            layer_bounds = {}
        else:
            layer_stats = []
            for layer in layer_data:
                if layer['otype'] == "table":
                    return errormessage("No tables allowed in layer view",details="Remove the layers that have tables and try again.")
                for key in request.vars.keys():
                    if not key.startswith("picture_"):
                        request.vars.pop(key)
                for key in layer.keys():
                    if not key.startswith("picture_"):
                        request.vars[key] = str(layer[key]).replace("%%","").encode('utf-8')
                request.vars.layerstats = "true"
                request.vars.layerrender = ""
                request.vars.add_notebook = ""
                request.vars.set_nb_settings = ""
                request.vars.download = ""
                layer_stats.append(index())
                if (datetime.now() - times[0][1]).seconds >= FG_SERVER_TIMEOUT:
                    return errormessage("Server timeout",details="This can result from excessive settings or handling too much data. Check your settings and try again.")
            #return repr(layer_stats) + GRAPH_HIDE
            layer_stats = [layer for layer in layer_stats if type(layer) == dict]
            #Determine bounds
            if len(layer_stats) == 0:
                return errormessage("No data to display",details="None of the layers could be rendered.")
            layer_bounds = layer_stats[0]
            for layer in layer_stats:
                for bound in layer.keys():
                    if bound.startswith("min_") and layer[bound] < layer_bounds[bound]:
                        layer_bounds[bound] = layer[bound]
                    if bound.startswith("max_") and layer[bound] > layer_bounds[bound]:
                        layer_bounds[bound] = layer[bound]
            #return repr(layer_bounds) + GRAPH_HIDE
        
        #Render each layer
        layer_output = []
        layer_valid_images = []
        for layer in layer_data:
            for key in request.vars.keys():
                if not key.startswith("picture_"):
                    request.vars.pop(key)
            for key in layer.keys():
                if not key.startswith("picture_"):
                    request.vars[key] = str(layer[key]).replace("%%","").encode('utf-8')
            for key in layer_diff.keys():
                request.vars[key] = str(layer_diff[key]).replace("%%","").encode('utf-8')
            for key in layer_bounds.keys():
                request.vars[key] = str(layer_bounds[key]).encode('utf-8')
            request.vars.layerstats = ""
            request.vars.layerrender = "true"
            request.vars.add_notebook = ""
            request.vars.set_nb_settings = ""
            request.vars.download = ""
            if len(layer_valid_images) > 0 and layer_setup != "side":
                request.vars.map_2 = request.vars.map_1 or ""
                request.vars.map_1 = ""
            #return repr(request.vars.items()) + GRAPH_HIDE
            layer_output.append(index())
            if (datetime.now() - times[0][1]).seconds >= FG_SERVER_TIMEOUT:
                return errormessage("Server timeout",details="This can result from excessive settings or handling too much data. Check your settings and try again.")
            if type(layer_output[-1]) == dict:
                layer_image_path = os.path.join(FG_IMAGE_FOLDER,layer_output[-1]['queryid']+"-comp."+layer_output[-1]['type'])
                if os.path.exists(layer_image_path):
                    layer_valid_images.append(layer_image_path)
            else:
                layer_output.pop()
        if len(layer_valid_images) == 0:
            return errormessage("No layers to display")
        
        #Merge the layers together
        if layer_setup == "overlap":
            image = os.path.join(FG_IMAGE_FOLDER,queryid + "-comp."+external_type)
            process = subprocess.Popen("convert " + " ".join(layer_valid_images) + " -flatten " +image,shell=True,stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            result = process.communicate()
        elif layer_setup == "side":
            image = os.path.join(FG_IMAGE_FOLDER,queryid + "-comp."+external_type)
            process = subprocess.Popen("montage " + " ".join(layer_valid_images) + " -geometry +0+0 " +image,shell=True,stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            result = process.communicate()
            process = subprocess.Popen("convert " + image + " -resize "+str(intable(request.vars.picture_width,640))+"x"+str(intable(request.vars.picture_height,480))+"\! " +image,shell=True,stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            result = process.communicate()
        elif layer_setup == "gif":
            image = os.path.join(FG_IMAGE_FOLDER,queryid + "-comp."+external_type)
            process = subprocess.Popen("convert " + " ".join(layer_valid_images) + " -loop 0 -delay 200 " +image,shell=True,stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            result = process.communicate()
        else:
            image = os.path.join(FG_IMAGE_FOLDER,queryid + "-comp."+external_type)
            process = subprocess.Popen("convert " + " ".join(layer_valid_images) + " -average " +image,shell=True,stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            result = process.communicate()
            
        if download_image:
            #https://en.wikipedia.org/wiki/Mime_type
            #https://en.wikipedia.org/wiki/HTTP_header
            #http://web2py.com/examples/default/examples
            #http://stackoverflow.com/questions/6591931/getting-file-size-in-python
            if external_type=="jpg":
                mimetype = "image/jpeg"
            else:
                mimetype = "image/"+external_type
            response.headers['Content-Type']=mimetype
            response.headers['Content-Length']=os.path.getsize(image)
            response.headers['Content-Disposition']='attachment; filename="'+layer_output[-1]['stats']['gtitle']+'.'+external_type+'"'
            return response.stream(open(image,'rb'),chunk_size=4096)
        
        #Generate thumbs
        thumb_paths = [os.path.join(FG_PORTAL_THUMBS,str(portal.id)+".jpg"),os.path.join(FG_DATASET_THUMBS,str(dataset.id)+".jpg")]
        if add_notebook:
            thumb_paths += [os.path.join(FG_NOTEBOOK_THUMBS,security_code+".jpg")]
        if set_nb_settings:
            db(db.notebook.id==notebook_test.first()).update(last_update=request.now)
            if os.path.exists(os.path.join(FG_NOTEBOOK_THUMBS,notebook_test.first().url+".jpg")):
                os.remove(os.path.join(FG_NOTEBOOK_THUMBS,notebook_test.first().url+".jpg"))
            thumb_paths += [os.path.join(FG_NOTEBOOK_THUMBS,notebook_test.first().url+".jpg")]
        for thumb_name in thumb_paths:
            if not os.path.exists(thumb_name):
                if external_type in ["png","jpg","gif"]:
                    times.append(("Generating thumbs",datetime.now()))
                    text = "generates thumbs for %s" % dataset.id
                    try:
                        db.auth_event.insert(user_id=auth.user_id,description="User %s %s" % (auth.user_id,text))
                    except:pass
                    print "%s: User %s %s" % (request.now,auth.user_id,text)
                    #http://studio.imagemagick.org/pipermail/magick-users/2003-October/011071.html
                    process = subprocess.Popen("%s %s -resize 300x200\\! %s" % (FG_IMAGEMAGICK_PATH,image,thumb_name),shell=True,stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
                    mstats = process.communicate("")
    
        #Manage notebook
        request.vars.add_notebook = add_notebook
        request.vars.set_nb_settings = set_nb_settings
        if add_notebook:
            nbentry = db.notebook.insert(url=security_code,portal=portal,description=layer_output[-1]['stats']['gtitle'])
        elif set_nb_settings:
            nbentry = notebook_test.first()
        else:
            nbentry = None
        #return repr(image) + GRAPH_HIDE
        
        #Use bounds, etc. from init layer and return
        layer_output[-1]['nbentry'] = nbentry
        layer_output[-1]['queryid'] = queryid
        layer_output[-1]['layer_setup'] = layer_setup
        if FG_HTTPS:
            protocol = "https"
        else:
            protocol = "http"
        layer_output[-1]['stats']["shorturl"]= "%s://%s/%s" % (protocol,request.env.http_host,security_code)
        #return repr(request.vars.items()) + GRAPH_HIDE
        #return repr(security_code) + GRAPH_HIDE
        with open(os.path.join(FG_SAVESHARE_FOLDER,security_code),'w') as f:
            f.write(json.dumps(dict(
                url=security_code,
                portal_id=portal.id,
                portal_name=portal.url,
                dataset_id=dataset.id,
                dataset_name=dataset.name,
                settings=savestate,
                status=""
            )))
        if set_nb_settings:
            shutil.copy(
                os.path.join(FG_SAVESHARE_FOLDER,security_code),
                os.path.join(FG_REVISIONS_FOLDER,notebook_test.first().url))
        return layer_output[-1]

    #times.append(("Preparing filters",datetime.now()))
    has_index = False
    if request.vars.otype == "scatter" and request.vars.map_1 and request.vars.linechart:
        avail_axes = ['xaxis','yaxis']
        havail_axes = ['xaxis','yaxis']
    if request.vars.otype == "scatter" and request.vars.linechart:
        avail_axes = ['xaxis','yaxis','zaxis']
        havail_axes = ['xaxis','yaxis','zaxis']
    elif request.vars.otype == "scatter" and request.vars.map_1:
        if request.vars.point_type == "0":
            avail_axes = ['xaxis','yaxis','color']
            havail_axes = ['xaxis','yaxis']
        else:
            avail_axes = ['xaxis','yaxis','size','color']
            havail_axes = ['xaxis','yaxis']
    elif request.vars.otype == "scatter":
        if request.vars.point_type == "0":
            avail_axes = ['xaxis','yaxis','zaxis','color']
            havail_axes = ['xaxis','yaxis','zaxis']
        elif request.vars.errorbars:
            avail_axes = ['xaxis','yaxis','size','zaxis','color']
            havail_axes = ['xaxis','yaxis']
        else:
            avail_axes = ['xaxis','yaxis','zaxis','size','color']
            havail_axes = ['xaxis','yaxis','zaxis']
    elif request.vars.otype == "hist" and request.vars.onedim:
        avail_axes = ['xaxis']
        havail_axes = ['xaxis']
        if request.vars.ystat in ['sum','std','var']:
            avail_axes.append('yaxis')
            havail_axes.append('yaxis')
    elif request.vars.otype == "hist":
        avail_axes = ['xaxis','yaxis']
        havail_axes = ['xaxis','yaxis']
        if request.vars.cstat in ['sum','std','var']:
            avail_axes.append('color')
        if request.vars.surface and request.vars.zstat in ['sum','std','var']:
            avail_axes.append('zaxis')
            havail_axes.append("zaxis")
    elif request.vars.otype == "table":
        avail_axes = ['tsort']
        havail_axes = []
    elif request.vars.otype == "barpie":
        avail_axes = ['tsort']
        havail_axes = []
    filters = []
    for key in request.vars.keys():
        if key.startswith("en_") and request.vars.get(key):
            filterid = key[3:]
            new_filter = {'var':request.vars.get("var_"+filterid).strip(),'min':request.vars.get("min_"+filterid).strip(),'max':request.vars.get("max_"+filterid).strip(),'opt':request.vars.get("opt_"+filterid).strip(),'list':request.vars.get("list_"+filterid).strip()}
            if new_filter['var'] and (new_filter['min'] or new_filter['max'] or ("lasso" in new_filter['var']) or ("list" in new_filter['opt'] and new_filter['list'])):
                filters.append(new_filter)
                if 'Index' in new_filter['var']:
                    has_index = True
                if 'index()' in new_filter['var']:
                    has_index = True
    highlights = []
    for key in request.vars.keys():
        if key.startswith("hen_") and request.vars.get(key):
            highlightid = key[4:]
            new_highlight = {'var':request.vars.get("hvar_"+highlightid).strip(),'min':request.vars.get("hmin_"+highlightid).strip(),'max':request.vars.get("hmax_"+highlightid).strip(),'opt':request.vars.get("hopt_"+highlightid).strip(),'list':request.vars.get("hlist_"+highlightid).strip()}
            if new_highlight['var'] and (new_highlight['min'] or new_highlight['max'] or ("lasso" in new_highlight['var']) or ("list" in new_highlight['opt'] and new_highlight['list'])):
                highlights.append(new_highlight)
                if 'Index' in new_highlight['var']:
                    has_index = True
                if 'index()' in new_highlight['var']:
                    has_index = True
    if request.vars.otype == "hist" and request.vars.onedim:
        if request.vars.ystat in ['mean','min','max']:
            avail_axes.append('yaxis')
            havail_axes.append('yaxis')
    elif request.vars.otype == "hist":
        if request.vars.cstat in ['mean','min','max']:
            avail_axes.append('color')
        if request.vars.surface and request.vars.zstat in ['mean','min','max']:
            avail_axes.append('zaxis')
            havail_axes.append("zaxis")
    axisinput = {}
    axismin = {}
    axismax = {}
    xexp = False
    yexp = False
    for axis in avail_axes:
        axisinput[axis] = request.vars.get(axis).strip()
        if 'Index' in axisinput[axis]:
            has_index = True
        if 'index()' in axisinput[axis]:
            has_index = True
        axismin[axis] = request.vars.get(axis[0]+"_min_bound").strip()
        axismax[axis] = request.vars.get(axis[0]+"_max_bound").strip()
        if (axis == "size" and request.vars.size_log) or (axis == "xaxis" and request.vars.otype == "hist" and request.vars.xaxis_log) or (axis == "yaxis" and not request.vars.onedim and request.vars.otype == "hist" and request.vars.yaxis_log):
            if axis == "xaxis":
                xexp = True
            if axis == "yaxis":
                yexp = True
            axisinput[axis] = "log10(%s)" % axisinput[axis]
            if floatable(axismin[axis]):
                axismin[axis] = str(np.log10(floatable(axismin[axis])))
            elif axismin[axis]:
                axismin[axis] = "log10(%s)" % axismin[axis]
            if floatable(axismax[axis]):
                axismax[axis] = str(np.log10(floatable(axismax[axis])))
            elif axismax[axis]:
                axismax[axis] = "log10(%s)" % axismax[axis]
        if True:#not (request.vars.otype == "hist" and ((axis == 'xaxis' and not request.vars.xaxis_log) or (axis == 'yaxis' and not request.vars.onedim and not request.vars.yaxis_log)) and not request.vars.point_info and floatable(request.vars.get(axis[0]+"_min_bound")) and floatable(request.vars.get(axis[0]+"_max_bound"))):
            new_filter = {'var':axisinput.get(axis),'min':axismin.get(axis),'max':axismax.get(axis),'opt':'is between'}
            if new_filter['min'] == "nan":
                new_filter['min'] = ""
            if new_filter['max'] == "nan":
                new_filter['max'] = ""
            if new_filter['var'] and (new_filter['min'] or new_filter['max']):
                filters.append(new_filter)
    if request.vars.otype == "hist" and request.vars.onedim and request.vars.ystat in ['mean','sum','min','max','std','var'] and request.vars.xaxis and request.vars.yaxis:
        for axis in ['xaxis','yaxis']:
            new_filter = {'var':axisinput[axis],'notnan':True}
            filters.append(new_filter)
    if request.vars.otype == "hist" and request.vars.surface and ((request.vars.cstat in ['mean','sum','min','max','std','var'] and request.vars.color) or (request.vars.zstat in ['mean','sum','min','max','std','var'] and request.vars.zaxis)) and request.vars.xaxis and request.vars.yaxis:
        for axis in ['xaxis','yaxis']:
            new_filter = {'var':axisinput[axis],'notnan':True}
            filters.append(new_filter)
    if request.vars.otype == "hist" and request.vars.surface and (request.vars.zstat in ['mean','sum','min','max','std','var'] and request.vars.zaxis) and request.vars.xaxis and request.vars.yaxis:
        for axis in ['zaxis']:
            new_filter = {'var':axisinput[axis],'notnan':True}
            filters.append(new_filter)
    if request.vars.otype == "hist" and request.vars.surface and (request.vars.cstat in ['mean','sum','min','max','std','var'] and request.vars.color) and request.vars.xaxis and request.vars.yaxis:
        for axis in ['color']:
            new_filter = {'var':axisinput[axis],'notnan':True}
            filters.append(new_filter)
    if request.vars.otype == "hist" and not request.vars.onedim and not request.vars.surface and request.vars.cstat in ['mean','sum','min','max','std','var'] and request.vars.xaxis and request.vars.yaxis and request.vars.color:
        for axis in ['xaxis','yaxis','color']:
            new_filter = {'var':axisinput[axis],'notnan':True}
            filters.append(new_filter)

    if has_index:
        a = rf.append_fields(a,'__Index__',np.arange(len(a)),usemask=False)

    #times.append(("Preparing translations",datetime.now()))
    trans = [(" ","",'other')]
    trans2 = [(" ","",'other'),("x","x",'variable')]
    if request.vars.otype == "scatter" and request.vars.zaxis:
        trans2.append(("y","y",'variable'))
    if request.vars.otype == "hist" and request.vars.surface:
        trans2.append(("y","y",'variable'))
    for i in col_names_sort:
        if i[0].count("\\") + i[0].count("\"") > 0:
            return errormessage("No backslashes or double quotes allowed in raw column names")
        if i[0] in a.dtype.names:
            if a[i[0]].dtype.kind == "S":
                trans.append((i[1],"a[r\""+i[0]+"\"]",'variable'))
            else:
                trans.append((i[1],"(a[r\""+i[0]+"\"]+0.)",'variable'))
    for i in ["(",")",'1','2','3','4','5','6','7','8','9','0','.']:
        trans.append((i,i,'other'))
        trans2.append((i,i,'other'))
    for i in ["nan","naN",'nAn','nAN','Nan','NaN','NAn','NAN']:
        trans.append((i,"np.nan",'other'))
    for i in ["inf","inF",'iNf','iNF','Inf','InF','INf','INF']:
        trans.append((i,"np.inf",'other'))
    for i in ["+","-","*","/"]:
        trans.append((i,i,'function'))
        trans2.append((i,i,'function'))
    trans.append(("%","%",'function'))
    trans.append(("^","**",'function'))
    trans2.append(("^","**",'function'))
    trans.append(("==","==",'function'))
    trans.append(("!=","!=",'function'))
    trans.append((">=",">=",'function'))
    trans.append(("<=","<=",'function'))
    trans.append(("<","<",'function'))
    trans.append((">",">",'function'))
    trans.append(("and","*",'function'))
    trans.append(("or","+",'function'))
    trans.append(("&&","*",'function'))
    trans.append(("||","+",'function'))
    trans.append(("&","*",'function'))
    trans.append(("|","+",'function'))
    for i in ["log(","abs(","sqrt(","log10(","exp(","ceil(","floor(","pi"]:
        trans.append((i,"np."+i,'function'))
        trans2.append((i,i,'function'))
    for i in ["sin(","cos(","tan(","sinh(","cosh(","tanh("]:
        trans.append((i,"np."+i,'function'))
        trans.append(("a"+i,"np.arc"+i,'function'))
        trans2.append((i,i,'function'))
        trans2.append(("a"+i,"a"+i,'function'))
    for i in ["min(","max(","mean(","median(","std(","var("]:
        trans.append((i,"np.nan"+i,'function'))
    trans.append(("rand()","np.random.random(len(a))",'function'))
    trans.append(("index()","a['__Index__']",'function'))
    trans.append(("gauss()","np.random.normal(size=len(a))",'function'))
    trans.append(("Random","np.random.random(len(a))",'function'))
    trans.append(("Index","a['__Index__']",'function'))
    trans.append(("Gaussian","np.random.normal(size=len(a))",'function'))
    trans.append(("e","*10**",'function'))
    trans2.append(("e","*10**",'function'))
    trans.append(("E","*10**",'function'))
    trans2.append(("E","*10**",'function'))
    #return repr(trans)

    times.append(("Arranging data",datetime.now()))
    fdat = compute_filters(filters,a,trans)
    if type(fdat) == str:
        return fdat
    #return repr(filters) +GRAPH_HIDE
    #return repr(fdat) +GRAPH_HIDE
    if fdat is not None:
        a = a[fdat]
    if request.vars.otype == "scatter" or (request.vars.otype=="hist" and request.vars.onedim):
        hdat = compute_filters(highlights,a,trans)
        if type(hdat) == str:
            return hdat
        if hdat is not None:
            hpoints = len(hdat[hdat==True])
            if hpoints == 0:
                hdat = None
    else:
        hdat = None

    #Assemble the table
    if request.vars.otype == "table":
        if request.vars.tsort:
            times.append(("Sorting data",datetime.now()))
            if request.vars.tsort in col_names:
                a = np.sort(a,order=[raw_col_names[col_names.index(request.vars.tsort)]])
            elif request.vars.tsort in raw_col_names:
                a = np.sort(a,order=[request.vars.tsort])
            else:
                return errormessage("Check the sort axis",details="Advanced functions are not supported for the sort axis.")
        times.append(("Building the table",datetime.now()))
        if request.vars.sort_order:
            a = a[::-1]
        if not request.vars.download:
            hdat = compute_filters(highlights,a,trans)
            if type(hdat) == str:
                return str
        show_rows = []
        for i in range(len(raw_col_names)):
            if request.vars.get("showrow"+str(i)):
                show_rows.append(i)
        if len(show_rows) == 0:
            show_rows = range(len(raw_col_names))
        #if "MSIE 7.0" in request.env.http_user_agent:
        #    begin_str = "<script>$('#loading').hide();</script><div style='position:absolute;left:0px;right:0px;top:0px;bottom:30px;overflow:scroll;background:#FFF;border:1px solid #CCC;'><table><thead><tr>"
        #else:
        begin_str = "<div style='position:absolute;left:0px;right:0px;top:50px;bottom:0px;background:#FFF;opacity:0.0' id='displayTableContainer'><table id='displayTable'><thead><tr>"
        head_start = "<th>"
        headers = []
        for i in col_names:
            current_header = i
            if not request.vars.download:
                if request.vars.tsort in [i,raw_col_names[col_names.index(i)]]:
                    if request.vars.sort_order:
                        header_function = "$('#sort_order').removeAttr('checked');refresh(true);"
                    else:
                        header_function = "$('#sort_order').attr('checked','checked');refresh(true);"
                else:
                    header_function = "$('#tsort').val('%s');$('#sort_order').removeAttr('checked');refresh(true);" % i
                current_header = '''<a class="table-header-text" onclick="%s" style='%scolor:black;font-weight:bold'>%s</a>''' % (header_function,descriptions.get(i) and " " or "cursor:pointer;",current_header)
                if descriptions.get(i):
                    current_header = "<abbr title='%s'>%s</abbr>" % (descriptions.get(i),current_header)
            headers.append(current_header)
        head_mid = ""
        head_end = "</th>"
        head_to_data = "</tr></thead><tbody>"
        row_start = "<tr>"
        row_start_highlight = "<tr class='warning'>"
        element_start = "<td>"
        element_mid = ""
        element_end = "</td>"
        row_end = "</tr>"
        end_str = """</tbody></table></div><script>window.scrollTo(0, 0);$('#spin').hide();$('label[name="lasso"]').hide();$('#reset_zoom').hide();setTimeout(function(){ %(aligncode)s$("#displayTableContainer").css('opacity','1.0'); }, 1);$('#graph_container').css('background-image','');</script>"""
        if request.vars.aligndecimals:
            end_str = end_str.replace("%(aligncode)s",'$("#displayTable").alignColumn([%(columns)s]);$("#displayTable").fixedHeaderTable();i = 0; $("#displayTable thead .fht-cell").map(function() { $(this).width($($(".fht-thead th")[i]).width()); i++ });')
        else:
            end_str = end_str.replace("%(aligncode)s",'$("#displayTable").fixedHeaderTable();')
        #if "MSIE 7.0" not in request.env.http_user_agent:
        #    end_str += """$("table.table").fixedHeaderTable({});$("table.table th").css("padding","4px 5px");$("table.table td").css("padding","4px 5px");$(".fht-tbody th").css("padding","0px 5px");$(".fht-tbody").height($(".fht-tbody").height()-10)</script>"""
        end_str += GRAPH_HIDE
        mimetype = "text/html"
        filetype = "htm"
        tbl_rows = intable(request.vars.tbl_rows)
        if request.vars.download:
            if request.vars.filetype_tbl[:4] == "html":
                begin_str = "<table><thead>"
                end_str = "</table>"
            if request.vars.filetype_tbl[:4] == "tsep":
                (begin_str,head_start,head_end,head_to_data,row_start,element_start,element_end,row_end,end_str,mimetype,filetype) = ("","","\t","\n","","","\t","\n","","text/plain","txt")
            if request.vars.filetype_tbl[:4] == "csep":
                (begin_str,head_start,head_end,head_to_data,row_start,element_start,element_end,row_end,end_str,mimetype,filetype) = ("","",",","\n","","",",","\n","","text/csv","csv")
            if request.vars.filetype_tbl[:4] == "ssep":
                (begin_str,head_start,head_end,head_to_data,row_start,element_start,element_end,row_end,end_str,mimetype,filetype) = ("#",""," ","\n","",""," ","\n","","text/plain","txt")
                headers = raw_col_names
            if request.vars.filetype_tbl[:4] == "ltex":
                #https://en.wikipedia.org/wiki/LaTeX
                #https://en.wikipedia.org/wiki/Internet_media_type
                #http://truben.no/latex/table/
                begin_str = "\\begin{table}\n    \\begin{tabular}{%s}\n" % ("l" * len(headers))
                head_start = "    "
                head_mid = " & "
                head_end = ""
                head_to_data = "\n"
                row_start = "    "
                element_start = ""
                element_end = ""
                element_mid = " & "
                row_end = "\n"
                end_str = "    \\end{tabular}\n\\end{table}"
                mimetype = "application/x-latex"
                filetype = "tex"
            if request.vars.filetype_tbl[-1] == "a":
                tbl_rows = len(a)
        with open(os.path.join(FG_TABLE_FOLDER,cachename + "-table.txt"),'w') as out:
            out.write(begin_str)
            if head_mid:
                out.write(head_start + head_mid.join([headers[i] for i in show_rows]) + head_end)
            else:
                for i in show_rows:
                    out.write(head_start+headers[i]+head_end)
            out.write(head_to_data)
            commas = request.vars.commas and element_end != ","
            if element_mid:
                for i in range(min(len(a),tbl_rows)):
                    out.write(row_start)
                    row = a[i]
                    out.write(element_start+element_mid.join([str(row[j]) for j in show_rows])+element_end)
                    out.write(row_end)
                    if i % 1000 == 0:
                        if (datetime.now() - times[0][1]).seconds >= FG_SERVER_TIMEOUT:
                            out.close()
                            shutil.move(out.name,os.path.join(FG_TRASH_TABLE_FOLDER,queryid+"-table.txt"))
                            return errormessage("Server timeout",details="This can result from excessive settings or handling too much data. Check your settings and try again.")
            else:
                for i in range(min(len(a),tbl_rows)):
                    if hdat is None:
                        out.write(row_start)
                    else:
                        if hdat[i]:
                            out.write(row_start_highlight)
                        else:
                            out.write(row_start)
                    row = a[i]
                    for j in show_rows:
                        if request.vars.download:
                            out.write(element_start+str(row[j])+element_end)
                        elif col_names[j] in html_names and row[j] != "null":
                            out.write(element_start+httplink(html_names[col_names[j]] % dict(zip(raw_col_names,row)),title=str(row[j]))+element_end)
                        else:
                            out.write(element_start+httplink(str(row[j]))+element_end)
                    out.write(row_end)
                    if i % 1000 == 0:
                        if (datetime.now() - times[0][1]).seconds >= FG_SERVER_TIMEOUT:
                            out.close()
                            shutil.move(out.name,os.path.join(FG_TRASH_TABLE_FOLDER,queryid+"-table.txt"))
                            return errormessage("Server timeout",details="This can result from excessive settings or handling too much data. Check your settings and try again.")
            align_columns = []
            for i in range(len(show_rows)):
                if a.dtype[show_rows[i]].kind != 'S' and col_names[show_rows[i]] not in html_names:
                    align_columns.append(str(i))
            out.write(end_str.replace("%(columns)s",",".join(align_columns)))
            if request.vars.download:
                #https://en.wikipedia.org/wiki/Mime_type
                #https://en.wikipedia.org/wiki/HTTP_header
                #http://web2py.com/examples/default/examples
                response.headers['Content-Type']=mimetype
                response.headers['Content-Length']=out.tell()
                response.headers['Content-Disposition']='attachment; filename="'+dataset.name+'.'+filetype+'"'
            else:
                times.append(("Finish",datetime.now()))
                tstr = ""
                for t in range(len(times)-1):
                    tdiff = times[t+1][1]-times[t][1]
                    tfloat = tdiff.seconds + (tdiff.microseconds / 1000000.0)
                    tstr += times[t][0] + ": " + str(tfloat) + "<br/>"
                tdiff = times[-1][1]-times[0][1]
                tfloat = tdiff.seconds + (tdiff.microseconds / 1000000.0)
                tstr += "TOTAL: " + str(tfloat)
                if tfloat > 5:
                    janitor() # janitor tax
                tstr += "<br/><br/>"
                tstr2 = ""
                tstr2 += "%s points displayed" % len(a)
                tstr3 = "%s rows" % len(a)
                if totalcount > len(a):
                    tstr2 += "<br/>%s points filtered out" % (totalcount - len(a))
                    tstr2 += "<br/>%s points total" % totalcount
                    tstr3 = "%s of %s rows" % (len(a),totalcount)
                tstr2 += "<br/><br/>"
                with open(os.path.join(FG_SAVESHARE_FOLDER,security_code),'w') as f:
                    f.write(json.dumps(dict(
                        url=security_code,
                        portal_id=portal.id,
                        portal_name=portal.url,
                        dataset_id=dataset.id,
                        dataset_name=dataset.name,
                        settings=savestate,
                        status=tstr+tstr2
                    )))
                if request.vars.set_nb_settings:
                    shutil.copy(
                        os.path.join(FG_SAVESHARE_FOLDER,security_code),
                        os.path.join(FG_REVISIONS_FOLDER,notebook_test.first().url))
                #savestate_id = db.savestate.insert(url=security_code,portal_id=portal.id,portal_name=portal.url,dataset_id=dataset.id,
                #            dataset_name=dataset.name,settings=json.dumps(savestate),status=tstr)
                if FG_HTTPS:
                    protocol = "https"
                else:
                    protocol = "http"
                if len(datasets) > 1:
                    longurl = "%s://%s/%s/%s/table-%s" % (protocol,request.env.http_host,portal.url,cleanurl(dataset.name),security_code)
                else:
                    longurl = "%s://%s/%s/table-%s" % (protocol,request.env.http_host,portal.url,security_code)
                shorturl = "%s://%s/%s" % (protocol,request.env.http_host, security_code)
                if request.vars.add_notebook:
                    nbentry = db.notebook.insert(url=security_code,portal=portal,description=portal.url)
                    nbtext = 'add_nb_entry(%s,"%s","%s","%s","%s","%s");$("html, body").animate({scrollTop: $(window).height()/2}, 0);' % (nbentry.id,nbentry.description,nbentry.url,(nbentry.zuser and 'You' or 'Guest'),nbentry.zpublic,abs(hash(random.random())))
                elif request.vars.set_nb_settings:
                    db(db.notebook.id==notebook_test.first()).update(last_update=request.now)
                    nbentry = notebook_test.first()
                    nbtext = 'alert("Your notebook entry has been updated.");$("html, body").animate({scrollTop: $(window).height()/2}, 200);$("#nb-item-%s a img").attr("src","/%s/run/notebook_serve/%s/%s")' % (nbentry.id,request.application,nbentry.id,abs(hash(nbentry.last_update)))
                else:
                    nbentry = None
                    nbtext = ''
                #Generate thumbs
                thumb_paths = [os.path.join(FG_PORTAL_THUMBS,str(portal.id)+".jpg"),os.path.join(FG_DATASET_THUMBS,str(dataset.id)+".jpg")]
                if request.vars.add_notebook:
                    thumb_paths += [os.path.join(FG_NOTEBOOK_THUMBS,security_code+".jpg")]
                if request.vars.set_nb_settings:
                    if os.path.exists(os.path.join(FG_NOTEBOOK_THUMBS,notebook_test.first().url+".jpg")):
                        os.remove(os.path.join(FG_NOTEBOOK_THUMBS,notebook_test.first().url+".jpg"))
                    thumb_paths += [os.path.join(FG_NOTEBOOK_THUMBS,notebook_test.first().url+".jpg")]
                for thumb_name in thumb_paths:
                    if not os.path.exists(thumb_name):
                        if plt:
                            #http://stackoverflow.com/questions/17232683/creating-tables-in-matplotlib
                            thumb_headers = [a.dtype.names[i] for i in show_rows[:3]]
                            thumb_data = a[[n for n in thumb_headers]][:10]
                            fig = plt.figure(figsize=(3,2),dpi=100)
                            ax = fig.add_subplot(111)
                            ax.axis('off')
                            thumb_table = ax.table(cellText=thumb_data,colLabels=thumb_headers,loc=0)
                            text = "generates thumbs for %s" % dataset.id
                            try:
                                db.auth_event.insert(user_id=auth.user_id,description="User %s %s" % (auth.user_id,text))
                            except:pass
                            print "%s: User %s %s" % (request.now,auth.user_id,text)
                            #print thumb_name
                            plt.savefig(thumb_name,bbox_inches="tight")
                out.write("<script>$('#status').html('%s');$('#status2').html('%s');$('#status3').html('%s');$('#status4').html('');longurl='%s';shorturl='%s';%s</script>" % (tstr,tstr2,tstr3,longurl,shorturl,nbtext))
        return response.stream(os.path.join(FG_TABLE_FOLDER,cachename + "-table.txt"),chunk_size=4096)

    #Assemble point info
    if request.vars.point_info and request.vars.otype == "scatter":
        a = a[np.bitwise_and(np.bitwise_and(np.bitwise_and(
            floatable(request.vars.point_info_x_min)<=sanitize(request.vars.xaxis,a,trans),
            sanitize(request.vars.xaxis,a,trans)<=floatable(request.vars.point_info_x_max)),
            floatable(request.vars.point_info_y_min)<=sanitize(request.vars.yaxis,a,trans)),
            sanitize(request.vars.yaxis,a,trans)<=floatable(request.vars.point_info_y_max))]
        if len(a) > 0:
            hdat = compute_filters(highlights,a,trans)
            if type(hdat) == str:
                return hdat
            if hdat is not None:
                hpoints = len(hdat[hdat==True])
                if hpoints > 0:
                    a = a[hdat]
                    is_highlight = True
                else:
                    is_highlight = False
            else:
                hpoints = None
                is_highlight = False
            return dict(col_names=col_names, html_names=html_names, result=a[0], nearby=len(a),raw_col_names=raw_col_names,hpoints=hpoints,is_highlight=is_highlight)
        else:
            return dict(result=[])

    #Assemble output data
    cols = []
    outputs = []
    output_strings = {}
    columndict = {'xaxis':'X-axis','yaxis':'Y-axis','zaxis':'Z-axis','size':'size axis','color':'color axis','tsort':'sort axis'}
    for axis in avail_axes:
        if axisinput.get(axis):
            axisdata = sanitize(axisinput.get(axis),a,trans,getstrings=True,keepstrings=(request.vars.otype == "barpie"))
            if isinstance(axisdata,str):
                return errormessage("Check the "+columndict[axis],details=axisdata)
            if axisdata is not None:
                cols.append(axis)
                outputs.append(axisdata[1])
                if axisdata[0] is not None:
                    output_strings[axis] = axisdata[0]
            else:
                return errormessage("Check the "+columndict[axis])
    #return repr(output_strings) + GRAPH_HIDE

    if request.vars.getstats:
        #Coorelation coefficient
        corrcoef = np.column_stack(outputs).astype("float64")
        if np.isnan(np.min(corrcoef)):
            corrcoef = corrcoef[~np.isnan(corrcoef).any(1)]
        if np.isinf(np.min(corrcoef)) or np.isinf(np.max(corrcoef)):
            corrcoef = corrcoef[~np.isinf(corrcoef).any(1)]
        coefmat = np.corrcoef(np.transpose(corrcoef))

        #Two variable statistics
        if "xaxis" in cols and "yaxis" in cols:
            xyaxis = np.column_stack(outputs[:2]).astype("float64")
            if np.isnan(np.min(xyaxis)):
                xyaxis = xyaxis[~np.isnan(xyaxis).any(1)]
            if np.isinf(np.min(xyaxis)) or np.isinf(np.max(xyaxis)):
                xyaxis = xyaxis[~np.isinf(xyaxis).any(1)]
            poly = np.polyfit(xyaxis[:,0],xyaxis[:,1],1)
            linreg = format_poly(0,0,poly[0],poly[1])
            poly = np.polyfit(xyaxis[:,0],xyaxis[:,1],2)
            quadreg = format_poly(0,poly[0],poly[1],poly[2])
            poly = np.polyfit(xyaxis[:,0],xyaxis[:,1],3)
            cubicreg = format_poly(poly[0],poly[1],poly[2],poly[3])
        else:
            linreg = None
            quadreg = None
            cubicreg = None

        #Single variable statistics
        statsdict = {'xaxis':'X-axis','yaxis':'Y-axis','zaxis':'Z-axis','size':'Size','color':'Color'}
        stats = []
        for i in range(len(cols)):
             outputs[i] = outputs[i].astype("float64")
             if np.isnan(np.min(outputs[i])):
                 outputs[i] = outputs[i][~np.isnan(outputs[i])]
             if np.isinf(np.min(outputs[i])) or np.isinf(np.max(outputs[i])):
                 outputs[i] = outputs[i][~np.isinf(outputs[i])]
             stats.append({"Axis":statsdict[cols[i]],"Name":axisinput[cols[i]],"Items":len(outputs[i]),"Minimum":np.min(outputs[i]),"5th percentile":np.percentile(outputs[i],5),"10th percentile":np.percentile(outputs[i],10),"25th percentile":np.percentile(outputs[i],25),"Median":np.median(outputs[i]),"75th percentile":np.percentile(outputs[i],75),"90th percentile":np.percentile(outputs[i],90),"95th percentile":np.percentile(outputs[i],95),"Maximum":np.max(outputs[i]),"Mean":np.mean(outputs[i]),"Standard deviation":np.std(outputs[i]),"Sum":np.sum(outputs[i]),"Variance":np.var(outputs[i])})
        order = ["Items","Minimum","5th percentile","10th percentile","25th percentile","Median","75th percentile","90th percentile","95th percentile","Maximum","Mean","Standard deviation","Variance","Sum"]
        return dict(stats=stats,order=order,linreg=linreg,quadreg=quadreg,cubicreg=cubicreg,coefmat=coefmat)

    if request.vars.otype != "barpie" and 'xaxis' not in cols:
        return errormessage("Check the X-axis",details="An X-axis is required.")
    if request.vars.otype == "scatter" and 'yaxis' not in cols:
        return errormessage("Check the Y-axis",details="A Y-axis is required.")
    if request.vars.otype == "barpie" and 'tsort' not in cols:
        return errormessage("Check the sort axis",details="A sort axis is required.")
    binstat = {}
    if request.vars.otype == "hist" and request.vars.onedim and request.vars.ystat in ['mean','sum','min','max','std','var']:
        if "yaxis" not in cols:
            return errormessage("Check the Y-axis",details="A Y-axis is required.")
        bincol = cols.index("yaxis")
        cols.pop(bincol)
        binstat['yaxis'] = outputs.pop(bincol)
    if request.vars.otype == "hist" and not request.vars.onedim and request.vars.cstat in ['mean','sum','min','max','std','var']:
        if "color" not in cols:
            return errormessage("Check the color axis",details="A color axis is required.")
        bincol = cols.index("color")
        cols.pop(bincol)
        binstat['color'] = outputs.pop(bincol)
    if request.vars.otype == "hist" and request.vars.surface and request.vars.zstat in ['mean','sum','min','max','std','var']:
        if "zaxis" not in cols:
            return errormessage("Check the Z-axis",details="A Z-axis is required.")
        bincol = cols.index("zaxis")
        cols.pop(bincol)
        binstat['zaxis'] = outputs.pop(bincol)
    #return repr(binstat) + GRAPH_HIDE
    for i in range(len(outputs)):
        if not isinstance(outputs[i],np.ndarray) and outputs[i] != 37235:
            outputs[i] = np.tile(outputs[i],len(a))
    dataout = np.column_stack(outputs)
    if dataout.dtype.name in ["int64","object"]:
        dataout = dataout.astype("float64")
    if hdat is not None:
        dataout_highlight = np.column_stack([outputs[i][hdat] for i in range(len(outputs)) if cols[i] in havail_axes])
        if dataout_highlight.dtype.name in ["int64","object"]:
            dataout_highlight = dataout_highlight.astype("float64")
    colstr = ":".join([str(i) for i in range(1,len(cols)+1)])
    formats = ""
    for i in range(len(cols)):
        formats += "%" + str(dataout.dtype)
    if hdat is not None:
        hnumcols = len([1 for i in cols if i in havail_axes])
        hformats = "%" + str(dataout_highlight.dtype)
        hformats *= hnumcols
        hcolstr = ":".join([str(i) for i in range(1,hnumcols+1)])

    #http://stackoverflow.com/questions/2695503/removing-pairs-of-elements-from-numpy-arrays-that-are-nan-or-another-value-in
    nancount = np.shape(dataout)[0]
    if nancount == 0:
        return errormessage("No data to display",details="All %s points were filtered out." % totalcount)
    mins = {}
    maxs = {}
    logok = {}
    if request.vars.otype != "barpie":
        for i in range(len(cols)):
            mins[cols[i]] = np.min(dataout[:,i])
            if np.isnan(mins[cols[i]]):
                dataout = dataout[~np.isnan(dataout).any(1)]
                if np.shape(dataout)[0] == 0:
                    return errormessage("No data to display",details="None of the %s points can be plotted because of missing data." % nancount)
                mins[cols[i]] = np.min(dataout[:,i])
            if np.isinf(mins[cols[i]]):
                dataout = dataout[~np.isinf(dataout).any(1)]
                if np.shape(dataout)[0] == 0:
                    return errormessage("No data to display",details="None of the %s points can be plotted because of missing data." % nancount)
                mins[cols[i]] = np.min(dataout[:,i])
            maxs[cols[i]] = np.max(dataout[:,i])
            if np.isinf(maxs[cols[i]]):
                dataout = dataout[~np.isinf(dataout).any(1)]
                if np.shape(dataout)[0] == 0:
                    return errormessage("No data to display",details="None of the %s points can be plotted because of missing data." % nancount)
                maxs[cols[i]] = np.max(dataout[:,i])
            if i != 'size':
                logfun = ((cols[i] == "xaxis" and request.vars.xaxis_log) or (cols[i] == "yaxis" and request.vars.yaxis_log) or (cols[i] == "zaxis" and request.vars.zaxis_log)) and (request.vars.otype == "scatter" or (request.vars.onedim and cols[i] == "yaxis"))
                if cols[i] in output_strings:
                    (mins[cols[i]],maxs[cols[i]]) = (-0.5,len(output_strings[cols[i]])-0.5)
                else:
                    (mins[cols[i]],maxs[cols[i]]) = bound_round(mins[cols[i]],maxs[cols[i]],logfun=logfun,inclusive=(cols[i]=='color'))
            if floatable(request.vars.get(cols[i][0]+"_min_bound")):
                mins[cols[i]] = floatable(request.vars.get(cols[i][0]+"_min_bound"))
                if request.vars.otype == "hist" and ((cols[i] == "xaxis" and request.vars.xaxis_log) or (cols[i] == "yaxis" and request.vars.yaxis_log and not request.vars.onedim)):
                    mins[cols[i]] = np.log10(mins[cols[i]])
                    logok[cols[i]] = True
            elif request.vars.otype == "scatter" and request.vars.map_1:
                if cols[i] == "xaxis":
                    mins["xaxis"] = -180
                elif cols[i] == "yaxis":
                    mins["yaxis"] = -85
            if floatable(request.vars.get(cols[i][0]+"_max_bound")):
                maxs[cols[i]] = floatable(request.vars.get(cols[i][0]+"_max_bound"))
                if request.vars.otype == "hist" and ((cols[i] == "xaxis" and request.vars.xaxis_log) or (cols[i] == "yaxis" and request.vars.yaxis_log and not request.vars.onedim)):
                    maxs[cols[i]] = np.log10(maxs[cols[i]])
            elif request.vars.otype == "scatter" and request.vars.map_1:
                if cols[i] == "xaxis":
                    maxs["xaxis"] = 180
                elif cols[i] == "yaxis":
                    maxs["yaxis"] = 85
            if mins[cols[i]] == maxs[cols[i]]:
                mins[cols[i]] -= 1
                maxs[cols[i]] += 1
    #return repr(mins) + GRAPH_HIDE
    plotcount = np.shape(dataout)[0]
    for (log,axis) in ((request.vars.xaxis_log,'xaxis'),(request.vars.yaxis_log,'yaxis'),(request.vars.zaxis_log,'zaxis'),(request.vars.color_log,'color'),(request.vars.size_log,'size')):
        if log and axis in output_strings:
            return errormessage("Check the %s" % columndict[axis],
                details="%s contains string data, which can't be used on a log scale." % columndict[axis])
        if log and axis in cols and not logok.has_key(axis) and mins[axis] <= 0:
            min_out = dataout[:,cols.index(axis)]
            min_out = min_out[min_out > 0]
            min_out = bound_round(np.min(min_out),np.max(min_out),logfun = True)[0]
            return errormessage("Check the %s" % columndict[axis],
                details="The %s has points that are zero or less, which can't be plotted on a log scale." % columndict[axis],
                button="Set min bound of %s to %s" % (columndict[axis],min_out),
                action="$('#%smin').val('%s');%sb = true;updateDisplay();refresh(true);" % (axis[0],min_out,axis[0]))
        if log and axis in cols and logok.has_key(axis) and (np.isneginf(mins[axis]) or np.isnan(mins[axis])):
            return errormessage("Check the %s" % columndict[axis],
                details="The %s has points that are negative infinity or unknown, which can't be plotted on a log scale." % columndict[axis],
                button="Set min bound of %s to zero" % columndict[axis],
                action="$('#%smin').val('0');updateDisplay();refresh(true);" % axis[0])

    if request.vars.otype == 'hist':
        if request.vars.layerrender:
            for c in cols:
                mins[c] = floatable(request.vars.get("min_"+c),mins[c])
                maxs[c] = floatable(request.vars.get("max_"+c),maxs[c])
        if floatable(request.vars.bins) <= 0:
            return errormessage("Check the X bins",details="There should be at least one bin.")
        if floatable(request.vars.binsy) <= 0 and not request.vars.onedim:
            return errormessage("Check the Y bins",details="There should be at least one bin.")
        if request.vars.discrete and 'yaxis' not in cols:
            bins = [np.arange(mins['xaxis']-0.5,maxs['xaxis']+1)]
            mins['xaxis'] -= 0.5
            maxs['xaxis'] += 0.5
        else:
            bins = [intable(request.vars.bins) or 100]
        if 'yaxis' in cols:
            bins += [intable(request.vars.binsy) or 100]
        #return repr(bins) + GRAPH_HIDE
        if 'yaxis' in cols:
            hrange = [(mins['xaxis'],maxs['xaxis']),(mins['yaxis'],maxs['yaxis'])]
        else:
            hrange = [(mins['xaxis'],maxs['xaxis'])]
            if hdat is not None:
                dataout2 = dataout[hdat].flatten()
            dataout = dataout.flatten()
        diag = request.vars.title
        #return repr(dataout) + GRAPH_HIDE
        if request.vars.onedim:
            if request.vars.ystat == "count":
                binstat["yaxis"] = np.zeros(len(dataout))
            (hdata,hbounds,junk) = binned_statistic(dataout,binstat.get('yaxis'),bins=bins,hrange=hrange,statistic=request.vars.ystat,diag=diag)
            if hdat is not None and 'yaxis' in binstat:
                (hdata2,hbounds2,junk) = binned_statistic(dataout2,binstat.get('yaxis')[hdat],bins=bins,hrange=hrange,statistic=request.vars.ystat,diag=diag)
            elif hdat is not None:
                (hdata2,hbounds2,junk) = binned_statistic(dataout2,binstat.get('yaxis'),bins=bins,hrange=hrange,statistic=request.vars.ystat,diag=diag)
            #if request.vars.yaxis_log:
            #    hdata = np.log10(hdata)
            #    hdata[np.isneginf(hdata)] = 0
        else:
            if request.vars.cstat == "count":
                binstat["color"] = np.zeros(len(dataout))
            #bin_start_time = time.time()
            #(hdata,hbounds,junk) = st.binned_statistic_dd(dataout,binstat.get('color'),bins=bins,range=hrange,statistic=request.vars.cstat)
            (hdata,hbounds,junk) = binned_statistic(dataout,binstat.get('color'),bins=bins,hrange=hrange,statistic=request.vars.cstat,diag=diag)
            #print time.time() - bin_start_time
            #return repr(hdata.dtype) + GRAPH_HIDE
            if request.vars.color_log:
            #    #    hdata = np.log10(hdata)
                hdata[hdata<=0] = np.nan
            if request.vars.surface:
                hdata[np.isnan(hdata)] = (request.vars.cstat == "count") and 0.1 or np.nanmin(hdata)
                if request.vars.zstat == "count":
                    binstat["zaxis"] = np.zeros(len(dataout))
                if request.vars.zstat == request.vars.cstat and request.vars.zstat == "count":
                    hdata2 = hdata
                elif request.vars.zstat == request.vars.cstat and request.vars.zaxis == request.vars.color:
                    hdata2 = hdata
                else:
                    (hdata2,hbounds,junk) = binned_statistic(dataout,binstat.get('zaxis'),bins=bins,hrange=hrange,statistic=request.vars.zstat,diag=diag)
                if request.vars.zaxis_log:
                    #hdata2 = np.log10(hdata2)
                    hdata2[hdata2<=0] = np.nan
                hdata2[np.isnan(hdata2)] = (request.vars.zstat == "count") and 0.1 or np.nanmin(hdata2)
        if request.vars.point_info:
            left_bound = right_bound = hbounds[0][0]
            for right_bound in hbounds[0]:
                if right_bound > floatable(request.vars.point_info_x):
                    break
                left_bound = right_bound
            if left_bound == right_bound:
                return dict(result=[])
            if request.vars.onedim or stackedhist:
                a = a[np.bitwise_and(
                left_bound<=sanitize(request.vars.xaxis,a,trans),
                sanitize(request.vars.xaxis,a,trans)<=right_bound)]
                if len(a) > 0:
                    hdat = compute_filters(highlights,a,trans)
                    if type(hdat) == str:
                        return hdat
                    if hdat is not None:
                        hpoints = len(hdat[hdat==True])
                        if hpoints > 0:
                            a = a[hdat]
                            is_highlight = True
                        else:
                            is_highlight = False
                    else:
                        hpoints = None
                        is_highlight = False
                    return dict(col_names=col_names, html_names=html_names, result=a[0], nearby=len(a),raw_col_names=raw_col_names,hpoints=hpoints,is_highlight=is_highlight)
                else:
                    return dict(result=[])
            lower_bound = upper_bound = hbounds[1][0]
            for upper_bound in hbounds[1]:
                if upper_bound > floatable(request.vars.point_info_y):
                    break
                lower_bound = upper_bound
            if lower_bound == upper_bound:
                return dict(result=[])
            a = a[np.bitwise_and(np.bitwise_and(np.bitwise_and(
                left_bound<=sanitize(request.vars.xaxis,a,trans),
                sanitize(request.vars.xaxis,a,trans)<=right_bound),
                lower_bound<=sanitize(request.vars.yaxis,a,trans)),
                sanitize(request.vars.yaxis,a,trans)<=upper_bound)]
            if len(a) > 0:
                hdat = compute_filters(highlights,a,trans)
                if type(hdat) == str:
                    return hdat
                if hdat is not None:
                    hpoints = len(hdat[hdat==True])
                    if hpoints > 0:
                        a = a[hdat]
                        is_highlight = True
                    else:
                        is_highlight = False
                else:
                    hpoints = None
                    is_highlight = False
                return dict(col_names=col_names, html_names=html_names, result=a[0], nearby=len(a),raw_col_names=raw_col_names,hpoints=hpoints,is_highlight=is_highlight)
            else:
                return dict(result=[])
        old_hbounds = copy.deepcopy(hbounds)
        for hb in hbounds:
            hb += ((hb[1] - hb[0])/2)
        if np.min(hdata) == 0 and np.max(hdata) == 0:
            return errormessage("No data to display",details="All %s points were filtered out." % totalcount)
        if np.all(np.isnan(hdata)):
            return errormessage("No data to display",details="All %s points were filtered out." % totalcount)
        if request.vars.surface: # Surface map
            if np.min(hdata2) == 0 and np.max(hdata2) == 0:
                return errormessage("No data to display",details="All %s points were filtered out." % totalcount)
            if np.all(np.isnan(hdata2)):
                return errormessage("No data to display",details="All %s points were filtered out." % totalcount)
            hdata = hdata.flatten()
            hdata2 = hdata2.flatten()
            dataout = np.column_stack((np.repeat(hbounds[0][:-1],bins[1]),np.tile(hbounds[1][:-1],bins[0]),hdata2,hdata))
            histtype = 2
            cols += ['color','zaxis']
            if request.vars.color_log:
                mins['color'] = np.min(hdata[hdata>0])
                maxs['color'] = np.max(hdata)
            else:
                mins['color'] = np.min(hdata)
                maxs['color'] = np.max(hdata)
            mins['zaxis'] = (request.vars.zstat == "count") and (request.vars.zaxis_log and 0.1 or 0) or np.min(hdata2)
            maxs['zaxis'] = np.max(hdata2)
            if request.vars.zaxis_log:
                (mins['zaxis'],maxs['zaxis']) = bound_round(np.min(hdata2[hdata2>0]),np.max(hdata2),logfun=True)
            elif np.min(hdata2) >= 0:
                mins['zaxis'] = 0
                (junk,maxs['zaxis']) = bound_round(0,np.max(hdata2),logfun=False)
            else:
                (mins['zaxis'],maxs['zaxis']) = bound_round(np.min(hdata2),np.max(hdata2),logfun=False)
            if request.vars.cstat in ['mean','min','max'] and floatable(request.vars.c_min_bound) and floatable(request.vars.c_max_bound):
                (mins['color'],maxs['color']) = (floatable(request.vars.c_min_bound),floatable(request.vars.c_max_bound))
            elif request.vars.cstat in ['mean','min','max'] and floatable(request.vars.c_min_bound):
                (mins['color'],junk) = bound_round(floatable(request.vars.c_min_bound),maxs['color'],logfun=request.vars.color_log)
            elif request.vars.cstat in ['mean','min','max'] and floatable(request.vars.c_max_bound):
                (junk,maxs['color']) = bound_round(mins['color'],floatable(request.vars.c_max_bound),logfun=request.vars.color_log)
            if request.vars.zstat in ['mean','min','max'] and floatable(request.vars.z_min_bound) and floatable(request.vars.z_max_bound):
                (mins['zaxis'],maxs['zaxis']) = (floatable(request.vars.z_min_bound),floatable(request.vars.z_max_bound))
            elif request.vars.zstat in ['mean','min','max'] and floatable(request.vars.z_min_bound):
                (mins['zaxis'],junk) = bound_round(floatable(request.vars.z_min_bound),maxs['color'],logfun=request.vars.zaxis_log)
            elif request.vars.zstat in ['mean','min','max'] and floatable(request.vars.z_max_bound):
                (junk,maxs['zaxis']) = bound_round(mins['zaxis'],floatable(request.vars.z_max_bound),logfun=request.vars.zaxis_log)
        elif 'yaxis' in cols and stackedhist: # Stacked histogram
            hdata = np.nan_to_num(hdata.T)
            hdata = np.array([np.sum(hdata[i:],axis=0) for i in range(len(hdata))])
            hdata = hdata.flatten()
            dataout = np.column_stack((np.tile(hbounds[0][:-1],bins[1]),hdata))
            histtype = 4
            max_hdata = np.max(hdata)
            if request.vars.color_log:
                min_hdata = np.min(hdata[hdata>0])
                if hdat is not None:
                    min_hdata = min(min_hdata,np.min(hdata2[hdata2>0]))
                (mins['yaxis'],maxs['yaxis']) = bound_round(min_hdata,max_hdata,logfun=True)
            elif np.min(hdata) >= 0:
                mins['yaxis'] = 0
                (junk,maxs['yaxis']) = bound_round(0,max_hdata,logfun=False)
            else:
                min_hdata = np.min(hdata)
                if hdat is not None:
                    min_hdata = min(min_hdata,np.min(hdata))
                (mins['yaxis'],maxs['yaxis']) = bound_round(min_hdata,max_hdata,logfun=False)
            if request.vars.ystat in ['mean','min','max'] and floatable(request.vars.y_min_bound) and floatable(request.vars.y_max_bound):
                (mins['yaxis'],maxs['yaxis']) = (floatable(request.vars.y_min_bound),floatable(request.vars.y_max_bound))
            elif request.vars.ystat in ['mean','min','max'] and floatable(request.vars.y_min_bound):
                (mins['yaxis'],junk) = bound_round(floatable(request.vars.y_min_bound),maxs['yaxis'],logfun=request.vars.yaxis_log)
            elif request.vars.ystat in ['mean','min','max'] and floatable(request.vars.y_max_bound):
                (junk,maxs['yaxis']) = bound_round(mins['yaxis'],floatable(request.vars.y_max_bound),logfun=request.vars.yaxis_log)
            #return repr(dataout) + GRAPH_HIDE
        elif 'yaxis' in cols: # Heatmap
            hdata = hdata.flatten()
            dataout = np.column_stack((np.repeat(hbounds[0][:-1],bins[1]),np.tile(hbounds[1][:-1],bins[0]),hdata))
            histtype = 2
            cols += ['color']
            if request.vars.cstat == 'count':
                dataout[dataout==0] = np.nan
            if request.vars.color_log:
                mins['color'] = np.min(hdata[hdata>0])
                maxs['color'] = np.max(hdata)
            else:
                mins['color'] = np.min(hdata)
                maxs['color'] = np.max(hdata)
            if request.vars.cstat in ['mean','min','max'] and floatable(request.vars.c_min_bound) and floatable(request.vars.c_max_bound):
                (mins['color'],maxs['color']) = (floatable(request.vars.c_min_bound),floatable(request.vars.c_max_bound))
            elif request.vars.cstat in ['mean','min','max'] and floatable(request.vars.c_min_bound):
                (mins['color'],junk) = bound_round(floatable(request.vars.c_min_bound),maxs['color'],logfun=request.vars.color_log)
            elif request.vars.cstat in ['mean','min','max'] and floatable(request.vars.c_max_bound):
                (junk,maxs['color']) = bound_round(mins['color'],floatable(request.vars.c_max_bound),logfun=request.vars.color_log)
        else: # Histogram
            dataout = np.column_stack((hbounds[0][:-1],hdata))
            max_hdata = np.max(hdata)
            if hdat is not None:
                dataout_highlight = np.column_stack((hbounds[0][:-1],hdata2))
                max_hdata = max(max_hdata,np.max(hdata2))
            histtype = 1
            cols += ['yaxis']
            if request.vars.yaxis_log:
                min_hdata = np.min(hdata[hdata>0])
                if hdat is not None:
                    min_hdata = min(min_hdata,np.min(hdata2[hdata2>0]))
                (mins['yaxis'],maxs['yaxis']) = bound_round(min_hdata,max_hdata,logfun=True)
            elif np.min(hdata) >= 0:
                mins['yaxis'] = 0
                (junk,maxs['yaxis']) = bound_round(0,max_hdata,logfun=False)
            else:
                min_hdata = np.min(hdata)
                if hdat is not None:
                    min_hdata = min(min_hdata,np.min(hdata))
                (mins['yaxis'],maxs['yaxis']) = bound_round(min_hdata,max_hdata,logfun=False)
            if request.vars.ystat in ['mean','min','max'] and floatable(request.vars.y_min_bound) and floatable(request.vars.y_max_bound):
                (mins['yaxis'],maxs['yaxis']) = (floatable(request.vars.y_min_bound),floatable(request.vars.y_max_bound))
            elif request.vars.ystat in ['mean','min','max'] and floatable(request.vars.y_min_bound):
                (mins['yaxis'],junk) = bound_round(floatable(request.vars.y_min_bound),maxs['yaxis'],logfun=request.vars.yaxis_log)
            elif request.vars.ystat in ['mean','min','max'] and floatable(request.vars.y_max_bound):
                (junk,maxs['yaxis']) = bound_round(mins['yaxis'],floatable(request.vars.y_max_bound),logfun=request.vars.yaxis_log)
        colstr = ":".join([str(i) for i in range(1,len(cols)+1)])
        formats = ""
        for i in range(len(cols)):
            formats += "%" + str(dataout.dtype)
        #return repr(mins) + GRAPH_HIDE
    else:
        histtype = -1
        
    if request.vars.layerstats:
        stats = {}
        for i in cols:
            #stats['var_'+i] = request.vars[i]
            stats['min_'+i] = mins[i]
            stats['max_'+i] = maxs[i]
        return stats
    if request.vars.layerrender:
        for c in cols:
            mins[c] = floatable(request.vars.get("min_"+c),mins[c])
            maxs[c] = floatable(request.vars.get("max_"+c),maxs[c])

    if 'size' in cols and not request.vars.errorbars:
        scol = cols.index('size')
        size_xpos = 'right'
        size_ypos = 'top'
        size_method = "outside"
        legend = str(request.vars.legend).split('-')
        if len(legend) == 3:
            if legend[1] == "center":
                size_ypos = "center"
            if legend[1] == "bottom":
                size_ypos = "bottom"
            if legend[2] == "center":
                size_xpos = "center"
            if legend[2] == "left":
                size_xpos = "left"
        if legend[0] == "in":
            size_method = "opaque"
        if legend[0] == "hide":
            size_method = "hide"
        if request.vars.size_rev:
            size_max_val = mins['size']
            size_min_val = maxs['size']
            dataout[:,scol] = ((maxs['size']-dataout[:,scol]) /(0.25*(maxs['size']-mins['size'])))*(floatable_g0(request.vars.point_size,10)/10)+0.5
            size_max = 0.5
            size_min = 0.4*floatable_g0(request.vars.point_size,10)+0.5
        else:
            size_min_val = mins['size']
            size_max_val = maxs['size']
            dataout[:,scol] = ((dataout[:,scol] - mins['size']) /(0.25*(maxs['size']-mins['size'])))*(floatable_g0(request.vars.point_size,10)/10)+0.5
            size_min = 0.5
            size_max = 0.4*floatable_g0(request.vars.point_size,10)+0.5
        NUM_OF_SIZE_LABELS = 5
        if request.vars.size_rev:
            size_labels_val = np.arange(size_min_val,size_max_val-1,(size_max_val-size_min_val)/(NUM_OF_SIZE_LABELS-1))[::-1]
            size_labels = np.arange(size_min,size_max-1,(size_max-size_min)/(NUM_OF_SIZE_LABELS-1))
        else:
            size_labels_val = np.arange(size_min_val,size_max_val+1,(size_max_val-size_min_val)/(NUM_OF_SIZE_LABELS-1))
            size_labels = np.arange(size_min,size_max+1,(size_max-size_min)/(NUM_OF_SIZE_LABELS-1))
        #return repr(size_labels) + GRAPH_HIDE

    if request.vars.otype == "barpie":
        if request.vars.pie:
            nitems = min(intable(request.vars.groups,invalid=5),10)
        else:
            nitems = intable(request.vars.groups,invalid=5)
        frequency = {}
        for val in dataout[:,0]:
            if(frequency.has_key(str(val))):
                frequency[str(val)] = frequency[str(val)]+1
            else:
                frequency[str(val)] = 1
        if request.vars.bpsort == "alpha":
            keys = sorted(frequency.keys())
        else:
            keys = []
            keys = list(sorted(frequency, key=frequency.get, reverse=True))

        data = np.zeros((len(frequency), 2))

        if(request.vars.bpsort == "alpha"):
            counter = 0
            values = frequency.values()
            for value in values:
                data[counter][1] = frequency[keys[counter]]
                counter += 1
        else:
            counter = 0
            values = frequency.values()
            for value in values:
                data[counter][1] = value
                counter += 1
            data = data[data[:,1].argsort()[::-1]]
            
        data[:,0]=range(len(frequency))

        if(len(frequency) <= nitems):
            dataout = data
        elif request.vars.bpother:
            dataout = data[0:nitems]
            dataout[nitems-1][1] = np.sum(data[(nitems-1):,1])
            keys = keys[0:nitems]
            keys[nitems-1] = "Other"
        else:
            dataout = data[0:nitems]
            keys = keys[0:nitems]
        
    if request.vars.xlabel:
        xlabel = gpesc(request.vars.xlabel)
    else:
        xlabel = gpesc(axisinput.get("xaxis"))

    if request.vars.ylabel:
        ylabel = gpesc(request.vars.ylabel)
    elif request.vars.otype == "hist" and (stackedhist or request.vars.onedim):
        if stackedhist:
            get_stat = request.vars.cstat
        else:
            get_stat = request.vars.ystat
        if get_stat == "mean":
            ylabel = "Average of "+gpesc(axisinput.get("yaxis"))
        elif get_stat == "sum":
            ylabel = "Sum of "+gpesc(axisinput.get("yaxis"))
        elif get_stat == "min":
            ylabel = "Minimum of "+gpesc(axisinput.get("yaxis"))
        elif get_stat == "max":
            ylabel = "Maximum of "+gpesc(axisinput.get("yaxis"))
        elif get_stat == "std":
            ylabel = "Standard deviation of "+gpesc(axisinput.get("yaxis"))
        elif get_stat == "var":
            ylabel = "Variance of "+gpesc(axisinput.get("yaxis"))
        else:
            ylabel = "Count"
    else:
        ylabel = gpesc(axisinput.get("yaxis")  or "")

    if request.vars.clabel:
        clabel = gpesc(request.vars.clabel)
    elif request.vars.otype == "hist" and not request.vars.onedim:
        if request.vars.cstat == "mean":
            clabel = "Average of "+gpesc(axisinput.get("color"))
        elif request.vars.cstat == "sum":
            clabel = "Sum of "+gpesc(axisinput.get("color"))
        elif request.vars.cstat == "min":
            clabel = "Minimum of "+gpesc(axisinput.get("color"))
        elif request.vars.cstat == "max":
            clabel = "Maximum of "+gpesc(axisinput.get("color"))
        elif request.vars.cstat == "std":
            clabel = "Standard deviation of "+gpesc(axisinput.get("color"))
        elif request.vars.cstat == "var":
            clabel = "Variance of "+gpesc(axisinput.get("color"))
        else:
            clabel = "Count"
    else:
        clabel = gpesc(axisinput.get("color") or "")
    if clabel.startswith("lasso:"):
        clabel = "Point in Selection"
        
    font_size = floatable_g0(request.vars.font_size,12)
    font_size *= (intable(request.vars.picture_width) * intable(request.vars.picture_height))
    font_size /= (1218*708)
    font_size = max(font_size,10)
    
    if 'color' in cols:
        if len(str(maxs['color'])) > len(str((maxs['color'] + mins['color'])/2)):
            coffset = 11.8765/min(font_size,100) - .2853
        elif len(str(mins['color'])) > len(str((maxs['color'] + mins['color'])/2)):
            coffset = 11.8765/min(font_size,100) - .2853
        elif request.vars.color_log and maxs['color'] > 1000 and mins['color'] < 0.001:
            coffset = 11.8765/min(font_size,100) - .2853
        else:
            coffset = 22.2923/min(font_size,100) - .3576

    if request.vars.slabel:
        slabel = gpesc(request.vars.slabel)
    else:
        slabel = gpesc(axisinput.get("size") or "")

    if request.vars.zlabel:
        zlabel = gpesc(request.vars.zlabel)
    elif request.vars.otype == "hist" and request.vars.surface:
        if request.vars.zstat == "mean":
            zlabel = "Average of "+gpesc(axisinput.get("zaxis"))
        elif request.vars.zstat == "sum":
            zlabel = "Sum of "+gpesc(axisinput.get("zaxis"))
        elif request.vars.zstat == "min":
            zlabel = "Minimum of "+gpesc(axisinput.get("zaxis"))
        elif request.vars.zstat == "max":
            zlabel = "Maximum of "+gpesc(axisinput.get("zaxis"))
        elif request.vars.zstat == "std":
            zlabel = "Standard deviation of "+gpesc(axisinput.get("zaxis"))
        elif request.vars.zstat == "var":
            zlabel = "Variance of "+gpesc(axisinput.get("zaxis"))
        else:
            zlabel = "Count"
    else:
        zlabel = gpesc(axisinput.get("zaxis") or "")

    if 'zaxis' in cols and not request.vars.errorbars and not request.vars.surface:
        gtitle = gpesc(request.vars.title) or ("%s vs. %s vs. %s" % (ylabel,xlabel,zlabel))
    elif 'yaxis' in cols and request.vars.yaxis and not request.vars.onedim:
        gtitle = gpesc(request.vars.title) or ("%s vs. %s" % (ylabel,xlabel))
    elif request.vars.otype == "barpie":
        gtitle = gpesc(request.vars.title) or gpesc(axisinput.get("tsort") or "")
    else:
        gtitle = gpesc(request.vars.title) or xlabel
    if 'color' in cols and ('color' in output_strings) or (not request.vars.show_colorbar and not gpesc(request.vars.title) and request.vars.show_colorbar_en):
        if 'size' in cols and not request.vars.errorbars and not gpesc(request.vars.title) and size_method == "hide":
            gtitle += " (color: %s, point size: %s)" % (gpesc(clabel),gpesc(slabel))
        else:
            gtitle += " (color: %s)" % gpesc(clabel)
    elif 'size' in cols and not request.vars.errorbars and not gpesc(request.vars.title) and size_method == "hide":
        gtitle += " (point size: %s)" % gpesc(slabel)

    times.append(("Running the data through Gnuplot",datetime.now()))
    #http://docs.python.org/faq/programming.html#is-there-an-equivalent-of-c-s-ternary-operator
    if intable(request.vars.point_type) in [0,1,4,5] and not request.vars.download and not (request.vars.otype == "scatter" and request.vars.map_1):
        default_terminal = FG_GNUPLOT_TERM
    else:
        default_terminal = "pngcairo"
    #return repr(default_terminal) + GRAPH_HIDE
    fontstr2 = ''
    if default_terminal == "png":
        for ff in range(len(font_list_ttf)):
            if font_list_ttf[ff][0] == gpesc(request.vars.font):
                fontstr2 = 'font "'+font_list_ttf[ff][1]+',%s"'
        if fontstr2 == '':
            fontstr2 = 'font "'+os.path.join(request.folder,"static","fonts","ttf","Bitstream-Vera-Sans.ttf")+',%s"'
    else:
        for ff in range(len(font_list)):
            if font_list[ff][0] == gpesc(request.vars.font):
                fontstr2 = 'font "'+font_list[ff][1]+',%s"'
        if fontstr2 == '':
            fontstr2 = 'font "Bitstream-Vera-Sans,%s"'
    fontstr = fontstr2 % min(font_size,100)
    #return repr(fontstr) + GRAPH_HIDE
    if request.vars.point_type == "0":
        request.vars.point_type = "1"
        request.vars.point_size = "1"
    if request.vars.otype == "hist":
        core_count = 1
        predicted_time = None
    elif request.vars.otype == "barpie":
        core_count = 1
        predicted_time = None
    elif request.vars.otype == "scatter" and request.vars.linechart:
        core_count = 1
        predicted_time = None
    elif not FG_ENABLE_ACCELERATION:
        core_count = 1
        predicted_time = None
    else:
        core_times = []
        for core_count in range(1,FG_MAX_CORES+1):
            core_times.append(gptime(plotcount/core_count,color=request.vars.color,size=request.vars.size,point_type=intable(request.vars.point_type)) + mergetime(pixels=intable(request.vars.picture_width)*intable(request.vars.picture_height),cores=core_count))
        core_count = core_times.index(min(core_times))+1
        predicted_time = min(core_times)
        #return repr(core_count) + GRAPH_HIDE
    if request.vars.pdftype == "a4":
        pdfsize = "size 29.7cm,21.0cm"
    elif request.vars.pdftype == "custom" and request.vars.pdfunits == "cm":
        pdfsize = "size %scm,%scm" % (floatable_g0(request.vars.pdfwidth,29.7),floatable_g0(request.vars.pdfheight,21))
    elif request.vars.pdftype == "custom":
        pdfsize = "size %s,%s" % (floatable_g0(request.vars.pdfwidth,11),floatable_g0(request.vars.pdfheight,8.5))
    else:
        pdfsize = "size 11,8.5"
    fdict = {"png":(core_count,default_terminal,"png","png",fontstr,"size %s,%s" % (gpesc(request.vars.picture_width),gpesc(request.vars.picture_height))),
             "gif":(core_count,default_terminal,"gif","gif",fontstr,"size %s,%s" % (gpesc(request.vars.picture_width),gpesc(request.vars.picture_height))),
             "jpg":(core_count,default_terminal,"jpg","jpg",fontstr,"size %s,%s" % (gpesc(request.vars.picture_width),gpesc(request.vars.picture_height))),
             "svg":(1,"svg","svg","svg","","size %s,%s" % (gpesc(request.vars.picture_width),gpesc(request.vars.picture_height))),
             "ps":(1,"postscript landscape","ps","ps","",""),
             "pdf":(1,"pdf","pdf","pdf","",pdfsize)}
    (cores,terminal,internal_type,external_type,font,size) = fdict[request.vars.download and request.vars.filetype or "png"]
    if request.vars.invert_colors and core_count == 1:
        bg = "000000"
        fg = "FFFFFF"
        gridcolor = (terminal=="png" and "333333" or "BBBBBB")
        datacolor = request.vars.datacolor or "E41A1C"
        if datacolor[0] == "#":
            datacolor = datacolor[1:]
        map_bg = "222222"
        map_country_color = "444444"
        map_road_color = "333333"
        map_outline_color_1 = "777777"
        map_outline_color_2 = "999999"
        hcolor = "FAD071"
    elif request.vars.invert_colors and core_count > 1:
        bg = "FFFFFF"
        fg = "000000"
        gridcolor = (terminal=="png" and "333333" or "BBBBBB")
        datacolor = request.vars.datacolor or "E41A1C"
        if datacolor[0] == "#":
            datacolor = datacolor[1:]
        datacolor = invert(datacolor)
        map_bg = "DDDDDD"
        map_country_color = "BCBCBC"
        map_road_color = "CDCDCD"
        map_outline_color_1 = "898989"
        map_outline_color_2 = "676767"
        hcolor = invert("FAD071")
    else:
        bg = "FFFFFF"
        fg = "000000"
        gridcolor = (terminal=="png" and "DDDDDD" or "555555")
        datacolor = request.vars.datacolor or "E41A1C"
        if datacolor[0] == "#":
            datacolor = datacolor[1:]
        map_bg = "DDDDDD"
        map_country_color = "F8F8F8"
        map_road_color = "CCCCCC"
        map_outline_color_1 = "999999"
        map_outline_color_2 = "777777"
        hcolor = "79CCF2"
    datacolor = datacolor.upper()
    p = [None for i in range(cores)]
    q = [Queue() for i in range(cores)]
    embspl = []
    for key in request.vars.keys():
        if key.startswith("emb_type_"):
            embspl.append(key[9:])
    box_invert = False
    if (request.vars.map_1 or request.vars.map_2) and request.vars.otype == "scatter":
        if request.vars.wm_label in ["opaque","inverted"]:
            maplevel = get_cities(mins['yaxis'],maxs['yaxis'],mins['xaxis'],maxs['xaxis'],graph_dir+queryid,intable(request.vars.wm_cities,25),40,8)
        else:
            maplevel = get_cities(mins['yaxis'],maxs['yaxis'],mins['xaxis'],maxs['xaxis'],graph_dir+queryid,intable(request.vars.wm_cities,25),70,8)
        if request.vars.wm_label == "inverted":
            box_invert = True
        map_area = abs(maxs['xaxis']-mins['xaxis'])*abs(maxs['yaxis']-mins['yaxis'])
        
    #return repr(embspl) + GRAPH_HIDE
    for i in range(cores):
        out = []
        curve = ""
        if 'color' in output_strings or ('yaxis' in output_strings and stackedhist):
            axis_to_check = stackedhist and 'yaxis' or 'color'
            for pp in range(len(pal)):
                if pal[pp][0] == gpesc(request.vars.palette):
                    pal_colors = colorFetch(pal[pp],len(output_strings[axis_to_check]))
                    break
            else:
                pal_colors = colorFetch(pal[0],4)
            if request.vars.color_rev:
                pal_colors = pal_colors[::-1]
            output_string_pairs = zip(output_strings[axis_to_check],pal_colors)
            if len(output_string_pairs) > 15:
                output_string_pairs = output_string_pairs[::max(len(output_string_pairs)/10,1)]
            for osp in output_string_pairs:
                curve += ', 1/0 with points pt 7 lc rgb "%s" t "%s"' % (osp[1],osp[0])
            if stackedhist:
                out.append('set key top left')
            else:
                out.append('set key bottom right')
        elif stackedhist:
            for pp in range(len(pal)):
                if pal[pp][0] == gpesc(request.vars.palette):
                    pal_colors = colorFetch(pal[pp],bins[1])
                    break
            else:
                pal_colors = colorFetch(pal[0],4)
            pal_text = []
            pal_bounds = old_hbounds[1]
            if request.vars.yaxis_log:
                pal_bounds = 10 ** pal_bounds
            for j in range(bins[1]):
                if j == 0:
                    pal_text.append("Below %g" % pal_bounds[1])
                elif j == (bins[1]-1):
                    pal_text.append("Above %g" % pal_bounds[-2])
                else:
                    pal_text.append("%g to %g" % (pal_bounds[j],pal_bounds[j+1]))
            output_string_pairs = zip(pal_text,pal_colors)
            if len(output_string_pairs) > 15:
                output_string_pairs = output_string_pairs[:-1:max(len(output_string_pairs)/10,1)] + [output_string_pairs[-1]]
            for osp in output_string_pairs:
                curve += ', 1/0 with points pt 7 lc rgb "%s" t "%s"' % (osp[1],osp[0])
            out.append('set key top left')
            #return repr(curve) + GRAPH_HIDE
        transparent = (i != 0) or request.vars.transparent
        out.append("set terminal %s noenhanced %s %s %s" % (terminal,font,(transparent and "transparent" or ("background '#%s'" % (box_invert and fg or bg))),size))
        out.append("set encoding utf8")
        if request.vars.invert_colors and core_count == 1 and not transparent:
            out.append('set object 997 rectangle from screen 0,0 to screen 1,1 fillcolor rgb"#000000" behind')
        elif box_invert and core_count == 1 and not transparent:
            out.append('set object 997 rectangle from screen 0,0 to screen 1,1 fillcolor rgb"#FFFFFF" behind')
        if request.vars.map_1 and request.vars.otype == "scatter" and i == 0:
            out.append("set object 998 rectangle from graph 0, graph 0 to graph 1, graph 1 behind fc rgb '#%s' noborder" % map_bg)
        out.append('set output "%s"' % (graph_name % (cores==1 and "comp" or i+1,internal_type)) )
        out.append('set border lt rgb "#%s"' % fg)
        out.append('set title "%s" tc rgb "#%s"' % (gtitle,i==(cores-1) and fg or bg))
        out.append('set key tc rgb "#%s"' % (i==(cores-1) and fg or bg))
        
        if request.vars.trendline:
            poly = np.polyfit(dataout[:,0],dataout[:,1],1)
            curvestr = "%.4g*x + %.4g" % tuple(poly)
            curvestr = curvestr.replace("e","*10**")
            polylabel = "%s (r^2=%.4g)" % (curvestr,np.corrcoef(np.transpose(dataout[:,:2]))[0][1])
            polylabel = polylabel.replace("**","^")
            curve += ', %s lw 3 lc rgb "#%s" title "%s"' % (curvestr, fg, polylabel)
        for emb in embspl:
            if request.vars.xaxis_log:
                if floatable(request.vars["emb_xpos_"+emb],1) <= 0:
                    return errormessage("Check your lines and annotations",details="One of your lines and annotations has an X position of zero or less, which can't be done on a log scale.")
                if floatable(request.vars["emb_xstart_"+emb],1) <= 0:
                    return errormessage("Check your lines and annotations",details="One of your lines and annotations has an X position of zero or less, which can't be done on a log scale.")
                if floatable(request.vars["emb_xend_"+emb],1) <= 0:
                    return errormessage("Check your lines and annotations",details="One of your lines and annotations has a Y position of zero or less, which can't be done on a log scale.")
            if request.vars.yaxis_log:
                if floatable(request.vars["emb_ypos_"+emb],1) <= 0:
                    return errormessage("Check your lines and annotations",details="One of your lines and annotations has a Y position of zero or less, which can't be done on a log scale.")
                if floatable(request.vars["emb_ystart_"+emb],1) <= 0:
                    return errormessage("Check your lines and annotations",details="One of your lines and annotations has a Y position of zero or less, which can't be done on a log scale.")
                if floatable(request.vars["emb_yend_"+emb],1) <= 0:
                    return errormessage("Check your lines and annotations",details="One of your lines and annotations has an X position of zero or less, which can't be done on a log scale.")
            if request.vars["emb_type_"+emb] == "text" and request.vars["emb_xpos_"+emb] and request.vars["emb_ypos_"+emb] and i == (cores-1):
                emb_font_size = floatable_g0(request.vars["emb_size_"+emb], font_size)
                emb_font_size *= (intable(request.vars.picture_width) * intable(request.vars.picture_height))
                emb_font_size /= (1218*708)
                emb_font_size = max(emb_font_size,10)
                emb_font_size = min(emb_font_size,100)
                #return repr(emb_font_size) + GRAPH_HIDE
                out.append('set label "%s" at %s,%s front tc rgb "#%s" %s %s' % (gpesc(request.vars["emb_text_"+emb].replace("\n","").replace("\r","")),floatable(request.vars["emb_xpos_"+emb]),floatable(request.vars["emb_ypos_"+emb]),colorable(request.vars["emb_color_"+emb],fg) or fg, fontstr2 % emb_font_size,floatable(request.vars["emb_angle_"+emb]) and "rotate by " + str(floatable(request.vars["emb_angle_"+emb])) or "norotate"))
            if request.vars["emb_type_"+emb] == "curve" and request.vars["emb_fx_"+emb]:
                curvestr = sanitize(request.vars["emb_fx_"+emb],None,trans2,textonly=True)
                if request.vars["emb_label_"+emb]:
                    curvelabel = gpesc(request.vars["emb_label_"+emb])
                else:
                    curvelabel = gpesc(request.vars["emb_fx_"+emb])
                if curvestr.startswith("VALID"):
                    curve += ', %s lw %s lc rgb "#%s" %s' % (curvestr[5:],intable(request.vars["emb_size_"+emb]) or 1,colorable(request.vars["emb_color_"+emb],fg) or fg, curvelabel.strip() and ('title "%s"'%curvelabel) or "notitle")
                elif "%" in request.vars["emb_fx_"+emb]:
                    return errormessage("Check the curve: %s" % request.vars["emb_fx_"+emb],details="Modulus is not supported on curves.")
                else:
                    return errormessage("Check the curve: %s" % request.vars["emb_fx_"+emb],details=curvestr)
            if request.vars["emb_type_"+emb] == "trendline" and request.vars.otype == "scatter":
                poly = np.polyfit(dataout[:,0],dataout[:,1],1)
                curvestr = "%.4g*x + %.4g" % tuple(poly)
                if request.vars["emb_label_"+emb]:
                    polylabel = gpesc(request.vars["emb_label_"+emb])
                else:
                    polylabel = "%s (r^2=%.4f)" % (curvestr,np.corrcoef(np.transpose(dataout[:,:2]))[0][1])
                curve += ', %s lw %s lc rgb "#%s" %s' % (curvestr,intable(request.vars["emb_size_"+emb]) or 1,colorable(request.vars["emb_color_"+emb],fg) or fg, polylabel.strip() and ("title '"+polylabel+"'") or "notitle")
            if request.vars["emb_type_"+emb] == "arrow" and request.vars["emb_xstart_"+emb] and request.vars["emb_ystart_"+emb] and request.vars["emb_xend_"+emb] and request.vars["emb_yend_"+emb] and i == (cores-1):
                out.append('set arrow from %s,%s to %s,%s lw %s front size %s,30 lc rgb "#%s"' %  (floatable(request.vars["emb_xstart_"+emb]),floatable(request.vars["emb_ystart_"+emb]),floatable(request.vars["emb_xend_"+emb]),floatable(request.vars["emb_yend_"+emb]),intable(request.vars["emb_size_"+emb]) or 3,intable(request.vars["emb_size_"+emb]) or 3,colorable(request.vars["emb_color_"+emb],fg) or fg))
            if request.vars["emb_type_"+emb] == "line" and request.vars["emb_xstart_"+emb] and request.vars["emb_ystart_"+emb] and request.vars["emb_xend_"+emb] and request.vars["emb_yend_"+emb] and i == (cores-1):
                out.append('set arrow from %s,%s to %s,%s nohead lw %s front lc rgb "#%s"' %  (floatable(request.vars["emb_xstart_"+emb]),floatable(request.vars["emb_ystart_"+emb]),floatable(request.vars["emb_xend_"+emb]),floatable(request.vars["emb_yend_"+emb]),intable(request.vars["emb_size_"+emb]) or 1,colorable(request.vars["emb_color_"+emb],fg) or fg))
        if i==0 and not request.vars.hidegrid:
            out.append('set grid %s lc rgb "#%s"' % ((request.vars.otype=="barpie") and "ytics" or "",gridcolor))
        if request.vars.xaxis_log and (request.vars.otype=="scatter"):
            out.append('set logscale x')
        if request.vars.yaxis_log and (request.vars.otype=="scatter" or request.vars.onedim):
            out.append('set logscale y')
        elif request.vars.color_log and (request.vars.otype=="hist" and stackedhist):
            out.append("set logscale y")
        out.append('set format x "%g"')
        out.append('set format y "%g"')
        out.append('set samples %s' % intable(request.vars.picture_width))
        if request.vars.otype != "barpie":
            if request.vars.xaxis_rev:
                out.append("set xrange [%s:%s]" % (nanable(maxs["xaxis"]),nanable(mins["xaxis"])))
            else:
                out.append("set xrange [%s:%s]" % (nanable(mins["xaxis"]),nanable(maxs["xaxis"])))
            out.append('set xlabel "%s" tc rgb "#%s"' % (xlabel,i==(cores-1) and fg or bg))
            out.append('set xtics tc rgb "#%s"' % (i==(cores-1) and fg or bg))
            if 'xaxis' in output_strings and not (request.vars.hist and request.vars.surface) and 'zaxis' not in cols:
                output_string_pairs = [(output_strings['xaxis'][j],j) for j in range(len(output_strings['xaxis']))]
                output_string_pairs = output_string_pairs[::max(len(output_string_pairs)/10,1)]
                #return repr(output_string_pairs) + GRAPH_HIDE
                out.append('set xtics (%s) rotate by 45 right offset' % ", ".join(['"%s" %s' % osp for osp in output_string_pairs]))
            if request.vars.yaxis_rev:
                out.append("set yrange [%s:%s]" % (nanable(maxs["yaxis"]),nanable(mins["yaxis"])))
            else:
                out.append("set yrange [%s:%s]" % (nanable(mins["yaxis"]),nanable(maxs["yaxis"])))
            out.append('set ylabel "%s" tc rgb "#%s"' % (ylabel,i==(cores-1) and fg or bg))
            out.append('set ytics tc rgb "#%s"' % (i==(cores-1) and fg or bg))
            if 'yaxis' in output_strings and not (request.vars.hist and request.vars.surface) and 'zaxis' not in cols and not stackedhist:
                output_string_pairs = [(output_strings['yaxis'][j],j) for j in range(len(output_strings['yaxis']))]
                output_string_pairs = output_string_pairs[::max(len(output_string_pairs)/10,1)]
                out.append('set ytics (%s) rotate by 45 right offset' % ", ".join(['"%s" %s' % osp for osp in output_string_pairs]))
            if 'zaxis' in cols:
                if request.vars.zaxis_rev:
                    out.append("set zrange [%s:%s]" % (nanable(maxs["zaxis"]),nanable(mins["zaxis"])))
                else:
                    out.append("set zrange [%s:%s]" % (nanable(mins["zaxis"]),nanable(maxs["zaxis"])))
                if request.vars.zaxis_log:# and request.vars.otype == "scatter":
                    out.append('set logscale z')
                out.append('set zlabel "%s" tc rgb "#%s" rotate by 90' % (zlabel,i==(cores-1) and fg or bg))
                out.append('set ztics tc rgb "#%s"' % (i==(cores-1) and fg or bg))
                if floatable(request.vars.angle1) is not None:
                    angle1 = floatable(request.vars.angle1)
                else:
                    angle1 = 60
                if floatable(request.vars.angle2) is not None:
                    angle2 = floatable(request.vars.angle2)
                else:
                    angle2 = 30
                if floatable(request.vars.zscale) is not None:
                    zscale = floatable(request.vars.zscale)
                else:
                    zscale = 1.0
                out.append('set view %s,%s,%s' % (angle1, angle2, zscale))
                out.append('set format z "%g"')
            if 'color' in cols:
                for pp in range(len(pal)):
                    if pal[pp][0] == gpesc(request.vars.palette):
                        out.append(palette(pal[pp][1],invert=(request.vars.invert_colors and core_count > 1),reverse=request.vars.color_rev))
                        break
                else:
                    out.append(palette(pal[0][1],invert=(request.vars.invert_colors and core_count > 1),reverse=request.vars.color_rev))
                if (request.vars.show_colorbar and 'color' not in output_strings) or (not request.vars.show_colorbar_en):
                    out.append('set colorbox vert')
                else:
                    out.append("unset colorbox")
                if "size" not in cols:
                    out.append("set rmargin %s" % (-0.0024*intable(request.vars.picture_width)+0.0413*font_size+5.2064))
                out.append("set cbrange [%s:%s]" % (nanable(mins["color"]),nanable(maxs["color"])))
                if request.vars.color_log:
                    out.append('set logscale cb')
                out.append('set cblabel "%s" offset %.2f,0 tc rgb "#%s"' % (clabel,coffset,i==(cores-1) and fg or bg))
                out.append('set cbtics tc rgb "#%s"' % (i==(cores-1) and fg or bg))
                out.append('set format cb "%g"')
            mapstr = ""
            mapstr2 = ""
            if request.vars.map_1 and request.vars.otype == "scatter" and i == 0:
                mapstr += '"%s" w filledcurves lc rgb "#%s" notitle, ' % (os.path.join(request.folder,"static","map","countries-new.txt"),map_country_color)
            if request.vars.map_1 and request.vars.otype == "scatter" and i == 0 and map_area < 4000:
                mapstr += '"%s" w lines lc rgb "#%s" notitle, ' % (os.path.join(request.folder,"static","map","roads-important.txt"),map_road_color)
            if request.vars.map_1 and request.vars.otype == "scatter" and i == 0 and map_area < 8500:
                mapstr += '"%s" w lines lw 1 lc rgb "#%s" notitle, ' % (os.path.join(request.folder,"static","map","states-major.txt"),map_outline_color_1)
            if request.vars.map_1 and request.vars.otype == "scatter" and i == 0 and map_area < 8500:
                mapstr += '"%s" w lines lw 1 lc rgb "#%s" notitle, ' % (os.path.join(request.folder,"static","map","countries-new.txt"),map_outline_color_2)
            if request.vars.map_1 and request.vars.otype == "scatter" and i == 0 and map_area >= 8500:
                mapstr += '"%s" w lines lw 1 lc rgb "#%s" notitle, ' % (os.path.join(request.folder,"static","map","countries-new.txt"),map_outline_color_1)
            if (request.vars.map_1 or request.vars.map_2) and request.vars.otype == "scatter" and i == (cores-1):
                if request.vars.wm_label in ["opaque","inverted"]:
                    box_color = box_invert and bg or fg
                    out.append("set style textbox opaque noborder")
                    mapstr2 += ', "%s%s-cities.txt" u 3:2:4 w labels boxed left offset 2,0 tc rgb "#%s" notitle, "" u 3:2 w points pt 7 lc rgb "#%s" notitle' % (graph_dir,queryid,box_color,fg)
                    mapstr2 += ', "%s%s-cities2.txt" u 3:2:4 w labels boxed right offset -2,0 tc rgb "#%s" notitle, "" u 3:2 w points pt 7 lc rgb "#%s" notitle' % (graph_dir,queryid,box_color,fg)
                else:
                    mapstr2 += ', "%s%s-cities.txt" u 3:2:4 w labels left offset 1,0 tc rgb "#%s" notitle, "" u 3:2 w points pt 7 lc rgb "#%s" notitle' % (graph_dir,queryid,fg,fg)
                    mapstr2 += ', "%s%s-cities2.txt" u 3:2:4 w labels right offset -1,0 tc rgb "#%s" notitle, "" u 3:2 w points pt 7 lc rgb "#%s" notitle' % (graph_dir,queryid,fg,fg)
            if hdat is not None and request.vars.otype == "scatter" and i == 0:
                if gptime(hpoints,point_type=7) > (gptime(hpoints,point_type=5) +1.0):
                    hpointtype = 5
                else:
                    hpointtype = 7
                mapstr += "'-' using %s binary record=%s format='%s' lt %s ps 3 lc rgb '#%s' notitle," % (hcolstr,hpoints,hformats,hpointtype,hcolor)
        if request.vars.otype == "hist":
            if histtype == 1:
                out.append('set style fill solid 1.0')
                out.append('set boxwidth %s' % (hbounds[0][1] - hbounds[0][0]))
                if hdat is not None:
                    out.append("plot '-' u %s binary record=%s format=\"%s\" with boxes lt rgb \"#%s\" notitle, '-' u %s binary format=\"%s\" with boxes lt rgb \"#%s\" notitle %s; show variables all" % (colstr,len(dataout),formats,datacolor,colstr,formats,hcolor,curve))
                else:
                    out.append("plot '-' u %s binary format=\"%s\" with boxes lt rgb \"#%s\" notitle %s; show variables all" % (colstr,formats,datacolor,curve))
            elif histtype == 4:
                out.append('set style fill solid 1.0')
                out.append('set boxwidth %s' % (hbounds[0][1] - hbounds[0][0]))
                for pp in range(len(pal)):
                    if pal[pp][0] == gpesc(request.vars.palette):
                        pal_colors = colorFetch(pal[pp],bins[1])
                        break
                else:
                    pal_colors = colorFetch(pal[0],bins[1])
                if request.vars.color_rev:
                    pal_colors = pal_colors[::-1]
                plot_string = []
                for j in range(bins[1]):
                    plot_string.append("'-' u %s binary record=%s format=\"%s\" with boxes lt rgb \"%s\" notitle" % (colstr,bins[0],formats,pal_colors[j]))
                out.append("plot %s %s; show variables all" % (", ".join(plot_string),curve) )
            elif request.vars.surface:
                out.append("set pm3d interpolate %s,%s" % (500/intable(bins[0]),500/intable(bins[1])))
                #out.append("set view map")
                #out.append("set cntrparam levels 5")
                #out.append("set contour both")
                if floatable(request.vars.angle1) is not None:
                    angle1 = floatable(request.vars.angle1)
                else:
                    angle1 = 60
                if floatable(request.vars.angle2) is not None:
                    angle2 = floatable(request.vars.angle2)
                else:
                    angle2 = 30
                if floatable(request.vars.zscale) is not None:
                    zscale = floatable(request.vars.zscale)
                else:
                    zscale = 1.0
                out.append('set view %s,%s,%s' % (angle1,angle2,zscale))
                out.append("splot '-' u %s w pm3d notitle %s; show variables all" % (colstr,curve))
                j = 0
                for line in dataout:
                    out.append(" ".join([str(a) for a in line]))
                    j += 1
                    if j % bins[1] == 0:
                        out.append("")
            else:
                out.append("plot '-' u %s binary format=\"%s\" with image notitle %s; show variables all" % (colstr,formats,curve))
        elif request.vars.otype == "barpie":
            if request.vars.pie:
                if(request.vars.bpcolor == "col1"):
                    colors = ["\'#FF1C5D\'", "\'#FFFD1C\'", "\'#3BFF00\'", "\'#FF0C00\'", "\'#1C2BFF\'", "\'#9019FF\'",  "\'#2CFFDA\'", "\'#FF6C20\'", "\'#C1A1FF\'","\'#63FF93\'"]
                else:
                    colors = ["\'#a6cee3\'", "\'#1f78b4|'", "\'#b2df8a\'", "\'#33a02c\'", "\'#fb9a99\'", "\'#e31a1c\'", "\'#fdbf6f\'",  "\'#ff7f00\'", "\'#cab2d6\'", "\'#6a3d9a\'"]
                out += ["set angles degree",
                "set yrange [0:1]",
                "set xrange [0:1]",
                "unset xtics",
                "unset ytics",
                "set style fill solid 1.0 border -1"]
                if request.vars.bpother:
                    totalCounts = len(a)
                else:
                    totalCounts = sum(row[1] for row in dataout)
                colorCounter = 0
                angle = 0
                angles = []
                endAngle = 0
                for b in range(len(dataout)):
                    percentage = dataout[b][1]/totalCounts
                    angle = endAngle
                    endAngle = angle + percentage*360.
                    angles.append([angle, endAngle])
                    curColor = colors[colorCounter]
                    center = 0.5+len(dataout)*0.05/2.
                    sliced = request.vars.bpsliced
                    if(request.vars.bplabel):
                        if(sliced and dataout[b][1]==maxSlice):
                            circleCommand = "set obj {0} circle arc [{1}:{2}] fc rgb {3} at screen {4}, {5} size screen 0.25 front".format((b+1), angle, endAngle, colors[colorCounter], .4+.03*math.cos(math.radians((angle+endAngle)/2.)), 0.5+.03*math.sin(math.radians((angle+endAngle)/2.)))
                            sliced = False
                        else:
                            circleCommand = "set obj {0} circle arc [{1}:{2}] fc rgb {3} at screen 0.4, 0.5 size screen 0.25 front".format((b+1), angle, endAngle, colors[colorCounter])
                        labelCommand = "set label '{0}: {1:.2f}%' at 0.73, {2}  point pt 5 lc rgb {3} ps 2 offset 1,-.2 tc rgb '#{4}'".format(keys[b], percentage*100., (center-b*.05), curColor, fg)
                    else:
                        if(sliced and dataout[b][1]==maxSlice):
                            circleCommand = "set obj {0} circle arc [{1}:{2}] fc rgb {3} at screen {4}, {5} size screen 0.25 front".format((b+1), angle, endAngle, colors[colorCounter], .5+.03*math.cos(math.radians((angle+endAngle)/2.)), 0.5+.03*math.sin(math.radians((angle+endAngle)/2.)))
                            sliced = False
                        else:
                            circleCommand = "set obj {0} circle arc [{1}:{2}] fc rgb {3} at screen 0.5, 0.5 size screen 0.25 front".format((b+1), angle, endAngle, colors[colorCounter])
                        xpoint = .4*math.cos(math.radians((angle+endAngle)/2.))+.5
                        ypoint = .4*math.sin(math.radians((angle+endAngle)/2.))+.5
                        labelCommand = "set label '{0}: {1:.2f}%' at {2}, {3} center tc rgb '#{4}'".format(keys[b], percentage*100., xpoint, ypoint, fg)
                    out.append(circleCommand)
                    out.append(labelCommand)
                    colorCounter += 1
                out.append("plot 1 notitle; show variables all")
            else:
                xtics = "set xtics ("
                for b in range(len(dataout)):
                    if (b == (len(dataout) - 1)):
                        xtics += "'" + str(keys[b])  + "' " + str(b) + ")"
                    else:
                        xtics += "'" + str(keys[b]) + "' " + str(b) + ", "
                bpfontsize = floatable_g0(request.vars.bpfontsize,font_size)
                bpfontsize *= (intable(request.vars.picture_width) * intable(request.vars.picture_height))
                bpfontsize /= (1218*708)
                bpfontsize = max(bpfontsize,10)
                bpfontsize = min(bpfontsize,100)
                out += ["set boxwidth 0.5",
                    "set style fill solid",
                    "set yrange[0:{0}]".format(bound_round(0,dataout[:, 1].max() + 0.5)[1]),
                    xtics,
                    "set xtics rotate by 45 right offset 1 font ',%s'" % bpfontsize,
                    "plot '-' using 1:2 binary format='%%float64%%float64' with boxes lc rgb '#%s' notitle %s; show variables all" % (datacolor,curve)]
        elif request.vars.errorbars and 'size' in cols and 'zaxis' in cols:
            out.append("plot %s '-' u %s binary format=\"%s\" with xyerrorbars %s ps %s pt %s notitle %s; show variables all" % (mapstr,colstr,formats,('color' in cols and 'palette' or ('lc rgb "#'+datacolor+'"')),str(floatable_g0(request.vars.point_size,10)/10),gpesc(request.vars.point_type or "1"),curve))
        elif request.vars.errorbars and 'size' in cols:
            out.append("plot %s '-' u %s binary format=\"%s\" with xerrorbars %s ps %s pt %s notitle %s; show variables all" % (mapstr,colstr,formats,('color' in cols and 'palette' or ('lc rgb "#'+datacolor+'"')),str(floatable_g0(request.vars.point_size,10)/10),gpesc(request.vars.point_type or "1"),curve))
        elif request.vars.errorbars and 'zaxis' in cols:
            out.append("plot %s '-' u %s binary format=\"%s\" with yerrorbars %s ps %s pt %s  notitle %s; show variables all" % (mapstr,colstr,formats,('color' in cols and 'palette' or ('lc rgb "#'+datacolor+'"')),str(floatable_g0(request.vars.point_size,10)/10),gpesc(request.vars.point_type or "1"),curve))
        elif request.vars.linechart:
            out.append("%splot %s '-' u %s binary format=\"%s\" with linespoints %s %s pt %s lw %s notitle %s %s; show variables all" % (('zaxis' in cols and "s" or ""),mapstr,colstr,formats,('color' in cols and 'palette' or ('lc rgb "#'+datacolor+'"')),('size' in cols and 'ps variable' or ("ps "+str(floatable_g0(request.vars.point_size,10)/10))),gpesc(request.vars.point_type or "1"),floatable_g0(request.vars.point_size,10)/5,curve,mapstr2))
        else:
            if 'size' in cols and size_method != "hide":
                out.append('set key box lc rgb "#%s" %s %s %s width 2 spacing %s title " %s %s"' % (fg, size_method, size_xpos, size_ypos, max(size_min,size_max),gpesc(slabel),"\\n" * int((floatable_g0(request.vars.point_size,10)-1)/5)))
                for sl in range(NUM_OF_SIZE_LABELS):
                    if size_labels_val[1] == int(size_labels_val[1]):
                        mapstr2 += ', 1/0 with points pt %s ps %s lc rgb "#%s" title "%i"' % (gpesc(request.vars.point_type or "1"), size_labels[sl], fg, size_labels_val[sl])
                    else:
                        mapstr2 += ', 1/0 with points pt %s ps %s lc rgb "#%s" title "%s"' % (gpesc(request.vars.point_type or "1"), size_labels[sl], fg, size_labels_val[sl])
            out.append("%splot %s '-' u %s binary format=\"%s\" with points %s %s pt %s notitle %s %s; show variables all" % (('zaxis' in cols and "s" or ""),mapstr,colstr,formats,('color' in cols and 'palette' or ('lc rgb "#'+datacolor+'"')),('size' in cols and 'ps variable' or ("ps "+str(floatable_g0(request.vars.point_size,10)/10))),gpesc(request.vars.point_type or "1"),curve,mapstr2))
        #return "<br>".join(out) + GRAPH_HIDE
        #return repr(dataout) + GRAPH_HIDE
        #print "Transfer data for process %s." % i
        if (histtype == 2 and request.vars.surface):
            p[i] = Process(target=plot, args=('\n'.join(out),q[i]))
        else:
            #junk = '\n'.join(out + [dataout[i::cores].tostring()])
            if hdat is not None and (request.vars.onedim and request.vars.otype=="hist"):
                p[i] = Process(target=plot, args=('\n'.join(out + [dataout[i::cores].tostring()+dataout_highlight.tostring() ]),q[i]))
            elif hdat is not None and i == 0:
                p[i] = Process(target=plot, args=('\n'.join(out + [dataout_highlight.tostring() +dataout[i::cores].tostring()]),q[i]))
            else:
                p[i] = Process(target=plot, args=('\n'.join(out + [dataout[i::cores].tostring()]),q[i]))
        #print "Starting helper process %s (pid=%s)." % (i,p[i].pid)
        p[i].start()
    success = True
    for i in range(cores):
        print "BEGIN %5s     helper process %s" % (p[i].pid,i)
        p[i].join()
        if q[i].get() < 0:
            success = False
        print "END   %5s %3s helper process %s" % (p[i].pid,p[i].exitcode,i)
    
    if not success:
        return errormessage("Server timeout",details="This can result from excessive settings or handling too much data. Check your settings and try again.")

    #Get stats
    #return "<pre style='text-align:left'>" + str(q[0].get()[1]).replace("\\n","<br>") + "</pre>" + GRAPH_HIDE
    sstr = (q[0].get()[1].splitlines())
    stats = {}
    for s in sstr:
        sspl = s.split("=",1)
        if len(sspl) > 1:
            stats[sspl[0].strip()] = sspl[1].strip()

    if cores > 1:
        times.append(("Merging images",datetime.now()))
        #http://studio.imagemagick.org/pipermail/magick-users/2003-October/011071.html
        process = subprocess.Popen("%s %s %s -flatten %s" % (FG_IMAGEMAGICK_PATH," ".join([graph_name % (i,internal_type) for i in range(1,cores+1)]),request.vars.invert_colors and core_count > 1 and "-negate" or "",graph_name % ("comp",external_type)),shell=True,stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
        print "BEGIN %5s     Imagemagick process" % process.pid
        mstats = process.communicate("")
        print "END   %5s %3s Imagemagick process" % (process.pid,process.returncode)

    #Starting download
    if request.vars.download:
        #https://en.wikipedia.org/wiki/Mime_type
        #https://en.wikipedia.org/wiki/HTTP_header
        #http://web2py.com/examples/default/examples
        #http://stackoverflow.com/questions/6591931/getting-file-size-in-python
        if external_type=="pdf":
            mimetype = "application/pdf"
        elif external_type=="ps":
            mimetype = "application/postscript"
            with open(graph_name % ("comp",external_type),'r') as f:
                text = f.read()
            with open(graph_name % ("comp",external_type),'w') as f:
                f.write(text.replace(graph_name % ("comp",external_type),"/%s.ps" % security_code))
        else:
            mimetype = "image/"+terminal
        response.headers['Content-Type']=mimetype
        response.headers['Content-Length']=os.path.getsize(graph_name % ("comp",external_type))
        response.headers['Content-Disposition']='attachment; filename="'+gtitle+'.'+external_type+'"'
        return response.stream(open(graph_name % ("comp",external_type),'rb'),chunk_size=4096)

    #Generate thumbs
    thumb_paths = [os.path.join(FG_PORTAL_THUMBS,str(portal.id)+".jpg"),os.path.join(FG_DATASET_THUMBS,str(dataset.id)+".jpg")]
    if request.vars.add_notebook:
        thumb_paths += [os.path.join(FG_NOTEBOOK_THUMBS,security_code+".jpg")]
    if request.vars.set_nb_settings:
        db(db.notebook.id==notebook_test.first()).update(last_update=request.now)
        if os.path.exists(os.path.join(FG_NOTEBOOK_THUMBS,notebook_test.first().url+".jpg")):
            os.remove(os.path.join(FG_NOTEBOOK_THUMBS,notebook_test.first().url+".jpg"))
        thumb_paths += [os.path.join(FG_NOTEBOOK_THUMBS,notebook_test.first().url+".jpg")]
    for thumb_name in thumb_paths:
        if not os.path.exists(thumb_name):
            if external_type in ["png","jpg","gif"]:
                times.append(("Generating thumbs",datetime.now()))
                text = "generates thumbs for %s" % dataset.id
                try:
                    db.auth_event.insert(user_id=auth.user_id,description="User %s %s" % (auth.user_id,text))
                except:pass
                print "%s: User %s %s" % (request.now,auth.user_id,text)
                #http://studio.imagemagick.org/pipermail/magick-users/2003-October/011071.html
                process = subprocess.Popen("%s %s -resize 300x200\\! %s" % (FG_IMAGEMAGICK_PATH,graph_name % ("comp",external_type),thumb_name),shell=True,stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
                mstats = process.communicate("")

    #Manage notebook
    if request.vars.add_notebook:
        nbentry = db.notebook.insert(url=security_code,portal=portal,description=request.vars.add_notebook_desc or gtitle)
    elif request.vars.set_nb_settings:
        nbentry = notebook_test.first()
    else:
        nbentry = None

    times.append(("Finish",datetime.now()))
    tstr = ""
    for t in range(len(times)-1):
        tdiff = times[t+1][1]-times[t][1]
        tfloat = tdiff.seconds + (tdiff.microseconds / 1000000.0)
        tstr += times[t][0] + ": " + str(tfloat) + "<br/>"
    tdiff = times[-1][1]-times[0][1]
    tfloat = tdiff.seconds + (tdiff.microseconds / 1000000.0)
    tstr += "TOTAL: %s<br/>" % tfloat
    #if tfloat > 10:
    #    janitor() # janitor tax
    tstr += "Predicted time: %s<br/>" % predicted_time
    tstr += "Cores used: %s<br/><br/>" % core_count
    tstr2 = ""
    tstr2 += "%s points displayed" % plotcount
    tstr3 = "%s points" % plotcount
    if nancount > plotcount:
        tstr2 += "<br/>%s points with missing data" % (nancount - plotcount)
    if totalcount > nancount:
        tstr2 += "<br/>%s points filtered out" % (totalcount - nancount)
    if totalcount > plotcount:
        tstr2 += "<br/>%s points total" % totalcount
        tstr3 = "%s of %s points" % (plotcount,totalcount)
    tstr2 += "<br/><br/>"
    if not request.vars.imageonly:
        with open(os.path.join(FG_SAVESHARE_FOLDER,security_code),'w') as f:
            f.write(json.dumps(dict(
                url=security_code,
                portal_id=portal.id,
                portal_name=portal.url,
                dataset_id=dataset.id,
                dataset_name=dataset.name,
                settings=savestate,
                status=tstr+tstr2
            )))
        if request.vars.set_nb_settings:
            shutil.copy(
                os.path.join(FG_SAVESHARE_FOLDER,security_code),
                os.path.join(FG_REVISIONS_FOLDER,notebook_test.first().url))
        #savestate_id = db.savestate.insert(url=security_code,portal_id=portal.id,portal_name=portal.url,dataset_id=dataset.id,
        #                dataset_name=dataset.name,settings=json.dumps(savestate),status=tstr+tstr2)
    comment_str = cleanurl(gtitle)
    if FG_HTTPS:
        protocol = "https"
    else:
        protocol = "http"
    if len(datasets) > 1:
        stats["longurl"]= "%s://%s/%s/%s/%s-%s" % (protocol,request.env.http_host,portal.url,cleanurl(dataset_name),comment_str,security_code)
    else:
        stats["longurl"]= "%s://%s/%s/%s-%s" % (protocol,request.env.http_host,portal.url,comment_str,security_code)
    stats["shorturl"]= "%s://%s/%s" % (protocol,request.env.http_host,security_code)
    stats["xexp"] = xexp
    stats["yexp"] = yexp
    stats["gtitle"] = gtitle

    #if request.vars.otype == "scatter":
        #f = open("/home/burgerdm/stats","a")
        #f.write(" ".join([str(int(request.vars.picture_height)*int(request.vars.picture_width)),str(plotcount),str(request.vars.color and 1 or 0),str( request.vars.size and 1 or 0), str(request.vars.zaxis and 1 or 0),request.vars.point_size,str(gptime),"\n"]))
        #f = open("/home/burgerdm/stats2","a")
        #f.write(" ".join([str(int(request.vars.picture_height)*int(request.vars.picture_width)),str(core_count),str(request.vars.color and 1 or 0),str(mtime),"\n"]))
        #f.close()

    if request.vars.imageonly:
        return queryid

    output = dict(queryid=queryid,type=external_type,status=tstr,status2=tstr2,status4=tstr3,zaxis=('zaxis' in cols or (histtype == 2 and request.vars.surface)),stats=stats,nbentry=nbentry)
    if FG_ENABLE_CACHE and not nbentry:
        with open(os.path.join(FG_CACHESTAT_FOLDER,"%s.txt" % cachename),"w") as f:
            f.write(json.dumps(output))
    return output

def image_serve():
    #https://en.wikipedia.org/wiki/Mime_type
    #https://en.wikipedia.org/wiki/HTTP_header
    #http://web2py.com/examples/default/examples
    #http://stackoverflow.com/questions/6591931/getting-file-size-in-python
    if len([aa for aa in request.args[0] if aa not in "1234567890-"]) > 0:
        return "Invalid"
    graph_name = os.path.join(FG_IMAGE_FOLDER,request.args[0]+"-comp.png")
    if os.path.exists(graph_name):
        response.headers['Content-Type']="image/png"
    else:
        graph_name = os.path.join(FG_IMAGE_FOLDER,request.args[0]+"-comp.gif")
        response.headers['Content-Type']="image/gif"
    response.headers['Content-Length']=os.path.getsize(graph_name)
    #response.headers['Content-Disposition']='attachment'
    #https://www.mnot.net/cache_docs/#CACHE-CONTROL
    response.headers['Cache-Control']='max-age=315360000'
    response.headers['Expires'] = 'Thu, 31 Dec 2037 23:59:59 GMT'
    response.headers['Pragma'] = 'cache'
    return response.stream(open(graph_name,'rb'),chunk_size=4096)

def public_thumb():
    url = request.vars.url
    if url.count("//") > 0:
        url = url.split("//")[1]
    if url.count("/") > 0:
        url = url.split("/")[1]
    if url.count("?d=") > 0:
        pstr = url.split("?d=")[0]
        dstr = url.split("?d=")[1]
    else:
        pstr = url
        dstr = None
    portal = db(db.portals.url==pstr).select().first()
    if not portal:
        return "Invalid"
    if portal.private == True:
        return "Invalid"
    if request.vars.d:
        dataset = db(db.datasets.portal==portal)(db.datasets.name==dstr).select().first()
    else:
        dataset = db(db.datasets.portal==portal)(db.datasets.is_default==True).select().first()
        if not dataset:
            dataset = db(db.datasets.portal==portal).select().first()
    if not dataset:
        return "Invalid"
    graph_name = os.path.join("/hd1/filtergraph-thumbs/d/",str(dataset.id)+".jpg")
    if not os.path.exists(graph_name):
        graph_name = os.path.join("/hd1/filtergraph-thumbs/nothumb.jpg")
    else:
        response.headers['Cache-Control']='max-age=315360000'
        response.headers['Expires'] = 'Thu, 31 Dec 2037 23:59:59 GMT'
        response.headers['Pragma'] = 'cache'
    response.headers['Content-Type']="image/jpeg"
    response.headers['Content-Length']=os.path.getsize(graph_name)
    #response.headers['Content-Disposition']='attachment'
    return response.stream(open(graph_name,'rb'),chunk_size=4096)

def thumb_serve():
    #https://en.wikipedia.org/wiki/Mime_type
    #https://en.wikipedia.org/wiki/HTTP_header
    #http://web2py.com/examples/default/examples
    #http://stackoverflow.com/questions/6591931/getting-file-size-in-python
    if request.args[0].startswith("d"):
        if request.args[0][1:] != str(long(request.args[0][1:])):
            return "Invalid"
        portal = db.datasets[long(request.args[0][1:])].portal
        admins = db(db.admins.portal==portal)(db.admins.zadmin==auth.user_id).select().first()
        if not admins:
            return "Invalid"
        graph_name = os.path.join(FG_DATASET_THUMBS,request.args[0][1:]+".jpg")
        response.headers['Content-Type']="image/jpeg"
        if not os.path.exists(graph_name):
            graph_name = os.path.join(request.folder,"static","images","nothumb.png")
            response.headers['Content-Type']="image/png"
        else:
            response.headers['Cache-Control']='max-age=315360000'
            response.headers['Expires'] = 'Thu, 31 Dec 2037 23:59:59 GMT'
            response.headers['Pragma'] = 'cache'
    else:
        if request.args[0] != str(long(request.args[0])):
            return "Invalid"
        admins = db(db.admins.portal==int(request.args[0]))(db.admins.zadmin==auth.user_id).select().first()
        if not admins:
            return "Invalid"
        graph_name = os.path.join(FG_PORTAL_THUMBS,request.args[0]+".jpg")
        response.headers['Content-Type']="image/jpeg"
        if not os.path.exists(graph_name):
            graph_name = os.path.join(request.folder,"static","images","nothumb.png")
            response.headers['Content-Type']="image/png"
        else:
            response.headers['Cache-Control']='max-age=315360000'
            response.headers['Expires'] = 'Thu, 31 Dec 2037 23:59:59 GMT'
            response.headers['Pragma'] = 'cache'
    response.headers['Content-Length']=os.path.getsize(graph_name)
    return response.stream(open(graph_name,'rb'),chunk_size=4096)

def notebook_serve():
    #https://en.wikipedia.org/wiki/Mime_type
    #https://en.wikipedia.org/wiki/HTTP_header
    #http://web2py.com/examples/default/examples
    #http://stackoverflow.com/questions/6591931/getting-file-size-in-python
    if request.args[0] != str(long(request.args[0])):
        return "Invalid"
    notebook = db(db.notebook.id == int(request.args[0])).select().first()
    if not notebook:
        return "Invalid"
    if notebook.portal.private:
        admins = db(db.admins.portal==notebook.portal)(db.admins.zadmin==auth.user_id).select().first()
        if not admins:
            return "Invalid"
    graph_name = os.path.join(FG_NOTEBOOK_THUMBS,notebook.url+".jpg")
    response.headers['Content-Type']="image/jpeg"
    if not os.path.exists(graph_name):
        graph_name = os.path.join(request.folder,"static","images","nothumb.png")
        response.headers['Content-Type']="image/png"
    else:
        response.headers['Cache-Control']='max-age=315360000'
        response.headers['Expires'] = 'Thu, 31 Dec 2037 23:59:59 GMT'
        response.headers['Pragma'] = 'cache'
    response.headers['Content-Length']=os.path.getsize(graph_name)
    return response.stream(open(graph_name,'rb'),chunk_size=4096)

def graph():
    return index()

def point_info():
    return index()

def mobile():
    return index()
    
def imageonly():
    return index()

def debug():
    return dict(request=request,response=response,session=session)

def merge():
    # specify form
    form = SQLFORM.factory(
        Field('images', 'text', default=request.vars.images, requires=IS_NOT_EMPTY()),
        Field('action', default=request.vars.action or 'Tile images', required=True, requires=IS_IN_SET(['Tile images', 'Animate images', 'Overlay images', 'Average images'])),
        Field('animation_speed', 'integer', default=25, requires=IS_INT_IN_RANGE(1,10000))
        )
    if form.process().accepted:
        response.flash = 'form accepted'
        saveshares = request.vars.images.splitlines()
        for i in range(len(saveshares)):
            if saveshares[i].count("//") > 0:
                saveshares[i] = saveshares[i].split("//")[1]
            if saveshares[i].count("/") > 0:
                saveshares[i] = saveshares[i].split("/")[1]
            if saveshares[i].count("dev-") > 0:
                saveshares[i] = saveshares[i].split("dev-")[1]
            if saveshares[i].startswith("{") and i > 0:
                saveshares[i] = json.loads(saveshares[i])
                for s in lastsettings:
                    if s not in saveshares[i]:
                        saveshares[i][s] = lastsettings[s]
            else:
                entry = db(db.savestate.url==saveshares[i]).select().first()
                saveshares[i] = json.loads(entry.settings)
                longnames = parse_list(db(db.datasets.id==entry.dataset_id).select().first().header_names)
            saveshares[i]['imageonly'] = 'imageonly'
            if i > 0 and request.vars.action == "Overlay images":
                saveshares[i]['transparent'] = 'transparent'
            for s in saveshares[i]:
                for name in longnames:
                    saveshares[i][s] = saveshares[i][s].encode('utf-8').replace("%%"+name.encode('utf-8')+"%%",longnames.get(name) or name)
                saveshares[i][s] = saveshares[i][s].replace("%%","")
                request.vars[s] = saveshares[i][s]
            lastsettings = saveshares[i]
            #return repr(saveshares[i])
            saveshares[i] = "/hd1/filtergraph-images/"+graph()+"-comp.png"
        #return repr(saveshares)
        queryid = str(abs(hash(random.random())))
        if request.vars.action == "Tile images":
            image = "/hd1/filtergraph-images/" + queryid + "-comp.png"
            process = subprocess.Popen("montage " + " ".join(saveshares) + " -geometry +0+0 " +image,shell=True,stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
        elif request.vars.action == "Average images":
            image = "/hd1/filtergraph-images/" + queryid + "-comp.png"
            process = subprocess.Popen("/hd1/gm/utilities/gm convert " + " ".join(saveshares) + " -average " +image,shell=True,stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
        elif request.vars.action == "Overlay images":
            image = "/hd1/filtergraph-images/" + queryid + "-comp.png"
            process = subprocess.Popen("/hd1/gm/utilities/gm convert " + " ".join(saveshares) + " -flatten " +image,shell=True,stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
        else:
            image = "/hd1/filtergraph-images/" + queryid + "-comp.gif"
            process = subprocess.Popen("/hd1/gm/utilities/gm convert " + " ".join(saveshares) + " -loop 0 -delay "+str(intable(request.vars.animation_speed))+" " +image,shell=True,stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
        mstats = process.communicate("")
        if mstats[1]:
            response.flash = mstats[1].split(" ",1)[1]
            return dict(form=form, image=None)
        return dict(form=form, image=queryid)
    elif form.errors:
        response.flash = 'form has errors'
    return dict(form=form,image=None)

def get_stats():
    request.vars.getstats = "true"
    return index()

def nb_delete():
    nbentry = db(db.notebook.id==int(request.args[0])).select().first()
    admins = db(db.admins.portal == nbentry.portal).select()
    #print auth.user_id
    #print repr([nbentry.zuser.id]+[a.zadmin for a in admins if not a.viewonly])
    permission_list = [a.zadmin.id for a in admins if not a.viewonly]
    if auth.user_id:
        permission_list.append(nbentry.zuser.id)
    if auth.user_id in permission_list:
        pass
    elif nbentry.session_id == FG_BROWSER_ID:
        pass
    else:
        return "alert('Invalid comment delete request.');"
    db(db.notebook.id==int(request.args[0])).delete()
    return "$('#nb-item-%s').fadeOut(500);setTimeout(function () {msnry.destroy();$('#nb-item-%s').remove();msnry = new Masonry(container);msnry.reloadItems();msnry.layout();}, 600)" % (request.args[0],request.args[0])

def nb_edit():
    desc = request.body.read()
    if auth.user_id:
        db(db.notebook.id==int(request.args[0]))(db.notebook.zuser==auth.user_id).update(description=desc,last_update=request.now)
    else:
        db(db.notebook.id==int(request.args[0]))(db.notebook.session_id==FG_BROWSER_ID).update(description=desc,last_update=request.now)
    return "$('#nb-desc-%s').text('%s');msnry.layout();" % (request.args[0],desc.replace("'","\\'"))

def nb_public():
    db(db.notebook.id==int(request.args[0]))(db.notebook.zuser==auth.user_id).update(zpublic=True,last_update=request.now)
    return "$('#nb-A-%s').show();$('#nb-C-%s').hide();$('#nb-E-%s').show();$('#nb-item-%s').css('border','1px #ddd solid');msnry.layout();" % (request.args[0],request.args[0],request.args[0],request.args[0])

def nb_private():
    db(db.notebook.id==int(request.args[0]))(db.notebook.zuser==auth.user_id).update(zpublic=False)
    return "$('#nb-A-%s').hide();$('#nb-C-%s').show();$('#nb-D-%s').hide();$('#nb-E-%s').hide();$('#nb-item-%s').css('border','1px #ddd dotted');msnry.layout();" % (request.args[0],request.args[0],request.args[0],request.args[0],request.args[0])

@auth.requires_login()
def nb_comment():
    cmt = request.vars.get("nb-textarea-"+request.args[0])
    cmt_id = db.comments.insert(nbentry=int(request.args[0]),description=cmt)
    db(db.notebook.id==int(request.args[0]))(db.notebook.zuser==auth.user_id).update(last_update=request.now)
    return "$('#nb-comments-%s').append('<span id=\"cmt_%s\"><b>%s %s:</b> %s <span class=\"icon\" data-icon=\"c\" title=\"Delete\" style=\"cursor:pointer\" onclick=\"cmt_delete(%s)\"></span><br/></span>');setTimeout(function () {msnry.layout();},250)" % (request.args[0],cmt_id,auth.user.first_name,auth.user.last_name,cmt,cmt_id)

@auth.requires_login()
def cmt_delete():
    cmt = db(db.comments.id==int(request.args[0])).select().first()
    admins = db(db.admins.portal == cmt.nbentry.portal).select()
    if auth.user_id in ([cmt.zuser.id,cmt.nbentry.zuser.id]+[a.zadmin.id for a in admins if not a.viewonly]):
        db(db.comments.id==int(request.args[0])).delete()
        return "$('#cmt_%s').remove();setTimeout(function () {msnry.layout();},250)" % request.args[0]
    else:
        return "alert('Invalid message delete request.');"
