# -*- coding: utf-8 -*-
import random
import time
import copy
from gluon.scheduler import Scheduler

migratevar = FG_DBASE_MIGRATE

#unique user id that doesn't get erased when you log in, log out, etc.
if request.cookies.has_key('uid'):
    FG_BROWSER_ID = request.cookies['uid'].value
else:
    FG_BROWSER_ID = ''.join(random.choice("ABCDEFGHJKLMNPQRSTUVWXYZ23456789") for x in range(20))
    response.cookies['uid'] = FG_BROWSER_ID
    response.cookies['uid']['expires'] = 24 * 7 * 3600
    response.cookies['uid']['path'] = '/'
    
#used to build cookie for filtergraph announcements
FG_CURRENT_TIME = time.time()
response.cookies['current_time'] = FG_CURRENT_TIME
response.cookies['current_time']['expires'] = 24 * 7 * 3600
response.cookies['current_time']['path'] = '/'
    
if request.cookies.has_key('last_visit_time') and request.cookies.has_key('current_time'):
    FG_LAST_VISIT_TIME = float(request.cookies['current_time'].value)
    if request.controller == "run" and time.time() > (FG_LAST_VISIT_TIME + 1):
        response.cookies['last_visit_time'] = FG_LAST_VISIT_TIME
    FG_SHOW_ANNOUNCEMENT = (float(request.cookies['last_visit_time'].value) < 1490114133L)
else:
    FG_LAST_VISIT_TIME = 0
    if request.controller == "run" and time.time() > (FG_LAST_VISIT_TIME + 1):
        response.cookies['last_visit_time'] = FG_LAST_VISIT_TIME
        response.cookies['last_visit_time']['expires'] = 24 * 7 * 3600
        response.cookies['last_visit_time']['path'] = '/'
    FG_SHOW_ANNOUNCEMENT = True

#########################################################################
## This scaffolding model makes your app work on Google App Engine too
## File is released under public domain and you can use without limitations
#########################################################################

## if SSL/HTTPS is properly configured and you want all HTTP requests to
## be redirected to HTTPS, uncomment the line below:
#request.requires_https()
#if not request.is_https:
#    redirect("https://"+request.env.http_host.split(':')[0]+request.env.web2py_original_uri)

if request.env.web2py_runtime_gae:            # if running on Google App Engine
    db = DAL('google:datastore')              # connect to Google BigTable
                                              # optional DAL('gae://namespace')
    session.connect(request, response, db = db) # and store sessions and tickets there
    ### or use the following lines to store sessions in Memcache
    # from gluon.contrib.memdb import MEMDB
    # from google.appengine.api.memcache import Client
    # session.connect(request, response, db = MEMDB(Client()))
elif FG_DBASE in ['mysql','postgresql','mssql','firebird','ingres','sybase','cubrid','sapdb','imap','mongodb']:
    db = DAL('%s://%s:%s@%s/%s' % (FG_DBASE,FG_DBASE_USERNAME,FG_DBASE_PASSWORD,FG_DBASE_HOST,FG_DBASE_NAME))
    session.connect(request,response,separate=True)
elif FG_DBASE in ['oracle','db2','informix']:
    db = DAL('%s://%s:%s@%s' % (FG_DBASE,FG_DBASE_USERNAME,FG_DBASE_PASSWORD,FG_DBASE_HOST))
    session.connect(request,response,separate=True)
elif FG_DBASE == "teradata":
    db = DAL('teradata://DSN=%s;UID=%s;PWD=%s;DATABASE=%s' % (FG_DBASE,FG_DBASE_HOST,FG_DBASE_USERNAME,FG_DBASE_PASSWORD,FG_DBASE_NAME))
    session.connect(request,response,separate=True)
else:
    db = DAL('sqlite://storage.sqlite',pool_size=1,check_reserved=['all'])
    session.connect(request,response,separate=True)

## now set up the scheduler
if request.env.web2py_runtime_gae:            # if running on Google App Engine
    sched_db = DAL('google:datastore')        # connect to Google BigTable
                                              # optional DAL('gae://namespace')
    session.connect(request, response, db = db) # and store sessions and tickets there
    ### or use the following lines to store sessions in Memcache
    # from gluon.contrib.memdb import MEMDB
    # from google.appengine.api.memcache import Client
    # session.connect(request, response, db = MEMDB(Client()))
elif FG_SCHED in ['mysql','postgresql','mssql','firebird','ingres','sybase','cubrid','sapdb','imap','mongodb']:
    sched_db = DAL('%s://%s:%s@%s/%s' % (FG_SCHED,FG_SCHED_USERNAME,FG_SCHED_PASSWORD,FG_SCHED_HOST,FG_SCHED_NAME))
elif FG_SCHED in ['oracle','db2','informix']:
    sched_db = DAL('%s://%s:%s@%s' % (FG_SCHED,FG_SCHED_USERNAME,FG_SCHED_PASSWORD,FG_SCHED_HOST))
elif FG_SCHED == "teradata":
    sched_db = DAL('teradata://DSN=%s;UID=%s;PWD=%s;DATABASE=%s' % (FG_SCHED,FG_SCHED_HOST,FG_SCHED_USERNAME,FG_SCHED_PASSWORD,FG_SCHED_NAME))
else:
    sched_db = DAL('sqlite://scheduler.sqlite',pool_size=1,check_reserved=['all'])
scheduler = Scheduler(sched_db)

## by default give a view/generic.extension to all actions from localhost
## none otherwise. a pattern can be 'controller/function.extension'
response.generic_patterns = ['*'] if request.is_local else []
## (optional) optimize handling of static files
# response.optimize_css = 'concat,minify,inline'
# response.optimize_js = 'concat,minify,inline'

#########################################################################
## Here is sample code if you need for
## - email capabilities
## - authentication (registration, login, logout, ... )
## - authorization (role based authorization)
## - services (xml, csv, json, xmlrpc, jsonrpc, amf, rss)
## - crud actions
## (more options discussed in gluon/tools.py)
#########################################################################

from gluon.tools import Auth, Crud, Service, PluginManager, prettydate
auth = Auth(db,hmac_key = Auth.get_or_create_key())
auth.settings.extra_fields['auth_user']= [
  Field('quota','integer',readable=False,writable=False,default=50),
  Field('noemail','boolean',label="Disable notifications",comment="Check here if you do not wish to receive e-mail notifications from Filtergraph. We may still contact you if you leave us feedback or if there are any issues with your account."),
  Field('api_key','string',readable=False,writable=False,default=(''.join(random.choice("0123456789abcdef") for x in range(32)))),]
crud, service, plugins = Crud(db), Service(), PluginManager()

auth.settings.expiration = 2592100  # seconds
auth.settings.long_expiration = 2592100  # seconds
auth.settings.remember_me_form = False

## create all tables needed by auth if not custom tables
auth.define_tables(username=True, signature=False,migrate=migratevar)

## configure email
mail = auth.settings.mailer
if FG_EMAIL_SERVER == "logging":
    mail.settings.server = 'logging'  # your SMTP server 'logging' or 
else:
    mail.settings.server = '%s:%s' % (FG_EMAIL_SERVER,FG_EMAIL_PORT)  # your SMTP server 'logging' or 
    mail.settings.sender = FG_EMAIL_ADDRESS         # your email
    mail.settings.login = '%s:%s' % (FG_EMAIL_USERNAME,FG_EMAIL_PASSWORD)      # your credentials or None

## configure auth policy
auth.settings.registration_requires_verification = False
auth.settings.registration_requires_approval = False
auth.settings.reset_password_requires_verification = True
auth.messages.reset_password = 'A password reset for Filtergraph has been requested for this e-mail.\n\nClick on the following link to reset your password: http://'+request.env.http_host+URL('default','user',args=['reset_password'])+'/%(key)s\n\nIf you did not request a password reset, you may disregard this message.\n\nSincerely,\nThe Filtergraph Team'

auth.settings.login_next = URL('portals')
auth.settings.logout_next = URL('index')
auth.settings.profile_next = URL('portals')
auth.settings.register_next = URL('portals')
#auth.settings.retrieve_username_next = URL('index')
#auth.settings.retrieve_password_next = URL('index')
auth.settings.change_password_next = URL('portals')
auth.settings.request_reset_password_next = URL('index')
auth.settings.reset_password_next = URL('portals')
#auth.settings.verify_email_next = URL('user', args='login')

## if you need to use OpenID, Facebook, MySpace, Twitter, Linkedin, etc.
## register with janrain.com, write your domain:api_key in private/janrain.key
from gluon.contrib.login_methods.rpx_account import use_janrain
use_janrain(auth, filename='private/janrain.key')

#########################################################################
## Define your tables below (or better in another model file) for example
##
## >>> db.define_table('mytable',Field('myfield','string'))
##
## Fields can be 'string','text','password','integer','double','boolean'
##       'date','time','datetime','blob','upload', 'reference TABLENAME'
## There is an implicit 'id integer autoincrement' field
## Consult manual for more options, validators, etc.
##
## More API examples for controllers:
##
## >>> db.mytable.insert(myfield='value')
## >>> rows=db(db.mytable.myfield=='value').select(db.mytable.ALL)
## >>> for row in rows: print row.id, row.myfield
#########################################################################

#http://stackoverflow.com/questions/2257441/python-random-string-generation-with-upper-case-letters-and-digits
db.define_table('portals',
    Field('name','string', required=True, requires=IS_NOT_EMPTY("This field is required.")),
    Field('url','string', required=True, requires=(IS_NOT_EMPTY("This field is required."),IS_ALPHANUMERIC('URL must only contain the lowercase letters a-z and the numbers 0-9.'),IS_LOWER(),IS_LENGTH(minsize=4,maxsize=16,error_message="URL must be between 4 and 16 characters."),IS_NOT_IN_DB(db,"portals.url","This URL is already taken."))),
    Field('zactive','boolean',readable=False,writable=False,default=True),
    Field('admin_code','string',readable=False,writable=False,default=(''.join(random.choice("ABCDEFGHJKLMNPQRSTUVWXYZ23456789") for x in range(20)))),
    Field('private','boolean',readable=False,writable=False,default=False),
    format='%(name)s',migrate=migratevar)
    
db.define_table('datasets',
    Field('portal','reference portals', readable=False, writable=False),
    Field('name','string', default="", required=True, requires=IS_NOT_EMPTY("This field is required.")),
    Field('zfile','upload', required=True, requires=(IS_NOT_EMPTY("This field is required."))),
    Field('description','text'),
    Field('ftype','string',readable=False, writable=False),
    Field('header_names','text', readable=False, writable=False, default=""),
    Field('description_names','text', readable=False, writable=False, default=""),
    Field('html_names','text', readable=False, writable=False, default=""),
    Field('settings','text', readable=False, writable=False, default=""),
    Field('status','string', default="", readable=False, writable=False),
    Field('urlfeed','string', default=""),
    Field('lur_task','integer'),
    Field('lur_tablename','string'),
    Field('lur_fix','string'),
    Field('is_default','boolean'),
    format='%(name)s',migrate=migratevar)

db.define_table('admins',
    Field('portal','reference portals'),
    Field('zadmin','reference auth_user'),
    Field('viewonly','boolean',readable=False,writable=False,default=False),migrate=migratevar)
    
db.define_table('savestate',
    Field('url','string'),
    Field('portal_id','integer'),
    Field('portal_name','string'),
    Field('dataset_id','integer'),
    Field('dataset_name','string'),
    Field('settings','text'),
    Field('ztime','datetime',default=request.now),
    Field('ipaddress','string',default=FG_BROWSER_ID),
    Field('status','text'),migrate=migratevar)

db.define_table('notebook',
    Field('zuser','reference auth_user',default=auth.user),
    Field('url','string'),
    Field('session_id','string',default=FG_BROWSER_ID),
    Field('description','text'),
    Field('ztime','datetime',default=request.now),
    Field('zpublic','boolean',default=False),
    Field('portal','reference portals'),
    Field('last_update','datetime',default=request.now),
    Field('showcase','boolean',default=False),migrate=migratevar)

db.define_table('comments',
    Field('zuser','reference auth_user',default=auth.user),
    Field('nbentry','reference notebook'),
    Field('description','text'),
    Field('ztime','datetime',default=request.now),migrate=migratevar)
    
db.define_table('subscriptions',
    Field('email','string'),
    Field('subscribe_id','string'),
    Field('noemail','boolean',default=False),migrate=migratevar)
    
db.define_table('links',
    Field('url','string'),
    Field('portal','reference portals'),
    Field('visits','integer',default=0),
    Field('viewonly','boolean',default=True),
    Field('invited_email','string'),migrate=migratevar)
    
db.define_table('requests',
    Field('name','string'),
    Field('email','string'),
    Field('portal','reference portals'),
    Field('zuser','reference auth_user'),
    Field('url','string'),migrate=migratevar)
    
db.define_table('temp_access',
    Field('session_id','string',default=FG_BROWSER_ID),
    Field('link','reference links'),
    Field('portal','reference portals'),migrate=migratevar)
    
db.define_table('feedback',
    Field("name","string",required=True),
    Field("email","string",required=True),
    Field("errorid","string",required=False),
    Field("zmessage","text"),
    Field('posted','datetime',default=request.now),migrate=migratevar)

## after defining tables, uncomment below to enable auditing
auth.enable_record_versioning(db)
