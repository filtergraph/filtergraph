# coding: utf8
#!/usr/bin/python

import os
import numpy as np
#import atpy
#import pyfits
#import vo
import sqlite3
#import xlrd
import csv
import shutil
#import h5py
import glob
import requests
import json
from datetime import datetime, timedelta

FILE_SPECS = """
<b>The following file types are supported:</b>
<ul>
    <li>ASCII text files (space separated, comma separated, tab separated, and fixed length)</li>
    <li>Microsoft Excel files (*.xls, *.xlsx)</li>
    <li>SQLite files (*.sqlite)</li>
    <li>VOTable files (*.vot, *.xml)</li>
    <li>FITS files (*.fits)</li>
    <li>IPAC files (*.tbl)</li>
    <li>Numpy files (*.npy)</li>
    <li>HDF5 files (*.h5)</li>
</ul>
<br/>
<b>For best results on ASCII and Excel files:</b>
<ul>
    <li><b>You may include NANs, but make sure you're consistent.</b> If there is one string of text used consistently in a numeric column such as "nan", "---" or "unknown", it will be treated as a missing value. If there is more than one, the whole column will be treated as text. When in doubt, use "nan" for missing values, for it is case insensitive and always treated as a missing value on numeric columns.</li>
    <li><b>Make sure each row has the same number of columns.</b> If a row has an incorrect number of columns, Filtergraph cannot determine which columns match up. Rows with incorrect columns will be ignored.</li>
    <li><b>Place a header in the first row to name each column.</b> If a header cannot be found, the column names will be assigned as Column1, Column2, etc. You may optionally use the '#' symbol to designate a header. If you include a header, make sure the name of each column is unique, otherwise the duplicate names will be modified.</li>
</ul>
<br/>
<b>Problems with uploading?</b>
<ul>
    <li><b>Try saving it in a different format.</b> You may want to open the file using a spreadsheet program such as Microsoft Excel, then save it to one of the other formats we support. If you are experienced with Python, you may use special libraries to save to the more technical file formats such as Numpy and HDF5.</li>
</ul>
"""

def loaddata(fname,tablename = "",ftype="",fixedlength="",fix=None,forcetype=None,queued=False,urlfeed=None,quota=1500):
    #sort_fname = os.path.join(SORT_FOLDER,fname)
    upload_fname = os.path.join(FG_UPLOAD_FOLDER,fname)
    cache_fname = os.path.join(FG_CACHE_FOLDER,fname) + ".npy"
    done_fname = os.path.join(FG_DONE_FOLDER,fname)
    trash_upload_fname = os.path.join(FG_TRASH_UPLOAD_FOLDER,fname)
    trash_cache_fname = os.path.join(FG_TRASH_CACHE_FOLDER,fname) + ".npy"
    
    #see if file is already in cache, or if this is a fix
    if urlfeed:
        loadfile = upload_fname
    elif fix:
        if os.path.exists(cache_fname):
            os.rename(cache_fname,trash_cache_fname)
        if os.path.exists(done_fname):
            loadfile = done_fname
        elif os.path.exists(upload_fname):
            loadfile = upload_fname
        else:
            return {"error":"Cannot find file. Please re-upload the file and try again."}
    elif os.path.exists(cache_fname):
        return {"data":cache_fname}
    elif os.path.exists(upload_fname):
        loadfile = upload_fname
    else:
        return {"error":"Cannot find file. Please re-upload the file and try again."}
    
    #start queue
    if not queued and FG_QUEUE_ENABLED:
        #if sched_db(sched_db.scheduler_run.start_time > datetime.now() - timedelta(days=1))(sched_db.scheduler_run.status != "COMPLETED").count() < 3:
        task = scheduler.queue_task(loaddata,immediate=True,timeout=3600,pvars=dict(fname=loadfile.split("/")[-1],tablename = tablename,ftype=ftype,fixedlength=fixedlength,fix=fix,forcetype=forcetype,urlfeed=urlfeed,quota=quota,queued=True),sync_output=2)
        return {"queue":task.uuid}
    
    #download file
    if urlfeed:
        response = requests.get(urlfeed,stream=True)
        try:
            file_length = float(response.headers['Content-Length'])
            if file_length >= (quota*1048576):
                return {"error":"File at URL exceeds quota."}
        except:
            file_length = None
        with open(upload_fname,"wb") as out:
            file_pos = 0
            for data in response.iter_content(chunk_size=1024):
                if file_length:
                    pct = file_pos*100/file_length
                    print "!clear!Downloading file (%.0f%%)" % pct
                else:
                    print "!clear!Downloading file (%.0f kb)" % (file_pos / 1024)
                file_pos += 1024
                out.write(data)
                if file_pos >= (quota*1048576):
                    return {"error":"File at URL exceeds quota."}
            #f = urllib2.urlopen(urlfeed)
            #out.write(f.read(quota*1048576))
            #f.close()
        if os.stat(upload_fname).st_size >= (quota*1048576):
            os.remove(upload_fname)
            return {"error":"File at URL exceeds quota."}
    
    janitor() # janitor tax
    
    #unchunk the file
    #http://stackoverflow.com/questions/13613336/python-concatenate-text-files
    chunks = sorted(glob.glob(loadfile + "-*"))
    for chunk in chunks:
        with open(loadfile,"a") as outfile:
            with open(chunk) as infile:
                outfile.write(infile.read())
        os.remove(chunk)
    #chunkid = 2
    #while os.path.exists("%s-%.5i" % (loadfile,chunkid)):
    #    with open(loadfile,"a") as outfile:
    #        with open("%s-%.5i" % (loadfile,chunkid)) as infile:
    #            outfile.write(infile.read())
    #    os.remove("%s-%.5i" % (loadfile,chunkid))
    #    chunkid += 1
    #    if chunkid >= 99999:
    #        break
    
    #run helper function
    result = loaddata2(loadfile=loadfile,tablename=tablename,ftype=ftype,fixedlength=fixedlength,fix=fix,forcetype=forcetype)
    if "data" in result:
        np.save(cache_fname, result['data'])
        result['data'] = cache_fname
        result['loadfile'] = loadfile
        result['done_fname'] = done_fname
        #shutil.move(loadfile,done_fname)
    return result
    
def lur_refresh(did):
    dataset = db.datasets[did]
    fname = dataset.zfile
    ftype = dataset.ftype
    tablename = dataset.lur_tablename
    fix = dataset.lur_fix
    urlfeed = dataset.urlfeed
    quota = 1500
    
    upload_fname = os.path.join(FG_UPLOAD_FOLDER,fname)
    cache_fname = os.path.join(FG_CACHE_FOLDER,fname) + ".npy"
    done_fname = os.path.join(FG_DONE_FOLDER,fname)
    trash_upload_fname = os.path.join(FG_TRASH_UPLOAD_FOLDER,fname)
    trash_cache_fname = os.path.join(FG_TRASH_CACHE_FOLDER,fname) + ".npy"
    
    if os.path.exists(trash_upload_fname):
        os.remove(trash_upload_fname)
    if os.path.exists(upload_fname):
        shutil.move(upload_fname,trash_upload_fname)
    
    #download file
    response = requests.get(urlfeed,stream=True)
    try:
        file_length = float(response.headers['Content-Length'])
        if file_length >= (quota*1048576):
            return {"error":"File at URL exceeds quota."}
    except:
        file_length = None
    with open(upload_fname,"wb") as out:
        file_pos = 0
        for data in response.iter_content(chunk_size=1024):
            if file_length:
                pct = file_pos*100/file_length
                print "!clear!Downloading file (%.0f%%)" % pct
            else:
                print "!clear!Downloading file (%.0f kb)" % (file_pos / 1024)
            file_pos += 1024
            out.write(data)
            if file_pos >= (quota*1048576):
                return {"error":"File at URL exceeds quota."}
        #f = urllib2.urlopen(urlfeed)
        #out.write(f.read(quota*1048576))
        #f.close()
    if os.stat(upload_fname).st_size >= (quota*1048576):
        os.remove(upload_fname)
        return {"error":"File at URL exceeds quota."}
        
    janitor() # janitor tax
    
    #run helper function
    if fix:
        result = loaddata2(loadfile=upload_fname,tablename=tablename,ftype=ftype,fix=json.loads(fix))
    else:
        result = loaddata2(loadfile=upload_fname,tablename=tablename,ftype=ftype)
    if "data" in result:
        if os.path.exists(trash_cache_fname):
            os.remove(trash_cache_fname)
        if os.path.exists(cache_fname):
            shutil.move(cache_fname,trash_cache_fname)
        np.save(cache_fname, result['data'])
        result['data'] = cache_fname
    return result