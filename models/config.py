# -*- coding: utf-8 -*-

import os, ConfigParser
config = ConfigParser.RawConfigParser()
config.read(os.path.join(request.env.gluon_parent,"filtergraph.cfg"))

#http://stackoverflow.com/questions/4028904/how-to-get-the-home-directory-in-python

FG_DBASE = config.get("database","database")
if FG_DBASE != "sqlite":
    FG_DBASE_USERNAME = config.get("database","username")
    FG_DBASE_PASSWORD = config.get("database","password")
    FG_DBASE_HOST = config.get("database","host")
    FG_DBASE_NAME = config.get("database","name")
try:
    FG_DBASE_MIGRATE = (config.get("database","migrate") == "true")
except:
    FG_DBASE_MIGRATE = True
    
try:
    FG_SCHED = config.get("scheduler","database")
except:
    FG_SCHED = FG_DBASE
if FG_SCHED != "sqlite":
    try:
        FG_SCHED_USERNAME = config.get("scheduler","username")
        FG_SCHED_PASSWORD = config.get("scheduler","password")
        FG_SCHED_HOST = config.get("scheduler","host")
        FG_SCHED_NAME = config.get("scheduler","name")
    except:
        FG_SCHED_USERNAME = FG_DBASE_USERNAME
        FG_SCHED_PASSWORD = FG_DBASE_PASSWORD
        FG_SCHED_HOST = FG_DBASE_HOST
        FG_SCHED_NAME = FG_DBASE_NAME
try:
    FG_SCHED_MIGRATE = (config.get("scheduler","migrate") == "true")
except:
    FG_SCHED_MIGRATE = FG_DBASE_MIGRATE

FG_EMAIL_SERVER = config.get("email","server")
FG_EMAIL_PORT = config.get("email","port")
FG_EMAIL_ADDRESS = config.get("email","address")
FG_EMAIL_USERNAME = config.get("email","username")
FG_EMAIL_PASSWORD = config.get("email","password")

FG_DATA_FOLDER = config.get("path","data")
FG_UPLOAD_FOLDER = os.path.join(FG_DATA_FOLDER,"upload")
FG_CACHE_FOLDER = os.path.join(FG_DATA_FOLDER,"cache")
FG_CACHESTAT_FOLDER = os.path.join(FG_DATA_FOLDER,"cachestat")
FG_DONE_FOLDER = os.path.join(FG_DATA_FOLDER,"done")
FG_TRASH_UPLOAD_FOLDER = os.path.join(FG_DATA_FOLDER,"trash-upload")
FG_TRASH_CACHE_FOLDER = os.path.join(FG_DATA_FOLDER,"trash-cache")
FG_IMAGE_FOLDER = os.path.join(FG_DATA_FOLDER,"image")
FG_BENCHMARK_FOLDER = os.path.join(FG_DATA_FOLDER,"benchmark")
FG_PORTAL_THUMBS = os.path.join(FG_DATA_FOLDER,"thumbs-portal")
FG_DATASET_THUMBS = os.path.join(FG_DATA_FOLDER,"thumbs-dataset")
FG_NOTEBOOK_THUMBS = os.path.join(FG_DATA_FOLDER,"thumbs-notebook")
FG_SAVESHARE_FOLDER = os.path.join(FG_DATA_FOLDER,"saveshare")
FG_TABLE_FOLDER = os.path.join(FG_DATA_FOLDER,"table")
FG_TRASH_TABLE_FOLDER = os.path.join(FG_DATA_FOLDER,"trash-table")
FG_REVISIONS_FOLDER = os.path.join(FG_DATA_FOLDER,"revisions")

FG_GNUPLOT_PATH = config.get("path","gnuplot")
FG_IMAGEMAGICK_PATH = config.get("path","imagemagick")

#http://lybniz2.sourceforge.net/safeeval.html
FG_ENABLE_ACCELERATION = (config.get("acceleration","enabled") == "true")
FG_MAX_CORES = config.get("acceleration","max_cores")
if FG_MAX_CORES == "auto":
    import multiprocessing
    FG_MAX_CORES = multiprocessing.cpu_count()
else:
    FG_MAX_CORES = int(FG_MAX_CORES)
FG_GNUPLOT_MODEL = eval(config.get("acceleration","gp_model"),{"__builtins__":None},{})
FG_IMAGEMAGICK_MODEL = eval(config.get("acceleration","im_model"),{"__builtins__":None},{})

try:
    FG_API_PORTAL_FROM_DRIVE = (config.get("api","from_drive") == "true")
except:
    FG_API_PORTAL_FROM_DRIVE = False

try:
    FG_ENABLE_CACHE = (config.get("acceleration","caching") == "true")
except:
    FG_ENABLE_CACHE = True
    
try:
    FG_JANITOR_CLEAR_FILES = float(config.get("janitor","clear_files"))
except:
    FG_JANITOR_CLEAR_FILES = 30
    
try:
    FG_JANITOR_FULL_DISK = float(config.get("janitor","full_disk"))
except:
    FG_JANITOR_FULL_DISK = 90
    
try:
    FG_JANITOR_CLEAR_WHEN_FULL = float(config.get("janitor","clear_when_full"))
except:
    FG_JANITOR_CLEAR_WHEN_FULL = 2
    
try:
    FG_QUEUE_ENABLED = (config.get("queue","enabled") == "true")
except:
    FG_QUEUE_ENABLED = False
    
try:
    FG_GNUPLOT_TERM = config.get("acceleration","terminal")
except:
    FG_GNUPLOT_TERM = "pngcairo"
    
try:
    FG_GOOGLE_ANALYTICS = config.get("googleanalytics","id")
except:
    FG_GOOGLE_ANALYTICS = None
    
try:
    FG_MAILJET_APIKEY = config.get("mailjet","apikey")
except:
    FG_MAILJET_APIKEY = None
    
try:
    FG_MAILJET_SECRETKEY = config.get("mailjet","secretkey")
except:
    FG_MAILJET_SECRETKEY = None
    
try:
    FG_MAILJET_FROM = config.get("mailjet","from")
except:
    FG_MAILJET_FROM = None
    
if request.vars.download or (not request.vars.interactive):
    try:
        FG_SERVER_TIMEOUT = float(config.get("limits","timeout_download"))
    except:
        FG_SERVER_TIMEOUT = 60
else:
    try:
        FG_SERVER_TIMEOUT = float(config.get("limits","timeout"))
    except:
        FG_SERVER_TIMEOUT = 30
        
try:
    FG_MAX_VARIABLES = float(config.get("limits","max_vars"))
except:
    FG_MAX_VARIABLES = 40
    
try:
    FG_MAX_FUNCTIONS = float(config.get("limits","max_funs"))
except:
    FG_MAX_FUNCTIONS = 40
    
try:
    FG_HTTP = (config.get("server","http") == "true")
except:
    FG_HTTP = False
    
try:
    FG_HTTPS = (config.get("server","https") == "true")
except:
    FG_HTTPS = True

if not os.path.exists(FG_DATA_FOLDER):
    os.makedirs(FG_DATA_FOLDER)
    os.makedirs(FG_UPLOAD_FOLDER)
    os.makedirs(FG_CACHE_FOLDER)
    os.makedirs(FG_DONE_FOLDER)
    os.makedirs(FG_TRASH_UPLOAD_FOLDER)
    os.makedirs(FG_TRASH_CACHE_FOLDER)
    os.makedirs(FG_IMAGE_FOLDER)
    os.makedirs(FG_BENCHMARK_FOLDER)
    os.makedirs(FG_PORTAL_THUMBS)
    os.makedirs(FG_DATASET_THUMBS)
    os.makedirs(FG_NOTEBOOK_THUMBS)

if not os.path.exists(FG_CACHESTAT_FOLDER):
    os.makedirs(FG_CACHESTAT_FOLDER)

if not os.path.exists(FG_SAVESHARE_FOLDER):
    os.makedirs(FG_SAVESHARE_FOLDER)
    
if not os.path.exists(FG_TABLE_FOLDER):
    os.makedirs(FG_TABLE_FOLDER)
    
if not os.path.exists(FG_TRASH_TABLE_FOLDER):
    os.makedirs(FG_TRASH_TABLE_FOLDER)
    
if not os.path.exists(FG_REVISIONS_FOLDER):
    os.makedirs(FG_REVISIONS_FOLDER)