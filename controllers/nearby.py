import os
from datetime import *
from gluon.tools import prettydate #last modified display 
# coding: utf8
# try something like
def index(): 
    return "This page is offline for maintenance."
    portal = db(db.portals.url==request.vars.p).select().first()
    if not portal:
        return "Could not find portal!<script>$('#loading').hide()</script>"
    response.title = portal.name
    datasets = db(db.datasets.portal==portal.id).select()
    if request.vars.d:
        dataset = datasets.find(lambda row: row.name==request.vars.d).first()
    else:
        dataset = datasets.first()
    if not dataset:
        return "Could not find dataset!<script>$('#loading').hide()</script>"
    fname = '/hd1/INVALID/home/burgerdm/web2py/applications/tools/uploads/'+dataset.file
    cache = '/hd1/INVALID/home/burgerdm/cache/d'+str(dataset.id)
    name_field = 0
    (mode, ino, dev, nlink, uid, gid, size, atime, mtime, ctime) = os.stat(fname)
    response.subtitle = "Last modified " + prettydate(datetime.fromtimestamp(mtime),T)
    cache += (str(mtime) + ".npy")
    
    # Edit the menu
    tools = [
            (XML('<img src="/'+request.application+'/static/_1.0.0/images/plotchart.png" /> '+T('Scatter Plot')), True, "/"+request.vars.p),
            (XML('<img src="/'+request.application+'/static/_1.0.0/images/chart_bar.png" /> '+T('Histogram')), True, "/"+request.vars.p+"?hist=true"),
            (XML('<img src="/'+request.application+'/static/_1.0.0/images/table.png" /> '+T('Table')), True, "/"+request.application+"/table/index?p="+request.vars.p+"&d="+(request.vars.d or ""))]
    if request.vars.p == "slowpokes":
        tools += [
            (XML('<img src="/'+request.application+'/static/_1.0.0/images/table.png" /> '+T('Nearby Objects')), True,  "/"+request.application+"/nearby/index?p="+request.vars.p+"&d="+(request.vars.d or ""))]
            
    if(len(datasets)==1):
        response.menu = tools
    else:
        submenu = []
        for i in range(0,len(datasets)):
            submenu += [(XML('<img src="/'+request.application+'/static/_1.0.0/images/chart_pie.png" /> '+datasets[i].name), True, "/"+request.vars.p+"?d="+datasets[i].name, 
            [])]
        response.menu = [
            (XML('<img src="/'+request.application+'/static/_1.0.0/images/chart_curve.png" /> '+"Datasets"), True, "/"+request.vars.p+"?d="+dataset.name, 
            submenu),
            (XML('<img src="/'+request.application+'/static/_1.0.0/images/wrench.png" /> '+"Tools"), True, "/"+request.vars.p+"?d="+dataset.name, tools)]
    
    
    response.files.append('/'+request.application+'/static/_1.0.0/css/base.css')   
    response.files.append('/'+request.application+'/static/_1.0.0/css/superfish.css')
    response.files.append('/'+request.application+'/static/_1.0.0/js/jquery-ui.min.js')     
    response.files.append('/'+request.application+'/static/_1.0.0/css/smoothness/jquery-ui-1.8.18.custom.css')
    
    return dict(dataset=dataset)

def nearby():
    return "This page is offline for maintenance."
    portal = db(db.portals.url==request.vars.p).select().first()
    if not portal:
        return "Could not find portal!<script>$('#loading').hide()</script>"
    response.title = portal.name
    datasets = db(db.datasets.portal==portal.id).select()
    if request.vars.d:
        dataset = datasets.find(lambda row: row.name==request.vars.d).first()
    else:
        dataset = datasets.first()
    if not dataset:
        return "Could not find dataset!<script>$('#loading').hide()</script>"
    fname = '/hd1/INVALID/home/burgerdm/web2py/applications/tools/uploads/'+dataset.file
    
    dist = floatable(request.vars.dist) or 100
    dist /= 3600 # convert to degrees
    
    #Parse header
    f = open(fname,'r')
    header_str = f.readline()
    if header_str.startswith("#"):
        header_str = header_str[1:]
    header = header_str.split()
         
    ra = []
    dec = []   
    for i in range(len(header)):
        if header[i].lower().count("ra") > 0:
            ra.append(i)
        if header[i].lower().count("dec") > 0:
            dec.append(i)
    if len(ra) > 0:
        ra = ra[0]
    else:
        return "Could not find RA in dataset!<script>$('#loading').hide()</script>"
    if len(dec) > 0:
        dec = dec[0]
    else:
        return "Could not find DEC in dataset!<script>$('#loading').hide()</script>"
        
    nearby_floats = []
    nearby_list = request.vars.nearby.splitlines()
    c = 1
    for n in nearby_list:
        nearby_split = n.split(",")
        if len(nearby_split) == 2 and floatable(nearby_split[0]) and floatable(nearby_split[1]):
            nearby_floats.append((floatable(nearby_split[0]),floatable(nearby_split[1]),c))
        elif len(nearby_split) == 3 and floatable(nearby_split[0]) and floatable(nearby_split[1]):
            nearby_floats.append((floatable(nearby_split[0]),floatable(nearby_split[1]),nearby_split[2]))
        else:
            return "Line %s is invalid (%s)<script>$('#loading').hide()</script>" % (c,n)
        c += 1
    
    out = []
    for line in f:
        line_split = line.split()
        line_ra = floatable(line_split[ra])
        line_dec = floatable(line_split[dec])
        for i in range(len(nearby_floats)):
            d = angsep(nearby_floats[i][0],nearby_floats[i][1],line_ra,line_dec)
            if d < dist:
                out.append((d,nearby_floats[i][2],line_split[ra],line_split[dec]))
    
    f.close()
    
    out_sort = sorted(out, key=lambda o: o[0])
    
    table = "<table id='hor-minimalist-b'><thead><tr><th>Distance</th><th>Compared to object</th><th>RA</th><th>DEC</th></tr></thead><tbody>"
    for o in out_sort:
        table += "<tr><td>%.6f</td><td>%s</td><td>%s</td><td>%s</td></tr>" % o
    table += "</tbody></table>"
    
    return table + "<script>$('#loading').hide()</script>"
