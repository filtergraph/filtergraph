# coding: utf8
# try something like
import numpy as np
@auth.requires_login()
def update():
    admins1 = db(db.admins.portal==18)(db.admins.viewonly==None)(db.admins.admin==auth.user_id).count()
    admins2 = db(db.admins.portal==18)(db.admins.viewonly==False)(db.admins.admin==auth.user_id).count()
    if (admins1 + admins2) == 0:
        return "Access denied"
    if request.vars.tessload:
        data = loaddata2('/home/paegerm/tessdwarfs.txt',ftype="txt")
    elif request.vars.tessupdate:
        data = loaddata2('/home/paegerm/tessdwarfs.txt',ftype="txt")
        if data.get('data'):
            np.save('/hd1/tess.dat', data['data'])
    else:
        data = None
    return dict(data = data)
