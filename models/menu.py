# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## Customize your APP title, subtitle and menus here
#########################################################################

response.logo = A(IMG(_src=URL('static','_1.0.0/images/filtergraph_logo_toolbar2.png'), _class="brand",_style="padding:5px 20px 5px 20px"),_href="/"+request.application+"/default/portals")
response.title = "Filtergraph"
response.subtitle = T('customize me!')

## read more at http://dev.w3.org/html5/markup/meta.name.html
response.meta.author = 'Dan Burger'
response.meta.description = 'Filtergraph is a web-based service for creating interactive data visualization portals.'
response.meta.keywords = 'data visualization astronomy web based statistical interface'
response.meta.generator = 'Web2py Web Framework'

## your http://google.com/analytics id
response.google_analytics_id = FG_GOOGLE_ANALYTICS

#########################################################################
## this is the main application menu add/remove items as required
#########################################################################

response.menu = []