# coding: utf8

import math, re, os, time, shutil
from matplotlib.path import Path
import numpy.lib.recfunctions as rf
try:
    from mailjet_rest import Client
    FG_HAS_MAILJET = True
except:
    FG_HAS_MAILJET = False
from gluon.contrib.markmin import markmin2html

#Constants
GRAPH_HIDE = "<script>$('#loading').hide();$('#thegraph').hide()</script>"
RESERVED_LIST = ["admin","examples","welcome","wiki","showcase","filtergraph","register","login","resetpassword","retrieveusername","facebook","twitter","youtube","googleplus","tumblr","feedback","team","terms","privacy","security","portal","portals","myportals","dataset","datafile","forum","newportal","create","admincode","help","profile","password","changepassword","purchase","faq","delete","settings","edit","version","start","benchmark",'access','allow','hypatia','aavso','vanderbilt']

#FUNCTIONS
def floatable(var,invalid=None):
    """Converts a variable to a float if possible, otherwise return invalid value"""
    try:
        return float(var)
    except:
        return invalid

def floatable_g0(var,invalid=None):
    """Converts a variable to a float if possible, otherwise return invalid value. If zero or less, return invalid value"""
    try:
        a = float(var)
        if a <= 0:
            return invalid
        else:
            return a
    except:
        return invalid
    
def intable(var,invalid=None):
    """Converts a variable to an integer if possible, otherwise return invalid value"""
    try:
        return int(float(var))
    except:
        return invalid

def colorable(var,fg=None):
    """Tests if a color string is valid"""
    try:
        if var[0] == "#":
            var2 = var[1:]
        else:
            var2 = var
        if len(var2) == 6:
            testvar = int(var2,16)
            if var2 == "000000":
                return fg
            else:
                return var2
        else:
            return None
    except:
        return None

def nanable(var,invalid=""):
    """Tests if variable is nan"""
    if var == var:
        return var
    else:
        return ""

def gpesc(var):
    """Prepares a string for safe import into gnuplot"""
    if var:
        return var.replace('\\','\\\\').replace('"','\\"').replace("() {","")
        #eliminates possibility of Shellshock bug
        #source: http://en.wikipedia.org/wiki/Shellshock_(software_bug)
    else:
        return ""
        
"""def disp(val,commas=True):
    try:
        if commas:
            retval = "{0:,.11g}".format(val)
            if "e" in retval and "." not in retval:
                return "{0:,.11g}".format(val).replace("e",".e")
            elif "." in retval:
                return "{0:,g}".format(val)
            else:
                return retval
        else:
            retval = "{0:.11g}".format(val)
            if "e" in retval and "." not in retval:
                return "{0:.11g}".format(val).replace("e",".e")
            elif "." in retval:
                return "{0:g}".format(val)
            else:
                return retval
    except:
        return val"""
        
def log(text):
    try:
        db.auth_event.insert(user_id=auth.user_id,description="User %s %s" % (auth.user_id,text))
    except:pass
    print "%s: User %s %s" % (request.now,auth.user_id,text)
    return

# Based on "angsep.py"
# Written by Erno Middelberg 2001
# http://www.atnf.csiro.au/people/Enno.Middelberg/python/angsep.py
def angsep(ra1deg, dec1deg, ra2deg, dec2deg):
    """Determines the angular separation of two points on the night sky. Used for Slowpokes portal"""
    if ra1deg==ra2deg and dec1deg==dec2deg: return 0
    # conversion of right ascension 1:
    ra1rad=ra1deg*math.pi/180
    # conversion of declination 1:
    dec1rad=dec1deg*math.pi/180
    # conversion of right ascension 2:
    ra2rad=ra2deg*math.pi/180
    # conversion of declination 2:
    dec2rad=dec2deg*math.pi/180
    # calculate scalar product for determination
    # of angular separation
    x=math.cos(ra1rad)*math.cos(dec1rad)*math.cos(ra2rad)*math.cos(dec2rad)
    y=math.sin(ra1rad)*math.cos(dec1rad)*math.sin(ra2rad)*math.cos(dec2rad)
    z=math.sin(dec1rad)*math.sin(dec2rad)
    rad=math.acos(x+y+z)
    # use Pythargoras approximation if rad < 1 arcsec
    if rad<0.000004848:
        rad=math.sqrt((math.cos(dec1rad)*(ra1rad-ra2rad))**2+(dec1rad-dec2rad)**2)
    # Angular separation
    deg=rad*180/math.pi
    return deg

def parse_list(s):
    """Old method for converting strings to dictionaries. Use JSON library instead"""
    val = {}
    if s:
        for s_match in s.splitlines():
            s_split = s_match.split('=',1)
            val[s_split[0].strip()] = s_split[1].strip()
    return val

def sanitize(s,a, translations, textonly=False, keepstrings=False, getstrings=False):
    """Safely performs the function s on an array a using translations.
    
    Keyword arguments:
    translations -- an array of tuples (input_string, output_string, type). All the input strings are converted to output strings. If type is 'v' it cannot be used as a function.
    textonly -- if true and valid returns the string 'VALID' followed by the output string, otherwise runs the output string through eval().
    """
    #Remove initial or from function. This is handled separately
    if s[:3].lower() == "or ":
        s = s[3:]
    if s.startswith("lasso:"):
        sspl = s.split(":")
        #print "sspl = "+repr(sspl)
        lasso_savestate_path = os.path.join(FG_SAVESHARE_FOLDER,sspl[1])
        with open(lasso_savestate_path) as f:
             lasso_savestate = json.loads(f.read())['settings']
        lasso_x = sanitize(lasso_savestate['xaxis'].replace("%%",""),a,translations)
        #print "lasso_x = "+repr(lasso_x)
        lasso_y = sanitize(lasso_savestate['yaxis'].replace("%%",""),a,translations)
        #print "lasso_y = "+repr(lasso_y)
        
        #lasso filters
        lasso_filters = []
        lasso_has_index = False
        for key in lasso_savestate.keys():
            if key.startswith("en_") and lasso_savestate.get(key):
                filterid = key[3:]
                new_filter = {'var':lasso_savestate.get("var_"+filterid).strip().replace("%%",""),'min':lasso_savestate.get("min_"+filterid).strip().replace("%%",""),'max':lasso_savestate.get("max_"+filterid).strip().replace("%%",""),'opt':lasso_savestate.get("opt_"+filterid).strip()}
                if new_filter['var'] and (new_filter['min'] or new_filter['max']):
                    lasso_filters.append(new_filter)
                    if 'Index' in new_filter['var']:
                        lasso_has_index = True
                    if 'index()' in new_filter['var']:
                        lasso_has_index = True
        for axis in ['xaxis','yaxis','color','size']:
            if lasso_savestate.get(axis):
                new_filter = {'var':lasso_savestate.get(axis).strip().replace("%%",""),'min':lasso_savestate.get(axis[0]+"_min_bound").strip().replace("%%",""),'max':lasso_savestate.get(axis[0]+"_max_bound").strip().replace("%%",""),'opt':"is between"}
                if new_filter['var'] and (new_filter['min'] or new_filter['max']):
                    lasso_filters.append(new_filter)
                    if 'Index' in new_filter['var']:
                        lasso_has_index = True
                    if 'index()' in new_filter['var']:
                        lasso_has_index = True
        if lasso_has_index and '__Index__' not in a.dtype.names:
            aa = rf.append_fields(a,'__Index__',np.arange(len(a)),usemask=False)
        else:
            aa = a
        
        lasso_path = [float(i) for i in sspl[2].split(",")]
        #print "lasso_path = "+repr(lasso_path)
        lasso_pathcode = [3 for i in range(len(lasso_path)/2)]
        lasso_pathcode[0] = 1
        lasso_pathcode[-1] = 2
        #print "lasso_pathcode = "+repr(lasso_pathcode)
        
        #if a path was given
        path = []
        #build up (x,y) pairs
        for p in xrange(0,len(lasso_path),2):
            if len(path) > 0:
                path = np.append(path,np.array([[lasso_path[p],lasso_path[p+1]]]),0)
            else:
                path = np.array([[lasso_path[p],lasso_path[p+1]]])
        #print "path = "+repr(path)
            
        #convert path to coordinates
        #for p in xrange(len(path)):
        #    path[p,0] = x_conv(path[p,0])
        #    path[p,1] = y_conv(path[p,1])

        #filter out points in the path
        #selected_data = np.array(filter(lambda p : Path(path,codes=pathcode).contains_point(p), dataout))
        #dataout = np.array(list(set(tuple(x) for x in dataout).difference(list(tuple(y) for y in selected_data))))
        lasso_data = np.column_stack([lasso_x,lasso_y])
        #print "lasso_data = "+repr(lasso_data)
        if len(lasso_filters) > 0:
            compute_lasso_filters = compute_filters(lasso_filters,aa,translations)
            if type(compute_lasso_filters) == str:
                return compute_lasso_filters
            result = np.logical_and(compute_lasso_filters,Path(path,codes=lasso_pathcode).contains_points(lasso_data))
        else:
            result = Path(path,codes=lasso_pathcode).contains_points(lasso_data)
        #print "result = "+repr(result)
        if getstrings:
            return (None,result)
        else:
            return result
    
    out = ""
    variable_count = 0
    function_count = 0
    while s != "":
        for i in translations:
            if s.startswith(i[0]):
                s = s[len(i[0]):]
                if s.startswith("(") and i[2] == "variable":
                    return "You cannot call a variable name as a function."
                out += i[1]
                if i[2] == "variable":
                    variable_count += 1
                if i[2] == "function":
                    function_count += 1
                break
        else:
            return "A function or variable name is invalid."
    paren = 0
    for ch in out:
        if ch == "(":
            paren += 1
        if ch == ")":
            paren -= 1
            if paren < 0:
                return "The parentheses are unbalanced."
    if paren != 0:
        return "The parentheses are unbalanced."
    if variable_count > FG_MAX_VARIABLES:
        return "Too many variables."
    if function_count > FG_MAX_FUNCTIONS:
        return "Too many functions."
    if textonly:
        return "VALID"+out

    #try:
    result = eval(out)
    if not isinstance(result,np.ndarray):
        return result
    elif result.dtype.kind == 'S' and not keepstrings:
        if getstrings:
            return np.unique(result,return_inverse=True)
        else:
            return np.unique(result,return_inverse=True)[1]
    else:
        if getstrings:
            return (None,result)
        else:
            return result
    #except:
    #    return "The function does not compute."

def bound_round(a1,a2,logfun=False,ignore=False,inclusive=False):
    """Neatly formats two bounds (a1,a2) for graph output. For instance, (2,49) is returned as (0,50).
    
    Keyword arguments:
    logfun -- round only to powers of ten. (2,49) returns as (1,100).
    ignore -- don't perform any bound rounding. (2,49) returns as (2,49).
    inclusive -- rounding up/down is inclusive. (0,10) returns as (-10,20).
    """
    if ignore:
        return (a1,a2)
    if a1 == a2:
        return (a1-1,a2+1)
    if math.floor(a1) == 0 and math.ceil(a2) == 360:
        return (0,360)
    if math.floor(a1) == -90 and math.ceil(a2) == 90:
        return (-90,90)
    if math.floor(a1) == -180 and math.ceil(a2) == 180:
        return (-180,180)
    if logfun:
        if a1 <= 0:
            z1 = a1
        else:
            z1 = 10**floor2(math.log10(a1),inclusive=inclusive)
        if a2 <= 0:
            z2 = a2
        else:
            z2 = 10**ceil2(math.log10(a2),inclusive=inclusive)
        return (z1,z2)
    b = abs(a2 - a1)
    if b >= 1:
        i = 1.0
        while i <= 1e300:
            for j in [1,2,5]:
                c1 = floor2(a1/(i*j),inclusive=inclusive) * i*j
                c2 = ceil2(a2/(i*j),inclusive=inclusive) * i*j
                d = abs(c2 - c1) / (i*j)
                if d <= 6:
                    return (c1,c2)
            i *= 10
    else:
        i = 1.0
        while i >= 1e-300:
            for j in [5,2,1]:
                c1 = floor2(a1/(i*j),inclusive=inclusive) * i*j
                c2 = ceil2(a2/(i*j),inclusive=inclusive) * i*j
                d = abs(c2 - c1) / (i*j)
                if d > 6:
                    return (c1,c2)
            i /= 10
    return (a1,a2)

def floor2(x,inclusive=False):
    """Allows inclusive rounding down of integers. If inclusive is true, 2 rounds down to 1"""
    if x == math.floor(x) and not inclusive:
        return x - 1
    else:
        return math.floor(x)

def ceil2(x,inclusive=False):
    """Allows inclusive rounding up of integers. If inclusive is true, 2 rounds up to 3"""
    if x == math.ceil(x) and not inclusive:
        return x + 1
    else:
        return math.ceil(x)

def errormessage(status,details="",button=None,action=None):
    """Error messages when plotting graphs"""
    if request.ajax:
        return response.render('run/error.html',status=status,details=details,button=button,action=action)
    else:
        return response.render('error/handle_error.html',message=status,details=details,errorid=request.url)

def httplink(s,title="Click here"):
    """Converts a string s to an http link with the title."""
    a = str(s)
    if a.startswith("http"):
        a = a.replace("&","&amp;").replace("<","&lt;").replace(">","&gt;").replace("\"","&quot;")
        b = str(title).replace("&","&amp;").replace("<","&lt;").replace(">","&gt;").replace("\"","&quot;")
        return '<a href="%s" target="_blank"><span data-icon="q"></span></a>&nbsp;%s' % (a,b)
    else:
        return a
    
def email_user(content,user=auth.user_id,subject="A message from Filtergraph",link=None,button=None,custom_name=None,override=False,safety=True):
    """Sends an email with the content and subject given to a user.
    
    Keyword arguments:
    user -- if user is 'qc' send to all users with membership in the qc group
            if user is an integer/long send to user with that id
            if user is string send to that email
            if user is a tuple or list send to all items in that list recursively
    override -- if false do not send if user has opted out of email notifications
    safety -- if true output to console only; do not send the email
    """
    log("sends email to user %s, subject %s" % (repr(user),repr(subject)))
    if user == "qc":
        qcid = db(db.auth_group.role=="qc").select().first().id
        qcpeople = db(db.auth_membership.group_id==qcid).select()
        return "<br/>".join([email_user(content=content,user=q.user_id.id,subject=subject,link=link,button=button,custom_name=None,override=override,safety=safety) for q in qcpeople])
    elif type(user) == int or type(user) == long:
        account = db.auth_user[user]
        email = account.email
        name = account.first_name
        noemail = account.noemail
    elif type(user) == str:
        email = user
        account = db(db.auth_user.email==email).select().first()
        if account:
            name = account.first_name
            noemail = account.noemail
        else:
            name = email
            noemail = db(db.subscriptions.email==email)(db.subscriptions.noemail==True).count() > 0
    elif type(user) == tuple or type(user) == list:
        return "<br/>".join([email_user(content=content,user=u,subject=subject,link=link,button=button,custom_name=None,override=override,safety=safety) for u in user])
    else:
        return False
    if custom_name:
        name = custom_name
    subscription = db(db.subscriptions.email == email).select().first()
    if subscription:
        subscribe_id = subscription.subscribe_id
        if subscription.noemail:
            noemail = True
    else:
        subscribe_id = ''.join(random.choice("ABCDEFGHJKLMNPQRSTUVWXYZ23456789") for x in range(20))
        db.subscriptions.insert(email=email,subscribe_id=subscribe_id)
    if noemail and not override:
        return "%s has opted out of e-mail notifications from Filtergraph; no email was sent." % name
    print "SENDING EMAIL"
    print email
    print subject
    print response.render("email.txt",dict(content=content,name=name,link=link,subject=subject,button=button,subscribe_id=subscribe_id))
    if safety:
        pass
    elif FG_HAS_MAILJET and FG_MAILJET_APIKEY:
        mailjet = Client(auth=(FG_MAILJET_APIKEY, FG_MAILJET_SECRETKEY))
        mj_email = {
            'FromName': 'Filtergraph',
            'FromEmail': FG_MAILJET_FROM,
            'Subject': subject,
            'Html-Part': response.render("email.html",dict(content=content,name=name,link=link,subject=subject,button=button,subscribe_id=subscribe_id)),
            'Text-Part': response.render("email.txt" ,dict(content=content,name=name,link=link,subject=subject,button=button,subscribe_id=subscribe_id)),
            'Recipients': [{'Email': email}]
        }
        mailjet.send.create(mj_email)
    else:
        htmlpart = response.render("email.html",dict(content=content,name=name,link=link,subject=subject,button=button,subscribe_id=subscribe_id)),
        textpart = response.render("email.txt" ,dict(content=content,name=name,link=link,subject=subject,button=button,subscribe_id=subscribe_id)),
        mail.send(email,subject,(textpart,htmlpart))
    return "Message to %s sent." % name

def palette(a,invert=False,reverse=False):
    """Converts a palette to a useful format for Gnuplot. Can invert/reverse a palette if needed."""
    b = [[aa[0],aa[1]/255.,aa[2]/255.,aa[3]/255.] for aa in a]
    if reverse:
        c = b[:]
        for i in range(len(c)):
            c[i] = [b[i][0],b[-i-1][1],b[-i-1][2],b[-i-1][3]]
        b = c
    if invert:
        b = [[bb[0],1-bb[1],1-bb[2],1-bb[3]] for bb in b]
    return "set palette defined (%s)" % ",".join(" ".join([str(bbb) for bbb in bb]) for bb in b)
    
def invert(s):
    """Inverts a HTML color"""
    try:
        red = 256 - int(s[:2],16)
        green = 256 - int(s[2:4],16)
        blue = 256 - int(s[4:],16)
        return "%2X%2X%2X" % (red,green,blue)
    except:
        return s

pal = [
            ("Rainbow",[[1,50,136,189],[2,153,213,148],[3,230,245,152],[4,254,224,139],[5,252,141,89],[6,213,62,79]],"Spectral / Cynthia Brewer / colorbrewer2.org"),
            ("Rainbow Discrete",[[1,68,170,153],[2,68,170,153],[2,153,153,51],[3,153,153,51],[3,170,68,153],[4,170,68,153],[4,136,204,238],[5,136,204,238],[5,51,34,136],[6,51,34,136],[6,136,34,85],[7,136,34,85],[7,187,179,85],[8,187,179,85],[8,204,102,118],[9,204,102,118],[9,17,119,51],[10,17,119,51],[10,221,204,119],[11,221,204,119]],"Paul Tol / https://personal.sron.nl/~pault/"),
            ("Red",[[1,254,224,210],[2,252,146,114],[3,222,45,38]],"Reds / Cynthia Brewer / colorbrewer2.org"),
            ("Green",[[1,229,245,224],[2,161,217,155],[3,49,163,84]],"Greens / Cynthia Brewer / colorbrewer2.org"),
            ("Blue",[[1,222,235,247],[2,158,202,225],[3,49,130,189]],"Blues / Cynthia Brewer / colorbrewer2.org"),
            ("Purple",[[1,254, 235, 226],[2, 251, 180, 185],[3, 247, 104, 161],[4, 197, 27, 138],[5, 122, 1, 119]], "RdPu / Cynthia Brewer / colorbrewer2.org"),
            ("Fall",[[1,255,255,178],[2,254,204,92],[3,253,141,60],[4,240,59,32],[5,189,0,38]],"YlOrRd / Cynthia Brewer / colorbrewer2.org"),
            ("Earthy",[[1,255, 255, 204],[2, 161, 218, 180], [3,65, 182, 196],[4, 44, 127, 184],[5, 37, 52, 148]], "YlGnBu / Cynthia Brewer / colorbrewer2.org"),
            ("Pink and Green",[[1,77,172,38],[2,184,225,134],[3,247,247,247],[4,241,182,218],[5,208,28,139]],"PiYG / Cynthia Brewer / colorbrewer2.org"),
            ("Red and Blue",[[1,5,113,176],[2,146,197,222],[3,247,247,247],[4,244,165,130],[5,202,0,32]],"RdBu / Cynthia Brewer / colorbrewer2.org"),
            ("Two Colors",[[1,228,26,28],[2,228,26,28],[2,55,126,184],[3,55,126,184]],"Set1 / Cynthia Brewer / colorbrewer2.org"),
            ("Three Colors",[[1,228,26,28],[2,228,26,28],[2,55,126,184],[3,55,126,184],[3,77,175,74],[4,77,175,74]],"Set1 / Cynthia Brewer / colorbrewer2.org"),
            ("Four Colors",[[1,228,26,28],[2,228,26,28],[2,55,126,184],[3,55,126,184],[3,77,175,74],[4,77,175,74],[4,152,78,163],[5,152,78,163]],"Set1 / Cynthia Brewer / colorbrewer2.org"),
            ("Five Colors",[[1,228,26,28],[2,228,26,28],[2,55,126,184],[3,55,126,184],[3,77,175,74],[4,77,175,74],[4,152,78,163],[5,152,78,163],[5,255,127,0],[6,255,127,0]],"Set1 / Cynthia Brewer / colorbrewer2.org"),
            ("Six Colors",[[1,228,26,28],[2,228,26,28],[2,55,126,184],[3,55,126,184],[3,77,175,74],[4,77,175,74],[4,152,78,163],[5,152,78,163],[5,255,127,0],[6,255,127,0],[6,255,255,51],[7,255,255,51]],"Set1 / Cynthia Brewer / colorbrewer2.org"),
            ("Seven Colors",[[1,228,26,28],[2,228,26,28],[2,55,126,184],[3,55,126,184],[3,77,175,74],[4,77,175,74],[4,152,78,163],[5,152,78,163],[5,255,127,0],[6,255,127,0],[6,255,255,51],[7,255,255,51],[7,166,86,40],[8,166,86,40]],"Set1 / Cynthia Brewer / colorbrewer2.org"),
            ("Eight Colors",[[1,228,26,28],[2,228,26,28],[2,55,126,184],[3,55,126,184],[3,77,175,74],[4,77,175,74],[4,152,78,163],[5,152,78,163],[5,255,127,0],[6,255,127,0],[6,255,255,51],[7,255,255,51],[7,166,86,40],[8,166,86,40],[8,247,129,191],[9,247,129,191]],"Set1 / Cynthia Brewer / colorbrewer2.org"),
            ("Nine Colors",[[1,228,26,28],[2,228,26,28],[2,55,126,184],[3,55,126,184],[3,77,175,74],[4,77,175,74],[4,152,78,163],[5,152,78,163],[5,255,127,0],[6,255,127,0],[6,255,255,51],[7,255,255,51],[7,166,86,40],[8,166,86,40],[8,247,129,191],[9,247,129,191],[9,153,153,153],[10,153,153,153]],"Set1 / Cynthia Brewer / colorbrewer2.org"),
            ("Two Pairs",[[1,166,206,227],[2,166,206,227],[2,31,120,180],[3,31,120,180],[3,178,223,138],[4,178,223,138],[4,51,160,44],[5,51,160,44]],"Paired / Cynthia Brewer / colorbrewer2.org"),
            ("Three Pairs",[[1,166,206,227],[2,166,206,227],[2,31,120,180],[3,31,120,180],[3,178,223,138],[4,178,223,138],[4,51,160,44],[5,51,160,44],[5,251,154,153],[6,251,154,153],[6,227,26,28],[7,227,26,28]],"Paired / Cynthia Brewer / colorbrewer2.org"),
            ("Four Pairs",[[1,166,206,227],[2,166,206,227],[2,31,120,180],[3,31,120,180],[3,178,223,138],[4,178,223,138],[4,51,160,44],[5,51,160,44],[5,251,154,153],[6,251,154,153],[6,227,26,28],[7,227,26,28],[7,253,191,111],[8,253,191,111],[8,255,127,0],[9,255,127,0]],"Paired / Cynthia Brewer / colorbrewer2.org"),
            ("Five Pairs",[[1,166,206,227],[2,166,206,227],[2,31,120,180],[3,31,120,180],[3,178,223,138],[4,178,223,138],[4,51,160,44],[5,51,160,44],[5,251,154,153],[6,251,154,153],[6,227,26,28],[7,227,26,28],[7,253,191,111],[8,253,191,111],[8,255,127,0],[9,255,127,0],[9,202,178,214],[10,202,178,214],[10,106,61,154],[11,106,61,154]],"Paired / Cynthia Brewer / colorbrewer2.org"),
            ("Two Scales",[[1,166,206,227],[3,31,120,180],[3,178,223,138],[5,51,160,44]],"Paired / Cynthia Brewer / colorbrewer2.org"),
            ("Three Scales",[[1,166,206,227],[3,31,120,180],[3,178,223,138],[5,51,160,44],[5,251,154,153],[7,227,26,28]],"Paired / Cynthia Brewer / colorbrewer2.org"),
            ("Four Scales",[[1,166,206,227],[3,31,120,180],[3,178,223,138],[5,51,160,44],[5,251,154,153],[7,227,26,28],[7,253,191,111],[9,255,127,0]],"Paired / Cynthia Brewer / colorbrewer2.org"),
            ("Five Scales",[[1,166,206,227],[3,31,120,180],[3,178,223,138],[5,51,160,44],[5,251,154,153],[7,227,26,28],[7,253,191,111],[9,255,127,0],[9,202,178,214],[11,106,61,154]],"Paired / Cynthia Brewer / colorbrewer2.org"),
            ("Grayscale",[[1,255,255,255],[2,0,0,0]],""),
            ("Old Rainbow",[[1,0,0,128],[2,0,0,255],[3,0,255,255],[4,0,255,0],[5,255,255,0],[6,255,0,0],[7,128,0,0]], ""),
     ]

font_list = [
    ("Bitstream Vera Sans","Bitstream-Vera-Sans","Bitstream / fontsquirrel.com"),
    ("Liberation Sans","Liberation-Sans","Red Hat / fontsquirrel.com"),
    ("Liberation Serif","Liberation-Serif","Red Hat / fontsquirrel.com"),
    ("Junction","Junction","League of Moveable Type / Open Font License"),
    ("ShareTech","ShareTech","Open Font License"),
    ("Viga","Viga","Open Font License"),
    ("Ubuntu","Ubuntu","Ubuntu Font Licence"),
    ("Open Sans","Open-Sans","Open Font License"),
    ("Roboto","Roboto","Apache License"),
    ("Maven Pro","Maven-Pro","Open Font License")
]

font_list_ttf = [
    ("Bitstream Vera Sans",os.path.join(request.folder,"static","fonts","ttf","Bitstream-Vera-Sans.ttf"),"Bitstream / fontsquirrel.com"),
    ("Liberation Sans",os.path.join(request.folder,"static","fonts","ttf","Liberation-Sans.ttf"),"Red Hat / fontsquirrel.com"),
    ("Liberation Serif",os.path.join(request.folder,"static","fonts","ttf","Liberation-Serif.ttf"),"Red Hat / fontsquirrel.com"),
    ("Junction",os.path.join(request.folder,"static","fonts","ttf","Junction.ttf"),"League of Moveable Type / Open Font License"),
    ("ShareTech",os.path.join(request.folder,"static","fonts","ttf","ShareTech.ttf"),"Open Font License"),
    ("Viga",os.path.join(request.folder,"static","fonts","ttf","Viga.ttf"),"Open Font License"),
    ("Ubuntu",os.path.join(request.folder,"static","fonts","ttf","Ubuntu.ttf"),"Ubuntu Font Licence"),
    ("Open Sans",os.path.join(request.folder,"static","fonts","ttf","Open-Sans.ttf"),"Open Font License"),
    ("Roboto",os.path.join(request.folder,"static","fonts","ttf","Roboto.ttf"),"Apache License"),
    ("Maven Pro",os.path.join(request.folder,"static","fonts","ttf","Maven-Pro.ttf"),"Open Font License")
]
    
def http_format(s):
    """Converts URLs to hyperlinks."""
    s = markmin2html.markmin2html(s,sep="br")
    s = s.replace("<a ","<a target='_blank' ")
    #print s
    return s

def dataset_namer(s,datasets=None,ignore=None):
    """Names datasets sequentially if two datasets have the same name. If 'file.txt' is uploaded and 'file.txt' already exists in the portal, upload as 'file.txt (2)'
    
    Keyword arguments:
    ignore -- if string is in the list of datasets, ignore that string"""
    if s.strip() == "":
        s2 = "data-txt"
    else:
        s2 = s.strip().replace("\t"," ")
    if datasets:
        s3 = s2
        s3_clean = cleanurl(s3)
        i = 1
        while i < 999:
            match = False
            for d in datasets:
                if cleanurl(d.name) == s3_clean and d.name != ignore:
                    match = True
            if match:
                i += 1
                s3 = "%s (%s)" % (s2,i)
                s3_clean = cleanurl(s3)
                continue
            return s3
        s3 = "%s (%s)" % (s2,i)
        return s3
    else:
        return s2

def get_cities(lat_min,lat_max,long_min,long_max,fname,entries,sep_lat,sep_long):
    lines = 0
    boxes = []
    box_lat = abs(lat_max - lat_min) / sep_lat
    box_long = abs(long_max - long_min) / sep_long
    #print box_lat
    #print box_long
    ranks = []
    with open(os.path.join(request.folder,"static","map","cities.txt")) as fin:
        with open(fname+"-cities.txt",'w') as fout:
            with open(fname+"-cities2.txt",'w') as fout2:
                #fout.write("Latitude: %s to %s\n" % (lat_min,lat_max))
                #fout.write("Longitude: %s to %s\n" % (long_min,long_max))
                for line in fin:
                    linespl = line.split()
                    latitude = float(linespl[1])
                    longitude = float(linespl[2])
                    test = False
                    if latitude >= lat_min and latitude <= lat_max:
                        if longitude >= long_min and longitude <= long_max:
                            test = True
                    if not test:
                        continue
                    #TEST SEQUENCE 1
                    if longitude <= long_max - box_long:
                        box_test = (latitude-box_lat,latitude+box_lat,longitude,longitude+box_long)
                    else:
                        box_test = (latitude-box_lat,latitude+box_lat,longitude-box_long,longitude)
                    for box in boxes:
                        if box_test[1] <= box[0]:
                            continue
                        if box_test[0] >= box[1]:
                            continue
                        if box_test[3] <= box[2]:
                            continue
                        if box_test[2] >= box[3]:
                            continue
                        test = False
                    if test:
                        if longitude <= long_max - box_long:
                            fout.write(line)
                        else:
                            fout2.write(line)
                        lines += 1
                        boxes.append(box_test)
                        ranks.append(int(linespl[0]))
                        if lines >= entries:
                            if len(ranks) == 0:
                                return 999
                            else:
                                return float(sum(ranks))/len(ranks)
                        continue
                    #TEST SEQUENCE 2
                    test = False
                    box_test = (latitude-box_lat,latitude+box_lat,longitude-box_long,longitude)
                    for box in boxes:
                        if box_test[1] <= box[0]:
                            continue
                        if box_test[0] >= box[1]:
                            continue
                        if box_test[3] <= box[2]:
                            continue
                        if box_test[2] >= box[3]:
                            continue
                        test = False
                    if test:
                        fout2.write(line)
                        lines += 1
                        boxes.append(box_test)
                        ranks.append(int(linespl[0]))
                        if len(ranks) == 0:
                            return 999
                        else:
                            return float(sum(ranks))/len(ranks)
    if len(ranks) == 0:
        return 999
    else:
        return float(sum(ranks))/len(ranks)

def janitor():
    day_range = [FG_JANITOR_CLEAR_FILES] + range(int(FG_JANITOR_CLEAR_FILES),int(FG_JANITOR_CLEAR_WHEN_FULL),-1) + [FG_JANITOR_CLEAR_WHEN_FULL]
    
    for days_to_delete in day_range:
        #print "Days to delete: %s" % days_to_delete
        for path in [FG_IMAGE_FOLDER,FG_CACHESTAT_FOLDER,FG_DONE_FOLDER,FG_TRASH_UPLOAD_FOLDER,FG_TRASH_CACHE_FOLDER,FG_UPLOAD_FOLDER,FG_TABLE_FOLDER,FG_TRASH_TABLE_FOLDER]:
            #print "Clearing out: " + path
            now = time.time()
            for f in os.listdir(path):
                f = os.path.join(path, f)
                if (now - os.stat(f).st_mtime) / 86400 > days_to_delete:
                    if os.path.isfile(f):
                        #print "Deleting: " + f
                        os.remove(f)
        s = os.statvfs(FG_DATA_FOLDER)
        used_pct = (float(s.f_blocks - s.f_bavail)/s.f_blocks) * 100
        #print "Used percent: %s" % used_pct
        if used_pct > FG_JANITOR_FULL_DISK:
            continue
        else:
            break
        
    appfolder = os.path.join(request.env.web2py_path,"applications",request.application)
    for days_to_delete in day_range:
        #print "Days to delete: %s" % days_to_delete
        for path in [os.path.join(appfolder,'sessions'),os.path.join(appfolder,'errors')]:
            #print "Clearing out: " + path
            now = time.time()
            for f in os.listdir(path):
                f = os.path.join(path, f)
                if (now - os.stat(f).st_mtime) / 86400 > days_to_delete:
                    if os.path.isfile(f):
                        #print "Deleting: " + f
                        os.remove(f)
                    elif os.path.isdir(f):
                        #print "Deleting folder: " + f
                        shutil.rmtree(f)
        s = os.statvfs(appfolder)
        used_pct = (float(s.f_blocks - s.f_bavail)/s.f_blocks) * 100
        #print "Used percent: %s" % used_pct
        if used_pct > FG_JANITOR_FULL_DISK:
            continue
        else:
            break

def formbuilder(mInput):
    #output = "<form class=\"form-horizontal col-sm-6\" style=\"padding-bottom:50px\" action=\"#\" enctype=\"multipart/form-data\" method=\"post\">"
    output = ""
    for inputs in mInput:
        output += chooseForm(inputs)
    output += '<div class="form-group"><div class="col-sm-offset-3 col-sm-9">'
    output += "\n\t<button type=\"submit\" class=\"btn btn-default\">Submit</button></div></div>"#</form>"
    return XML(output)
    
def chooseForm(inputs):
    if(inputs[2]=='text'):
        return textForm(inputs)
    elif(inputs[2]=='url'):
        return urlForm(inputs)
    elif(inputs[2]=='upload'):
        return uploadForm(inputs)
    elif(inputs[2]=='checkbox'):
        return checkBoxForm(inputs)
    elif(inputs[2]=='password'):
        return passwordForm(inputs)
    elif(inputs[2]=='email'):
        return emailForm(inputs)
    elif(inputs[2]=='textbox'):
        return textBoxForm(inputs)
    
def textForm(inputs):
    textOutput = "\n\t<div class=\"form-group\" id='%s_block'>" % inputs[0]
    textOutput += "\n\t\t<label for=\"{0}\" style=\"margin-top:15px;cursor:pointer\" class=\"col-sm-3 control-label\">{1}</label>".format(inputs[0], inputs[1])
    textOutput += "\n\t\t<div class=\"col-sm-9\">"
    textOutput += "\n\t\t\t<input  type=\"{0}\" class=\"form-control\" id=\"{1}\" name=\"{1}\" placeholder=\"{2}\" value=\"{3}\">".format(inputs[2],inputs[0], inputs[1], inputs[3] or "")
    textOutput += "<span class=\"label label-danger\">%s</span>" % (inputs[4] or "")
    textOutput += "\n\t\t</div>\n\t</div>"
    return textOutput
    
def textBoxForm(inputs):
    textOutput = "\n\t<div class=\"form-group\" id='%s_block'>" % inputs[0]
    textOutput += "\n\t\t<label for=\"{0}\" style=\"margin-top:15px;cursor:pointer\" class=\"col-sm-3 control-label\">{1}</label>".format(inputs[0], inputs[1])
    textOutput += "\n\t\t<div class=\"col-sm-9\">"
    textOutput += "\n\t\t\t<textarea class=\"form-control\" id=\"{1}\" name=\"{1}\" placeholder=\"{2}\">{3}</textarea>".format(inputs[2],inputs[0], inputs[1], inputs[3] or "")
    textOutput += "<span class=\"label label-danger\">%s</span>" % (inputs[4] or "")
    textOutput += "\n\t\t</div>\n\t</div>"
    return textOutput
    
def urlForm(inputs):
    textOutput = "\n\t<div class=\"form-group\" id='%s_block'>" % inputs[0]
    textOutput += "\n\t\t<label for=\"{0}\" style=\"margin-top:15px;cursor:pointer\" class=\"col-sm-3 control-label\">{1}</label>".format(inputs[0], inputs[1])
    textOutput += "\n\t\t<div class=\"col-sm-9\">"
    textOutput += '<div class="input-group" style="height:55px"><span style="border:none" class="input-group-addon">%s</span>' % inputs[5]
    textOutput += "\n\t\t\t<input  type=\"{0}\" class=\"form-control\" id=\"{1}\" name=\"{1}\" placeholder=\"{2}\" value=\"{3}\"></div>".format("text",inputs[0], inputs[1], inputs[3] or "")
    textOutput += "<span class=\"label label-danger\">%s</span>" % (inputs[4] or "")
    textOutput += '<div id="checkportal"></div>'
    textOutput += "\n\t\t</div>\n\t</div>"
    return textOutput
    
def uploadForm(inputs):
    uploadOutput = "\n\t<div class=\"form-group\" id='%s_block'>" % inputs[0]
    uploadOutput += "\n\t\t<label for=\"{0}\" style=\"margin-top:15px;cursor:pointer\" class=\"col-sm-3 control-label\">{1}</label>".format(inputs[0], inputs[1])
    uploadOutput += "\n\t\t<div class=\"col-sm-9\">"
    uploadOutput += "\n\t\t\t<input type=\"file\" class=\"form-control\" id=\"{1}\" name=\"{1}\" placeholder=\"{2}\">".format(inputs[2],inputs[0],inputs[1])
    uploadOutput += "<span class=\"label label-danger\">%s</span>" % (inputs[4] or "")
    uploadOutput += "\n\t\t</div>\n\t</div>"
    return uploadOutput
    
def checkBoxForm(inputs):
    cbOutput =  "<div class=\"col-sm-offset-3 col-sm-9\" id='%s_block'>" % inputs[0]
    cbOutput += "\n\t<div class=\"checkbox\">"
    cbOutput += "\n\t\t<label style=\"margin-top:15px;cursor:pointer\">"
    cbOutput += "\n\t\t\t{0}".format(inputs[1])
    cbOutput += "\n\t\t\t<input  type=\"{0}\" id=\"{1}\" name=\"{1}\" {2} aria-label=\"{3}\">".format(inputs[2],inputs[0], inputs[3] and 'checked="checked"' or "", inputs[1])
    cbOutput += "<span class=\"label label-danger\">%s</span>" % (inputs[4] or "")
    cbOutput += "\n\t\t</label>\n\t</div><br/></div>"
    return cbOutput
    
def passwordForm(inputs):
    textOutput = "\n\t<div class=\"form-group\" id='%s_block'>" % inputs[0]
    textOutput += "\n\t\t<label for=\"{0}\" style=\"margin-top:15px;cursor:pointer\" class=\"col-sm-3 control-label\">{1}</label>".format(inputs[0], inputs[1])
    textOutput += "\n\t\t<div class=\"col-sm-9\">"
    textOutput += "\n\t\t\t<input type=\"password\" class=\"form-control\" id=\"{1}\" name=\"{1}\" placeholder=\"{2}\" value=\"{3}\">".format(inputs[2],inputs[0], inputs[1], inputs[3] or "")
    textOutput += "<span class=\"label label-danger\">%s</span>" % (inputs[4] or "")
    textOutput += "\n\t\t</div>\n\t</div>"
    return textOutput
    
def emailForm(inputs):
    textOutput = "\n\t<div class=\"form-group\" id='%s_block'>" % inputs[0]
    textOutput += "\n\t\t<label for=\"{0}\" style=\"margin-top:15px;cursor:pointer\" class=\"col-sm-3 control-label\">{1}</label>".format(inputs[0], inputs[1])
    textOutput += "\n\t\t<div class=\"col-sm-9\">"
    textOutput += "\n\t\t\t<input type=\"email\" class=\"form-control\" id=\"{1}\" name=\"{1}\" placeholder=\"{2}\" value=\"{3}\">".format(inputs[2],inputs[0], inputs[1], inputs[3] or "")
    textOutput += "<span class=\"label label-danger\">%s</span>" % (inputs[4] or "")
    textOutput += "\n\t\t</div>\n\t</div>"
    return textOutput

def secure_page():
    if FG_HTTP and not FG_HTTPS:
        if request.is_https:
            redirect("http://"+request.env.http_host.split(':')[0]+request.env.web2py_original_uri)
    if FG_HTTPS and not FG_HTTP:
        if not request.is_https:
            redirect("https://"+request.env.http_host.split(':')[0]+request.env.web2py_original_uri)
    return

#http://stackoverflow.com/questions/10819715/comparing-numpy-arrays-so-that-nans-compare-equal#10821267
def equals_nan(a,b):
    try:
        return np.bitwise_or(a==b, np.bitwise_and(np.isnan(a), np.isnan(b)))
    except:
        return a==b
    
def not_equals_nan(a,b):
    return np.bitwise_not(equals_nan(a,b))

def compute_filters(filters,a,trans):
    fdat = None
    for f in filters:
        if f.has_key('notnan'):
            f['vare'] = sanitize(f['var'],a,trans,keepstrings=True)
            if fdat is None:
                fdat = ~np.isnan(f['vare'])
            elif isinstance(f["vare"],str):
                return errormessage("Check this filter: "+f['var'],details=f["vare"])
            else:
                fdat = np.bitwise_and(fdat,~np.isnan(f['vare']))
        else:
            string_type = False
            for i in ['var','min','max']:
                if string_type:
                    f[i+'e'] = f[i]
                elif f[i]:
                    f[i+"e"] = sanitize(f[i],a,trans,keepstrings=True)
                    if isinstance(f[i+"e"],str):
                        return errormessage("Check this filter: "+f['var'],details=f[i+"e"])
                    if f[i+"e"] is None:
                        return errormessage("Check this filter: "+f['var'])
                    if not (isinstance(f['vare'],int) or isinstance(f['vare'],float)):
                        if f['vare'].dtype.kind == 'S':
                            string_type = True
                else:
                    f[i+"e"] = None
            if string_type and floatable(f['min']) is not None and floatable(f['max']) is not None:
                continue
            if f['opt'] == "isn't between":
                if f['mine'] is not None and f['maxe'] is not None:
                    if fdat is None:
                        fdat = np.bitwise_or(f['mine']>=f['vare'],f['vare']>=f['maxe'])
                    elif f['var'][:3].lower() == "or ":
                        fdat = np.bitwise_or(fdat,np.bitwise_or(f['mine']>=f['vare'],f['vare']>=f['maxe']))
                    else:
                        fdat = np.bitwise_and(fdat,np.bitwise_or(f['mine']>=f['vare'],f['vare']>=f['maxe']))
                elif f['mine'] is not None:
                    if fdat is None:
                        fdat = f['mine']>=f['vare']
                    elif f['var'][:3].lower() == "or ":
                        fdat = np.bitwise_or(fdat,f['mine']<=f['vare'])
                    else:
                        fdat = np.bitwise_and(fdat,f['mine']<=f['vare'])
                elif f['maxe'] is not None:
                    if fdat is None:
                        fdat = f['vare']>=f['maxe']
                    elif f['var'][:3].lower() == "or ":
                        fdat = np.bitwise_or(fdat,f['vare']>=f['maxe'])
                    else:
                        fdat = np.bitwise_and(fdat,f['vare']>=f['maxe'])
            elif f['opt'] == "is equal to":
                if f['mine'] is not None and f['maxe'] is not None:
                    if fdat is None:
                        fdat = np.bitwise_or(equals_nan(f['mine'],f['vare']),equals_nan(f['vare'],f['maxe']))
                    elif f['var'][:3].lower() == "or ":
                        fdat = np.bitwise_or(fdat,np.bitwise_or(equals_nan(f['mine'],f['vare']),equals_nan(f['vare'],f['maxe'])))
                    else:
                        fdat = np.bitwise_and(fdat,np.bitwise_or(equals_nan(f['mine'],f['vare']),equals_nan(f['vare'],f['maxe'])))
                elif f['mine'] is not None:
                    if fdat is None:
                        fdat = equals_nan(f['mine'],f['vare'])
                    elif f['var'][:3].lower() == "or ":
                        fdat = np.bitwise_or(fdat,equals_nan(f['mine'],f['vare']))
                    else:
                        fdat = np.bitwise_and(fdat,equals_nan(f['mine'],f['vare']))
                elif f['maxe'] is not None:
                    if fdat is None:
                        fdat = equals_nan(f['vare'],f['maxe'])
                    elif f['var'][:3].lower() == "or ":
                        fdat = np.bitwise_or(fdat,equals_nan(f['vare'],f['maxe']))
                    else:
                        fdat = np.bitwise_and(fdat,equals_nan(f['vare'],f['maxe']))
            elif f['opt'] == "is in list":
                list = f['list'].replace(",","\n").replace("[COMMA]",",").splitlines()
                list = [item.strip() for item in list]
                if string_type:
                    list = np.sort(list)
                else:
                    list = np.sort([floatable(item) for item in list])
                mask = np.searchsorted(list,f['vare'],side="left") != np.searchsorted(list,f['vare'],side="right")
                if fdat is None:
                    fdat = mask
                elif f['var'][:3].lower() == "or ":
                    fdat = np.bitwise_or(fdat,mask)
                else:
                    fdat = np.bitwise_and(fdat,mask)
            elif f['opt'] == "isn't in list":
                list = f['list'].replace(",","\n").replace("[COMMA]",",").splitlines()
                list = [item.strip() for item in list]
                if string_type:
                    list = np.sort(list)
                else:
                    list = np.sort([floatable(item) for item in list])
                mask = np.searchsorted(list,f['vare'],side="left") == np.searchsorted(list,f['vare'],side="right")
                if fdat is None:
                    fdat = mask
                elif f['var'][:3].lower() == "or ":
                    fdat = np.bitwise_or(fdat,mask)
                else:
                    fdat = np.bitwise_and(fdat,mask)
            elif f['opt'] == "is in lasso":
                if fdat is None:
                    fdat = f['vare']
                elif f['var'][:3].lower() == "or ":
                    fdat = np.bitwise_or(fdat,f['vare'])
                else:
                    fdat = np.bitwise_and(fdat,f['vare'])
            elif f['opt'] == "isn't in lasso":
                if fdat is None:
                    fdat = f['vare']==0
                elif f['var'][:3].lower() == "or ":
                    fdat = np.bitwise_or(fdat,f['vare']==0)
                else:
                    fdat = np.bitwise_and(fdat,f['vare']==0)
            elif f['opt'] == "isn't equal to":
                if f['mine'] is not None and f['maxe'] is not None:
                    if fdat is None:
                        fdat = np.bitwise_and(not_equals_nan(f['mine'],f['vare']),not_equals_nan(f['vare'],f['maxe']))
                    elif f['var'][:3].lower() == "or ":
                        fdat = np.bitwise_or(fdat,np.bitwise_and(not_equals_nan(f['mine'],f['vare']),not_equals_nan(f['vare'],f['maxe'])))
                    else:
                        fdat = np.bitwise_and(fdat,np.bitwise_and(not_equals_nan(f['mine'],f['vare']),not_equals_nan(f['vare'],f['maxe'])))
                elif f['mine'] is not None:
                    if fdat is None:
                        fdat = not_equals_nan(f['mine'],f['vare'])
                    elif f['var'][:3].lower() == "or ":
                        fdat = np.bitwise_or(fdat,not_equals_nan(f['mine'],f['vare']))
                    else:
                        fdat = np.bitwise_and(fdat,not_equals_nan(f['mine'],f['vare']))
                elif f['maxe'] is not None:
                    if fdat is None:
                        fdat = not_equals_nan(f['vare'],f['maxe'])
                    elif f['var'][:3].lower() == "or ":
                        fdat = np.bitwise_or(fdat,not_equals_nan(f['vare'],f['maxe']))
                    else:
                        fdat = np.bitwise_and(fdat,not_equals_nan(f['vare'],f['maxe']))
            elif f['opt'] == "starts with" or (f['opt'] == "is between" and request.vars.oldrules and string_type):
                if (f['min'] != "" or f['max'] != "") and not string_type:
                    return errormessage("Check this filter: "+f['var'],details="The 'starts with' option is for non-numeric variables only.")
                if f['min'] != "" and f['max'] != "":
                    if fdat is None:
                        fdat = np.bitwise_or(np.array([element.startswith(f['min']) for element in f['vare']]),
                                             np.array([element.startswith(f['max']) for element in f['vare']]))
                    elif f['var'][:3].lower() == "or ":
                        fdat = np.bitwise_or(fdat,np.bitwise_or(np.array([element.startswith(f['min']) for element in f['vare']]),
                                             np.array([element.startswith(f['max']) for element in f['vare']])))
                    else:
                        fdat = np.bitwise_and(fdat,np.bitwise_or(np.array([element.startswith(f['min']) for element in f['vare']]),
                                             np.array([element.startswith(f['max']) for element in f['vare']])))
                elif f['min'] != "":
                    if fdat is None:
                        fdat = np.array([element.startswith(f['min']) for element in f['vare']])
                    elif f['var'][:3].lower() == "or ":
                        fdat = np.bitwise_or(fdat,np.array([element.startswith(f['min']) for element in f['vare']]))
                    else:
                        fdat = np.bitwise_and(fdat,np.array([element.startswith(f['min']) for element in f['vare']]))
                elif f['max'] != "":
                    if fdat is None:
                        fdat = np.array([element.startswith(f['max']) for element in f['vare']])
                    elif f['var'][:3].lower() == "or ":
                        fdat = np.bitwise_or(fdat,np.array([element.startswith(f['max']) for element in f['vare']]))
                    else:
                        fdat = np.bitwise_and(fdat,np.array([element.startswith(f['max']) for element in f['vare']]))
            elif f['opt'] == "ends with":
                if (f['min'] != "" or f['max'] != "") and not string_type:
                    return errormessage("Check this filter: "+f['var'],details="The 'ends with' option is for non-numeric variables only.")
                if f['min'] != "" and f['max'] != "":
                    if fdat is None:
                        fdat = np.bitwise_or(np.array([element.endswith(f['min']) for element in f['vare']]),
                                             np.array([element.endswith(f['max']) for element in f['vare']]))
                    elif f['var'][:3].lower() == "or ":
                        fdat = np.bitwise_or(fdat,np.bitwise_or(np.array([element.endswith(f['min']) for element in f['vare']]),
                                             np.array([element.endswith(f['max']) for element in f['vare']])))
                    else:
                        fdat = np.bitwise_and(fdat,np.bitwise_or(np.array([element.endswith(f['min']) for element in f['vare']]),
                                             np.array([element.endswith(f['max']) for element in f['vare']])))
                elif f['min'] != "":
                    if fdat is None:
                        fdat = np.array([element.endswith(f['min']) for element in f['vare']])
                    elif f['var'][:3].lower() == "or ":
                        fdat = np.bitwise_or(fdat,np.array([element.endswith(f['min']) for element in f['vare']]))
                    else:
                        fdat = np.bitwise_and(fdat,np.array([element.endswith(f['min']) for element in f['vare']]))
                elif f['max'] != "":
                    if fdat is None:
                        fdat = np.array([element.endswith(f['max']) for element in f['vare']])
                    elif f['var'][:3].lower() == "or ":
                        fdat = np.bitwise_or(fdat,np.array([element.endswith(f['max']) for element in f['vare']]))
                    else:
                        fdat = np.bitwise_and(fdat,np.array([element.endswith(f['max']) for element in f['vare']]))
            elif f['opt'] == "contains":
                if (f['min'] != "" or f['max'] != "") and not string_type:
                    return errormessage("Check this filter: "+f['var'],details="The 'contains' option is for non-numeric variables only.")
                if f['min'] != "" and f['max'] != "":
                    if fdat is None:
                        fdat = np.bitwise_or(np.array([element.find(f['min']) >= 0 for element in f['vare']]),
                                             np.array([element.endswith(f['max']) for element in f['vare']]))
                    elif f['var'][:3].lower() == "or ":
                        fdat = np.bitwise_or(fdat,np.bitwise_or(np.array([element.find(f['min']) >= 0 for element in f['vare']]),
                                             np.array([element.endswith(f['max']) for element in f['vare']])))
                    else:
                        fdat = np.bitwise_and(fdat,np.bitwise_or(np.array([element.find(f['min']) >= 0 for element in f['vare']]),
                                             np.array([element.endswith(f['max']) for element in f['vare']])))
                elif f['min'] != "":
                    if fdat is None:
                        fdat = np.array([element.find(f['min']) >= 0 for element in f['vare']])
                    elif f['var'][:3].lower() == "or ":
                        fdat = np.bitwise_or(fdat,np.array([element.find(f['min']) >= 0 for element in f['vare']]))
                    else:
                        fdat = np.bitwise_and(fdat,np.array([element.find(f['min']) >= 0 for element in f['vare']]))
                elif f['max'] != "":
                    if fdat is None:
                        fdat = np.array([element.find(f['max']) >= 0 for element in f['vare']])
                    elif f['var'][:3].lower() == "or ":
                        fdat = np.bitwise_or(fdat,np.array([element.find(f['max']) >= 0 for element in f['vare']]))
                    else:
                        fdat = np.bitwise_and(fdat,np.array([element.find(f['max']) >= 0 for element in f['vare']]))
            elif f["opt"] == "in percentiles":
                if type(f['mine']) not in [int,float,np.int,np.float,type(None)]:
                    return errormessage("Check this filter: "+f['var'],details="Percentile bounds should be numbers.")
                if type(f['maxe']) not in [int,float,np.int,np.float,type(None)]:
                    return errormessage("Check this filter: "+f['var'],details="Percentile bounds should be numbers.")
                if f['mine'] is not None and f['maxe'] is not None:
                    if fdat is None:
                        fdat = np.bitwise_and(np.nanpercentile(f['vare'],f['mine'])<=f['vare'],f['vare']<=np.nanpercentile(f['vare'],f['maxe']))
                    elif f['var'][:3].lower() == "or ":
                        fdat = np.bitwise_or(fdat,np.bitwise_and(np.nanpercentile(f['vare'],f['mine'])<=f['vare'],f['vare']<=np.nanpercentile(f['vare'],f['maxe'])))
                    else:
                        fdat = np.bitwise_and(fdat,np.bitwise_and(np.nanpercentile(f['vare'],f['mine'])<=f['vare'],f['vare']<=np.nanpercentile(f['vare'],f['maxe'])))
                elif f['mine'] is not None:
                    if fdat is None:
                        fdat = np.nanpercentile(f['vare'],f['mine'])<=f['vare']
                    elif f['var'][:3].lower() == "or ":
                        fdat = np.bitwise_or(fdat,np.nanpercentile(f['vare'],f['mine'])<=f['vare'])
                    else:
                        fdat = np.bitwise_and(fdat,np.nanpercentile(f['vare'],f['mine'])<=f['vare'])
                elif f['maxe'] is not None:
                    if fdat is None:
                        fdat = f['vare']<=np.nanpercentile(f['vare'],f['maxe'])
                    elif f['var'][:3].lower() == "or ":
                        fdat = np.bitwise_or(fdat,f['vare']<=np.nanpercentile(f['vare'],f['maxe']))
                    else:
                        fdat = np.bitwise_and(fdat,f['vare']<=np.nanpercentile(f['vare'],f['maxe']))
            elif f["opt"] == "out percentiles":
                if type(f['mine']) not in [int,float,np.int,np.float,type(None)]:
                    return errormessage("Check this filter: "+f['var'],details="Percentile bounds should be numbers.")
                if type(f['maxe']) not in [int,float,np.int,np.float,type(None)]:
                    return errormessage("Check this filter: "+f['var'],details="Percentile bounds should be numbers.")
                if f['mine'] is not None and f['maxe'] is not None:
                    if fdat is None:
                        fdat = np.bitwise_or(np.nanpercentile(f['vare'],f['mine'])>=f['vare'],f['vare']>=np.nanpercentile(f['vare'],f['maxe']))
                    elif f['var'][:3].lower() == "or ":
                        fdat = np.bitwise_or(fdat,np.bitwise_or(np.nanpercentile(f['vare'],f['mine'])>=f['vare'],f['vare']>=np.nanpercentile(f['vare'],f['maxe'])))
                    else:
                        fdat = np.bitwise_and(fdat,np.bitwise_or(np.nanpercentile(f['vare'],f['mine'])>=f['vare'],f['vare']>=np.nanpercentile(f['vare'],f['maxe'])))
                elif f['mine'] is not None:
                    if fdat is None:
                        fdat = np.nanpercentile(f['vare'],f['mine'])>=f['vare']
                    elif f['var'][:3].lower() == "or ":
                        fdat = np.bitwise_or(fdat,np.nanpercentile(f['vare'],f['mine'])>=f['vare'])
                    else:
                        fdat = np.bitwise_and(fdat,np.nanpercentile(f['vare'],f['mine'])>=f['vare'])
                elif f['maxe'] is not None:
                    if fdat is None:
                        fdat = f['vare']>=np.nanpercentile(f['vare'],f['maxe'])
                    elif f['var'][:3].lower() == "or ":
                        fdat = np.bitwise_or(fdat,f['vare']>=np.nanpercentile(f['vare'],f['maxe']))
                    else:
                        fdat = np.bitwise_and(fdat,f['vare']>=np.nanpercentile(f['vare'],f['maxe']))
            else: # is between
                if f['mine'] is not None and f['maxe'] is not None:
                    if fdat is None:
                        fdat = np.bitwise_and(f['mine']<=f['vare'],f['vare']<=f['maxe'])
                    elif f['var'][:3].lower() == "or ":
                        fdat = np.bitwise_or(fdat,np.bitwise_and(f['mine']<=f['vare'],f['vare']<=f['maxe']))
                    else:
                        fdat = np.bitwise_and(fdat,np.bitwise_and(f['mine']<=f['vare'],f['vare']<=f['maxe']))
                elif f['mine'] is not None:
                    if fdat is None:
                        fdat = f['mine']<=f['vare']
                    elif f['var'][:3].lower() == "or ":
                        fdat = np.bitwise_or(fdat,f['mine']<=f['vare'])
                    else:
                        fdat = np.bitwise_and(fdat,f['mine']<=f['vare'])
                elif f['maxe'] is not None:
                    if fdat is None:
                        fdat = f['vare']<=f['maxe']
                    elif f['var'][:3].lower() == "or ":
                        fdat = np.bitwise_or(fdat,f['vare']<=f['maxe'])
                    else:
                        fdat = np.bitwise_and(fdat,f['vare']<=f['maxe'])
    return fdat
    
def format_poly(*p):
    result = []
    if p[0] != 0:
        result.append("%g<i>x</i><sup>3</sup>" % p[0])
    if p[1] != 0:
        result.append("%g<i>x</i><sup>2</sup>" % p[1])
    if p[2] != 0:
        result.append("%g<i>x</i>" % p[2])
    if p[3] != 0:
        result.append("%g" % p[3])
    return " + ".join(result).replace(" + -"," - ").replace("1<i>x</i>","<i>x</i>")

import pandas as pd    
import datashader as ds
try:
    import scipy.stats as st
except:pass
def binned_statistic(sample, values, bins, hrange, statistic="mean",diag=None):
    #bin_start_time = time.time()
    default_slow = (len(sample.shape) == 1) and (statistic in ['mean','sum','count']) or statistic == "cumulative"
    if diag == "slow" or (default_slow and diag != "fast"):
        try:
            result = st.binned_statistic_dd(sample, values=values, bins=bins, range=hrange, statistic=(statistic == "cumulative" and 'count' or statistic))
        except:
            result = None
    else:
        result = None
    if len(sample.shape) == 2 and result is None:
        df = pd.DataFrame(sample,columns=['__XAXIS__','__YAXIS__'])
        df['__COLOR__'] = values
        reductions = {'count':ds.count,'max':ds.max,'min':ds.min,'mean':ds.mean,'std':ds.std,'sum':ds.sum,'var':ds.var}
        reduction = reductions[statistic]
        cvs = ds.Canvas(plot_width=bins[0]+1, plot_height=bins[1]+1, x_range=hrange[0], y_range=hrange[1])
        agg = cvs.points(df, '__XAXIS__', '__YAXIS__', reduction('__COLOR__'))
        xbounds = np.linspace(hrange[0][0],hrange[0][1],bins[0]+1)
        ybounds = np.linspace(hrange[1][0],hrange[1][1],bins[1]+1)
        result_data = agg.data.T[:-1,:-1].astype(float)
        del(df)
        del(cvs)
        del(agg)
        result = (result_data,(xbounds,ybounds),None)
    elif result is None:
        df = pd.DataFrame(sample,columns=['__XAXIS__'])
        df['__YAXIS__'] = np.zeros(len(sample))
        df['__COLOR__'] = values
        reductions = {'count':ds.count,'max':ds.max,'min':ds.min,'mean':ds.mean,'std':ds.std,'sum':ds.sum,'var':ds.var}
        reduction = reductions[statistic]
        cvs = ds.Canvas(plot_width=bins[0]+1, plot_height=2, x_range=hrange[0], y_range=(-1,1))
        agg = cvs.points(df, '__XAXIS__', '__YAXIS__', reduction('__COLOR__'))
        xbounds = np.linspace(hrange[0][0],hrange[0][1],bins[0]+1)
        result_data = agg.data[0][:-1].astype(float)
        del(df)
        del(cvs)
        del(agg)
        result = (result_data,(xbounds,),None)
    if statistic == "cumulative":
        result_data = np.cumsum(result[0],axis=0)
        result = (result_data,result[1],result[2])
    #print "NEW METHOD: %s s" % (time.time() - bin_start_time)
    #bin_start_time = time.time()
    #old_method = st.binned_statistic_dd(sample, values=values, bins=bins, range=hrange, statistic=statistic)
    #print "OLD METHOD: %s s" % (time.time() - bin_start_time)
    return result
    
def cleanurl(s):
    comment_str = re.sub("[^A-Za-z0-9]","-",s.lower())
    comment_str = "-".join([c for c in comment_str.split("-") if c != ""])
    return comment_str
    
def get_protocol():
    if FG_HTTPS:
        return "https://"
    else:
        return "http://"

def is_dynamic_field(field):
    if field in ['xaxis','yaxis','color','size','zaxis','tsort']:
        return True
    elif "_bound" in field:
        return True
    elif "var_" in field:
        return True
    elif "min_" in field:
        return True
    elif "max_" in field:
        return True
    else:
        return False