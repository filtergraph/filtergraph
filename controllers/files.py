import os, subprocess, datetime, pickle
import numpy as np

@auth.requires_membership('qc')
def index():
    return dict()
    
@auth.requires_membership('qc')
def version():
    process = subprocess.Popen('git log --date=short --pretty=format:"%ad  %s"',shell=True,stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE,cwd=request.folder)
    mstats = process.communicate("")
    versionlog = mstats[0].split("\n")
    for i in range(len(versionlog)):
        versionlog[i] = versionlog[i].split(" ",1)
    return dict(versionlog=versionlog)
 
@auth.requires_membership('qc')
def events():
    if request.env.http_host != "127.0.0.1:8000":
        return "BLOCKED"
    events = db(db.auth_event.description.contains("Logged-in")).select()
    users = {}
    for e in events:
        if e.user_id != None and e.user_id in users:
            users[e.user_id] += 1
        else:
            users[e.user_id] = 1
    admins = db(db.admins.id>0).select()
    admin_array = [a.admin for a in admins]
    retval = []
    for i in users.items():
        if i[0] in admin_array:
            retval.append([i[0],i[1],db.auth_user[i[0]].first_name,db.auth_user[i[0]].email])
    return repr(sorted(retval, key=lambda k: k[1]))
    
@auth.requires_membership('qc')
def filecount():
    if request.env.http_host != "127.0.0.1:8000":
        return "BLOCKED"
    files = db(db.datasets.id>0).select(db.datasets.portal)
    portals = {}
    for f in files:
        if f.portal in portals:
            portals[f.portal] += 1
        else:
            portals[f.portal] = 1
    admins = db(db.admins.id>0).select()
    portal_count = {}
    for a in admins:
        if a.portal not in portals:
            pass
        elif a.admin.id in portal_count:
            portal_count[a.admin.id] += portals[a.portal]
        else:
            portal_count[a.admin.id] = portals[a.portal]
    retval = []
    for i in portal_count.items():
        retval.append([i[0],i[1],db.auth_user[i[0]].first_name,db.auth_user[i[0]].email])
    return repr(sorted(retval, key=lambda k: k[1])) 
      
@auth.requires_membership('qc')
def saveshare():
    if request.env.http_host != "127.0.0.1:8000":
        return "BLOCKED"
    #saveshares = db(db.savestate.id>0).select(db.savestate.time)
    #months = {}
    #for s in saveshares:
    #    mstr = "%s %s/%s" % (s.time.year*12+s.time.month,s.time.month,s.time.year)
    #    if mstr in months:
    #        months[mstr] += 1
    #    else:
    #        months[mstr] = 1
    admins = db(db.admins.id>0).select()
    portal_admins = {}
    for a in admins:
        if a.portal.name == "TESS":
            pass
        elif a.portal.id in portal_admins:
            portal_admins[a.portal.id].append(a.admin.id)
        else:
            portal_admins[a.portal.id] = [a.admin.id]
    saveshares = db(db.savestate.id>0).select(db.savestate.portal_id)
    saveshare_admins = {}
    for s in saveshares:
        if s.portal_id in portal_admins:
            for a in portal_admins[s.portal_id]:
                if a in saveshare_admins:
                    saveshare_admins[a] += 1
                else:
                    saveshare_admins[a] = 1
    retval = []
    for i in saveshare_admins.items():
        retval.append([i[0],i[1],db.auth_user[i[0]].first_name,db.auth_user[i[0]].email])
    return repr(sorted(retval, key=lambda k: k[1]))

@auth.requires_membership('qc')
def files():
    if request.vars.working:
        db(db.datasets.id == int(request.vars.working)).update(status="working")
    datasets = db(db.datasets.id>0).select()
    output = {}
    for d in datasets:
        f = {}
        f['name'] = d.name
        f['portal'] = d.portal.name
        f['portalid'] = d.portal.id
        f['portalurl'] = d.portal.url
        f['ftype'] = d.ftype
        f['status'] = d.status
        f['rows'] = None
        f['cols'] = None
        if os.path.exists(os.path.join("/hd1/filtergraph-uploads", d.file)):
            f['upload1'] = "Yes"
        else:
            f['upload1'] = ""
        if os.path.exists(os.path.join("/hd1/filtergraph-uploads", d.file)):
            f['upload2'] = "Yes"
        else:
            f['upload2'] = ""
        if os.path.exists(os.path.join("/hd1/trash-olddata", d.file)):
            f['upload3'] = "Yes"
        else:
            f['upload3'] = ""
        if os.path.exists(os.path.join(CACHE_FOLDER, d.file+".npy")):
            f['cache'] = "Yes"
            try:
                a = np.load(os.path.join(CACHE_FOLDER, d.file+".npy"))
                f['rows'] = len(a)
                f['cols'] = len(a.dtype)
            except:pass
        else:
            f['cache'] = ""
        if os.path.exists(os.path.join(DONE_FOLDER, d.file)):
            f['done'] = "Yes"
        else:
            f['done'] = ""
        if os.path.exists("/hd1/filtergraph-thumbs/d/%s.jpg" % d.id):
            f['thumb'] = "Yes"
        else:
            f['thumb'] = ""
        admins = []
        for a in db(db.admins.portal == d.portal).select():
            admins.append(a.admin.first_name + " " + a.admin.last_name)
        f['admin'] = ", ".join(admins)
        output[d.id] = f
    return dict(output=output)

@auth.requires_membership('qc')
def download():
    d = db.datasets[int(request.args[1])]
    if request.args[0] == 'upload1':
        fname = os.path.join("/hd1/filtergraph-uploads", d.file)
    elif request.args[0] == 'upload2':
        fname = os.path.join("/hd1/filtergraph-uploads", d.file)
    elif request.args[0] == 'upload3':
        fname = os.path.join("/hd1/trash-olddata", d.file)
    elif request.args[0] == 'cache':
        fname = os.path.join(CACHE_FOLDER, d.file+".npy")
    else:# request.args[0] == 'done':
        fname = os.path.join(DONE_FOLDER, d.file)
    response.headers['Content-Length']=os.path.getsize(fname)
    response.headers['Content-Disposition']='attachment; filename=scan-for-viruses-first-%s' % d.file
    return response.stream(open(fname,'rb'),chunk_size=4096)
    
@auth.requires_membership('qc')
def loaderr():
    err = pickle.load(open("/hd1/filtergraph-errors/10.1.140.96.2013-09-02.15-51-49.f07e9ab2-19c8-4027-a43d-0b05aec6e9f8"))
    return dict(err=err['traceback'])
