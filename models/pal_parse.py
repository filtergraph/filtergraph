#Code by Kenneth Li

import json

#Convert a RGB list (of the form [256, 256, 256]) to hex value
def rgbToHex(rgbList):    
    def leadZero(x):
        if(len(hex(x)[2:])==1):
            return '0'+hex(x)[2:]
        return hex(x)[2:]
    return "#"+"".join(leadZero(x) for x in rgbList)

#Return hex color value based on a point between two color steps
def gradientCalc(rgb1, rgb2, pct): 
    #print(rgb1, rgb2)
    #print("pct: ",pct)
    RGB = [0,0,0]
    RGB[0] = int(rgb1[0]+(rgb2[0]-rgb1[0])*pct) 
    RGB[1] = int(rgb1[1]+(rgb2[1]-rgb1[1])*pct)
    RGB[2] = int(rgb1[2]+(rgb2[2]-rgb1[2])*pct)
    return RGB

#Given a palette, return boolean value if there are repeating color steps
def uniqueSeq(palRange):
    if(len(palRange)==palRange[-1][0]):
        return True
    return False

#Given a palette (indices and color values) and target position, return single RGB color list
def stepNeighbors(palRange, target):
    index = [x[0] for x in palRange]
    nextStep = [i for i,v in enumerate(index) if v>=target][0] #get next step index in palRange after target
    #decision tree to check for gradient, coinciding point, and two coinciding points
    if(index[nextStep]==target):
        if index[nextStep] != index[-1] and index[nextStep+1] ==target:
            return palRange[nextStep+1][1:]
        return palRange[nextStep][1:]
    else:
        priorStep = [i for i,v in enumerate(index) if v<target][-1]
        pct = round((target-index[priorStep])/(index[nextStep]-index[priorStep]),3)
        return gradientCalc(palRange[priorStep][1:],palRange[nextStep][1:],pct)


#Returns an array of hex colors values given a palette and number of selections from the palette
def colorFetch(palette,n): 
    step = (palette[1][-1][0]-1)/(n-1.0)
    result = []
    uniqueTest = uniqueSeq(palette[1])
    for x in range(0,n):
        result.append(rgbToHex(stepNeighbors(palette[1], step*x+1)))   
    return result

