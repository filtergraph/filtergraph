import re, urllib2
from xml.dom import minidom

# coding: utf8
# try something like
def get_images():
    try:
        if request.vars.name:
            locstr = request.vars.name
        else:
            locstr = request.vars.ra + "," + request.vars.dec
        a  = urllib2.urlopen("http://irsa.ipac.caltech.edu/cgi-bin/FinderChart/nph-finder?locstr=%s&survey=SDSS&survey=2MASS&mode=prog" % locstr)
        b = a.read()
        a.close()
        c = minidom.parseString(b)
        d = c.getElementsByTagName("image")
        result = []
        for dd in d:
            image = {}
            for i in ['surveyname','band','obsdate','jpgurl','shrunkjpgurl']:
                image[i] = dd.getElementsByTagName(i)[0].firstChild.data.strip()
            result.append(image)
        return dict(result=result)
    except:
        return "No image found."
    
def get_spectrum():
    if request.vars.plate:
        plate = request.vars.plate
        mjd = request.vars.mjd
        fiber = request.vars.fiber
        query = "plate=%s&mjd=%s&fiber=%s" % (plate,mjd,fiber)
    else:
        ra = request.vars.ra
        dec = request.vars.dec
        query = "ra=%s&dec=%s" % (ra,dec)
    a = urllib2.urlopen("http://skyserver.sdss3.org/dr8/en/tools/explore/obj.asp?"+query)
    a.close()
    a = urllib2.urlopen("http://skyserver.sdss3.org/dr8/en/tools/explore/OETOC.asp?"+query)
    b = a.read()
    a.close()
    c = re.search("loadSummary\('(.*)'\)",b)
    d = c.groups()[0].replace('&amp;','&')
    a = urllib2.urlopen("http://skyserver.sdss3.org/dr8/en/tools/explore/summary.asp?id="+d)
    b = a.read()
    a.close()
    c = re.search("SpecObjID = (\d*)",b)
    if c:
        d = c.groups()[0]
        return dict(spectrum_id=d)
    else:
        return "No spectra could be found for this object."

def galacticmodel():
    from gluon.tools import Recaptcha
    form = FORM(Recaptcha(request,
            '6Lcs-dUSAAAAADU6uZt2XhVO9xi22TYe5qe4lpuT', '6Lcs-dUSAAAAAM5aJIspuTnOykEEBXcBrZHztMYO'),INPUT(_type='submit'))
    if form.accepts(request,session):
        return dict(form=None)
    elif form.errors:
        response.flash = 'Form has errors. Please try again.'
        return dict(form=form)
    else:
        return dict(form=form)
