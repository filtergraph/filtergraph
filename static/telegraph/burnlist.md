# To Do


# Changes



// - - - - - - - - -

# Good to Go
[x] Home
[x] UI Style Guide
[x] Dashboard
[x] Portal - Edit/Create
[x] Edit Portal - General Settings
[x] Edit Portal - Data Files


# Archived Changes
[x] Check that Control Panel Consistent with Filtergraph controls
[x] Home - gap on right (phone)
[x] Dashboard - filter labels need margin-bottom, delete buttons need margin-top
[x] Go ahead and make portal edit mobile?
[x] Compress, Minify, etc all deliverables (css_X js_ icons_)